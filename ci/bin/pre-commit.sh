#!/usr/bin/env bash
set -xe

echo "current: $PWD"
rootDir="$(cd $PWD/../ && PWD)"
serviceDir=$rootDir
clientDir=$rootDir/client-apps

pushd $clientDir
    npm run precommit
    npm run test
popd

pushd $serviceDir
    dotnet test
popd
