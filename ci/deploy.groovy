pipeline {
	agent { label 'slc-qasappjla01-Linux' }

	options {
		//Only one concurrent build at a time on this project
        disableConcurrentBuilds()
        //Build discard policy
        buildDiscarder(logRotator(numToKeepStr: '5'))
	}

	parameters {
        choice(name: 'DEPLOY_ENV', choices: ['QA', 'DEMO', 'RC', 'PROD'], description: 'environments to deploy to')
        string(name: 'SERVER', defaultValue: '', description: 'Server deploy to.')
        string(name: 'ARTIFACT_NAME', defaultValue: '', description: 'Artifact name.')
    }

    environment {
        REPO_NAME = "Origination.Ui"
		PROJECT_NAME = "origination.ui"
        BRANCH_NAME = "${env.GIT_BRANCH.substring(env.GIT_BRANCH.lastIndexOf('/') + 1, env.GIT_BRANCH.length())}"
		ARTIFACT_VERSION = "${sh(returnStdout: true, script: 'git log -1 --pretty=%h').trim()}"
		ARTIFACT_NAME = "${env.PROJECT_NAME}_${env.ARTIFACT_VERSION}"
		ARTIFACTORY_URL = "https://art.proginternal.net"
        IMAGE_BUILDER = "docker-base.art.proginternal.net/dotnet3-node12-builder:1.0"
	}

	stages {
        stage('environments') {
			steps {
                sh 'printenv'
			}
		}

		stage('Check Artifact Name') {
			steps {
				script {
                    if (params.DEPLOY_ENV == '') {
                        error('DEPLOY_ENV is required.')
                    }

					if (params.ARTIFACT_NAME == '') {
						error("ARTIFACT_NAME is required.")
					}

                    sh "echo 'deploying to $params.DEPLOY_ENV $params.SERVER with artifact: $params.ARTIFACT_NAME'"
				}
			}
		}

		stage('deploy') {
			steps {
				script {
                    sh "echo '$params.ARTIFACT_NAME deployed successfully.'"
				}
			}
		}
	}
}
