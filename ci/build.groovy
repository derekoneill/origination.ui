#!groovy

pipeline {
	agent { label 'slc-qasappjla01-Linux' }

	options {
		//Only one concurrent build at a time on this project
        disableConcurrentBuilds()
        //Build discard policy
        buildDiscarder(logRotator(numToKeepStr: '5', daysToKeepStr: '5'))
	}

    parameters {
        // temporarily use forge channel and decide if we want to create a new channel and replace it here.
        string(name: 'SlackChannel', defaultValue: '#forge-jira-jenkins', description: 'The name of the slack channel that you want build notifications published to.')
        string(name: 'DEPLOY_BRANCH', defaultValue: 'master', description: 'Enter the branch you want to deploy.')
    }

	environment {
        // These environments are required by this ci template and this template will be reused by other's projects and we can parametize these
        // to support other project in the future.
        REPO_NAME = "origination.ui"
		PROJECT_NAME = "Origination.Ui"
        CLIENT_APP_ROOT = "client-apps"
        BRANCH_NAME = "${env.GIT_BRANCH.substring(env.GIT_BRANCH.lastIndexOf('/') + 1, env.GIT_BRANCH.length())}"
		ARTIFACT_VERSION = "${sh(returnStdout: true, script: 'git log -1 --pretty=%h').trim()}"
		ARTIFACT_NAME = "${env.PROJECT_NAME}_${env.ARTIFACT_VERSION}"
		ARTIFACTORY_URL = "https://art.proginternal.net"
        SONARQUBE_URL = "http://10.100.63.34:9000"
        IMAGE_BUILDER = "docker-base.art.proginternal.net/dotnet3-node12-builder:1.2"
	}

	stages {

        stage('environments') {
			steps {
                // print out the environments information.
                sh 'printenv'
			}
		}

        stage('Verify Service') {
            steps {
                script {
                    withCredentials([string(credentialsId: 'Sonarqube', variable: 'token')]) {
                        // verify service:  run unit tests and start code analysis with sonarqube as well if we failed any of them, we will fail the build.
                        sh "docker run -t --rm -v $WORKSPACE:/workspace $IMAGE_BUILDER bash -c \
                            'dotnet sonarscanner begin -k:${PROJECT_NAME}_service -d:sonar.login=$token -d:sonar.host.url=$SONARQUBE_URL -n:$REPO_NAME -d:sonar.verbose=true -v:$BUILD_NUMBER -d:sonar.exclusions=**/$CLIENT_APP_ROOT/**,**/bin/**,**/dist/**,**/obj/** && \
                            dotnet restore && dotnet build -c Release && dotnet test && \
                            dotnet sonarscanner end -d:sonar.login=$token'"
                    }
                }
            }
        }

        stage('Verify Presentation') {
            steps {
                script {
                    withCredentials([string(credentialsId: 'Sonarqube', variable: 'token')]) {
                        // Verify the presentation: by running unit tests and potentially we would add e2e test here as well and start code quality analysis with sonar qube.
                        sh "docker run -t --rm -v $WORKSPACE/$PROJECT_NAME/$CLIENT_APP_ROOT:/workspace $IMAGE_BUILDER npm install --silent"
                        sh "docker run -t --rm -v $WORKSPACE/$PROJECT_NAME/$CLIENT_APP_ROOT:/workspace $IMAGE_BUILDER npm run test"
                        def sonarScannerCliImg = "sonarsource/sonar-scanner-cli"
                        sh "docker run -t --rm -e SONAR_LOGIN=$token -v $WORKSPACE/$PROJECT_NAME/$CLIENT_APP_ROOT:/usr/src $sonarScannerCliImg -X -Dsonar.verbose=true -Dproject.settings=./sonar-project.properties"
                    }
                }
            }
        }

        stage('Build and bunble assets') {
            // this is the step after we verify all our tests and static code analysis to build and bundle all the assets together here.
            steps {
                script {
                    sh "docker run -t --rm -v $WORKSPACE/$PROJECT_NAME/:/workspace $IMAGE_BUILDER dotnet restore"
                    sh "sudo rm -rf $WORKSPACE/$PROJECT_NAME/publish"
                    sh "docker run -t --rm -v $WORKSPACE/$PROJECT_NAME/:/workspace $IMAGE_BUILDER dotnet publish -c Release -r linux-x64 --self-contained --no-dependencies -v:q -o /workspace/publish"
                }
            }
        }


        stage('Push to Artifactory') {
            steps {
                // this is the step when everything is ready to push up to the artifactory and kick off all the default deployment environments.
                // for now we will try to continue delivering our code to kubernetes and lagacy system, such as IIS farms(need to go through CM team's CD pipelines.)
                script {
                    sh "rm -rf *.zip *.tar.gz"
                    def ARTIFACT_DATE = now.format("yyyyMMdd", TimeZone.getTimeZone('UTC'))
                    def ARTIFACT_FILE_NAME = "${PROJECT_NAME}-${ARTIFACT_DATE}-${GIT_TAG}-${env.BUILD_NUMBER}.zip"
                    pwsh(returnStdout: true, label: 'Zip Artifacts', script: "Compress-Archive $WORKSPACE/$PROJECT_NAME/publish $ARTIFACT_FILE_NAME")
                    sh "ls -la $WORKSPACE"

                    withCredentials([string(credentialsId: 'ArtifactoryAPIKey', variable: 'API_KEY')]) {
                        //Push artifact to artifactory
                        sh "jfrog rt u '*.zip' 'virtual-progleasing/$PROJECT_NAME/$ARTIFACT_FILE_NAME' --build-name=$PROJECT_NAME --build-number=$ARTIFACT_VERSION --url='$ARTIFACTORY_URL/artifactory' --apikey=$API_KEY"
                        //Publish Build
                        sh "jfrog rt bp $PROJECT_NAME $ARTIFACT_VERSION --url='$ARTIFACTORY_URL/artifactory' --apikey=$API_KEY"
                    }
                }
            }
        }

        stage('Deploy to QA') {
            steps {
                script {
                    // create the docker image and continue to deploy to the kubernete QA cluster.
                    build job: 'build-docker-image', propagate: false, wait: false, parameters: [
                        [$class: 'StringParameterValue', name: 'SlackChannel', value: "${params.SlackChannel}"],
                        [$class: 'StringParameterValue', name: 'GitRepository', value: "${REPO_NAME}"],
                        [$class: 'StringParameterValue', name: 'BranchName', value: "${params.DEPLOY_BRANCH}"],
                        [$class: 'BooleanParameterValue', name: 'SendSlackNotifications', value: "${params.SlackChannel}"],
                        [$class: 'StringParameterValue', name: 'MasterBranchName', value: "$BRANCH_NAME"]
                    ]

                    // kick of the legacy deploy job that will roll our artifacts to iis farm for now.
                    build job: "$PROJECT_NAME-deploy", wait: false, parameters: [
                        [$class: 'StringParameterValue', name: 'DEPLOY_ENV', value: "QA"],
                        [$class: 'StringParameterValue', name: 'SERVER', value: "13"],
                        [$class: 'StringParameterValue', name: 'ARTIFACT_NAME', value: "$ARTIFACT_NAME"]
                    ]
                }
            }
        }
    }
}
