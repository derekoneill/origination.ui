FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build

LABEL mainter="eric.nguyen@progleasing.com; Eric Nguyen"

WORKDIR /workspace

# Install node js.
RUN apt-get update && \
    apt-get install -y wget && \
    apt-get install -y gnupg2 && \
    wget -qO- https://deb.nodesource.com/setup_12.x | bash - && \
    apt-get install -y build-essential nodejs

# Install all necessary additional software (utils, jre)
RUN apt-get install -y \
    openjdk-11-jre \
    apt-transport-https \
    ca-certificates \
    gnupg-agent \
    software-properties-common

# Install dotnet-sonarscanner.
RUN dotnet tool install --global dotnet-sonarscanner
RUN dotnet tool install --global coverlet.console

ENV PATH="/root/.dotnet/tools:${PATH}"

CMD [ "dotnet", "--version" ]
