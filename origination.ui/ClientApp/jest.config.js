module.exports = {
  preset: 'jest-preset-angular',
  setupFilesAfterEnv: ['<rootDir>/projects/setup-jest.ts'],
  testPathIgnorePatterns: ['<rootDir>/mode_modules', '<rootDir>/dist/'],
  moduleDirectories: ['node_modules'],
  roots: ['<rootDir>/projects'],
  testMatch: ['**/__tests__/**/*.+(ts|tsx)', '**/?(*.)+(spec|test).+(ts|tsx)'],
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
  },
  collectCoverage: true,
  coverageThreshold: {
    global: {
      lines: 100,
      branches: 100,
      statements: 100,
      functions: 100,
    },
  },
};
