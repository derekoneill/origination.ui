import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComprehensionSlideshowComponent } from './comprehension-slideshow.component';

describe('ComprehensionSlideshowComponent', () => {
  let component: ComprehensionSlideshowComponent;
  let fixture: ComponentFixture<ComprehensionSlideshowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComprehensionSlideshowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComprehensionSlideshowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
