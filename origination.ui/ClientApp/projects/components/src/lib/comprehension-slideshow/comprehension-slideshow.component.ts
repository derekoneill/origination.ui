import { Component, Input, OnInit } from '@angular/core';
import { ComprehensionSlideshow } from '../models/comprehension-slideshow.model';
import { ComprehensionSlideshowEvents, ComprehensionSlideshowService } from '../services/comprehension-slideshow.service';

@Component({
    selector: 'uae-comprehension-slideshow',
    templateUrl: './comprehension-slideshow.component.html',
    styleUrls: ['./comprehension-slideshow.component.scss']
})
export class ComprehensionSlideshowComponent implements OnInit {

    @Input()
    public model: ComprehensionSlideshow;

    @Input()
    public slideshowId: string;

    public currentSlide: number;

    constructor(
        private readonly comprehensionSlideshowService: ComprehensionSlideshowService,
    ) { }

    public ngOnInit(): void {
        this.currentSlide = this.comprehensionSlideshowService.getCurrentSlide(this.slideshowId);
    }

    public next(): void {
        if (this.currentSlide < (this.model.slides.length - 1)) {
            this.comprehensionSlideshowService.pushEvent(ComprehensionSlideshowEvents.Next);
        } else {
            this.comprehensionSlideshowService.pushEvent(ComprehensionSlideshowEvents.Completed);
        }
        this.comprehensionSlideshowService.setCurrentSlide(this.slideshowId, this.currentSlide++);
    }

    public previous(): void {
        if (this.currentSlide > 0) {
            this.comprehensionSlideshowService.pushEvent(ComprehensionSlideshowEvents.Previous);
        } else {
            this.comprehensionSlideshowService.pushEvent(ComprehensionSlideshowEvents.Referrer);
        }
        this.comprehensionSlideshowService.setCurrentSlide(this.slideshowId, this.currentSlide--);
    }
}
