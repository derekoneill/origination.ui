import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderEvents, HeaderService } from '../services/header.service';
import { ApplicationPath } from 'projects/originationUi/src/application-path.enum';
import { ComprehensionRoute } from 'projects/comprehension/src';

@Component({
    selector: 'uae-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    constructor(
        private readonly headerService: HeaderService,
        private readonly router: Router,
    ) { }

    public ngOnInit(): void { }

    public previous(): void {
        this.headerService.pushEvent(HeaderEvents.Previous);
        history.back();
    }

    public exit(): void {
        this.headerService.pushEvent(HeaderEvents.Exit);
        this.router.navigate(['comprehension', 'slideshow']);
    }
}
