import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermsSheetComponent } from './terms-sheet.component';

describe('TermsSheetComponent', () => {
  let component: TermsSheetComponent;
  let fixture: ComponentFixture<TermsSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
