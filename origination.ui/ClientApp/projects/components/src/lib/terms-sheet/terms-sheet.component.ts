import { Component, Input, OnInit } from '@angular/core';
import { TermsSheet } from '../models/terms-sheet.model';
import { TermsSheetEvents, TermsSheetService } from '../services/terms-sheet.service';

@Component({
    selector: 'uae-terms-sheet',
    templateUrl: './terms-sheet.component.html',
    styleUrls: ['./terms-sheet.component.scss']
})
export class TermsSheetComponent implements OnInit {

    @Input()
    model: TermsSheet;

    constructor(
        private readonly termsSheetService: TermsSheetService,
    ) { }

    public ngOnInit(): void {
    }

    public agreeToTerms(): void {
        this.termsSheetService.pushEvent(TermsSheetEvents.Agree);
    }
}
