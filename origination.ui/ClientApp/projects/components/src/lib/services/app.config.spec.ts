import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppConfig } from './app.config';

describe('ContentService', () => {
  let httpTestingController: HttpTestingController;
  let testedService: AppConfig;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AppConfig],
    });
    this.httpTestingController = TestBed.inject(HttpTestingController);
    this.testedService = TestBed.inject(AppConfig);
  });

  afterEach(() => {
    this.httpTestingController.verify();
  });

  it('should call appropriate content endpoint', () => {
    let mockResult = 'MockResult';
    this.testedService.load().then(() => expect(this.testedService.settings).not.toBeNull());
    const req = this.httpTestingController.expectOne(`assets/config.json`);
    req.flush(mockResult);
  });

  it('should catch error of no settings', () => {
    let mockResult = 'MockResult';
    const mockErrorResponse = { status: 400, statusText: 'Bad Request' };

    this.testedService.load().then(() => expect(this.testedService.settings).toBeNull());
    const req = this.httpTestingController.expectOne(`assets/config.json`);
    req.flush(mockResult, mockErrorResponse);
  });
});
