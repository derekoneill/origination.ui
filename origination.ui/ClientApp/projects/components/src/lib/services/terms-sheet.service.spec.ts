import { TestBed } from '@angular/core/testing';

import { TermsSheetService } from './terms-sheet.service';

describe('TermsSheetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TermsSheetService = TestBed.inject(TermsSheetService);
    expect(service).toBeTruthy();
  });
});
