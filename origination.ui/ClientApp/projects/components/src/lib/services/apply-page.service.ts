import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export enum ApplyPageEvents {
    Continue,
}


@Injectable({
    providedIn: 'root'
})
export class ApplyPageService {
    private events = new Subject<ApplyPageEvents>();
    public eventStream$ = this.events.asObservable();

    constructor() { }

    public pushEvent(event: ApplyPageEvents): void {
        this.events.next(event);
    }
}
