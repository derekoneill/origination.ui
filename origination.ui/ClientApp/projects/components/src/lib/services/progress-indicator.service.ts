import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export enum ProgressIndicatorEvents {
  Continue,
}

@Injectable({
  providedIn: 'root',
})
export class ProgressIndicatorService {
  private events = new Subject<ProgressIndicatorEvents>();
  public eventStream$ = this.events.asObservable();

  constructor() {}

  public pushEvent(event: ProgressIndicatorEvents): void {
    this.events.next(event);
  }
}
