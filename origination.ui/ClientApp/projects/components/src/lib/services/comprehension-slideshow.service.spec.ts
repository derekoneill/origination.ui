import { TestBed } from '@angular/core/testing';

import { ComprehensionSlideshowService } from './comprehension-slideshow.service';

describe('ComprehensionSlideshowService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ComprehensionSlideshowService = TestBed.inject(ComprehensionSlideshowService);
    expect(service).toBeTruthy();
  });
});
