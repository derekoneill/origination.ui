import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export enum ComprehensionSlideshowEvents {
    Completed,
    Next,
    Previous,
    Referrer,
}

export interface ComprehensionSlideState {
    slideshowId: string;
    currentSlide: number;
}

@Injectable({
    providedIn: 'root'
})
export class ComprehensionSlideshowService {
    private events = new Subject<ComprehensionSlideshowEvents>();
    private slideshowStates: ComprehensionSlideState[] = [];

    public eventStream$ = this.events.asObservable();

    constructor() { }

    public pushEvent(event: ComprehensionSlideshowEvents): void {
        this.events.next(event);
    }

    public getCurrentSlide(slideId: string): number {
        const slide = this.slideshowStates.find(slide => slide.slideshowId === slideId);
        if (!slide) {
            this.createSlideState(slideId);
            return this.getCurrentSlide(slideId);
        }
        return slide.currentSlide;
    }

    public setCurrentSlide(slideId: string, currentSlide: number): void {
        const slide = this.slideshowStates.find(slide => slide.slideshowId === slideId);
        slide.currentSlide = currentSlide;
    }

    private createSlideState(slideId: string): void {
        this.slideshowStates.push({
            slideshowId: slideId,
            currentSlide: 0
        });
    }
}
