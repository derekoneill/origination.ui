import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { IConfig } from '../models/config.model';
import { map } from 'rxjs/operators';
import { AppConfig } from './app.config';

@Injectable({
  providedIn: 'root',
})
export class ContentService {
  private baseUrl: string;
  constructor(private http: HttpClient) {
    this.baseUrl = AppConfig.settings.contentApiUrl;
  }

  getContent(pageKey: string, instanceType?: string, language?: string): Observable<string> {
    let uri = `${this.baseUrl}/api/content?pageKey=${pageKey}`;
    if (instanceType != undefined)
      uri = `${uri}&instanceType=${instanceType}`;
    if (language != undefined)
      uri = `${uri}&language=${language}`;
    return this.http.get<string>(uri);
  }
}
