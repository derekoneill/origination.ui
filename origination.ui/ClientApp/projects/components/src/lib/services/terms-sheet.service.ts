import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export enum TermsSheetEvents {
    Agree,
}


@Injectable({
    providedIn: 'root'
})
export class TermsSheetService {
    private events = new Subject<TermsSheetEvents>();
    public eventStream$ = this.events.asObservable();

    constructor() { }

    public pushEvent(event: TermsSheetEvents): void {
        this.events.next(event);
    }
}
