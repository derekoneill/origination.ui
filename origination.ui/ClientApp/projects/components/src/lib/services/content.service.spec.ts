import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { IConfig } from '../models/config.model';
import { ContentService } from './content.service';
import { AppConfig } from './app.config';

describe('ContentService', () => {
  let httpTestingController: HttpTestingController;
  let config: IConfig;
  beforeEach(() => {
    this.config = <IConfig>{ contentApiUrl: 'some-content-api-url' };
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    AppConfig.settings = this.config;
    this.httpTestingController = TestBed.inject(HttpTestingController);
    this.testedService = TestBed.inject(ContentService);
  });

  afterEach(() => {
    this.httpTestingController.verify();
  });

  it('should call appropriate content endpoint', () => {
    let contentKey = 'testkey';
    let mockResult = 'MockResult';
    this.testedService.getContent(contentKey).subscribe((res) => expect(res).toEqual(mockResult));
    const req = this.httpTestingController.expectOne(
      `${this.config.contentApiUrl}/api/content?pageKey=${contentKey}`
    );
    req.flush(mockResult);
  });
});
