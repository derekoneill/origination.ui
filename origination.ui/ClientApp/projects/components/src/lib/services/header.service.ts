import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

export enum HeaderEvents {
    Previous,
    Exit,
}

@Injectable({
    providedIn: 'root'
})
export class HeaderService {
    private events = new Subject<HeaderEvents>();
    public eventStream$ = this.events.asObservable();

    constructor() { }

    public pushEvent(event: HeaderEvents): void {
        this.events.next(event);
    }
}
