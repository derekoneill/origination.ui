export class GlobalContent {
    constructor(
        private _resultsLoaderContent: string[],
        private _securityAffirmation: string,
    ) { }

    public get securityAffirmation(): string {
        return this._securityAffirmation;
    }

    public get resultsLoaderContent(): string[] {
        return this._resultsLoaderContent;
    }
}

export class GlobalContentFactory {
    public getGlobalContent(dto: GlobalContentDto): GlobalContent {
        return new GlobalContent(
            dto.resultsLoaderContent,
            dto.securityAffirmation,
        );
    }
}

export interface GlobalContentDto {
    resultsLoaderContent: string[];
    securityAffirmation: string;
}