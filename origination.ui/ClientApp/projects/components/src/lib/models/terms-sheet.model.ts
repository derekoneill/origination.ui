import { Injectable } from '@angular/core';

export class TermsSheet {
    public constructor(
        private _agreementLabel: string,
        private _bodyCopy: string,
        private _heading: string,
        private _terms: string[],
    ) { }

    public get agreementLabel(): string {
        return this._agreementLabel;
    }

    public get bodyCopy(): string {
        return this._bodyCopy;
    }

    public get heading(): string {
        return this._heading;
    }

    public get terms(): string[] {
        return this._terms;
    }
}

@Injectable()
export class TermsSheetFactory {
    public constructor() { }

    public getTermsSheet(dto: TermsSheetDto): TermsSheet {
        return new TermsSheet(
            dto.agreementLabel,
            dto.bodyCopy,
            dto.heading,
            dto.terms,
        );
    }
}

export interface TermsSheetDto {
    agreementLabel: string;
    bodyCopy: string;
    heading: string;
    terms: string[];
}
