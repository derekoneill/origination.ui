import { Injectable } from '@angular/core';
import {
  ProgressIndicatorStep,
  ProgressIndicatorStepDto,
  ProgressIndicatorStepFactory,
} from './progress-indicator-step.model';

export class ProgressIndicator {
  public constructor(
    private _bodyCopy: string,
    private _heading: string,
    private _nextLabel: string,
    private _steps: ProgressIndicatorStep[]
  ) {}

  public get bodyCopy(): string {
    return this._bodyCopy;
  }

  public get heading(): string {
    return this._heading;
  }

  public get nextLabel(): string {
    return this._nextLabel;
  }

  public get steps(): ProgressIndicatorStep[] {
    return this._steps;
  }
}

@Injectable()
export class ProgressIndicatorFactory {
  public constructor(private readonly progressIndicatorStepFactory: ProgressIndicatorStepFactory) {}

  public getProgressIndicator(dto: ProgressIndicatorDto): ProgressIndicator {
    return new ProgressIndicator(
      dto.bodyCopy,
      dto.heading,
      dto.nextLabel,
      this.progressIndicatorStepFactory.getProgressIndicatorSteps(dto.steps)
    );
  }
}

export interface ProgressIndicatorDto {
  bodyCopy: string;
  heading: string;
  nextLabel: string;
  steps: ProgressIndicatorStepDto[];
}
