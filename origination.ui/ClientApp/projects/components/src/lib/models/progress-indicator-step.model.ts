import { Injectable } from '@angular/core';

export class ProgressIndicatorStep {
  public constructor(private _bodyCopy: string, private _heading: string) {}

  public get bodyCopy(): string {
    return this._bodyCopy;
  }

  public get heading(): string {
    return this._heading;
  }
}

@Injectable()
export class ProgressIndicatorStepFactory {
  public constructor() {}

  public getProgressIndicatorStep(dto: ProgressIndicatorStepDto): ProgressIndicatorStep {
    return new ProgressIndicatorStep(dto.bodyCopy, dto.heading);
  }

  public getProgressIndicatorSteps(dtos: ProgressIndicatorStepDto[]): ProgressIndicatorStep[] {
    if (!dtos) {
      return;
    }
    return dtos.map((x) => this.getProgressIndicatorStep(x));
  }
}

export interface ProgressIndicatorStepDto {
  bodyCopy: string;
  heading: string;
}
