import { Injectable } from '@angular/core';

export class ApplyPage {
    public constructor(
        private _heading: string,
        private _bodyCopy: string,
        private _nextLabel: string,
        private _affirmSecurity: boolean,
    ) { }

    public get heading(): string {
        return this._heading;
    }

    public get bodyCopy(): string {
        return this._bodyCopy;
    }

    public get nextLabel(): string {
        return this._nextLabel;
    }

    public get affirmSecurity(): boolean {
        return this._affirmSecurity;
    }
}

@Injectable()
export class ApplyPageFactory {
    public constructor() { }

    public getApplyPage(dto: ApplyPageDto): ApplyPage {
        return new ApplyPage(
            dto.heading,
            dto.bodyCopy,
            dto.nextLabel,
            dto.affirmSecurity,
        );
    }
}

export interface ApplyPageDto {
    affirmSecurity?: boolean;
    bodyCopy: string;
    heading: string;
    nextLabel: string;
}
