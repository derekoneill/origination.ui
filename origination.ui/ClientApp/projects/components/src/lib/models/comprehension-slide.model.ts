import { Injectable } from '@angular/core';

export class ComprehensionSlide {
    public constructor(
        private _bodyCopy: string,
        private _heading: string,
        private _icon: string,
    ) { }

    public get bodyCopy(): string {
        return this._bodyCopy;
    }

    public get heading(): string {
        return this._heading;
    }

    public get icon(): string {
        return this._icon;
    }
}

@Injectable()
export class ComprehensionSlideFactory {
    public getComprehensionSlide(dto: ComprehensionSlideDto): ComprehensionSlide {
        return new ComprehensionSlide(
            dto.bodyCopy,
            dto.heading,
            dto.icon,
        );
    }

    public getComprehensionSlides(dtos: ComprehensionSlideDto[]): ComprehensionSlide[] {
        if (!dtos) {
            return;
        }
        return dtos.map(x => this.getComprehensionSlide(x));
    }
}

export interface ComprehensionSlideDto {
    bodyCopy: string;
    heading: string;
    icon: string;
}
