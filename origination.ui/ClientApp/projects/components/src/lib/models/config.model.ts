export interface IConfig {
  originApiUrl: string;
  contentApiUrl: string;
}
