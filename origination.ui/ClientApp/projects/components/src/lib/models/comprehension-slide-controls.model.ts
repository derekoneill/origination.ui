import { Injectable } from '@angular/core';

export class ComprehensionSlideControls {
    public constructor(
        private _nextLabel: string,
        private _previousLabel: string,
    ) { }

    public get nextLabel(): string {
        return this._nextLabel;
    }

    public get previousLabel(): string {
        return this._previousLabel;
    }
}

@Injectable()
export class ComprehensionSlideControlsFactory {
    public getComprehensionSlideControls(dto: ComprehensionSlideControlsDto): ComprehensionSlideControls {
        return new ComprehensionSlideControls(
            dto.nextLabel,
            dto.previousLabel,
        );
    }
}

export interface ComprehensionSlideControlsDto {
    nextLabel: string;
    previousLabel: string;
}
