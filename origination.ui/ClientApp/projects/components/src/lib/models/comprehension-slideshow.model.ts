import { Injectable } from '@angular/core';
import { ComprehensionSlideControls, ComprehensionSlideControlsDto, ComprehensionSlideControlsFactory } from './comprehension-slide-controls.model';
import { ComprehensionSlide, ComprehensionSlideDto, ComprehensionSlideFactory } from './comprehension-slide.model';

export class ComprehensionSlideshow {
    public constructor(
        private _slideControls: ComprehensionSlideControls,
        private _slides: ComprehensionSlide[],
    ) { }

    public get slideControls(): ComprehensionSlideControls {
        return this._slideControls;
    }

    public get slides(): ComprehensionSlide[] {
        return this._slides;
    }
}

@Injectable()
export class ComprehensionSlideshowFactory {
    public constructor(
        private readonly comprehensionSlideControlsFactory: ComprehensionSlideControlsFactory,
        private readonly comprehensionSlideFactory: ComprehensionSlideFactory,
    ) { }

    public getComprehensionSlideControls(dto: ComprehensionSlideshowDto): ComprehensionSlideshow {
        return new ComprehensionSlideshow(
            this.comprehensionSlideControlsFactory.getComprehensionSlideControls(dto.slideControls),
            this.comprehensionSlideFactory.getComprehensionSlides(dto.slides),
        );
    }
}

export interface ComprehensionSlideshowDto {
    slideControls: ComprehensionSlideControlsDto;
    slides: ComprehensionSlideDto[];
}
