import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PgButtonModule, PgIconComponent, PgIconModule } from '@progleasing/grit-core';
import { ApplyPageComponent } from './apply-page/apply-page.component';
import { ComprehensionSlideshowComponent } from './comprehension-slideshow/comprehension-slideshow.component';
import { HeaderComponent } from './header/header.component';
import { ApplyPageFactory } from './models/apply-page.model';
import { ComprehensionSlideControlsFactory } from './models/comprehension-slide-controls.model';
import { ComprehensionSlideFactory } from './models/comprehension-slide.model';
import { ComprehensionSlideshowFactory } from './models/comprehension-slideshow.model';
import { GlobalContentFactory } from './models/global-content.model';
import { TermsSheetFactory } from './models/terms-sheet.model';
import { ComprehensionSlideshowService } from './services/comprehension-slideshow.service';
import { TermsSheetComponent } from './terms-sheet/terms-sheet.component';
import { ProgressIndicatorComponent } from './progress-indicator/progress-indicator.component';
import { ProgressIndicatorFactory } from './models/progress-indicator.model';
import { ProgressIndicatorStepFactory } from './models/progress-indicator-step.model';

@NgModule({
  declarations: [
    ApplyPageComponent,
    ComprehensionSlideshowComponent,
    HeaderComponent,
    ProgressIndicatorComponent,
    TermsSheetComponent,
  ],
  imports: [CommonModule, PgButtonModule, PgIconModule],
  exports: [
    ApplyPageComponent,
    ComprehensionSlideshowComponent,
    HeaderComponent,
    PgIconComponent,
    ProgressIndicatorComponent,
    TermsSheetComponent,
  ],
  providers: [
    ApplyPageFactory,
    ComprehensionSlideControlsFactory,
    ComprehensionSlideFactory,
    ComprehensionSlideshowFactory,
    ComprehensionSlideshowService,
    GlobalContentFactory,
    ProgressIndicatorFactory,
    ProgressIndicatorStepFactory,
    TermsSheetFactory,
  ],
})
export class ComponentsModule {}
