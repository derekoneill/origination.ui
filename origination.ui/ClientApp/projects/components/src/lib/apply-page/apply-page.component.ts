import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ApplyPageDto } from '../models/apply-page.model';
import { GlobalContentDto } from '../models/global-content.model';
import { ApplyPageEvents, ApplyPageService } from '../services/apply-page.service';

@Component({
    selector: 'uae-apply-page',
    templateUrl: './apply-page.component.html',
    styleUrls: ['./apply-page.component.scss']
})
export class ApplyPageComponent implements OnInit {

    @Input()
    public model: ApplyPageDto;

    @Input()
    public formGroup: FormGroup;
    public securityAffirmation: string;

    /**
     * This will be coming from some place else. A call to the global content cms endpoint
     * here or higher up the chain and referencing a content store someplace. Reused global
     * content
     */
    private globalContent: GlobalContentDto = {
        resultsLoaderContent: ['Processing you information'],
        securityAffirmation: 'Your personal information is secure and private'
    };

    constructor(
        private readonly applyPageService: ApplyPageService,
    ) { }

    ngOnInit() {
        this.securityAffirmation = this.globalContent.securityAffirmation;
    }

    continue() {
        this.applyPageService.pushEvent(ApplyPageEvents.Continue);
    }

}
