import { Component, Input, OnInit } from '@angular/core';
import {
  ProgressIndicatorService,
  ProgressIndicatorEvents,
} from '../services/progress-indicator.service';

@Component({
  selector: 'uae-progress-indicator',
  templateUrl: './progress-indicator.component.html',
  styleUrls: ['./progress-indicator.component.scss'],
})
export class ProgressIndicatorComponent implements OnInit {
  @Input()
  public model: any;

  @Input()
  public currentStep: number;

  constructor(private readonly progressIndicatorService: ProgressIndicatorService) {}

  ngOnInit() {}

  continue() {
    this.progressIndicatorService.pushEvent(ProgressIndicatorEvents.Continue);
  }
}
