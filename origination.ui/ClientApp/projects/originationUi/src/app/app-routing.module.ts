import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { COMPREHENSION_ROUTES } from 'projects/comprehension/src/index';
import { APPLY_ROUTES } from 'projects/apply/src/index';
import { ApplicationPath } from '../application-path.enum';

const routes: Routes = [
  {
    path: ApplicationPath.Comprehension,
    children: COMPREHENSION_ROUTES,
  },
  {
    path: ApplicationPath.Apply,
    children: APPLY_ROUTES,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
