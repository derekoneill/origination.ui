import { Routes } from '@angular/router';
import { ComprehensionRoute } from './comprehension-route.enum';
import { LeasingExplainedComponent } from './leasing-explained/leasing-explained.component';
import { TermsSheetComponent } from './terms-sheet/terms-sheet.component';
import { ProgressIndicatorComponent } from './progress-indicator/progress-indicator.component';

export const COMPREHENSION_ROUTES: Routes = [
  {
    path: ComprehensionRoute.ProgressIndicator,
    component: ProgressIndicatorComponent,
  },
  {
    path: ComprehensionRoute.Slideshow,
    component: LeasingExplainedComponent,
  },
  {
    path: ComprehensionRoute.TermsSheet,
    component: TermsSheetComponent,
  },
];
