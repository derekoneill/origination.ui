import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ApplicationPath } from 'projects/originationUi/src/application-path.enum';
import { ApplyRoute } from 'projects/apply/src';
import { ProgressIndicatorDto } from 'projects/components/src/lib/models/progress-indicator.model';
import {
  ProgressIndicatorService,
  ProgressIndicatorEvents,
} from 'projects/components/src/lib/services/progress-indicator.service';
import { ComprehensionRoute } from '../comprehension-route.enum';

@Component({
  selector: 'lib-progress-indicator',
  templateUrl: './progress-indicator.component.html',
  styleUrls: ['./progress-indicator.component.scss'],
})
export class ProgressIndicatorComponent implements OnInit, OnDestroy {
  private progressIndicatorEventsSubscription: Subscription;

  // This will come from the CMS in the near future
  public progressIndicator: ProgressIndicatorDto = {
    bodyCopy: "Next, we'll show you the cost of leasing and how much you’ll pay over time.",
    heading: 'Account created!',
    nextLabel: 'Continue',
    steps: [
      {
        heading: 'Create account',
        bodyCopy: 'John Doe<br>(845)399-0334<br>johndoe@gmail.com<br>08/16/1988<br>•••-••-6666',
      },
      {
        heading: 'See how leasing works',
        bodyCopy: 'See the cost of leasing and estimate how much you could pay over time.',
      },
      {
        heading: 'Get approved',
        bodyCopy: 'Fill out a quick application so we know what will fit within your budget. ',
      }
    ],
  };

  constructor(
    private readonly progressIndicatorService: ProgressIndicatorService,
    private readonly router: Router
  ) { }

  public ngOnInit(): void {
    this.progressIndicatorEventsSubscription = this.progressIndicatorService.eventStream$.subscribe(
      (progressIndicatorEvent) => {
        switch (progressIndicatorEvent) {
          case ProgressIndicatorEvents.Continue:
            this.router.navigate([ApplicationPath.Comprehension, ComprehensionRoute.Slideshow]);
            break;
        }
      }
    );
  }

  public ngOnDestroy(): void {
    this.progressIndicatorEventsSubscription.unsubscribe();
  }
}
