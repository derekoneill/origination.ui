import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  TermsSheetEvents,
  TermsSheetService,
} from 'projects/components/src/lib/services/terms-sheet.service';
import { Subscription, Observable } from 'rxjs';
import { ApplicationPath } from 'projects/originationUi/src/application-path.enum';
import { ApplyRoute } from 'projects/apply/src';
import { ContentService } from 'projects/components/src/lib/services/content.service';

@Component({
  selector: 'lib-terms-sheet',
  templateUrl: './terms-sheet.component.html',
  styleUrls: ['./terms-sheet.component.scss'],
})
export class TermsSheetComponent implements OnInit, OnDestroy {
  public termsSheetSubscription: Observable<string>;

  private readonly contentPageKey = 'terms-sheets';
  private readonly instanceType = 'lease-cost-terms';

  subs: Subscription[] = [];

  constructor(
    private readonly termsSheetService: TermsSheetService,
    private readonly router: Router,
    private readonly contentService: ContentService
  ) {}

  public ngOnInit(): void {
    this.termsSheetSubscription = this.contentService.getContent(
      this.contentPageKey,
      this.instanceType
    );
    this.subs.push(this.termsSheetSubscription.subscribe());

    this.subs.push(
      this.termsSheetService.eventStream$.subscribe((termSheetEvent) => {
        switch (termSheetEvent) {
          case TermsSheetEvents.Agree:
            this.router.navigate([ApplicationPath.Apply, ApplyRoute.HomeAddress]);
            break;
        }
      })
    );
  }

  public ngOnDestroy(): void {
    this.subs.forEach((s) => s.unsubscribe());
  }
}
