import { NgModule } from '@angular/core';
import { LeasingExplainedComponent } from './leasing-explained/leasing-explained.component';
import { TermsSheetComponent } from './terms-sheet/terms-sheet.component';
import { ComponentsModule } from 'projects/components/src';
import { CommonModule } from '@angular/common';
import { ProgressIndicatorComponent } from './progress-indicator/progress-indicator.component';

@NgModule({
  declarations: [LeasingExplainedComponent, ProgressIndicatorComponent, TermsSheetComponent],
  imports: [ComponentsModule, CommonModule],
  exports: [],
})
export class ComprehensionModule {}
