import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeasingExplainedComponent } from './leasing-explained.component';

describe('LeasingExplainedComponent', () => {
  let component: LeasingExplainedComponent;
  let fixture: ComponentFixture<LeasingExplainedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeasingExplainedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeasingExplainedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
