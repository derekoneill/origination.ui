import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ComprehensionSlideshowDto } from 'projects/components/src/lib/models/comprehension-slideshow.model';
import {
  ComprehensionSlideshowEvents,
  ComprehensionSlideshowService,
} from 'projects/components/src/lib/services/comprehension-slideshow.service';
import { Subscription, Observable } from 'rxjs';
import { ApplicationPath } from 'projects/originationUi/src/application-path.enum';
import { ComprehensionRoute } from '../comprehension-route.enum';
import { ContentService } from 'projects/components/src/lib/services/content.service';

@Component({
  selector: 'leasing-explained',
  templateUrl: './leasing-explained.component.html',
  styleUrls: ['./leasing-explained.component.scss'],
})
export class LeasingExplainedComponent implements OnInit, OnDestroy {
  public slideShowDataSubscription: Observable<string>;
  public comprehensionSlideshowId = 'PAC';

  private readonly contentPageKey = 'comprehension-slideshow';

  subs: Subscription[] = [];

  constructor(
    private readonly comprehensionSlideshowService: ComprehensionSlideshowService,
    private readonly router: Router,
    private readonly contentService: ContentService
  ) {}

  public ngOnInit(): void {
    this.slideShowDataSubscription = this.contentService.getContent(this.contentPageKey);
    this.subs.push(this.slideShowDataSubscription.subscribe());

    this.subs.push(
      this.comprehensionSlideshowService.eventStream$.subscribe((comprehensionSlideshowEvent) => {
        switch (comprehensionSlideshowEvent) {
          case ComprehensionSlideshowEvents.Completed:
            this.router.navigate([ApplicationPath.Comprehension, ComprehensionRoute.TermsSheet]);
            break;
          case ComprehensionSlideshowEvents.Referrer:
            console.log('You should move back to referrer. Whoever that is.');
            break;
        }
      })
    );
  }

  public ngOnDestroy(): void {
    this.subs.forEach((s) => s.unsubscribe());
  }
}
