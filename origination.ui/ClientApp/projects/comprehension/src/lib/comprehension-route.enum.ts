export enum ComprehensionRoute {
  ProgressIndicator = 'progress-indicator',
  Slideshow = 'slideshow',
  TermsSheet = 'terms-sheet',
}
