export enum ApplyRoute {
    HomeAddress = 'home-address',
    IncomeInformation = 'income-information',
    BankInformation = 'bank-information',
    TermsSheet = 'terms-sheet',
    Results = 'results',
}