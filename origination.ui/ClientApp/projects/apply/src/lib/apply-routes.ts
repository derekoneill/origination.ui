
import { Routes } from '@angular/router';
import { ApplyRoute } from './apply-route.enum';
import { HomeAddressComponent } from './home-address/home-address.component';

export const APPLY_ROUTES: Routes = [
    {
        path: ApplyRoute.HomeAddress,
        component: HomeAddressComponent,
    },
];