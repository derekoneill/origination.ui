import { NgModule } from '@angular/core';
import { ComponentsModule } from 'projects/components/src';
import { HomeAddressComponent } from './home-address/home-address.component';

@NgModule({
  declarations: [
    HomeAddressComponent,
  ],
  imports: [
    ComponentsModule,
  ],
  exports: [
    HomeAddressComponent,
  ]
})
export class ApplyModule { }
