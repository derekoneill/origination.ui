import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApplyPageDto } from 'projects/components/src/lib/models/apply-page.model';
import {
  ApplyPageEvents,
  ApplyPageService,
} from 'projects/components/src/lib/services/apply-page.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'home-address',
  templateUrl: './home-address.component.html',
  styleUrls: ['./home-address.component.scss'],
})
export class HomeAddressComponent implements OnDestroy {
  public sampleInfo: ApplyPageDto;
  public formGroup: FormGroup;

  private applyPageEventsSubscription: Subscription;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly applyPageService: ApplyPageService
  ) {}

  public ngOnInit(): void {
    this.sampleInfo = {
      affirmSecurity: false,
      bodyCopy: 'Please provide your current mailing address.',
      heading: 'Home address',
      nextLabel: 'Continue',
    };

    this.formGroup = this.formBuilder.group({
      exampleFormControl: [undefined, []],
    });

    this.applyPageEventsSubscription = this.applyPageService.eventStream$.subscribe(
      (applyPageEvent) => {
        if (applyPageEvent === ApplyPageEvents.Continue) {
          console.log('Exciting things are happening. Maybe.');
        }
      }
    );
  }

  public ngOnDestroy(): void {
    this.applyPageEventsSubscription.unsubscribe();
  }
}
