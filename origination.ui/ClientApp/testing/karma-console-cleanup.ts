// Intercept the console.warn to ignore the WARN: 'Spec 'MyService should do this' has no expectations.'
const origConsoleWarn = console.warn;
console.warn = (...args: any[]) => {
    if (args.length === 0) {
        return;
    }
    const arg0Value: string = args[0];
    if (arg0Value.includes('Spec ') && arg0Value.includes('has no expectations.')) {
        return;
    }

    origConsoleWarn.apply(null, args);
};