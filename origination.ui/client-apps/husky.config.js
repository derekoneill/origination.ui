const tasks = (arr) => arr.join(' && ');

module.exports = {
  hooks: {
    'pre-commit': tasks(['../../ci/bin/pre-commit.sh']),
  },
};
