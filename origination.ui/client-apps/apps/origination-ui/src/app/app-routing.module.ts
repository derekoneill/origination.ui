import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UnifiedApps } from '@ua/shared/state';

const routes: Routes = [
  {
    path: UnifiedApps.comprehension.route,
    loadChildren: () =>
      import('@ua/app-features/comprehension').then(
        (m) => m.ComprehensionModule
      ),
  },
  {
    path: UnifiedApps.apply.route,
    loadChildren: () =>
      import('@ua/app-features/apply').then((m) => m.ApplyModule),
  },
  {
    path: '',
    redirectTo: UnifiedApps.comprehension.route,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: true,
      enableTracing: false,
      onSameUrlNavigation: 'reload',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
