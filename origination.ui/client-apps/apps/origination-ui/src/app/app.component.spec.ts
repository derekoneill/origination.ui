import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { ComponentsModule } from '@ua/shared/ui';
import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

describe('AppComponent', () => {
  let spectator: Spectator<AppComponent>;
  const createComponent = createComponentFactory({
    component: AppComponent,
    imports: [
      RouterTestingModule,
      ComponentsModule,
      StoreModule,
      EffectsModule,
    ],
  });

  beforeEach(
    () =>
      (spectator = createComponent({
        detectChanges: true,
      }))
  );

  it('should have correct properties and child elements', () => {
    expect(spectator.component.title).toEqual('originationUi');
  });
});
