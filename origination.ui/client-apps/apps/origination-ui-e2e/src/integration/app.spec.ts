describe('origination-ui', () => {
  beforeEach(() => cy.visit('/'));

  it('should render the horizontal header with arrow back icon and exit icon correctly.', () => {
    cy.get('div.application-header__previous-control grit-icon').contains(
      'arrow_back'
    );
    cy.get('div.application-header__branding img')
      .invoke('attr', 'src')
      .should('contain', 'assets/progressive-logo-small.svg');
    cy.get('div.application-header__exit-control grit-icon').contains('close');
  });
});
