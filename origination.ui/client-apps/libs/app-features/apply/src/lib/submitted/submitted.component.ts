import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Subscription } from 'rxjs';
import {
  Session,
  ApplicationResult,
  Order,
  ApplicationApprovalStatus,
} from '@ua/shared/data-access';
import { UnifiedApps } from '@ua/shared/state';
import * as fromFlowRoutes from '../+state/flow-routes';
import * as fromApply from '../+state';
import * as fromApplicationResult from '../+state/application-result';
import * as fromSharedState from '@ua/shared/state';

@Component({
  selector: 'ua-submitted-application',
  templateUrl: './submitted.component.html',
  styleUrls: ['./submitted.component.scss'],
})
export class SubmittedComponent implements OnInit, OnDestroy, AfterViewInit {
  subs: Subscription[] = [];
  session: Session;
  shortAppEnabled = false;
  order: Order;
  isFinished = false;

  constructor(private store$: Store<fromApply.ApplyState>) {}

  ngOnInit(): void {
    this.subs.push(
      this.store$
        .pipe(select(fromApply.getShortAppSelector))
        .subscribe((isShortApp) => (this.shortAppEnabled = isShortApp))
    );
    this.subs.push(
      this.store$
        .pipe(select(fromApply.getSessionSelector))
        .subscribe((session) => (this.session = session))
    );
  }

  ngOnDestroy(): void {
    this.subs.forEach((s) => s.unsubscribe());
  }

  ngAfterViewInit() {
    this.submitApplication();
    this.subs.push(
      this.store$
        .pipe(select(fromApply.getApplicationResultSelector))
        .subscribe((subResults) => this.handleSubmissionResults(subResults))
    );
  }

  submitApplication() {
    this.store$.dispatch(
      fromApplicationResult.submitApplication({
        sessionId: this.session.sessionId,
        shortApp: true,
      })
    );
  }

  handleSubmissionResults(appResult: ApplicationResult) {
    if (appResult.sessionId === undefined) return;
    if (!appResult.sessionId.match(/[^0-]+/g)) {
      // Default session id (all 0's) means something went wrong
      this.store$.dispatch(
        fromSharedState.navigateRoute({
          route: UnifiedApps.apply.children.somethingBroke.route,
          params: { sid: this.session.sessionId },
        })
      );
      return;
    }

    if (
      appResult.applicationApprovalStatus === ApplicationApprovalStatus.Denied
    ) {
      this.store$.dispatch(
        fromSharedState.navigateRoute({
          route: UnifiedApps.problem.children.unableToApprove.route,
          params: { sid: this.session.sessionId },
        })
      );
    } else if (
      appResult.applicationApprovalStatus !==
        ApplicationApprovalStatus.Approved &&
      appResult.applicationApprovalStatus ===
        ApplicationApprovalStatus.PreApproved
    ) {
      this.store$.dispatch(
        fromSharedState.navigateRoute({
          route: UnifiedApps.apply.children.applicationPending.route,
          params: { sid: this.session.sessionId },
        })
      );
    }

    this.stopLoader();
  }

  stopLoader() {
    this.isFinished = true;
    this.onContinue();
  }

  onContinue() {
    this.store$.dispatch(
      fromFlowRoutes.changeApplyFlowRoute({
        from: UnifiedApps.apply.children.submittedApplication.route,
        direction: fromFlowRoutes.RouteDirection.Next,
        params: { sid: this.session.sessionId },
      })
    );
  }
}
