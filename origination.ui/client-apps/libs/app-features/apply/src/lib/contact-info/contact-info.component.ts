import {
  mergeMap,
  debounceTime,
  distinctUntilChanged,
  skip,
} from 'rxjs/operators';
import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import { BehaviorSubject, Subscription, combineLatest } from 'rxjs';
import {
  IConfig,
  Application,
  Session,
  AuthenticationMethod,
  LocationService,
} from '@ua/shared/data-access';
import {
  cityValidator,
  Constants,
  contains,
  containsPattern,
  emailCharacters,
  emailFormat,
  phoneLength,
  phoneTextMask,
  stateValidator,
  street1Validator,
  street2Validator,
  zipValidator,
} from '@ua/shared/utilities';
import { Store, select } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { UnifiedApps } from '@ua/shared/state';
import * as fromAuthentications from '../+state/authentication';
import * as fromApply from '../+state';
import * as fromAnalytic from '../+state/analytic';
import * as fromFlowRoutes from '../+state/flow-routes';

@Component({
  selector: 'ua-contact-info',
  templateUrl: './contact-info.component.html',
  styleUrls: ['./contact-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactInfoComponent implements OnInit, OnDestroy, AfterViewInit {
  subs: Subscription[] = [];

  statePatternRegex = /[a-z ]+/i;
  contactInfoFormGroup: FormGroup;

  statuses = fromAuthentications.Status;
  saveResumableStatus: fromAuthentications.SavingResumableStatus;

  addressList: any[];
  addressQuerySubject: BehaviorSubject<string> = new BehaviorSubject<string>(
    ''
  );
  errorBusinessState = null;
  zipCheckLoadingSubject = new BehaviorSubject(false);
  Constants = Constants;

  phoneTextMask = phoneTextMask;
  config: IConfig;
  session: Session;
  authenticationState: fromAuthentications.AuthenticationState;
  application: Application;
  contactMethods = AuthenticationMethod;

  constructor(
    private store$: Store<fromApply.State>,
    private formBuilder: FormBuilder,
    private router: Router,
    private locationService: LocationService,
    private cdRef: ChangeDetectorRef,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    this.subs.push(
      this.store$
        .pipe(select(fromApply.getConfigSelector))
        .subscribe((config) => (this.config = config)),
      this.store$
        .pipe(select(fromApply.getApplicationSelector))
        .subscribe((application) => (this.application = application)),
      this.store$
        .pipe(select(fromApply.getSessionSelector))
        .subscribe((session) => (this.session = session)),
      this.store$
        .pipe(select(fromApply.getAuthenticationStateSelector))
        .subscribe(
          (authenticationState) =>
            (this.authenticationState = authenticationState)
        ),
      this.store$
        .pipe(select(fromApply.getSaveResumableStatusSelector))
        .subscribe(
          (saveResumableStatus) =>
            (this.saveResumableStatus = saveResumableStatus)
        )
    );

    this.contactInfoFormGroup = this.formBuilder.group({
      emailAddress: [
        this.application.emailAddress,
        [
          Validators.required,
          emailFormat,
          contains('@', 'atSymbol'),
          contains('.', 'period'),
          emailCharacters,
          containsPattern(/\..{2,}/, 'domain'),
        ],
      ],
      mobilePhone: [
        this.application.phoneNumber,
        [Validators.required, this.validateAreaCode(), phoneLength],
      ],
      street1: [
        this.application.street1,
        [
          Validators.required,
          Validators.pattern(/^([a-z0-9\.,\-\&\#\/'\s])+$/i),
          street1Validator(),
        ],
      ],
      street2: [
        this.application.street2,
        [
          Validators.pattern(/^([a-z0-9\.,\-\&\#\/'\s])+$/i),
          street2Validator(),
        ],
      ],
      city: [this.application.city, [Validators.required, cityValidator()]],
      state: [this.application.state, [Validators.required, stateValidator()]],
      zip: [this.application.zip, [Validators.required, zipValidator()]],
      marketingOptIn: [false],
    });
  }

  ngAfterViewInit() {
    this.subs.push(
      this.contactInfoFormGroup
        .get('street1')
        .valueChanges.pipe(skip(1))
        .subscribe((x) => this.handleQueryStringChanged(x))
    );

    this.subs.push(
      this.contactInfoFormGroup.get('state').valueChanges.subscribe((x) => {
        if (!x) {
          // Remove zip validation if its there. Since its tied to this control
          const zipControl = this.contactInfoFormGroup.get('zip');
          zipControl.setErrors({});
          return;
        }
        this.validateStateZipMatch();

        const test = x.toUpperCase();
        const control = this.contactInfoFormGroup.get('state');
        if (Constants.StatesWeDontDoBusinessIn[test]) {
          this.errorBusinessState = Constants.StatesWeDontDoBusinessIn[test];
          control.setErrors({ noBusiness: true });
        } else if (Constants.StatesWeDontShipTo[test]) {
          this.errorBusinessState = Constants.StatesWeDontShipTo[test];
          control.setErrors({ noShip: true });
        } else if (
          this.usedItemsBlocked() &&
          Constants.UsedItemStatesBlocked[test]
        ) {
          this.errorBusinessState = Constants.UsedItemStatesBlocked[test];
          control.setErrors({ noUsedItems: true });
        }
      }),

      this.contactInfoFormGroup.get('zip').valueChanges.subscribe((x) => {
        if (!x || (x.length !== 5 && x.length !== 9)) {
          return;
        }
        this.validateStateZipMatch();
      }),

      this.addressQuerySubject
        .asObservable()
        .pipe(
          skip(1),
          distinctUntilChanged(),
          debounceTime(10),
          mergeMap((q) => {
            if (q && q.length) {
              return this.locationService.queryLocations(q);
            }
            return [];
          })
        )
        .subscribe((results) => {
          this.addressList = results;
        }),
      combineLatest(
        this.contactInfoFormGroup.statusChanges,
        this.zipCheckLoadingSubject.asObservable()
      ).subscribe()
    );

    this.store$.dispatch(
      fromAnalytic.pageViewEvent(
        this.session,
        true,
        this.router.url,
        this.translateService.instant('ContactInfo.Title'),
        this.translateService.instant('SectionIdentifier.Apply.Title')
      )
    );
  }

  ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }

  validateAreaCode() {
    return (c: FormControl) => {
      const val: string = c.value;
      if (val && (val[0] === '1' || val[0] === '0')) {
        return { areaCode: true };
      }
      return null;
    };
  }

  handleAddressItemSelected(item) {
    this.locationService
      .getLocationDetailByLocationId(item.locationId)
      .subscribe((details) => {
        const zip = details.address.postalCode || '';
        this.contactInfoFormGroup
          .get('street1')
          .setValue(details.address.streetAddress || '');
        this.contactInfoFormGroup.get('street2').setValue('');
        this.contactInfoFormGroup
          .get('city')
          .setValue(details.address.city || '');
        this.contactInfoFormGroup
          .get('state')
          .setValue(details.address.region || '');
        this.contactInfoFormGroup.get('zip').setValue(zip.substr(0, 5));
      });
  }

  handleStateSelected(item) {
    this.contactInfoFormGroup.get('state').setValue(item.value);
  }

  handleQueryStringChanged(query) {
    this.addressQuerySubject.next(query);
  }

  onContinue() {
    if (this.contactInfoFormGroup.valid) {
      this.store$.dispatch(
        fromAnalytic.formFillEvent(
          'marketing-opt-in',
          this.contactInfoFormGroup.get('marketingOptIn').value
        )
      );

      const application: Application = {
        ...this.application,
        sessionId: this.session.sessionId,
        phoneNumber: this.contactInfoFormGroup.get('mobilePhone').value,
        emailAddress: this.contactInfoFormGroup.get('emailAddress').value,
        street1: this.contactInfoFormGroup.get('street1').value,
        street2: this.contactInfoFormGroup.get('street2').value,
        city: this.contactInfoFormGroup.get('city').value,
        state: this.contactInfoFormGroup.get('state').value,
        zip: this.contactInfoFormGroup.get('zip').value,
        marketingOptIn: this.contactInfoFormGroup.get('marketingOptIn').value,
      };

      this.store$.dispatch(
        fromApply.saveApplicationResumableStateSplitter({
          resumableState: {
            sessionId: this.session.sessionId,
            resumeData: {
              authId: this.authenticationState.authenticationId,
              ssn: this.authenticationState.ssn,
              route: this.router.url,
            },
          },
          application: application,
          nextRoute: {
            from: UnifiedApps.apply.children.contactInfo.route,
            direction: fromFlowRoutes.RouteDirection.Next,
            params: { sid: this.session.sessionId },
          },
        })
      );
    }
  }

  addressComplete() {
    return (
      this.contactInfoFormGroup.get('street1').valid &&
      this.contactInfoFormGroup.get('street2').valid &&
      this.contactInfoFormGroup.get('city').valid &&
      this.contactInfoFormGroup.get('state').valid &&
      this.contactInfoFormGroup.get('zip').valid
    );
  }

  private usedItemsBlocked() {
    if (!this.config.storesBlockingUsedItems) {
      return false;
    }

    return this.config.storesBlockingUsedItems.includes(this.session.storeId);
  }

  private validateStateZipMatch() {
    this.zipCheckLoadingSubject.next(true);
    const state = this.contactInfoFormGroup.get('state').value;
    let zip: string = this.contactInfoFormGroup.get('zip').value;
    zip = zip.split('-')[0];

    if (state && state.length === 2 && zip) {
      this.locationService
        .validateStateAndZip({
          stateCode: state,
          zipCode: zip,
        })
        .subscribe((res) => {
          if (res) {
            const errs = this.contactInfoFormGroup.get('zip').errors || {};
            delete errs['zipStateMismatch'];
          } else {
            this.contactInfoFormGroup
              .get('zip')
              .setErrors({ zipStateMismatch: true }, { emitEvent: true });
          }
          this.contactInfoFormGroup.updateValueAndValidity({
            onlySelf: false,
            emitEvent: true,
          });
          this.zipCheckLoadingSubject.next(false);
          this.cdRef.detectChanges();
        });
    }
  }
}
