import { createAction, union } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { FeatureFlags } from '@ua/shared/data-access';
import { generateId } from '@ua/shared/utilities';

export interface FeatureFlagsRequest {
  storeId: number;
  ldUserKey: string;
}

const FeatureFlagsPrefix = '[Feature-Flags]';

export const getFeatureFlags = createAction(
  `${FeatureFlagsPrefix} Get Feature Flags`,
  (
    featureFlagsRequest: FeatureFlagsRequest,
    correlationId: string = generateId()
  ) => ({
    featureFlagsRequest,
    correlationId,
  })
);

export const getFeatureFlagsSuccess = createAction(
  `${FeatureFlagsPrefix} Get Feature Flags Success`,
  (featureFlags: FeatureFlags, correlationId: string = generateId()) => ({
    featureFlags,
    correlationId,
  })
);

export const getFeatureFlagsError = createAction(
  `${FeatureFlagsPrefix} Get Feature Flags Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

const allActions = union({
  getFeatureFlags,
  getFeatureFlagsSuccess,
  getFeatureFlagsError,
});

export type FeatureFlagsActions = typeof allActions;
