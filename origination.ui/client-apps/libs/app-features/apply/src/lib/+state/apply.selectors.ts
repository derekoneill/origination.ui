import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromSharedState from '@ua/shared/state';
import * as fromApplyReducer from './apply.reducers';
import * as fromFlowRoutes from './flow-routes';
import { IConfig } from '@ua/shared/data-access';
import * as fromUi from './ui';
import * as fromSession from './session';
import * as fromFeatureFlags from './feature-flags';
import * as fromApplicationResult from './application-result';
import * as fromResume from './resume';
import * as fromPayment from './payment';
import * as fromOrder from './order';
import * as fromApplication from './application';
import { ResumeState } from './resume';

export interface State extends fromSharedState.State {
  apply: fromApplyReducer.ApplyState;
}

const getApplyStateSelector = createFeatureSelector<
  fromApplyReducer.ApplyState
>('apply');
export const getAuthenticationStateSelector = createSelector(
  getApplyStateSelector,
  (state) => state.authenticationInformation
);
export const getAuthenticationIdSelector = createSelector(
  getAuthenticationStateSelector,
  (state) => state.authenticationId
);
export const getConsentSelector = createSelector(
  getAuthenticationStateSelector,
  (state) => state.consent
);
export const getAuthenticationCodeSelector = createSelector(
  getAuthenticationStateSelector,
  (state) => state.authenticationCode
);
export const getSsnSelector = createSelector(
  getAuthenticationStateSelector,
  (state) => state.ssn
);
export const getContactMethodSelector = createSelector(
  getAuthenticationStateSelector,
  (state) => state.authenticationMethod
);
export const getSendCodeStatusSelector = createSelector(
  getAuthenticationStateSelector,
  (state) => state.sendAuthenticationCodeStatus
);
export const getResendCodeStatusSelector = createSelector(
  getAuthenticationStateSelector,
  (state) => state.resendAuthenticationCodeStatus
);
export const getVerifyAuthCodeStatusSelector = createSelector(
  getAuthenticationStateSelector,
  (state) => state.verifyAuthenticationCodeStatus
);
export const getVerifySsnStatusSelector = createSelector(
  getAuthenticationStateSelector,
  (state) => state.verifySsnStatus
);
export const getSaveResumableStatusSelector = createSelector(
  getAuthenticationStateSelector,
  (state) => state.savingResumableStatus
);

export const getApplicationStateSelector = createSelector(
  getApplyStateSelector,
  (state) => state.application
);
export const getApplicationSelector = createSelector(
  getApplicationStateSelector,
  (state) => state.application
);

export const getAnalyticStateSelector = createSelector(
  getApplyStateSelector,
  (state) => state.analytics
);

export const getSessionStateSelector = createSelector(
  getApplyStateSelector,
  (state) => state.session
);
export const getSessionSelector = createSelector(
  getSessionStateSelector,
  (state) => state.session
);

export const getResumeStateSelector = createSelector(
  getApplyStateSelector,
  (state) => state.resumable
);
export const getResumeSelector = createSelector(
  getResumeStateSelector,
  (state) => state.resume
);

export const getApplicationResultStateSelector = createSelector(
  getApplyStateSelector,
  (state) => state.applicationResult
);
export const getApplicationResultSelector = createSelector(
  getApplicationResultStateSelector,
  (state) => state.applicationResult
);

export const getFeatureFlagsStateSelector = createSelector(
  getApplyStateSelector,
  (state) => state.featureFlags
);
export const getShortAppSelector = createSelector(
  getFeatureFlagsStateSelector,
  (state) => state.isShortApp
);

export const getConfigSelector = createSelector(
  getApplyStateSelector,
  (state) => state.config
);

export const getPaymentInformationStateSelector = createSelector(
  getApplyStateSelector,
  (state) => state.payment
);
export const getPaymentInformationSelector = createSelector(
  getPaymentInformationStateSelector,
  (state) => state.paymentInformation
);

//
// export const getAnalyticStateSelector = createFeatureSelector<
//   Array<{ [key: string]: any }>
//   >('analytics');
// export const getCurrentRouteStateSelector = createFeatureSelector<
//   fromFlowRoutes.FlowRouteState
//   >('currentRoute');
// export const getConfigSelector = createFeatureSelector<IConfig>('config');
// export const getUiSelector = createFeatureSelector<fromUi.UiState>('ui');
//
// export const getSessionStateSelector = createFeatureSelector<
//   fromSession.SessionState
//   >('session');
// export const getSessionSelector = createSelector(
//   getSessionStateSelector,
//   (state) => state.session
// );
//
// export const getFeatureFlagsStateSelector = createFeatureSelector<
//   fromFeatureFlags.FeatureFlagsState
//   >('featureFlags');
// export const getShortAppSelector = createSelector(
//   getFeatureFlagsStateSelector,
//   (state) => state.isShortApp
// );
// export const getMonthlyPayFreqDisableSelector = createSelector(
//   getFeatureFlagsStateSelector,
//   (state) => state.monthlyPayFreqDisabled
// );
// export const getSkipCreditCardSelector = createSelector(
//   getFeatureFlagsStateSelector,
//   (state) => state.skipCreditCard
// );
//
// export const getApplicationResultStateSelector = createFeatureSelector<fromApplicationResult.ApplicationResultState>('applicationResult');
// export const getApplicationResultSelector = createSelector(
//   getApplicationResultStateSelector,
//   (state) => state.applicationResult
// );
//
// export const getResumeStateSelector = createFeatureSelector<fromResume.ResumeState>('resumable');
// export const getResumeSelector = createSelector(
//   getResumeStateSelector,
//   (state) => state.resume
// );
//
// export const getPaymentStateSelector = createFeatureSelector<
//   fromPayment.PaymentState
//   >('payment');
// export const getPaymentInformationSelector = createSelector(
//   getPaymentStateSelector,
//   (state) => state.paymentInformation
// );
//
// export const getOrderStateSelector = createFeatureSelector<
//   fromOrder.OrderState
//   >('order');
// export const getOrderSelector = createSelector(
//   getOrderStateSelector,
//   (state) => state.order
// );

export const getSessionAndApplicationAndResumeStateSelector = createSelector(
  getSessionStateSelector,
  getApplicationStateSelector,
  getResumeStateSelector,
  (
    sessionState: fromSession.SessionState,
    applicationState: fromApplication.ApplicationState,
    resumeState: ResumeState
  ) => {
    return { sessionState, applicationState, resumeState };
  }
);
