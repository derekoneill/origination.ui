import { HttpErrorResponse } from '@angular/common/http';
import { AuthenticationMethod } from '@ua/shared/data-access';
import { createReducer, on } from '@ngrx/store';
import * as fromActions from './authenticate.actions';

export enum Status {
  Init = 'Init',
  Sending = 'Sending',
  Success = 'Success',
  Error = 'Error',
}

export interface SendAuthenticationCodeStatus {
  status: Status;
  count: number;
  error?: HttpErrorResponse;
}

export interface ResendAuthenticationCodeStatus {
  status: Status;
  count: number;
  error?: HttpErrorResponse;
}

export interface VerifiedAuthenticationCode {
  verifyAt: Date;
  token: string;
  locked: boolean;
  refreshToken: string;
  expiresAt: Date;
}

export interface VerifyAuthenticationCodeStatus {
  authenticationToken: VerifiedAuthenticationCode;
  count: number;
  isResumable: boolean;
  status: Status;
  error?: HttpErrorResponse;
}

export interface VerifySsnStatus {
  count: number;
  numberOfFailures: number;
  status: Status;
  error?: HttpErrorResponse;
}

export interface SavingResumableStatus {
  count: number;
  status: Status;
  error?: HttpErrorResponse;
}

export interface AuthenticationState {
  authenticationId: string;
  consent: boolean;
  authenticationCode: string;
  ssn: string;
  authenticationMethod: AuthenticationMethod;
  sendAuthenticationCodeStatus: SendAuthenticationCodeStatus;
  resendAuthenticationCodeStatus: ResendAuthenticationCodeStatus;
  verifyAuthenticationCodeStatus: VerifyAuthenticationCodeStatus;
  verifySsnStatus: VerifySsnStatus;
  savingResumableStatus: SavingResumableStatus;
}

export function initAuthenticationState(
  overrides: Partial<AuthenticationState> = {}
): AuthenticationState {
  return {
    authenticationId: undefined,
    consent: false,
    authenticationCode: undefined,
    ssn: undefined,
    authenticationMethod: AuthenticationMethod.SMS,
    sendAuthenticationCodeStatus: { count: 0, status: Status.Init },
    resendAuthenticationCodeStatus: { count: 0, status: Status.Init },
    verifyAuthenticationCodeStatus: {
      count: 0,
      isResumable: false,
      status: Status.Init,
      authenticationToken: {
        expiresAt: undefined,
        locked: false,
        verifyAt: undefined,
        token: '',
        refreshToken: '',
      },
    },
    verifySsnStatus: { numberOfFailures: 0, count: 0, status: Status.Init },
    savingResumableStatus: { count: 0, status: Status.Init },
    ...overrides,
  };
}

export const resetAuthenticationCodeHandler = (
  state: AuthenticationState
): AuthenticationState => ({
  ...state,
  authenticationCode: '',
  ssn: '',
});

export const sendAuthenticationCodeHandler = (
  state: AuthenticationState,
  action
): AuthenticationState => ({
  ...state,
  authenticationId: action.sendAuthenticationPayload.authId,
  consent: action.sendAuthenticationPayload.consent,
  sendAuthenticationCodeStatus: {
    ...state.sendAuthenticationCodeStatus,
    status: Status.Sending,
    count: state.sendAuthenticationCodeStatus.count + 1,
  },
});

export const sendAuthenticationCodeErrorHandler = (
  state: AuthenticationState,
  action
): AuthenticationState => ({
  ...state,
  sendAuthenticationCodeStatus: {
    ...state.sendAuthenticationCodeStatus,
    status: Status.Error,
    error: action.error,
  },
});

export const resendAuthenticationCodeHandler = (
  state: AuthenticationState
): AuthenticationState => ({
  ...state,
  resendAuthenticationCodeStatus: {
    ...state.resendAuthenticationCodeStatus,
    status: Status.Sending,
    count: state.resendAuthenticationCodeStatus.count + 1,
  },
});

export const resendAuthenticationCodeSuccessHandler = (
  state: AuthenticationState
): AuthenticationState => ({
  ...state,
  resendAuthenticationCodeStatus: {
    ...state.resendAuthenticationCodeStatus,
    status: Status.Success,
  },
});

export const resendAuthenticationCodeErrorHandler = (
  state: AuthenticationState,
  action
): AuthenticationState => ({
  ...state,
  resendAuthenticationCodeStatus: {
    ...state.resendAuthenticationCodeStatus,
    status: Status.Error,
    error: action.error,
  },
});

export const verifyAuthenticationCodeSplitterHandler = (
  state: AuthenticationState,
  action
): AuthenticationState => ({
  ...state,
  authenticationCode: action.payload.authenticationCode,
  authenticationId:
    action.verifyAuthenticationCodeSplitterPayload.authenticatedId,
  verifyAuthenticationCodeStatus: {
    ...state.verifyAuthenticationCodeStatus,
    status: Status.Sending,
    count: state.verifyAuthenticationCodeStatus.count + 1,
  },
});

export const verifyAuthenticationCodeAggregatorSuccessHandler = (
  state: AuthenticationState,
  action
): AuthenticationState => ({
  ...state,
  authenticationCode: action.payload.authenticationCode,
  authenticationId:
    action.verifyAuthenticationCodeSplitterPayload.authenticatedId,
  verifyAuthenticationCodeStatus: {
    ...state.verifyAuthenticationCodeStatus,
    authenticationToken:
      action.verifyAuthenticationCodeSplitterPayload.authenticationToken,
    isResumable: action.verifyAuthenticationCodeSplitterPayload.isAvailable,
  },
});

export const reducer = createReducer(
  initAuthenticationState(),
  on(
    fromActions.resetAuthenticationCodeAndSsnWhenDoneMoveAwayFromVerifyCodePage,
    resetAuthenticationCodeHandler
  ),
  on(fromActions.sendAuthenticationCode, sendAuthenticationCodeHandler),
  on(
    fromActions.sendAuthenticationCodeError,
    sendAuthenticationCodeErrorHandler
  ),
  on(fromActions.resendAuthenticationCode, resendAuthenticationCodeHandler),
  on(
    fromActions.resendAuthenticationCodeSuccess,
    resendAuthenticationCodeSuccessHandler
  ),
  on(
    fromActions.resendAuthenticationCodeError,
    resendAuthenticationCodeErrorHandler
  ),
  on(
    fromActions.verifyAuthenticationCodeSplitter,
    verifyAuthenticationCodeSplitterHandler
  ),
  on(
    fromActions.verifyAuthenticationCodeAggregatorSuccess,
    verifyAuthenticationCodeAggregatorSuccessHandler
  )
);
