import { UnifiedApps } from '@ua/shared/state';
import * as fromFlowRouteActions from './flow.route.actions';

export function nextRoute(
  routePayload: fromFlowRouteActions.RoutePayload
): string {
  if (routePayload.from.includes(UnifiedApps.apply.children.getStarted.route)) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.verifyCode.route}`;
  }

  if (routePayload.from.includes(UnifiedApps.apply.children.verifyCode.route)) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.basicInfo.route}`;
  }

  if (routePayload.from.includes(UnifiedApps.apply.children.basicInfo.route)) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.contactInfo.route}`;
  }

  if (
    routePayload.from.includes(UnifiedApps.apply.children.contactInfo.route)
  ) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.incomeInfo.route}`;
  }

  if (routePayload.from.includes(UnifiedApps.apply.children.incomeInfo.route)) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.bankInfo.route}`;
  }

  if (routePayload.from.includes(UnifiedApps.apply.children.bankInfo.route)) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.submitApplication.route}`;
  }

  if (
    routePayload.from.includes(
      UnifiedApps.apply.children.submitApplication.route
    )
  ) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.submittedApplication.route}`;
  }

  if (
    routePayload.from.includes(
      UnifiedApps.apply.children.submittedApplication.route
    )
  ) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.applicationPending.route}`;
  }

  return routePayload.from;
}

export function previousRoute(
  routePayload: fromFlowRouteActions.RoutePayload
): string {
  if (routePayload.from.includes(UnifiedApps.apply.children.getStarted.route)) {
    return 'terms-sheet';
  }

  if (routePayload.from.includes(UnifiedApps.apply.children.verifyCode.route)) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.getStarted.route}`;
  }

  if (routePayload.from.includes(UnifiedApps.apply.children.basicInfo.route)) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.verifyCode.route}`;
  }

  if (
    routePayload.from.includes(UnifiedApps.apply.children.contactInfo.route)
  ) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.basicInfo.route}`;
  }

  if (routePayload.from.includes(UnifiedApps.apply.children.incomeInfo.route)) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.contactInfo.route}`;
  }

  if (routePayload.from.includes(UnifiedApps.apply.children.bankInfo.route)) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.incomeInfo.route}`;
  }

  if (
    routePayload.from.includes(
      UnifiedApps.apply.children.submitApplication.route
    )
  ) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.bankInfo.route}`;
  }

  if (
    routePayload.from.includes(
      UnifiedApps.apply.children.submittedApplication.route
    )
  ) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.submitApplication.route}`;
  }

  if (
    routePayload.from.includes(
      UnifiedApps.apply.children.applicationPending.route
    )
  ) {
    return `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.submittedApplication.route}`;
  }

  return routePayload.from;
}
