import { Application, AuthenticationMethod } from '@ua/shared/data-access';
import { HttpErrorResponse } from '@angular/common/http';
import * as fromActions from './application.actions';
import { createReducer, on } from '@ngrx/store';

export interface ApplicationState {
  application: Application;
  isLoaded: boolean;
  error?: HttpErrorResponse;
}

export function initApplication(
  overrides: Partial<ApplicationState> = {}
): ApplicationState {
  return {
    application: {
      firstName: '',
      lastName: '',
      dateOfBirth: '',
      defaultContactMechanism: AuthenticationMethod.SMS,
      driverLicense: '',
      driverLicenseState: '',
      sessionId: undefined,
      emailAddress: '',
      marketingOptIn: false,
      monthlyGrossIncome: 0,
      bankAccountNumber: '',
      bankRoutingNumber: '',
      phoneNumber: '',
      zip: '',
      ssn: '',
      city: '',
      state: '',
      street1: '',
      street2: '',
    },
    isLoaded: false,
    ...overrides,
  };
}

export const onSuccessApplicationHandler = (
  state: ApplicationState,
  action
): ApplicationState => ({
  ...state,
  application: {
    ...state.application,
    ...action.application,
  },
  isLoaded: true,
});

export const onErrorApplicationHandler = (
  state: ApplicationState,
  action
): ApplicationState => ({
  ...state,
  error: action.error,
  isLoaded: true,
});

export const reducer = createReducer(
  initApplication(),
  on(
    fromActions.createApplicationBySessionIdSuccess,
    fromActions.getApplicationBySessionIdSuccess,
    fromActions.updateApplicationSuccess,
    onSuccessApplicationHandler
  ),
  on(
    fromActions.createApplicationBySessionIdError,
    fromActions.getApplicationBySessionIdError,
    fromActions.updateApplicationError,
    onErrorApplicationHandler
  )
);
