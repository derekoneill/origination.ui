import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  AuthenticationService,
  getFormattedContactInfo,
  InitSessionRequest,
  LoginStatusReason,
  OtpAuthenticateRequest,
  OtpSendRequest,
} from '@ua/shared/data-access';

import { forkJoin, Observable, of, race, throwError } from 'rxjs';
import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import {
  catchError,
  filter,
  first,
  map,
  mergeMap,
  switchMap,
} from 'rxjs/operators';
import { Router } from '@angular/router';

import * as fromFlowRoutes from '../flow-routes';
import * as fromFeatureFlags from '../feature-flags';
import * as fromSession from '../session';
import * as fromResume from '../resume';
import * as fromApplyActions from '../apply.actions';
import * as fromActions from './authenticate.actions';
import * as fromReducers from './authentication.reducers';

@Injectable()
export class AuthenticationEffects {
  sendAuthenticationCode$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.sendAuthenticationCode),
      mergeMap((action) =>
        this.sendAuthenticationCode(action.sendAuthenticationPayload).pipe(
          map((_) =>
            fromActions.sendAuthenticationCodeSuccess(action.correlationId)
          ),
          catchError((error) =>
            of(
              fromActions.sendAuthenticationCodeError(
                error,
                action.correlationId
              )
            )
          )
        )
      )
    )
  );

  resendAuthenticationCode$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.resendAuthenticationCode),
      mergeMap((action) =>
        this.sendAuthenticationCode(action.sendAuthenticationPayload).pipe(
          map((_) =>
            fromActions.resendAuthenticationCodeSuccess(action.correlationId)
          ),
          catchError((error) =>
            of(
              fromActions.resendAuthenticationCodeError(
                error,
                action.correlationId
              )
            )
          )
        )
      )
    )
  );

  sendAuthenticationCodeSuccess$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.sendAuthenticationCodeSuccess),
      map((action) =>
        fromFlowRoutes.changeApplyFlowRoute(
          {
            from: this.router.url,
            direction: fromFlowRoutes.RouteDirection.Next,
            params: {},
          },
          action.correlationId
        )
      )
    )
  );

  verifyAuthenticationCode$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.verifyAuthenticationCode),
      mergeMap((action) => {
        const verifyAuthCodeRequest = <OtpAuthenticateRequest>{
          authenticationId:
            action.verifyAuthenticationCodeSplitterPayload.authenticationId,
          code:
            action.verifyAuthenticationCodeSplitterPayload.authenticationCode,
          sessionId: action.verifyAuthenticationCodeSplitterPayload.sessionId,
          authenticationMethod: getFormattedContactInfo(
            action.verifyAuthenticationCodeSplitterPayload.authenticationId
          ).contactMethod,
        };
        const verifyAt = new Date();
        return this.authenticationService
          .authenticateOtpCode(verifyAuthCodeRequest)
          .pipe(
            map((res) =>
              fromActions.verifyAuthenticationCodeSuccess(
                <fromReducers.VerifiedAuthenticationCode>{
                  verifyAt: verifyAt,
                  token: res.token,
                  refreshToken: res.refreshToken,
                  locked: res.locked,
                  expiresAt: res.expDate,
                },
                action.correlationId
              )
            ),
            catchError((error) =>
              of(
                fromActions.verifyAuthenticationCodeError(
                  error,
                  action.correlationId
                )
              )
            )
          );
      })
    )
  );

  verifyAuthCodeSplitter$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.verifyAuthenticationCodeSplitter),
      switchMap((action) => {
        return [
          fromActions.verifyAuthenticationCode(
            {
              ...action.verifyAuthenticationCodeSplitterPayload,
            },
            action.correlationId
          ),
          fromFeatureFlags.getFeatureFlags(
            {
              storeId: action.verifyAuthenticationCodeSplitterPayload.storeId,
              ldUserKey:
                action.verifyAuthenticationCodeSplitterPayload.authenticationId,
            },
            action.correlationId
          ),
          fromApplyActions.checkForLoginResume(
            action.verifyAuthenticationCodeSplitterPayload,
            action.correlationId
          ),
        ];
      })
    )
  );

  verifyAuthCodeAggregator$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.verifyAuthenticationCodeSplitter),
      switchMap((action) => {
        const verify$ = this.actions$.pipe(
          ofType(fromActions.verifyAuthenticationCodeSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const feature$ = this.actions$.pipe(
          ofType(fromFeatureFlags.getFeatureFlagsSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const check$ = this.actions$.pipe(
          ofType(fromApplyActions.checkForLoginResumeSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const failed$ = this.actions$.pipe(
          ofType(
            fromFeatureFlags.getFeatureFlagsError,
            fromActions.verifyAuthenticationCodeError,
            fromApplyActions.checkForLoginResumeError
          ),
          filter((a) => a.correlationId === action.correlationId),
          first(),
          switchMap((error) => throwError(error))
        );

        return race(
          forkJoin({
            verify: verify$,
            feature: feature$,
            check: check$,
          }),
          failed$
        ).pipe(
          map((success) =>
            fromActions.verifyAuthenticationCodeAggregatorSuccess(
              <fromActions.VerifyAuthenticationCodeAggregatorSuccessPayload>{
                ...action.verifyAuthenticationCodeSplitterPayload,
                authenticationToken: success.verify.verifiedAuthenticationCode,
                checkResumable: success.check.checkResumeAvailableResponse,
              },
              action.correlationId
            )
          ),
          // use this to report verify authentication code error to reflect the series of things happens when the user
          // enter the authentication code.  We handle this in the reducer.
          catchError((error) =>
            of(
              fromActions.verifyAuthenticationCodeAggregatorError(
                error,
                action.correlationId
              )
            )
          )
        );
      })
    )
  );

  /**
   * decide to start new application or resume the previous application.
   * if the application is resumable we will ask the user for the ssn with
   * the authencation id to get the resumable state.
   */
  verifyAuthCodeAggregatorSuccess$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.verifyAuthenticationCodeAggregatorSuccess),
      switchMap((action) => {
        if (
          !action.verifyAuthenticationCodeAggregatorSuccessPayload
            .checkResumable.isAvailable
        ) {
          // resume not available, let start new session and new application.
          return [
            fromSession.initSession(
              <InitSessionRequest>{
                ...action.verifyAuthenticationCodeAggregatorSuccessPayload,
              },
              action.correlationId
            ),
          ];
        } else {
          // resume available, let continue to verify the ssn to allow the user to resume the application.
          return [];
        }
      })
    )
  );

  verifySsnSplitter$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.verifySsnSplitter),
      map((action) =>
        fromResume.getResumableStateByAuthenticationId(
          action.getResumableStateByAuthenticationIdRequest,
          action.correlationId
        )
      )
    )
  );

  /**
   * this occurs when the application is resumable and we ask the user to enter his/her ssn(last four)
   * with his authentication id (contact id) to get the application's data and resumable state to resume
   * the application.
   */
  verifySsnAggregator$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromResume.getResumableStateByAuthenticationId),
      switchMap((action) => {
        const success$ = this.actions$.pipe(
          ofType(fromResume.getResumableStateByAuthenticationIdSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const failed$ = this.actions$.pipe(
          ofType(fromResume.getResumableStateByAuthenticationIdError),
          filter((a) => a.correlationId === action.correlationId),
          first(),
          switchMap((error) => throwError(error))
        );

        return race(success$, failed$).pipe(
          map((success) =>
            fromActions.verifySsnAggregatorSuccess(
              success.resumeStateResponse,
              action.correlationId
            )
          ),
          catchError((error) =>
            of(
              fromActions.verifySsnAggregatorError(error, action.correlationId)
            )
          )
        );
      })
    )
  );

  verifySsnAggregatorSuccess$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.verifySsnAggregatorSuccess),
      switchMap((action) => {
        const actions: Action[] = [];
        // after verify that the previous application can be resumed by the login and his/her ssn, now
        // we can continue to resume the application.
        if (
          action.resumeStateResponse.resumable &&
          action.resumeStateResponse.loginStatusReason !==
            LoginStatusReason.ResumeFailedInvalidLogin &&
          action.resumeStateResponse.loginStatusReason !==
            LoginStatusReason.ResumeFailedApplicationDenied &&
          action.resumeStateResponse.loginStatusReason !==
            LoginStatusReason.ResumeFailedSessionNotFound
        ) {
          actions.push(
            fromApplyActions.resumeApplicationByLoginSplitter(
              action.resumeStateResponse,
              action.correlationId
            )
          );
        } else {
          // give the user the times to get it right.
          actions.push(
            fromActions.verifySsnAggregatorError(
              <HttpErrorResponse>{
                status: 400,
                statusText: action.resumeStateResponse.loginStatusReason.toString(),
              },
              action.correlationId
            )
          );
        }
        return actions;
      })
    )
  );

  private sendAuthenticationCode(
    sendAuthenticationPayload: fromActions.SendAuthenticationPayload
  ): Observable<void> {
    const { contactId, contactMethod } = getFormattedContactInfo(
      sendAuthenticationPayload.authenticationId
    );
    const otpRequest: OtpSendRequest = {
      authenticationId: contactId,
      authenticationMethod: contactMethod,
      sessionId: sendAuthenticationPayload.sessionId,
    };
    return this.authenticationService.sendOtpCode(otpRequest);
  }

  constructor(
    private actions$: Actions,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}
}
