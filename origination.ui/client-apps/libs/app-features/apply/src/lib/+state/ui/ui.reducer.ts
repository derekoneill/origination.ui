import * as fromUiActions from './ui.actions';
import { createReducer, on } from '@ngrx/store';

export enum ViewPortType {
  XS = 'width > 300px && width < 600px',
  SM = 'width > 600px && width < 1024px',
  MD = 'width > 1024px && width < 1280px',
  LG = 'width > 1280px',
}

export enum ResultType {
  ApprovedItemsInCartWithinThreshold = 'Approved Items In Cart Within Threshold',
  ApprovedNoItemsInCart = 'Approved No Items In Cart',
  ApprovedItemsInCartMoreThanApproval = 'Approved Items In Cart More Than Approval',
  ApprovedApplyOnly = 'Approved Apply Only',
}

export enum WindowType {
  IFRAME = 'IFRAME',
  WINDOW = 'WINDOW',
  REDIRECT = 'REDIRECT',
}

export interface UiState {
  windowType: WindowType;
  ecommercePlatform: string;
  checkoutUrl: string;
  completeUrl: string;
  returnToCheckoutUrl: string;
  applyOnly: boolean;
  returnToMerchantUrl: string;
  compositeId: string;
  resultType: ResultType;
  viewPort: ViewPortType;
}

export function initUi(overrides?: Partial<UiState>): UiState {
  return {
    windowType: WindowType.WINDOW,
    ecommercePlatform: undefined,
    checkoutUrl: undefined,
    completeUrl: undefined,
    returnToCheckoutUrl: undefined,
    applyOnly: false,
    returnToMerchantUrl: undefined,
    compositeId: undefined,
    viewPort: ViewPortType.XS,
    resultType: ResultType.ApprovedNoItemsInCart,
    ...overrides,
  };
}

export const setUiHandler = (state: UiState, action): UiState => ({
  ...state,
  ...action.uiStates,
});

export const reducer = createReducer(
  initUi(),
  on(fromUiActions.setUiState, setUiHandler)
);
