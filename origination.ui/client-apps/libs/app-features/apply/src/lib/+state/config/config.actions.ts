import { createAction, union } from '@ngrx/store';
import { IConfig } from '@ua/shared/data-access';
import { generateId } from '@ua/shared/utilities';

const ConfigPrefix = '[Config]';

export const setConfig = createAction(
  `${ConfigPrefix} Set Configs`,
  (config: IConfig, correlationId: string = generateId()) => ({
    config,
    correlationId,
  })
);

const allActions = union({
  setConfig,
});

export type ConfigActions = typeof allActions;
