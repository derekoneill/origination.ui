import { HttpErrorResponse } from '@angular/common/http';
import * as fromFeatureFlags from '.';

describe('feature flags state reducer', () => {
  let featureFlags: fromFeatureFlags.FeatureFlagsState;
  beforeEach(() => {
    featureFlags = fromFeatureFlags.initFeatureFlags(<
      fromFeatureFlags.FeatureFlagsState
    >{
      isShortApp: true,
      skipCreditCard: true,
      monthlyPayFreqDisabled: true,
    });
  });

  it('should handle the GetFeatureFlags action correctly', () => {
    featureFlags = fromFeatureFlags.reducer(
      featureFlags,
      fromFeatureFlags.getFeatureFlags({
        storeId: 55555,
        ldUserKey: 'eric.nguyen@progleasing.com',
      })
    );
    expect(featureFlags.isLoaded).toEqual(false);
    expect(featureFlags.monthlyPayFreqDisabled).toEqual(true);
    expect(featureFlags.monthlyPayFreqDisabled).toEqual(true);
    expect(featureFlags.isShortApp).toEqual(true);
  });

  it('should handle the GetFeatureFlagsSuccess action correctly', () => {
    featureFlags = fromFeatureFlags.reducer(
      featureFlags,
      fromFeatureFlags.getFeatureFlagsSuccess({
        isShortApp: true,
        skipCreditCard: false,
        monthlyPayFreqDisabled: false,
      })
    );
    expect(featureFlags.isLoaded).toEqual(true);
    expect(featureFlags.isShortApp).toEqual(true);
    expect(featureFlags.skipCreditCard).toEqual(false);
    expect(featureFlags.monthlyPayFreqDisabled).toEqual(false);
  });

  it('should handle the GetFeatureFlagsError correctly', () => {
    featureFlags = fromFeatureFlags.reducer(
      featureFlags,
      fromFeatureFlags.getFeatureFlagsError(<HttpErrorResponse>{
        status: 400,
        statusText: 'ldUserKey is required',
      })
    );
    expect(featureFlags.error).toEqual(<HttpErrorResponse>{
      status: 400,
      statusText: 'ldUserKey is required',
    });
  });
});
