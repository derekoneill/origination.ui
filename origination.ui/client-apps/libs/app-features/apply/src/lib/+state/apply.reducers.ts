import { AuthenticationMethod, IConfig } from '@ua/shared/data-access';
import { ActionReducerMap, createReducer, on } from '@ngrx/store';
import * as fromUi from './ui';
import * as fromSession from './session';
import * as fromResume from './resume';
import * as fromApplication from './application';
import * as fromApplicationResult from './application-result';
import * as fromOrder from './order';
import * as fromPayment from './payment';
import * as fromFeatureFlags from './feature-flags';
import * as fromFlowRoute from './flow-routes';
import * as fromConfig from './config';
import * as fromAnalytic from './analytic';
import * as fromSharedState from '@ua/shared/state';
import * as fromAuthentication from './authentication';
import * as fromActions from './apply.actions';

export interface ApplyState extends fromSharedState.State {
  authenticationInformation: fromAuthentication.AuthenticationState;
  ui: fromUi.UiState;
  session: fromSession.SessionState;
  resumable: fromResume.ResumeState;
  application: fromApplication.ApplicationState;
  applicationResult: fromApplicationResult.ApplicationResultState;
  order: fromOrder.OrderState;
  payment: fromPayment.PaymentState;
  featureFlags: fromFeatureFlags.FeatureFlagsState;
  config: IConfig;
  currentRoute: fromFlowRoute.FlowRouteState;
  analytics: Array<{ [key: string]: any }>;
}

export const allReducers: ActionReducerMap<ApplyState> = {
  authenticationInformation: fromAuthentication.reducer,
  ui: fromUi.reducer,
  session: fromSession.reducer,
  resumable: fromResume.reducer,
  application: fromApplication.reducer,
  applicationResult: fromApplicationResult.reducer,
  order: fromOrder.reducer,
  payment: fromPayment.reducer,
  featureFlags: fromFeatureFlags.reducer,
  config: fromConfig.reducer,
  currentRoute: fromFlowRoute.reducer,
  analytics: fromAnalytic.reducer,
};

export const updateApplicationSuccessHandler = (
  state: ApplyState,
  action
): ApplyState => ({
  ...state,
  authenticationInformation: {
    ...state.authenticationInformation,
    authenticationId:
      action.application.defaultContactMechanism === AuthenticationMethod.SMS
        ? action.application.phoneNumber
        : action.application.emailAddress,
    authenticationMethod: action.application.defaultContactMechanism,
  },
  application: {
    ...state.application,
    ...action.application,
  },
});

export const startApplicationOrResumeApplicationByLoginAggregatorSuccessHandler = (
  state: ApplyState
): ApplyState => ({
  ...state,
  authenticationInformation: {
    ...state.authenticationInformation,
    ssn: '',
    authenticationCode: '',
    verifyAuthenticationCodeStatus: {
      ...state.authenticationInformation.verifyAuthenticationCodeStatus,
      status: fromAuthentication.Status.Success,
    },
  },
});

export const saveApplicationResumableStateSplitterHandler = (
  state: ApplyState
): ApplyState => ({
  ...state,
  authenticationInformation: {
    ...state.authenticationInformation,
    savingResumableStatus: {
      ...state.authenticationInformation.savingResumableStatus,
      count: state.authenticationInformation.savingResumableStatus.count + 1,
      status: fromAuthentication.Status.Sending,
    },
  },
});

export const saveApplicationResumableStateAggregatorSuccessHandler = (
  state: ApplyState
): ApplyState => ({
  ...state,
  authenticationInformation: {
    ...state.authenticationInformation,
    savingResumableStatus: {
      ...state.authenticationInformation.savingResumableStatus,
      status: fromAuthentication.Status.Success,
    },
  },
});

export const saveApplicationResumableStateAggregatorErrorHandler = (
  state: ApplyState,
  action
): ApplyState => ({
  ...state,
  authenticationInformation: {
    ...state.authenticationInformation,
    savingResumableStatus: {
      ...state.authenticationInformation.savingResumableStatus,
      status: fromAuthentication.Status.Error,
      error: action.error,
    },
  },
});

// export const reducer = createReducer(
//   <ApplyState>{
//     application: fromApplication.initApplication()
//   },
//   on(fromApplication.updateApplicationSuccess, updateApplicationSuccessHandler),
//   on(
//       fromActions.startNewSessionAndApplicationAggregatorSuccess,
//       fromActions.resumeApplicationByLoginAggregatorSuccess,
//       startApplicationOrResumeApplicationByLoginAggregatorSuccessHandler,
//     ),
//   on(fromActions.saveApplicationResumableStateSplitter, saveApplicationResumableStateSplitterHandler),
//   on(fromActions.saveApplicationResumableStateAggregatorSuccess, saveApplicationResumableStateAggregatorSuccessHandler),
//   on(fromActions.saveApplicationResumableStateAggregatorError, saveApplicationResumableStateAggregatorErrorHandler)
// );

export const reducer = allReducers;
