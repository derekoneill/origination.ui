import { createAction, union } from '@ngrx/store';
import { generateId } from '@ua/shared/utilities';
import {
  Application,
  BySessionId,
  CheckResumeAvailableResponse,
  ResumableState,
  ResumeStateResponse,
  Session,
  ValidateZipStateRequest,
} from '@ua/shared/data-access';
import { HttpErrorResponse } from '@angular/common/http';
import * as fromSharedState from '@ua/shared/state';
import * as fromFlowRoutes from './flow-routes';
import * as fromAuthentications from './authentication';

export interface SaveApplicationResumableStateSplitterPayload {
  resumableState: ResumableState;
  application: Application;
  nextRoute: fromFlowRoutes.RoutePayload;
}

const ApplyPrefix = '[Apply]';
export const walkBackInApplyFlow = createAction(
  `${ApplyPrefix} Walk Back In Apply Flow`,
  (
    navigateRoute: fromSharedState.NavigateRoutePayload,
    correlationId: string = generateId()
  ) => ({
    navigateRoute,
    correlationId,
  })
);

export const saveNewSessionInCookieForResumable = createAction(
  `${ApplyPrefix} Save New Session In Cookie For Resumable`,
  (bySessionId: BySessionId, correlationId: string = generateId()) => ({
    bySessionId,
    correlationId,
  })
);

export const validateZipAndState = createAction(
  `${ApplyPrefix} Validate Zip And State`,
  (
    validateZipStateRequest: ValidateZipStateRequest,
    correlationId: string = generateId()
  ) => ({
    validateZipStateRequest,
    correlationId,
  })
);

export const validateZipAndStateSuccess = createAction(
  `${ApplyPrefix} Validate Zip And State Success`,
  (success: boolean, correlationId: string = generateId()) => ({
    success,
    correlationId,
  })
);

export const validateZipAndStateError = createAction(
  `${ApplyPrefix} Validate Zip And State Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const checkForLoginResume = createAction(
  `${ApplyPrefix} Check For Login Resume`,
  (
    verifyAuthCodeSplitterPayload: fromAuthentications.VerifyAuthenticationCodeSplitterPayload,
    correlationId: string = generateId()
  ) => ({
    verifyAuthCodeSplitterPayload,
    correlationId,
  })
);

export const checkForLoginResumeSuccess = createAction(
  `${ApplyPrefix} Check For Login Resume Success`,
  (
    checkResumeAvailableResponse: CheckResumeAvailableResponse,
    correlationId: string = generateId()
  ) => ({
    checkResumeAvailableResponse,
    correlationId,
  })
);

export const checkForLoginResumeError = createAction(
  `${ApplyPrefix} Check For Login Resume Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const resumeApplicationByLoginSplitter = createAction(
  `${ApplyPrefix} Resume Application By Login Splitter`,
  (
    resumeStateResponse: ResumeStateResponse,
    correlationId: string = generateId()
  ) => ({
    resumeStateResponse,
    correlationId,
  })
);

export const resumeApplicationByLoginAggregatorSuccess = createAction(
  `${ApplyPrefix} Resume Application By Login Aggregator Success`,
  (
    resumeStateResponse: ResumeStateResponse,
    correlationId: string = generateId()
  ) => ({
    resumeStateResponse,
    correlationId,
  })
);

export const resumeApplicationByLoginAggregatorError = createAction(
  `${ApplyPrefix} Resume Application By Login Aggregator Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const startNewSessionAndApplicationSplitter = createAction(
  `${ApplyPrefix} Start New Session And Application Splitter`,
  (
    verifyAuthCodeSplitterPayload: fromAuthentications.VerifyAuthenticationCodeSplitterPayload,
    correlationId: string = generateId()
  ) => ({
    verifyAuthCodeSplitterPayload,
    correlationId,
  })
);

export const startNewSessionAndApplicationAggregatorSuccess = createAction(
  `${ApplyPrefix} Start New Session And Application Aggregator Success`,
  (bySessionId: BySessionId, correlationId: string = generateId()) => ({
    bySessionId,
    correlationId,
  })
);

export const startNewSessionAndApplicationAggregatorError = createAction(
  `${ApplyPrefix} Start New Session And Application Aggregator Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const saveApplicationResumableStateSplitter = createAction(
  `${ApplyPrefix} Save Application Resumable State Splitter`,
  (
    saveApplicationResumableStateSplitterPayload: SaveApplicationResumableStateSplitterPayload,
    correlationId: string = generateId()
  ) => ({
    saveApplicationResumableStateSplitterPayload,
    correlationId,
  })
);

export const saveApplicationResumableStateAggregatorSuccess = createAction(
  `${ApplyPrefix} Save Application Resumable State Aggregator Success`,
  (
    routePayload: fromFlowRoutes.RoutePayload,
    correlationId: string = generateId()
  ) => ({
    routePayload,
    correlationId,
  })
);

export const saveApplicationResumableStateAggregatorError = createAction(
  `${ApplyPrefix} Save Application Resumable State Aggregator Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export interface ResumeApplicationBySessionPayload {
  application: Application;
  session: Session;
}

export const resumeApplicationBySessionSplitter = createAction(
  `${ApplyPrefix} Resume Application By Session Splitter`,
  (
    resumeApplicationBySessionPayload: ResumeApplicationBySessionPayload,
    correlationId: string = generateId()
  ) => ({
    resumeApplicationBySessionPayload,
    correlationId,
  })
);

export const resumeApplicationBySessionAggregatorSuccess = createAction(
  `${ApplyPrefix} Resume Application By Session Aggregator Success`,
  (
    resumeStateResponse: ResumeStateResponse,
    correlationId: string = generateId()
  ) => ({
    resumeStateResponse,
    correlationId,
  })
);

export const resumeApplicationBySessionAggregatorError = createAction(
  `${ApplyPrefix} Resume Application By Session Aggregator Error`,
  (bySessionId: BySessionId, correlationId: string = generateId()) => ({
    bySessionId,
    correlationId,
  })
);

const allActions = union({
  walkBackInApplyFlow,
  saveNewSessionInCookieForResumable,
  validateZipAndState,
  validateZipAndStateSuccess,
  validateZipAndStateError,
  checkForLoginResume,
  checkForLoginResumeSuccess,
  checkForLoginResumeError,
  resumeApplicationByLoginSplitter,
  resumeApplicationByLoginAggregatorSuccess,
  resumeApplicationByLoginAggregatorError,
  startNewSessionAndApplicationSplitter,
  startNewSessionAndApplicationAggregatorSuccess,
  startNewSessionAndApplicationAggregatorError,
  saveApplicationResumableStateSplitter,
  saveApplicationResumableStateAggregatorSuccess,
  saveApplicationResumableStateAggregatorError,
  resumeApplicationBySessionSplitter,
  resumeApplicationBySessionAggregatorSuccess,
  resumeApplicationBySessionAggregatorError,
});

export const ApplyActions = typeof allActions;
