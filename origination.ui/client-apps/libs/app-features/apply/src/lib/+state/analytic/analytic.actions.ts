import { createAction, union } from '@ngrx/store';
import { Session } from '@ua/shared/data-access';

export enum Event {
  EVENT_TRACKER = 'eventTracker',
  VIRTUAL_PAGEVIEW = 'virtualPageview',
}

export enum EventCategory {
  APPLICATION_ENGAGEMENT = 'Application Engagement',
  APPLICATION_START = 'Application Start',
  APPLICATION_RESUME = 'Application Resume',
  APPLICATION_SUBMISSION = 'Application Submission',
  APPLICATION_EXIT = 'Application Exit',
  PAYMENT_ESTIMATOR = 'Payment Estimator',
  VALUE_PROP_SCREEN = 'Value Prop Screen',
  OTP = 'OTP',
  CHECK_OUT = 'Checkout',
  NAVIGATION = 'Navigation',
  ALERT = 'Alert',
  ERRORS = 'Errors',
}

export enum HitType {
  PAGE_TRACKING_HIT = 'pageview',
  EVENT_TRACKING_HIT = 'event',
}

export enum ButtonKind {
  BUTTON = 'Button',
  LINK = 'Link',
}

export interface AnalyticPayload {
  event: Event;
  eventCategory: EventCategory;
  hitType: HitType;
  eventValue: number;
  eventNonInteraction: number;
  eventLabel: string;
  eventAction: string;
  [key: string]: string | number | boolean;
}

const basedAnalyticPayload = {
  event: Event.EVENT_TRACKER,
  eventCategory: EventCategory.APPLICATION_START,
  hitType: HitType.EVENT_TRACKING_HIT,
  eventValue: 0,
  eventNonInteraction: 0,
  eventLabel: '',
  eventAction: '',
};

const ActionType = '[Analytic]';
export const pageViewEvent = createAction(
  `${ActionType} Page View Event`,
  (
    session: Session,
    isShortApp: boolean,
    location: string,
    title: string,
    sectionTitle: string
  ) => {
    let page = location;
    if (location.indexOf('#') >= 0) {
      page = location.split('#')[1];
    }
    if (page.indexOf('?') >= 0) {
      page = page.split('?')[0];
    }
    const flow = isShortApp ? 'slimapp' : 'original';
    return {
      ...basedAnalyticPayload,
      event: Event.VIRTUAL_PAGEVIEW,
      title: title,
      sectionId: sectionTitle,
      page: page,
      location: location,
      storeId: session.storeId,
      customerId: session.sessionId,
      flowType: flow,
      flowName: flow,
      cartId: session.originalSessionId, // TODO: get this added in GTM
      version: 'ecom',
      hitType: HitType.PAGE_TRACKING_HIT,
      merchantName: session.storeName,
      lenderName: 'Progressive',
      lenderType: 'Primary',
      textToApplyEnabled: 'false',
      inStoreFlag: 'false',
      paymentEstEnabled: 'true',
      bankLookupEnabled: 'false',
      rldEnabled: 'false',
      localizationEnabled: 'false',
      storeLocatorEnabled: 'false',
    };
  }
);

export const applicationStartEvent = createAction(
  `${ActionType} Application Start Event`,
  (source: string) => {
    return {
      ...basedAnalyticPayload,
      eventLabel: source,
      eventAction: 'Apply Now Button Click',
    };
  }
);

export const applicationStartSuccessEvent = createAction(
  `${ActionType} Application Start Event Success`,
  () => {
    return {
      ...basedAnalyticPayload,
      eventAction: 'Start Application Success',
      applicationStarted: 'true',
      applicationsStarted: 1,
    };
  }
);

export const applicationStartErrorEvent = createAction(
  `${ActionType} Application Start Event Error`,
  (error: string) => {
    return {
      ...basedAnalyticPayload,
      eventAction: 'Start Application Error',
      eventLabel: error,
      errors: 1,
    };
  }
);

export const buttonClickEvent = createAction(
  `${ActionType} Button Click Event`,
  (
    name: string,
    kind: ButtonKind,
    action?: string,
    category?: EventCategory
  ) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.APPLICATION_ENGAGEMENT,
      eventAction: action || `${kind} Click`,
      eventLabel: name,
      paymentEstimatorUsed:
        category === EventCategory.PAYMENT_ESTIMATOR ? 'Yes' : '',
    };
  }
);

export const modalOpenEvent = createAction(
  `${ActionType} Modal Open Event`,
  (modalTitle: string, modalWindowName: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.APPLICATION_ENGAGEMENT,
      eventAction: 'Modal Window',
      eventLabel: modalTitle,
      eventValue: 0,
      eventNonInteraction: 0,
      modalWindowName: modalWindowName,
    };
  }
);

export const formFillEvent = createAction(
  `${ActionType} Form Fill Event`,
  (fieldName: string, fieldValue: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.APPLICATION_ENGAGEMENT,
      eventAction: 'Form Fill',
      eventLabel: fieldValue,
      formFieldName: fieldName,
    };
  }
);

export const formFieldError = createAction(
  `${ActionType} Form Fill Error Event`,
  (fieldName: string, errorMessage: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.APPLICATION_ENGAGEMENT,
      eventAction: 'Form Fill Error',
      eventLabel: errorMessage,
      formFieldName: fieldName,
      errors: 1,
    };
  }
);

export const formElementInteractionEvent = createAction(
  `${ActionType} Form Element Interaction Event`,
  (formElementName: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.APPLICATION_ENGAGEMENT,
      eventAction: 'Form Element',
      eventLabel: formElementName,
    };
  }
);

export const formElementInteractionErrorEvent = createAction(
  `${ActionType} Form Element Interaction Error Event`,
  (error: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.APPLICATION_ENGAGEMENT,
      eventAction: 'Form Element Error',
      eventLabel: error,
      errors: 1,
    };
  }
);

export const sessionTimeoutErrorEvent = createAction(
  `${ActionType} Session Timeout Error Event`,
  (error: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.APPLICATION_ENGAGEMENT,
      eventAction: 'Session Timeout Error',
      eventLabel: error,
      errors: 1,
    };
  }
);

export const applicationResumeEvent = createAction(
  `${ActionType} Application Resume Event`,
  () => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.APPLICATION_RESUME,
      eventAction: 'Resume Button Click',
    };
  }
);

export const applicationResumeSuccessEvent = createAction(
  `${ActionType} Application Resume Success Event`,
  (resumedPage: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.APPLICATION_RESUME,
      eventAction: 'Resume Success',
      eventLabel: resumedPage,
      applicationsResumed: 1,
      applicationResumed: true,
    };
  }
);

export const applicationResumeErrorEvent = createAction(
  `${ActionType} Application Resume Error Event`,
  (error: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.APPLICATION_RESUME,
      eventAction: 'Resume Error',
      eventLabel: error,
      errors: 1,
    };
  }
);

export const applicationSubmissionEvent = createAction(
  `${ActionType} Application Submission Event`,
  (
    applicationCompletionTime: number,
    approvalStatus: string,
    approvalAmount: number,
    leaseId: number,
    cartAmount: number
  ) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.APPLICATION_SUBMISSION,
      eventAction: 'Submit Success',
      eventLabel: approvalStatus,
      approvalStatus: approvalStatus,
      applicationSubmitted: 'true',
      applicationsSubmitted: 1,
      leaseApprovalAmount: approvalAmount,
      applicationCompletionTime: applicationCompletionTime,
      leaseId: leaseId,
      cartAmount: cartAmount,
    };
  }
);

export const applicationSubmissionErrorEvent = createAction(
  `${ActionType} Application Submission Error Event`,
  (error: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.APPLICATION_SUBMISSION,
      eventAction: 'Submit Error',
      eventLabel: error,
      errors: 1,
    };
  }
);

export const paymentEstimatorEvent = createAction(
  `${ActionType} Payment Estimator Event`,
  () => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.PAYMENT_ESTIMATOR,
      eventAction: 'Estimate Button Click',
      eventLabel: 'Estimate payments',
    };
  }
);

export const paymentEstimatorFormFillEvent = createAction(
  `${ActionType} Payment Estimator Form Fill Event`,
  (fieldName: string, fieldValue: string, action?: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.PAYMENT_ESTIMATOR,
      eventAction: action || 'Form Fill',
      eventLabel: fieldValue,
      formFieldName: fieldName,
      paymentEstimatorUsed: 'Yes',
    };
  }
);

export const paymentEstimatorSuccessEvent = createAction(
  `${ActionType} Payment Estimator Success Event`,
  (
    state: string,
    leasePeriod: string,
    estimatorFequency: string,
    estimatedTotal: number,
    costOfLease: number,
    totalCostOfLease: number,
    recurringPaymentAmt: number,
    payments: number
  ) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.PAYMENT_ESTIMATOR,
      eventAction: 'Estimator Success',
      eventLabel: state,
      paymentEstResult: 'Success',
      paymentEstLeasePeriod: leasePeriod,
      paymentEstFrequency: estimatorFequency,
      payEstEngagements: 1,
      payEstEstimatedTotal: estimatedTotal,
      payEstCostOfLease: costOfLease,
      payEstTotalCostOfLease: totalCostOfLease,
      payEstRecurringPayment: recurringPaymentAmt,
      payEstPayments: payments,
    };
  }
);

export const paymentEstimatorErrorEvent = createAction(
  `${ActionType} Payment Estimator Error Event`,
  (error: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.PAYMENT_ESTIMATOR,
      eventAction: 'Estimate Error',
      eventLabel: error,
      errors: 1,
      paymentEstResult: 'Error',
    };
  }
);

export const paymentEstimatorSliderChangeEvent = createAction(
  `${ActionType} Payment Estimator Slider Change Event`,
  (newSliderValue: number) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.PAYMENT_ESTIMATOR,
      eventAction: 'Slider Change',
      eventLabel: 'slider value selected: ' + newSliderValue,
    };
  }
);

export const errorEvent = createAction(
  `${ActionType} Error Event`,
  (errorType: string, errorMessage: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.ERRORS,
      eventAction: errorType,
      eventLabel: errorMessage,
      errors: 1,
    };
  }
);

export const valuePropScrollEvent = createAction(
  `${ActionType} Value Prop Scroll Event`,
  (scrollMarkerReached: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.VALUE_PROP_SCREEN,
      eventAction: 'Scroll',
      eventLabel: scrollMarkerReached,
    };
  }
);

export const applicationExitEvent = createAction(
  `${ActionType} Application Exit Event`,
  (pageKey: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.APPLICATION_EXIT,
      eventAction: 'Exit Button Click',
      eventLabel: pageKey,
    };
  }
);

export const otpSendCodeEvent = createAction(
  `${ActionType} Otp Send Code Event`,
  () => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.OTP,
      eventAction: 'Send Code Click',
    };
  }
);

export const otpResendCodeEvent = createAction(
  `${ActionType} Otp Resent Code Event`,
  () => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.OTP,
      eventAction: 'Resend Code Click',
    };
  }
);

export const otpCodeEntryEvent = createAction(
  `${ActionType} Otp Code Entry Event`,
  (
    otpSendCodeTimestamp: number,
    result: string,
    authenticationMethod: string
  ) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.OTP,
      eventAction: 'OTP Code Entry',
      eventLabel: authenticationMethod + ' - ' + result,
      otpCompletionTime: otpSendCodeTimestamp,
    };
  }
);

export const otpSsnEntryEvent = createAction(
  `${ActionType} Otp Ssn Entry Event`,
  (result: string, resultReason: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.OTP,
      eventAction: 'SSN/ITIN Entry',
      eventLabel: result,
      resultReason: resultReason,
    };
  }
);

export const otpErrorEvent = createAction(
  `${ActionType} Otp Error Event`,
  (error: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.OTP,
      eventAction: 'OTP Error',
      eventLabel: error,
      errors: 1,
    };
  }
);

export const alertEvent = createAction(
  `${ActionType} Alert Event`,
  (
    pageKey: string,
    alertType: 'Flag' | 'Card',
    alertLabel: string,
    alertDescription: string,
    isError?: boolean
  ) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.ALERT,
      eventAction: alertType,
      eventLabel: alertLabel,
      formFieldName: alertDescription,
      eventSource: pageKey,
      errors: isError ? 1 : undefined,
    };
  }
);

export const navigateLinkClickEvent = createAction(
  `${ActionType} Navigate Link Click Event`,
  (fromPage: string, toPage: string) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.NAVIGATION,
      eventAction: 'Back Link Click',
      eventLabel: fromPage + ' to ' + toPage,
    };
  }
);

export const leaseCreationEvent = createAction(
  `${ActionType} Lease Creation Event`,
  (
    leaseCreationTime: number,
    subtotal: number,
    remainingInvoiceTotal: number,
    recurringPaymentAmount: number,
    utilizationAmount: number,
    remainingBalance: number,
    initialPaymentAmount: number,
    initialPaymentTax: number,
    initialPaymentTotal: number
  ) => {
    return {
      ...basedAnalyticPayload,
      eventCategory: EventCategory.CHECK_OUT,
      eventAction: 'Create Lease',
      eventLabel: 'Success',
      eventValue: subtotal,
      eventNonInteraction: 0,
      leasesCreated: 1,
      leaseCreationTime: leaseCreationTime,
      leaseSubtotalAmount: subtotal,
      leaseRemainingInvoiceTotal: remainingInvoiceTotal,
      leaseRecurringPaymentAmount: recurringPaymentAmount,
      leaseUtilizationAmount: utilizationAmount,
      leaseRemainingBalance: remainingBalance,
      leaseInitialPaymentAmount: initialPaymentAmount,
      leaseInitialPaymentTax: initialPaymentTax,
      leaseInitialPaymentTotal: initialPaymentTotal,
    };
  }
);

export const allActions = union({
  pageViewEvent,
  applicationStartEvent,
  applicationStartSuccessEvent,
  applicationStartErrorEvent,
  buttonClickEvent,
  modalOpenEvent,
  formFillEvent,
  formFieldError,
  formElementInteractionEvent,
  formElementInteractionErrorEvent,
  sessionTimeoutErrorEvent,
  applicationResumeEvent,
  applicationResumeSuccessEvent,
  applicationResumeErrorEvent,
  applicationSubmissionEvent,
  applicationSubmissionErrorEvent,
  paymentEstimatorEvent,
  paymentEstimatorFormFillEvent,
  paymentEstimatorSuccessEvent,
  paymentEstimatorErrorEvent,
  paymentEstimatorSliderChangeEvent,
  errorEvent,
  valuePropScrollEvent,
  applicationExitEvent,
  otpSendCodeEvent,
  otpCodeEntryEvent,
  otpSsnEntryEvent,
  otpErrorEvent,
  alertEvent,
  navigateLinkClickEvent,
  leaseCreationEvent,
});

export type AnalyticActions = typeof allActions;

/*
 * Returns the number of seconds between endDate and startDate.
 * If startDate is later than endDate, returns 0 (cannot have negative elapsed time)
 * This is used for analytics, as GA accepts Time values as a number representing seconds
 */
export function getElapsedTimeInSeconds(
  startDate: Date,
  endDate: Date
): number {
  if (!startDate || !endDate) {
    return 0;
  }

  const ms = endDate.getTime() - startDate.getTime();
  if (ms < 0) {
    return 0;
  }

  return ms / 1000;
}
