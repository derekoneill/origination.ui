import { createAction, union } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import {
  Session,
  BySessionId,
  InitSessionRequest,
} from '@ua/shared/data-access';
import { generateId } from '@ua/shared/utilities';

export const SessionPrefix = '[Session]';

export const getSessionBelongToApplication = createAction(
  `${SessionPrefix} Get Session Belong To Application`,
  (bySessionId: BySessionId, correlationId: string = generateId()) => ({
    bySessionId,
    correlationId,
  })
);

export const getSession = createAction(
  `${SessionPrefix} Get Session By Session Id`,
  (bySessionId: BySessionId, correlationId: string = generateId()) => ({
    bySessionId,
    correlationId,
  })
);

export const getSessionSuccess = createAction(
  `${SessionPrefix} Get Session Success`,
  (session: Session, correlationId: string = generateId()) => ({
    session,
    correlationId,
  })
);

export const getSessionError = createAction(
  `${SessionPrefix} Get Session Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const initSession = createAction(
  `${SessionPrefix} Init Session`,
  (
    initSessionRequest: InitSessionRequest,
    correlationId: string = generateId()
  ) => ({
    initSessionRequest,
    correlationId,
  })
);

export const initSessionSuccess = createAction(
  `${SessionPrefix} Init Session Success`,
  (bySessionId: BySessionId, correlationId: string = generateId()) => ({
    bySessionId,
    correlationId,
  })
);

export const initSessionError = createAction(
  `${SessionPrefix} Init Session Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const changeSessionIdToSessionIdBelongToTheCurrentApplication = createAction(
  `${SessionPrefix} Change SessionId To SessionId Belong To The Current Application`,
  (correlationId: string = generateId()) => ({
    correlationId,
  })
);

const allActions = union({
  getSessionBelongToApplication,
  getSession,
  getSessionSuccess,
  getSessionError,
  initSession,
  initSessionSuccess,
  initSessionError,
  changeSessionIdToSessionIdBelongToTheCurrentApplication,
});

export type SessionActions = typeof allActions;
