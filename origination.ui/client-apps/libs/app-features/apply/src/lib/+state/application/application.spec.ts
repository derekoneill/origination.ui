import { HttpErrorResponse } from '@angular/common/http';
import { Application, BySessionId } from '@ua/shared/data-access';
import * as fromApplication from './index';
import * as fromRoot from '..';

describe('application state reducer', () => {
  let applicationState: fromApplication.ApplicationState;
  beforeEach(() => {
    applicationState = fromApplication.initApplication();
  });

  it('should handle the GetApplicationBySessionId action correctly.', () => {
    expect(applicationState.application.firstName).toEqual('');
    expect(applicationState.application.emailAddress).toEqual('');
    expect(applicationState.isLoaded).toEqual(false);
    applicationState = fromApplication.reducer(
      applicationState,
      fromApplication.getApplicationBySessionId(<BySessionId>{
        sessionId: '547c8702-e6b6-46f6-a5d3-7a48716522a0',
      })
    );
    expect(applicationState.isLoaded).toEqual(false);
  });

  it('should handle the GetApplicationBySessionIdSuccess correctly.', () => {
    expect(applicationState.application.firstName).toEqual('');
    expect(applicationState.application.emailAddress).toEqual('');
    expect(applicationState.isLoaded).toEqual(false);
    applicationState = fromApplication.reducer(
      applicationState,
      fromApplication.getApplicationBySessionIdSuccess(<Application>{
        firstName: 'Eric',
        lastName: 'Nguyen',
        emailAddress: 'eric.nguyen@progleasing.com',
        phoneNumber: '801-222-3333',
        street1: '10145 S 4444 W',
        city: 'South Jordan',
        zip: '84009',
        state: 'UT',
      })
    );
    expect(applicationState.isLoaded).toEqual(true);
    expect(applicationState.application.firstName).toEqual('Eric');
    expect(applicationState.application.lastName).toEqual('Nguyen');
    expect(applicationState.application.street1).toEqual('10145 S 4444 W');
    expect(applicationState.application.city).toEqual('South Jordan');
    expect(applicationState.application.zip).toEqual('84009');
    expect(applicationState.application.state).toEqual('UT');
  });

  it('should handle the GetApplicationBySessionIdError correctly.', () => {
    expect(applicationState.application.firstName).toEqual('');
    expect(applicationState.application.emailAddress).toEqual('');
    expect(applicationState.isLoaded).toEqual(false);
    applicationState = fromApplication.reducer(
      applicationState,
      fromApplication.getApplicationBySessionIdError(<HttpErrorResponse>{
        status: 500,
        statusText: 'Internal Server Error',
      })
    );
    expect(applicationState.error).toEqual(<HttpErrorResponse>{
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should initialize the application state correctly', () => {
    expect(applicationState.application.firstName).toEqual('');
    expect(applicationState.application.emailAddress).toEqual('');
    expect(applicationState.isLoaded).toEqual(false);
    applicationState = fromApplication.reducer(
      undefined,
      fromApplication.getApplicationBySessionId(<BySessionId>{
        sessionId: 'xyz',
      })
    );
    expect(applicationState.application.firstName).toEqual('');
    expect(applicationState.application.emailAddress).toEqual('');
    expect(applicationState.isLoaded).toEqual(false);
  });
});
