import * as fromAnalytic from '.';
import * as fromFeatureFlags from '../feature-flags';
import * as fromUi from '../ui';
import * as fromSession from '../session';
import * as fromRoot from '..';

describe('analytic fromAnalytic', () => {
  let featureFlags: fromFeatureFlags.FeatureFlagsState;
  let uiState: fromUi.UiState;
  let sessionState: fromSession.SessionState;
  let analyticState: Array<{ [key: string]: any }>;
  let analyticBaseState;
  beforeEach(() => {
    featureFlags = fromFeatureFlags.initFeatureFlags();
    uiState = fromUi.initUi();
    sessionState = fromSession.initSessionState({
      session: {
        sessionId: '411778d7-633e-4eb8-9a1a-cc722330ade1',
        originalSessionId: '411778d7-633e-4eb8-9a1a-cc722330ade1',
        storeId: 88888,
        locale: 'en_US',
        orderToken: '3456789',
        storeName: 'Progressive Store',
      },
    });

    analyticBaseState = {
      event: fromAnalytic.Event.EVENT_TRACKER,
      eventCategory: fromAnalytic.EventCategory.APPLICATION_START,
      hitType: fromAnalytic.HitType.EVENT_TRACKING_HIT,
      eventValue: 0,
      eventNonInteraction: 0,
      eventLabel: '',
      eventAction: '',
    };
  });

  it('PageViewEvent() should have all its correct properties', () => {
    const page = 'welcome';
    const expectedPage = '/welcome/';
    const location = `https://vdc-qaswebapp13.stormwind.local/eComUI/#/${page}/?sid=411778d7-633e-4eb8-9a1a-cc722330ade1`;
    const pageView = fromAnalytic.pageViewEvent(
      sessionState.session,
      false,
      location,
      'Title Testing Page',
      'SectionTitle Of Testing Page'
    );
    analyticState = fromAnalytic.reducer([], pageView);
    expect(analyticState).toEqual([
      {
        event: fromAnalytic.Event.VIRTUAL_PAGEVIEW,
        eventCategory: fromAnalytic.EventCategory.APPLICATION_START,
        hitType: fromAnalytic.HitType.PAGE_TRACKING_HIT,
        eventValue: 0,
        eventNonInteraction: 0,
        eventLabel: '',
        eventAction: '',
        title: 'Title Testing Page',
        sectionId: 'SectionTitle Of Testing Page',
        page: expectedPage,
        location: location,
        storeId: 88888,
        customerId: '411778d7-633e-4eb8-9a1a-cc722330ade1',
        flowType: 'original',
        flowName: 'original',
        cartId: '411778d7-633e-4eb8-9a1a-cc722330ade1',
        version: 'ecom',
        merchantName: 'Progressive Store',
        lenderName: 'Progressive',
        lenderType: 'Primary',
        textToApplyEnabled: 'false',
        inStoreFlag: 'false',
        paymentEstEnabled: 'true',
        bankLookupEnabled: 'false',
        rldEnabled: 'false',
        localizationEnabled: 'false',
        storeLocatorEnabled: 'false',
        type: pageView.type,
      },
    ]);
  });

  it('ApplicationStartEvent() should have all its correct properties', () => {
    const applicationStartEvent = fromAnalytic.applicationStartEvent(
      'application start event'
    );
    analyticState = fromAnalytic.reducer([], applicationStartEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventLabel: 'application start event',
        eventAction: 'Apply Now Button Click',
        type: applicationStartEvent.type,
      },
    ]);
  });

  it('ApplicationStartSuccessEvent() should have all its correct properties', () => {
    const applicationStartSuccessEvent = fromAnalytic.applicationStartSuccessEvent();
    analyticState = fromAnalytic.reducer([], applicationStartSuccessEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventAction: 'Start Application Success',
        applicationStarted: 'true',
        applicationsStarted: 1,
        type: applicationStartSuccessEvent.type,
      },
    ]);
  });

  it('ApplicationStartErrorEvent() should have all its correct propperties', () => {
    const applicationStartErrorEvent = fromAnalytic.applicationStartErrorEvent(
      'Could not start application'
    );
    analyticState = fromAnalytic.reducer([], applicationStartErrorEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventAction: 'Start Application Error',
        eventLabel: 'Could not start application',
        errors: 1,
        type: applicationStartErrorEvent.type,
      },
    ]);
  });

  it('ButtonClickEvent() should have all its correct properties', () => {
    const buttonClickEvent = fromAnalytic.buttonClickEvent(
      'disclosed click',
      fromAnalytic.ButtonKind.LINK,
      'action clicked',
      fromAnalytic.EventCategory.PAYMENT_ESTIMATOR
    );
    analyticState = fromAnalytic.reducer([], buttonClickEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.APPLICATION_ENGAGEMENT,
        eventAction: 'action clicked',
        eventLabel: 'disclosed click',
        paymentEstimatorUsed: 'Yes',
        type: buttonClickEvent.type,
      },
    ]);
  });

  it('ModalOpenEvent() should have all its correct properties', () => {
    const modalOpenEvent = fromAnalytic.modalOpenEvent(
      'Test Modal Title',
      'Test Window Name'
    );
    analyticState = fromAnalytic.reducer([], modalOpenEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.APPLICATION_ENGAGEMENT,
        eventAction: 'Modal Window',
        eventLabel: 'Test Modal Title',
        eventValue: 0,
        eventNonInteraction: 0,
        modalWindowName: 'Test Window Name',
        type: modalOpenEvent.type,
      },
    ]);
  });

  it('FormFillEvent() should have all its correct properties', () => {
    const formFillEvent = fromAnalytic.formFillEvent(
      'ssn field',
      '555-55-5555'
    );
    analyticState = fromAnalytic.reducer([], formFillEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.APPLICATION_ENGAGEMENT,
        eventAction: 'Form Fill',
        eventLabel: '555-55-5555',
        formFieldName: 'ssn field',
        type: formFillEvent.type,
      },
    ]);
  });

  it('FormFieldError() should have all its correct properties', () => {
    const formFieldError = fromAnalytic.formFieldError(
      'AuthenticationCode',
      'Invalid Code'
    );
    analyticState = fromAnalytic.reducer([], formFieldError);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.APPLICATION_ENGAGEMENT,
        eventAction: 'Form Fill Error',
        eventLabel: 'Invalid Code',
        formFieldName: 'AuthenticationCode',
        errors: 1,
        type: formFieldError.type,
      },
    ]);
  });

  it('FormElementInteractionEvent() should have all its correct properties', () => {
    const formElementInteractionEvent = fromAnalytic.formElementInteractionEvent(
      'ssn element'
    );
    analyticState = fromAnalytic.reducer([], formElementInteractionEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.APPLICATION_ENGAGEMENT,
        eventAction: 'Form Element',
        eventLabel: 'ssn element',
        type: formElementInteractionEvent.type,
      },
    ]);
  });

  it('FormElementInteractionErrorEvent() should have all its correct properties', () => {
    const formElementInteractionErrorEvent = fromAnalytic.formElementInteractionErrorEvent(
      'ssn element interaction with error'
    );
    analyticState = fromAnalytic.reducer([], formElementInteractionErrorEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.APPLICATION_ENGAGEMENT,
        eventAction: 'Form Element Error',
        eventLabel: 'ssn element interaction with error',
        errors: 1,
        type: formElementInteractionErrorEvent.type,
      },
    ]);
  });

  it('SessionTimeoutErrorEvent() should have all its correct properties', () => {
    const sessionTimeoutErrorEvent = fromAnalytic.sessionTimeoutErrorEvent(
      'session timeout'
    );
    analyticState = fromAnalytic.reducer([], sessionTimeoutErrorEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.APPLICATION_ENGAGEMENT,
        eventAction: 'Session Timeout Error',
        eventLabel: 'session timeout',
        errors: 1,
        type: sessionTimeoutErrorEvent.type,
      },
    ]);
  });

  it('ApplicationResumeEvent() should have all its correct properties', () => {
    const applicationResumeEvent = fromAnalytic.applicationResumeEvent();
    analyticState = fromAnalytic.reducer([], applicationResumeEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.APPLICATION_RESUME,
        eventAction: 'Resume Button Click',
        eventLabel: '',
        type: applicationResumeEvent.type,
      },
    ]);
  });

  it('ApplicationResumeSuccessEvent() should have all its correct properties', () => {
    const applicationResumeSuccessEvent = fromAnalytic.applicationResumeSuccessEvent(
      '/apply/get-started'
    );
    analyticState = fromAnalytic.reducer([], applicationResumeSuccessEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.APPLICATION_RESUME,
        eventAction: 'Resume Success',
        eventLabel: '/apply/get-started',
        applicationsResumed: 1,
        applicationResumed: true,
        type: applicationResumeSuccessEvent.type,
      },
    ]);
  });

  it('ApplicationResumeErrorEvent() should have all its correct properties', () => {
    const applicationResumeErrorEvent = fromAnalytic.applicationResumeErrorEvent(
      'Cannot resume the /apply/get-started'
    );
    analyticState = fromAnalytic.reducer([], applicationResumeErrorEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.APPLICATION_RESUME,
        eventAction: 'Resume Error',
        eventLabel: 'Cannot resume the /apply/get-started',
        errors: 1,
        type: applicationResumeErrorEvent.type,
      },
    ]);
  });

  it('ApplicationSubmissionEvent() should have all its correct properties', () => {
    const applicationCompletionTime = new Date().getMilliseconds() + 500;
    const approvalStatus = 'approved';
    const approvalAmount = 2000;
    const leaseId = 123456;
    const cartAmount = 950;
    const applicationSubmissionEvent = fromAnalytic.applicationSubmissionEvent(
      applicationCompletionTime,
      approvalStatus,
      approvalAmount,
      leaseId,
      cartAmount
    );
    analyticState = fromAnalytic.reducer([], applicationSubmissionEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.APPLICATION_SUBMISSION,
        eventAction: 'Submit Success',
        eventLabel: approvalStatus,
        approvalStatus: approvalStatus,
        applicationSubmitted: 'true',
        applicationsSubmitted: 1,
        leaseApprovalAmount: approvalAmount,
        applicationCompletionTime: applicationCompletionTime,
        leaseId: leaseId,
        cartAmount: cartAmount,
        type: applicationSubmissionEvent.type,
      },
    ]);
  });

  it('ApplicationSubmissionErrorEvent() should have all its correct properties', () => {
    const errorMessage = 'No Session Id For Submission';
    const applicationSubmissionErrorEvent = fromAnalytic.applicationSubmissionErrorEvent(
      errorMessage
    );
    analyticState = fromAnalytic.reducer([], applicationSubmissionErrorEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.APPLICATION_SUBMISSION,
        eventAction: 'Submit Error',
        eventLabel: errorMessage,
        errors: 1,
        type: applicationSubmissionErrorEvent.type,
      },
    ]);
  });

  it('PaymentEstimatorEvent() should have all its correct properties', () => {
    const paymentEstimatorEvent = fromAnalytic.paymentEstimatorEvent();
    analyticState = fromAnalytic.reducer([], paymentEstimatorEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.PAYMENT_ESTIMATOR,
        eventAction: 'Estimate Button Click',
        eventLabel: 'Estimate payments',
        type: paymentEstimatorEvent.type,
      },
    ]);
  });

  it('PaymentEstimatorFormFillEvent() should have all its correct properties', () => {
    const fieldName = 'payment field';
    const fieldValue = '500';
    const action = 'submit-payment';
    const paymentEstimatorFormFillEvent = fromAnalytic.paymentEstimatorFormFillEvent(
      fieldName,
      fieldValue,
      action
    );
    analyticState = fromAnalytic.reducer([], paymentEstimatorFormFillEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.PAYMENT_ESTIMATOR,
        eventAction: action,
        eventLabel: fieldValue,
        formFieldName: fieldName,
        paymentEstimatorUsed: 'Yes',
        type: paymentEstimatorFormFillEvent.type,
      },
    ]);
  });

  it('PaymentEstimatorSuccessEvent() should have all its correct properties', () => {
    const state = 'UT';
    const leasePeriod = '12 months';
    const estimatorFrenquency = 'monthly';
    const estimatorTotal = 1000;
    const costOfLease = 1075;
    const totalCostOfLease = 2075;
    const recurringPaymentAmount = 173;
    const payments = 12;
    const paymentEstimatorSuccessEvent = fromAnalytic.paymentEstimatorSuccessEvent(
      'UT',
      '12 months',
      'monthly',
      1000,
      1075,
      2075,
      173,
      12
    );
    analyticState = fromAnalytic.reducer([], paymentEstimatorSuccessEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.PAYMENT_ESTIMATOR,
        eventAction: 'Estimator Success',
        eventLabel: state,
        paymentEstResult: 'Success',
        paymentEstLeasePeriod: leasePeriod,
        paymentEstFrequency: estimatorFrenquency,
        payEstEngagements: 1,
        payEstEstimatedTotal: estimatorTotal,
        payEstCostOfLease: costOfLease,
        payEstTotalCostOfLease: totalCostOfLease,
        payEstRecurringPayment: recurringPaymentAmount,
        payEstPayments: payments,
        type: paymentEstimatorSuccessEvent.type,
      },
    ]);
  });

  it('PaymentEstimatorErrorEvent() should have all its correct properties', () => {
    const errorMessage = 'Missing Information Error';
    const paymentEstimatorErrorEvent = fromAnalytic.paymentEstimatorErrorEvent(
      errorMessage
    );
    analyticState = fromAnalytic.reducer([], paymentEstimatorErrorEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.PAYMENT_ESTIMATOR,
        eventAction: 'Estimate Error',
        eventLabel: errorMessage,
        errors: 1,
        paymentEstResult: 'Error',
        type: paymentEstimatorErrorEvent.type,
      },
    ]);
  });

  it('PaymentEstimatorSliderChangeEvent() should have all its correct properties', () => {
    const liderValue = 500;
    const paymentEstimatorSliderChangeEvent = fromAnalytic.paymentEstimatorSliderChangeEvent(
      liderValue
    );
    analyticState = fromAnalytic.reducer([], paymentEstimatorSliderChangeEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.PAYMENT_ESTIMATOR,
        eventAction: 'Slider Change',
        eventLabel: 'slider value selected: ' + liderValue,
        type: paymentEstimatorSliderChangeEvent.type,
      },
    ]);
  });

  it('ErrorEvent() should have all its correct properties', () => {
    const errorType = 'payment';
    const errorMessage = 'cannot make payment';
    const errorEvent = fromAnalytic.errorEvent(errorType, errorMessage);
    analyticState = fromAnalytic.reducer([], errorEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.ERRORS,
        eventAction: errorType,
        eventLabel: errorMessage,
        errors: 1,
        type: errorEvent.type,
      },
    ]);
  });

  it('ValuePropScrollEvent() should have all its correct properties', () => {
    const scrollMarkerReached = 'reach to max';
    const valuePropScrollEvent = fromAnalytic.valuePropScrollEvent(
      scrollMarkerReached
    );
    analyticState = fromAnalytic.reducer([], valuePropScrollEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.VALUE_PROP_SCREEN,
        eventAction: 'Scroll',
        eventLabel: scrollMarkerReached,
        type: valuePropScrollEvent.type,
      },
    ]);
  });

  it('ApplicationExitEvent() should have all its correct properties', () => {
    const pageKey = 'https://vdc-qaswebapp13.stormwind.local/eComUI/#/welcome';
    const applicationExitEvent = fromAnalytic.applicationExitEvent(pageKey);
    analyticState = fromAnalytic.reducer([], applicationExitEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.APPLICATION_EXIT,
        eventAction: 'Exit Button Click',
        eventLabel: pageKey,
        type: applicationExitEvent.type,
      },
    ]);
  });

  it('OtpSendCodeEvent() should have all its correct properties', () => {
    const otpSendCodeEvent = fromAnalytic.otpSendCodeEvent();
    analyticState = fromAnalytic.reducer([], otpSendCodeEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.OTP,
        eventAction: 'Send Code Click',
        type: otpSendCodeEvent.type,
      },
    ]);
  });

  it('OtpCodeEntryEvent() should have all its correct properties', () => {
    const otpSendCodeTimestamp = new Date().getMilliseconds() + 500;
    const result = 'application is approved';
    const authenticationMethod = 'SMS';
    const otpCodeEntryEvent = fromAnalytic.otpCodeEntryEvent(
      otpSendCodeTimestamp,
      result,
      authenticationMethod
    );
    analyticState = fromAnalytic.reducer([], otpCodeEntryEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.OTP,
        eventAction: 'OTP Code Entry',
        eventLabel: authenticationMethod + ' - ' + result,
        otpCompletionTime: otpSendCodeTimestamp,
        type: otpCodeEntryEvent.type,
      },
    ]);
  });

  it('OtpSsnEntryEvent() should have all its correct properties', () => {
    const result = 'application is denied.';
    const resultReason = 'income information provided is too low';
    const otpSsnEntryEvent = fromAnalytic.otpSsnEntryEvent(
      result,
      resultReason
    );
    analyticState = fromAnalytic.reducer([], otpSsnEntryEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.OTP,
        eventAction: 'SSN/ITIN Entry',
        eventLabel: result,
        resultReason: resultReason,
        type: otpSsnEntryEvent.type,
      },
    ]);
  });

  it('OtpErrorEvent() should have all its correct properties', () => {
    const errorMessage = 'income information provided is too low';
    const otpErrorEvent = fromAnalytic.otpErrorEvent(errorMessage);
    analyticState = fromAnalytic.reducer([], otpErrorEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.OTP,
        eventAction: 'OTP Error',
        eventLabel: errorMessage,
        errors: 1,
        type: otpErrorEvent.type,
      },
    ]);
  });

  it('AlertEvent() should have all its correct properties', () => {
    const pageKey = 'https://vdc-qaswebapp13.stormwind.local/eComUI/#/welcome';
    const alertType = 'Card';
    const alertLabel = 'session';
    const alertDescription = 'session timeout';
    const isError = true;
    const alertEvent = fromAnalytic.alertEvent(
      pageKey,
      alertType,
      alertLabel,
      alertDescription,
      isError
    );
    analyticState = fromAnalytic.reducer([], alertEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.ALERT,
        eventAction: alertType,
        eventLabel: alertLabel,
        formFieldName: alertDescription,
        eventSource: pageKey,
        errors: 1,
        type: alertEvent.type,
      },
    ]);
  });

  it('NavigateLinkClickEvent() should have all its correct properties', () => {
    const fromPage = 'https://vdc-qaswebapp13.stormwind.local/eComUI/#/welcome';
    const toPage =
      'https://vdc-qaswebapp13.stormwind.local/eComUI/#/apply/get-started';
    const navigateLinkClickEvent = fromAnalytic.navigateLinkClickEvent(
      fromPage,
      toPage
    );
    analyticState = fromAnalytic.reducer([], navigateLinkClickEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.NAVIGATION,
        eventAction: 'Back Link Click',
        eventLabel: fromPage + ' to ' + toPage,
        type: navigateLinkClickEvent.type,
      },
    ]);
  });

  it('LeaseCreationEvent() should have all its correct properties', () => {
    const subtotal = 500;
    const remainingInvoiceTotal = 250;
    const recurringPaymentAmount = 30;
    const utilizationAmount = 25;
    const remainingBalance = 200;
    const initialPaymentAmount = 500;
    const initialPaymentTax = 20;
    const initialPaymentTotal = 35;
    const leaseCreateTime = new Date().getMilliseconds() + 500;
    const leaseCreationEvent = fromAnalytic.leaseCreationEvent(
      leaseCreateTime,
      subtotal,
      remainingInvoiceTotal,
      recurringPaymentAmount,
      utilizationAmount,
      remainingBalance,
      initialPaymentAmount,
      initialPaymentTax,
      initialPaymentTotal
    );
    analyticState = fromAnalytic.reducer([], leaseCreationEvent);
    expect(analyticState).toEqual([
      {
        ...analyticBaseState,
        eventCategory: fromAnalytic.EventCategory.CHECK_OUT,
        eventAction: 'Create Lease',
        eventLabel: 'Success',
        eventValue: subtotal,
        eventNonInteraction: 0,
        leasesCreated: 1,
        leaseCreationTime: leaseCreateTime,
        leaseSubtotalAmount: subtotal,
        leaseRemainingInvoiceTotal: remainingInvoiceTotal,
        leaseRecurringPaymentAmount: recurringPaymentAmount,
        leaseUtilizationAmount: utilizationAmount,
        leaseRemainingBalance: remainingBalance,
        leaseInitialPaymentAmount: initialPaymentAmount,
        leaseInitialPaymentTax: initialPaymentTax,
        leaseInitialPaymentTotal: initialPaymentTotal,
        type: leaseCreationEvent.type,
      },
    ]);
  });

  it('should persist only 10 latest events', () => {
    const errorMessage = 'income information provided is too low';
    const otpErrorEvent = fromAnalytic.otpErrorEvent(errorMessage);
    const pageKey = 'https://vdc-qaswebapp13.stormwind.local/eComUI/#/welcome';
    const applicationExitEvent = fromAnalytic.applicationExitEvent(pageKey);
    analyticState = fromAnalytic.reducer(undefined, otpErrorEvent);
    analyticState = fromAnalytic.reducer(analyticState, otpErrorEvent);
    analyticState = fromAnalytic.reducer(analyticState, otpErrorEvent);
    analyticState = fromAnalytic.reducer(analyticState, otpErrorEvent);
    analyticState = fromAnalytic.reducer(analyticState, otpErrorEvent);
    analyticState = fromAnalytic.reducer(analyticState, otpErrorEvent);
    analyticState = fromAnalytic.reducer(analyticState, otpErrorEvent);
    analyticState = fromAnalytic.reducer(analyticState, otpErrorEvent);
    analyticState = fromAnalytic.reducer(analyticState, otpErrorEvent);
    analyticState = fromAnalytic.reducer(analyticState, otpErrorEvent);
    analyticState = fromAnalytic.reducer(analyticState, otpErrorEvent);
    analyticState = fromAnalytic.reducer(analyticState, applicationExitEvent);
    expect(analyticState).toEqual([
      otpErrorEvent,
      otpErrorEvent,
      otpErrorEvent,
      otpErrorEvent,
      otpErrorEvent,
      otpErrorEvent,
      otpErrorEvent,
      otpErrorEvent,
      otpErrorEvent,
      applicationExitEvent,
    ]);
  });
});
