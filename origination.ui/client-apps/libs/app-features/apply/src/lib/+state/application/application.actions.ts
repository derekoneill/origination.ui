import { HttpErrorResponse } from '@angular/common/http';
import { generateId } from '@ua/shared/utilities';
import {
  Application,
  InvoiceResult,
  LeaseSessionInfo,
  CancelContractResult,
  BySessionId,
} from '@ua/shared/data-access';
import { createAction, union } from '@ngrx/store';

const ApplicationPrefix = '[Application]';

export const getApplicationBySessionId = createAction(
  `${ApplicationPrefix} Get Application By SessionId`,
  (bySessionId: BySessionId, correlationId: string = generateId()) => ({
    bySessionId,
    correlationId,
  })
);

export const getApplicationBySessionIdSuccess = createAction(
  `${ApplicationPrefix} Get Application By SessionId Success`,
  (application: Application, correlationId: string = generateId()) => ({
    application,
    correlationId,
  })
);
export const getApplicationBySessionIdError = createAction(
  `${ApplicationPrefix} Get Application By SessionId Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const createApplicationBySessionId = createAction(
  `${ApplicationPrefix} Create Application By SessionId`,
  (bySessionId: BySessionId, correlationId: string = generateId()) => ({
    bySessionId,
    correlationId,
  })
);
export const createApplicationBySessionIdSuccess = createAction(
  `${ApplicationPrefix} Create Application By SessionId Success`,
  (application: Application, correlationId: string = generateId()) => ({
    application,
    correlationId,
  })
);
export const createApplicationBySessionIdError = createAction(
  `${ApplicationPrefix} Create Application By SessionId Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const updateApplication = createAction(
  `${ApplicationPrefix} Update Application`,
  (application: Application, correlationId: string = generateId()) => ({
    application,
    correlationId,
  })
);
export const updateApplicationSuccess = createAction(
  `${ApplicationPrefix} Update Application Success`,
  (application: Application, correlationId: string = generateId()) => ({
    application,
    correlationId,
  })
);
export const updateApplicationError = createAction(
  `${ApplicationPrefix} Update Application Error`,
  (application: HttpErrorResponse, correlationId: string = generateId()) => ({
    application,
    correlationId,
  })
);

export const cancelContract = createAction(
  `${ApplicationPrefix} Cancel Contract`,
  (bySessionId: BySessionId, correlationId: string = generateId()) => ({
    bySessionId,
    correlationId,
  })
);

export const cancelContractSuccess = createAction(
  `${ApplicationPrefix} Cancel Contract Success`,
  (
    cancelContractResult: CancelContractResult,
    correlationId: string = generateId()
  ) => ({
    cancelContractResult,
    correlationId,
  })
);

export const cancelContractError = createAction(
  `${ApplicationPrefix} Cancel Contract Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const submitInvoice = createAction(
  `${ApplicationPrefix} Submit Invoice`,
  (bySessionId: BySessionId, correlationId: string = generateId()) => ({
    bySessionId,
    correlationId,
  })
);

export const submitInvoiceSuccess = createAction(
  `${ApplicationPrefix} Submit Invoice Success`,
  (invoiceResult: InvoiceResult, correlationId: string = generateId()) => ({
    invoiceResult,
    correlationId,
  })
);

export const submitInvoiceError = createAction(
  `${ApplicationPrefix} Submit Invoice Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const submitLeaseSessionInfo = createAction(
  `${ApplicationPrefix} Submit Lease Session Info`,
  (
    leaseSessionInfo: LeaseSessionInfo,
    correlationId: string = generateId()
  ) => ({
    leaseSessionInfo,
    correlationId,
  })
);

export const submitLeaseSessionInfoSuccess = createAction(
  `${ApplicationPrefix} Submit Lease Session Info Success`,
  (
    leaseSessionInfo: LeaseSessionInfo,
    correlationId: string = generateId()
  ) => ({
    leaseSessionInfo,
    correlationId,
  })
);

export const submitLeaseSessionInfoError = createAction(
  `${ApplicationPrefix} Submit Lease Session Info Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

const allActions = union({
  getApplicationBySessionId,
  getApplicationBySessionIdSuccess,
  getApplicationBySessionIdError,
  createApplicationBySessionId,
  createApplicationBySessionIdSuccess,
  createApplicationBySessionIdError,
  updateApplication,
  updateApplicationSuccess,
  updateApplicationError,
  cancelContract,
  cancelContractSuccess,
  cancelContractError,
  submitInvoice,
  submitInvoiceSuccess,
  submitInvoiceError,
  submitLeaseSessionInfo,
  submitLeaseSessionInfoSuccess,
  submitLeaseSessionInfoError,
});

export type SessionActions = typeof allActions;
