import { createAction, union } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { BySessionId, Order } from '@ua/shared/data-access';
import { generateId } from '@ua/shared/utilities';

export const getOrderBySessionId = createAction(
  '[Order] Get Order By Session Id',
  (bySessionId: BySessionId, correlationId: string = generateId()) => ({
    bySessionId,
    correlationId,
  })
);

export const getOrderBySessionIdSuccess = createAction(
  '[Order] Get Order By Session Id Success',
  (order: Order, correlationId: string = generateId()) => ({
    order,
    correlationId,
  })
);

export const getOrderBySessionIdError = createAction(
  '[Order] Get Order By Session Id Error',
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

const allActions = union({
  getOrderBySessionId,
  getOrderBySessionIdSuccess,
  getOrderBySessionIdError,
});

export type OrderActions = typeof allActions;
