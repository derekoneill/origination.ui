import { createAction, union } from '@ngrx/store';
import { generateId } from '@ua/shared/utilities';
import { HttpErrorResponse } from '@angular/common/http';
import {
  AuthenticationMethod,
  CheckResumeAvailableResponse,
  GetResumableStateByAuthenticationIdRequest,
  ResumeStateResponse,
} from '@ua/shared/data-access';

import * as fromReducers from './authentication.reducers';

const AuthenticationPrefix = '[Authentication]';

export interface SendAuthenticationPayload {
  authenticationId: string;
  consent?: boolean;
  sessionId: string;
}

export interface VerifyAuthenticationCodeSplitterPayload {
  authenticationId: string;
  authenticationCode: string;
  sessionId: string;
  storeId: number;
}

export interface VerifyAuthenticationCodeAggregatorSuccessPayload
  extends VerifyAuthenticationCodeSplitterPayload {
  authenticationToken: fromReducers.VerifiedAuthenticationCode;
  checkResumable: CheckResumeAvailableResponse;
}

export interface AuthenticationInformationPayload {
  authenticationId: string;
  consent: boolean;
  authenticationMethod: AuthenticationMethod;
}

export const sendAuthenticationCode = createAction(
  `${AuthenticationPrefix} Send Auth Code`,
  (
    sendAuthenticationPayload: SendAuthenticationPayload,
    correlationId: string = generateId()
  ) => ({
    sendAuthenticationPayload,
    correlationId,
  })
);

export const sendAuthenticationCodeSuccess = createAction(
  `${AuthenticationPrefix} Send Auth Code Success`,
  (correlationId: string = generateId()) => ({
    correlationId,
  })
);

export const sendAuthenticationCodeError = createAction(
  `${AuthenticationPrefix} Send Auth Code Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const resendAuthenticationCode = createAction(
  `${AuthenticationPrefix} Resend Auth Code`,
  (
    sendAuthenticationPayload: SendAuthenticationPayload,
    correlationId: string = generateId()
  ) => ({
    sendAuthenticationPayload,
    correlationId,
  })
);

export const resendAuthenticationCodeSuccess = createAction(
  `${AuthenticationPrefix} Resent Auth Code Success`,
  (correlationId: string = generateId()) => ({
    correlationId,
  })
);

export const resendAuthenticationCodeError = createAction(
  `${AuthenticationPrefix} Resent Auth Code Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const verifyAuthenticationCode = createAction(
  `${AuthenticationPrefix} Verify The Auth Code`,
  (
    verifyAuthenticationCodeSplitterPayload: VerifyAuthenticationCodeSplitterPayload,
    correlationId: string = generateId()
  ) => ({
    verifyAuthenticationCodeSplitterPayload,
    correlationId,
  })
);

export const verifyAuthenticationCodeSuccess = createAction(
  `${AuthenticationPrefix} Verify The Auth Code Success`,
  (
    verifiedAuthenticationCode: fromReducers.VerifiedAuthenticationCode,
    correlationId: string = generateId()
  ) => ({
    verifiedAuthenticationCode,
    correlationId,
  })
);

export const verifyAuthenticationCodeError = createAction(
  `${AuthenticationPrefix}  Verify The Auth Code Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const verifyAuthenticationCodeSplitter = createAction(
  `${AuthenticationPrefix} Verify Auth Code Splitter`,
  (
    verifyAuthenticationCodeSplitterPayload: VerifyAuthenticationCodeSplitterPayload,
    correlationId: string = generateId()
  ) => ({
    verifyAuthenticationCodeSplitterPayload,
    correlationId,
  })
);

export const verifyAuthenticationCodeAggregatorSuccess = createAction(
  `${AuthenticationPrefix} Verify Auth Code Aggregator Success`,
  (
    verifyAuthenticationCodeAggregatorSuccessPayload: VerifyAuthenticationCodeAggregatorSuccessPayload,
    correlationId: string = generateId()
  ) => ({
    verifyAuthenticationCodeAggregatorSuccessPayload,
    correlationId,
  })
);

export const verifyAuthenticationCodeAggregatorError = createAction(
  `${AuthenticationPrefix} Verify Auth Code Aggregator Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const verifySsnSplitter = createAction(
  `${AuthenticationPrefix} Verify Ssn Splitter`,
  (
    getResumableStateByAuthenticationIdRequest: GetResumableStateByAuthenticationIdRequest,
    correlationId: string = generateId()
  ) => ({
    getResumableStateByAuthenticationIdRequest,
    correlationId,
  })
);

export const verifySsnAggregatorSuccess = createAction(
  `${AuthenticationPrefix} Verify Ssn Aggregator Success`,
  (
    resumeStateResponse: ResumeStateResponse,
    correlationId: string = generateId()
  ) => ({
    resumeStateResponse,
    correlationId,
  })
);

export const verifySsnAggregatorError = createAction(
  `${AuthenticationPrefix} Verify Ssn Aggregator Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const resetAuthenticationCodeAndSsnWhenDoneMoveAwayFromVerifyCodePage = createAction(
  `${AuthenticationPrefix} Reset AuthenticationCode And Ssn When Move Away From Verify Code Route`,
  (correlationId: string = generateId()) => ({
    correlationId,
  })
);

const allActions = union({
  sendAuthenticationCode,
  sendAuthenticationCodeSuccess,
  sendAuthenticationCodeError,
  resendAuthenticationCode,
  resendAuthenticationCodeSuccess,
  resendAuthenticationCodeError,
  verifyAuthenticationCode,
  verifyAuthenticationCodeSuccess,
  verifyAuthenticationCodeError,
  verifyAuthenticationCodeSplitter,
  verifyAuthenticationCodeAggregatorSuccess,
  verifyAuthenticationCodeAggregatorError,
  verifySsnSplitter,
  verifySsnAggregatorSuccess,
  verifySsnAggregatorError,
  resetAuthenticationCodeAndSsnWhenDoneMoveAwayFromVerifyCodePage,
});

export const ApplyActions = typeof allActions;
