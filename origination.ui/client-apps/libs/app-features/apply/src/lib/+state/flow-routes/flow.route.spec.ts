import * as fromFlowRoutes from '.';
import { HttpErrorResponse } from '@angular/common/http';

describe('flow routes state reducer', () => {
  let flowRoutesState: fromFlowRoutes.FlowRouteState;
  beforeEach(() => {
    flowRoutesState = fromFlowRoutes.initFlowRoutes();
  });

  it('handle change route success action', () => {
    flowRoutesState = fromFlowRoutes.initFlowRoutes(<
      fromFlowRoutes.FlowRouteState
    >{
      route: '',
    });
    flowRoutesState = fromFlowRoutes.reducer(
      flowRoutesState,
      fromFlowRoutes.changeApplyFlowRouteSuccess('/apply/get-started')
    );
    expect(flowRoutesState.route).toEqual('/apply/get-started');
  });

  it('handle change route error action', () => {
    flowRoutesState = fromFlowRoutes.initFlowRoutes();
    flowRoutesState = fromFlowRoutes.reducer(
      flowRoutesState,
      fromFlowRoutes.changeApplyFlowRouteError(<HttpErrorResponse>{
        status: 400,
        statusText: 'Could not navigate to the specified page',
        error: 'not exist',
      })
    );
    expect(flowRoutesState.route).toEqual('');
    expect(flowRoutesState.error).toEqual(<HttpErrorResponse>{
      status: 400,
      statusText: 'Could not navigate to the specified page',
      error: 'not exist',
    });
  });
});
