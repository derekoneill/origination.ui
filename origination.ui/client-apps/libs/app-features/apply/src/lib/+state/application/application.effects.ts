import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Effect, ofType, Actions, createEffect } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { mergeMap, catchError, map } from 'rxjs/operators';

import * as fromActions from './application.actions';
import {
  Application,
  CancelContractResult,
  InvoiceResult,
  LeaseSessionInfo,
  ApplicationService,
} from '@ua/shared/data-access';

@Injectable()
export class ApplicationEffects {
  constructor(
    private actions$: Actions,
    private applicationService: ApplicationService
  ) {}

  getApplication$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getApplicationBySessionId),
      mergeMap((action) =>
        this.applicationService
          .getApplicationBySessionId(action.bySessionId.sessionId)
          .pipe(
            map((application: Application) =>
              fromActions.getApplicationBySessionIdSuccess(
                application,
                action.correlationId
              )
            ),
            catchError((error) =>
              of(
                fromActions.getApplicationBySessionIdError(
                  error,
                  action.correlationId
                )
              )
            )
          )
      )
    )
  );

  createApplication$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.createApplicationBySessionId),
      mergeMap((action) =>
        this.applicationService
          .createApplicationBySessionId(action.bySessionId.sessionId)
          .pipe(
            map((application: Application) =>
              fromActions.createApplicationBySessionIdSuccess(
                application,
                action.correlationId
              )
            ),
            catchError((error) =>
              of(
                fromActions.createApplicationBySessionIdError(
                  error,
                  action.correlationId
                )
              )
            )
          )
      )
    )
  );

  updateApplication$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.updateApplication),
      mergeMap((action) =>
        this.applicationService.updateApplication(action.application).pipe(
          map((_) =>
            fromActions.updateApplicationSuccess(
              action.application,
              action.correlationId
            )
          ),
          catchError((error) =>
            of(fromActions.updateApplicationError(error, action.correlationId))
          )
        )
      )
    )
  );

  cancelContract$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.cancelContract),
      mergeMap((action) =>
        this.applicationService
          .cancelContract(action.bySessionId.sessionId)
          .pipe(
            map((cancelContractResult: CancelContractResult) =>
              fromActions.cancelContractSuccess(
                cancelContractResult,
                action.correlationId
              )
            ),
            catchError((error) =>
              of(fromActions.cancelContractError(error, action.correlationId))
            )
          )
      )
    )
  );

  submitInvoice$: Observable<Action> = this.actions$.pipe(
    ofType(fromActions.submitInvoice),
    mergeMap((action) =>
      this.applicationService.submitInvoice(action.bySessionId.sessionId).pipe(
        map((invoiceResult: InvoiceResult) =>
          fromActions.submitInvoiceSuccess(invoiceResult, action.correlationId)
        ),
        catchError((error) =>
          of(fromActions.submitInvoiceError(error, action.correlationId))
        )
      )
    )
  );

  submitLeaseSessionInfo$: Observable<Action> = this.actions$.pipe(
    ofType(fromActions.submitLeaseSessionInfo),
    mergeMap((action) =>
      this.applicationService
        .submitLeaseSessionInfo(action.leaseSessionInfo)
        .pipe(
          map((leaseSessionInfo: LeaseSessionInfo) =>
            fromActions.submitLeaseSessionInfoSuccess(
              leaseSessionInfo,
              action.correlationId
            )
          ),
          catchError((error) =>
            of(
              fromActions.submitLeaseSessionInfoError(
                error,
                action.correlationId
              )
            )
          )
        )
    )
  );
}
