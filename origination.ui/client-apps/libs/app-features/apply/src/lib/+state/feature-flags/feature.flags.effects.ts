import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { FeatureFlags, FeatureFlagService } from '@ua/shared/data-access';
import * as fromActions from './feature.flags.actions';

@Injectable()
export class FeatureFlagsEffects {
  constructor(
    private actions$: Actions,
    private featureFlagService: FeatureFlagService
  ) {}

  getFeatureFlags$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getFeatureFlags),
      mergeMap((action) =>
        this.featureFlagService
          .getFeatureFlags(
            action.featureFlagsRequest.storeId,
            action.featureFlagsRequest.ldUserKey
          )
          .pipe(
            map((featureFlags: FeatureFlags) =>
              fromActions.getFeatureFlagsSuccess(
                {
                  ...featureFlags,
                },
                action.correlationId
              )
            ),
            catchError((error) =>
              of(fromActions.getFeatureFlagsError(error, action.correlationId))
            )
          )
      )
    )
  );
}
