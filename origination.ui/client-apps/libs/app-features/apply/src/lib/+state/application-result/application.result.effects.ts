import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { ApplicationResult, ApplicationService } from '@ua/shared/data-access';
import * as fromApplicationResultActions from './application.result.actions';

@Injectable()
export class ApplicationResultEffects {
  constructor(
    private actions$: Actions,
    private applicationService: ApplicationService
  ) {}

  getApplicationResult$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromApplicationResultActions.getApplicationResultBySessionId),
      mergeMap((action) =>
        this.applicationService
          .getApplicationResultBySessionId(action.bySessionId.sessionId)
          .pipe(
            map((response) =>
              fromApplicationResultActions.getApplicationResultBySessionIdSuccess(
                response,
                action.correlationId
              )
            ),
            catchError((error) =>
              of(
                fromApplicationResultActions.getApplicationResultBySessionIdError(
                  error,
                  action.correlationId
                )
              )
            )
          )
      )
    )
  );

  submitApplication$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromApplicationResultActions.submitApplication),
      mergeMap((action) =>
        this.applicationService
          .submitApplication(
            action.submitApplicationPayload.sessionId,
            action.submitApplicationPayload.shortApp
          )
          .pipe(
            map((applcationResult: ApplicationResult) =>
              fromApplicationResultActions.submitApplicationSuccess(
                applcationResult,
                action.correlationId
              )
            ),
            catchError((error) =>
              of(
                fromApplicationResultActions.submitApplicationError(
                  error,
                  action.correlationId
                )
              )
            )
          )
      )
    )
  );
}
