import { createAction, union } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { generateId } from '@ua/shared/utilities';
import { BySessionId, ApplicationResult } from '@ua/shared/data-access';

export interface SubmitApplicationPayload {
  sessionId: string;
  shortApp: boolean;
}

const ApplicationPrefix = '[Application]';
export const getApplicationResultBySessionId = createAction(
  `${ApplicationPrefix} Get Application Result By SessionId`,
  (bySessionId: BySessionId, correlationId: string = generateId()) => ({
    bySessionId,
    correlationId,
  })
);

export const getApplicationResultBySessionIdSuccess = createAction(
  `${ApplicationPrefix} Get Application Result By SessionId Success`,
  (
    applicationResult: ApplicationResult,
    correlationId: string = generateId()
  ) => ({
    applicationResult,
    correlationId,
  })
);

export const getApplicationResultBySessionIdError = createAction(
  `${ApplicationPrefix} Get Application Result By SessionId Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const submitApplication = createAction(
  `${ApplicationPrefix} Submit Application`,
  (
    submitApplicationPayload: SubmitApplicationPayload,
    correlationId: string = generateId()
  ) => ({
    submitApplicationPayload,
    correlationId,
  })
);

export const submitApplicationSuccess = createAction(
  `${ApplicationPrefix} Submit Application Success`,
  (
    applicationResult: ApplicationResult,
    correlationId: string = generateId()
  ) => ({
    applicationResult,
    correlationId,
  })
);

export const submitApplicationError = createAction(
  `${ApplicationPrefix} Submit Application Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

const allActions = union({
  getApplicationResultBySessionId,
  getApplicationResultBySessionIdSuccess,
  getApplicationResultBySessionIdError,
  submitApplication,
  submitApplicationSuccess,
  submitApplicationError,
});

export type ApplicationResultActions = typeof allActions;
