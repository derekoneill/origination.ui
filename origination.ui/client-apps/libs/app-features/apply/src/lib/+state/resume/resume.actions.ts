import { createAction, union } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import {
  ResumeStateResponse,
  GetResumableStateBySessionRequest,
  GetResumableStateByAuthenticationIdRequest,
  ResumableState,
} from '@ua/shared/data-access';
import { generateId } from '@ua/shared/utilities';

const ResumePrefix = '[Resume-Application]';

export const getResumableStateBySession = createAction(
  `${ResumePrefix} Get Resumable State By Session`,
  (
    getResumableStateBySessionRequest: GetResumableStateBySessionRequest,
    correlationId: string = generateId()
  ) => ({
    getResumableStateBySessionRequest,
    correlationId,
  })
);

export const getResumableStateBySessionSuccess = createAction(
  `${ResumePrefix} Get Resumable State By Session Success`,
  (
    resumeStateResponse: ResumeStateResponse,
    correlationId: string = generateId()
  ) => ({
    resumeStateResponse,
    correlationId,
  })
);

export const getResumableStateBySessionError = createAction(
  `${ResumePrefix} Get Resumable State By Session Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const getResumableStateByAuthenticationId = createAction(
  `${ResumePrefix} Get Resumable State By AuthenticationId`,
  (
    getResumableStateByAuthenticationIdRequest: GetResumableStateByAuthenticationIdRequest,
    correlationId: string = generateId()
  ) => ({
    getResumableStateByAuthenticationIdRequest,
    correlationId,
  })
);

export const getResumableStateByAuthenticationIdSuccess = createAction(
  `${ResumePrefix} Get Resumable State By AuthenticationId Success`,
  (
    resumeStateResponse: ResumeStateResponse,
    correlationId: string = generateId()
  ) => ({
    resumeStateResponse,
    correlationId,
  })
);

export const getResumableStateByAuthenticationIdError = createAction(
  `${ResumePrefix} Get Resumable State By AuthenticationId Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const saveResumableState = createAction(
  `${ResumePrefix} Save Resumable State`,
  (resumableState: ResumableState, correlationId: string = generateId()) => ({
    resumableState,
    correlationId,
  })
);

export const saveResumableStateSuccess = createAction(
  `${ResumePrefix} Save Resumable State Success`,
  (resumableState: ResumableState, correlationId: string = generateId()) => ({
    resumableState,
    correlationId,
  })
);

export const saveResumableStateError = createAction(
  `${ResumePrefix} Save Resumable State Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

const allActions = union({
  getResumableStateBySession,
  getResumableStateBySessionSuccess,
  getResumableStateBySessionError,
  getResumableStateByAuthenticationId,
  getResumableStateByAuthenticationIdSuccess,
  getResumableStateByAuthenticationIdError,
  saveResumableState,
  saveResumableStateSuccess,
  saveResumableStateError,
});

export type ResumeActions = typeof allActions;
