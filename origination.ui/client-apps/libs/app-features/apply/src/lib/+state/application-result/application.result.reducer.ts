import {
  ApplicationApprovalStatus,
  ApplicationResult,
} from '@ua/shared/data-access';
import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import * as fromActions from './application.result.actions';

export interface ApplicationResultState {
  applicationResult: ApplicationResult;
  isLoaded: boolean;
  error?: HttpErrorResponse;
}

export function initApplicationResult(
  overrides: Partial<ApplicationResultState> = {}
): ApplicationResultState {
  return {
    applicationResult: {
      sessionId: '',
      applicationApprovalStatus: ApplicationApprovalStatus.PreApproved,
      approvalAmount: 0,
      approvalDateUtc: undefined,
      approved: false,
      initialPaymentAmount: 0,
      leaseId: 0,
      eSignUrl: '',
    },
    isLoaded: false,
    ...overrides,
  };
}

export const getOrSubmitApplicationResultBySessionIdSuccessHandler = (
  state: ApplicationResultState,
  action
): ApplicationResultState => ({
  ...state,
  applicationResult: {
    ...action.applicationResult,
  },
  isLoaded: true,
});

export const getOrSubmitApplicationResultBySessionIdErrorHandler = (
  state: ApplicationResultState,
  action
): ApplicationResultState => ({
  ...state,
  error: action.error,
  isLoaded: true,
});

export const reducer = createReducer(
  initApplicationResult(),
  on(
    fromActions.getApplicationResultBySessionIdSuccess,
    fromActions.submitApplicationSuccess,
    getOrSubmitApplicationResultBySessionIdSuccessHandler
  ),
  on(
    fromActions.getApplicationResultBySessionIdError,
    fromActions.submitApplicationError,
    getOrSubmitApplicationResultBySessionIdErrorHandler
  )
);
