import { Order } from '@ua/shared/data-access';
import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import * as fromActions from './order.actions';

export interface OrderState {
  order: Order;
  isLoaded: boolean;
  error?: HttpErrorResponse;
}

export function initOrder(overrides: Partial<OrderState> = {}): OrderState {
  return {
    order: {
      subTotal: 0,
      orderLines: [],
      orderAmount: 0,
      orderTaxAmount: 0,
    },
    isLoaded: false,
    ...overrides,
  };
}

export const getOrderBySessionIdSuccessHandler = (
  state: OrderState,
  action
): OrderState => ({
  ...state,
  order: action.order,
  isLoaded: true,
});

export const getOrderBySessionIdErrorHandler = (
  state: OrderState,
  action
): OrderState => ({
  ...state,
  error: action.error,
  isLoaded: true,
});

export const reducer = createReducer(
  initOrder(),
  on(fromActions.getOrderBySessionIdSuccess, getOrderBySessionIdSuccessHandler),
  on(fromActions.getOrderBySessionIdError, getOrderBySessionIdErrorHandler)
);
