import { createAction, union } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import {
  BySessionId,
  PaymentInformation,
  CreditCard,
} from '@ua/shared/data-access';
import { generateId } from '@ua/shared/utilities';

const PaymentPrefix = '[Payment]';

export const getPaymentInformationBySessionId = createAction(
  `${PaymentPrefix} Get Payment Information By Session`,
  (bySessionId: BySessionId, correlationId: string = generateId()) => ({
    bySessionId,
    correlationId,
  })
);

export const getPaymentInformationBySessionIdSuccess = createAction(
  `${PaymentPrefix} Get Payment Information By Session Success`,
  (
    paymentInformation: PaymentInformation,
    correlationId: string = generateId()
  ) => ({
    paymentInformation,
    correlationId,
  })
);

export const getPaymentInformationBySessionIdError = createAction(
  `${PaymentPrefix} Get Payment Information By Session Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const updatePaymentInformation = createAction(
  `${PaymentPrefix} Update Payment Information`,
  (
    paymentInformation: PaymentInformation,
    correlationId: string = generateId()
  ) => ({
    paymentInformation,
    correlationId,
  })
);

export const updatePaymentInformationSuccess = createAction(
  `${PaymentPrefix} Update Payment Information Success`,
  (
    paymentInformation: PaymentInformation,
    correlationId: string = generateId()
  ) => ({
    paymentInformation,
    correlationId,
  })
);

export const updatePaymentInformationError = createAction(
  `${PaymentPrefix} Update Payment Information Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const submitPayment = createAction(
  `${PaymentPrefix} Submit Payment`,
  (
    paymentInformation: PaymentInformation,
    correlationId: string = generateId()
  ) => ({
    paymentInformation,
    correlationId,
  })
);

export const submitPaymentSuccess = createAction(
  `${PaymentPrefix} Submit Payment Success`,
  (
    paymentInformation: PaymentInformation,
    correlationId: string = generateId()
  ) => ({
    paymentInformation,
    correlationId,
  })
);

export const submitPaymentError = createAction(
  `${PaymentPrefix} Submit Payment Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const submitCreditCardInformation = createAction(
  `${PaymentPrefix} Submit Credit Card Information`,
  (creditCard: CreditCard, correlationId: string = generateId()) => ({
    creditCard,
    correlationId,
  })
);

export const submitCreditCardInformationSuccess = createAction(
  `${PaymentPrefix} Submit Credit Card Information Success`,
  (creditCard: CreditCard, correlationId: string = generateId()) => ({
    creditCard,
    correlationId,
  })
);

export const submitCreditCardInformationError = createAction(
  `${PaymentPrefix} Submit Credit Card Information Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

const allActions = union({
  getPaymentInformationBySessionId,
  getPaymentInformationBySessionIdSuccess,
  getPaymentInformationBySessionIdError,
  updatePaymentInformation,
  updatePaymentInformationSuccess,
  updatePaymentInformationError,
  submitPayment,
  submitPaymentSuccess,
  submitPaymentError,
  submitCreditCardInformation,
  submitCreditCardInformationSuccess,
  submitCreditCardInformationError,
});

export type PaymentActions = typeof allActions;
