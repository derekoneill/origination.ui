import * as fromConfig from '.';
import * as fromRoot from '..';
import { IConfig } from '@ua/shared/data-access';

describe('config state reducer', () => {
  it('should handle the SetConfig action correctly', () => {
    const initConfig = <IConfig>{
      contentApiUrl: 'https://content/api',
      originApiUrl: 'https://origination/api',
      sessionTimeOutSeconds: 900,
      sessionTimeOutWarningSeconds: 60,
      storesBlockingUsedItems: [6666],
      eCommerceApiUrl: 'https://vdc-qaswebapp13.stormwind.local/eComApi',
      paymentServiceApiUrl: 'https://vdc-qaswebapp13.stormwind.local/eComUI',
      appInsightsKey: 'some-insights-keys',
    };
    let config = fromConfig.initConfig(initConfig);
    expect(config).toEqual(initConfig);
    config = fromConfig.reducer(
      undefined,
      fromConfig.setConfig(<IConfig>{
        contentApiUrl: 'https://content/api',
        originApiUrl: 'https://origination/api',
        sessionTimeOutSeconds: 300,
        sessionTimeOutWarningSeconds: 50,
        storesBlockingUsedItems: [55555],
        eCommerceApiUrl:
          'https://vdc-qaswebapp13.stormwind.local/changedEcomApi',
        paymentServiceApiUrl:
          'https://vdc-qaswebapp13.stormwind.local/changedEComUI',
        appInsightsKey: 'changed-some-insights-keys',
      })
    );
    expect(config.sessionTimeOutSeconds).toEqual(300);
    expect(config.sessionTimeOutWarningSeconds).toEqual(50);
    expect(config.storesBlockingUsedItems).toEqual([55555]);
    expect(config.eCommerceApiUrl).toEqual(
      'https://vdc-qaswebapp13.stormwind.local/changedEcomApi'
    );
    expect(config.paymentServiceApiUrl).toEqual(
      'https://vdc-qaswebapp13.stormwind.local/changedEComUI'
    );
    expect(config.appInsightsKey).toEqual('changed-some-insights-keys');
  });
});
