import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { NativeService } from '@ua/shared/data-access';
import * as fromActions from './analytic.actions';

@Injectable()
export class AnalyticEffects {
  constructor(
    private actions$: Actions,
    private nativeService: NativeService
  ) {}

  reportAnalyticEvent$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(
        fromActions.alertEvent,
        fromActions.applicationExitEvent,
        fromActions.applicationResumeErrorEvent,
        fromActions.applicationResumeEvent,
        fromActions.applicationResumeSuccessEvent,
        fromActions.applicationStartErrorEvent,
        fromActions.applicationStartEvent,
        fromActions.applicationStartSuccessEvent,
        fromActions.applicationSubmissionErrorEvent,
        fromActions.applicationSubmissionEvent,
        fromActions.buttonClickEvent,
        fromActions.errorEvent,
        fromActions.formElementInteractionErrorEvent,
        fromActions.formElementInteractionEvent,
        fromActions.formFieldError,
        fromActions.leaseCreationEvent,
        fromActions.modalOpenEvent,
        fromActions.navigateLinkClickEvent,
        fromActions.otpCodeEntryEvent,
        fromActions.otpErrorEvent,
        fromActions.otpResendCodeEvent,
        fromActions.otpSendCodeEvent,
        fromActions.otpSsnEntryEvent,
        fromActions.pageViewEvent,
        fromActions.formFillEvent,
        fromActions.paymentEstimatorErrorEvent,
        fromActions.paymentEstimatorEvent,
        fromActions.paymentEstimatorFormFillEvent,
        fromActions.paymentEstimatorSliderChangeEvent,
        fromActions.paymentEstimatorSuccessEvent,
        fromActions.sessionTimeoutErrorEvent,
        fromActions.valuePropScrollEvent
      ),
      mergeMap((action) => {
        this.nativeService.window.dataLayer.push({
          ...action,
        });
        return of(<Action>{ type: '[Analytic] Report Analytic Event Success' });
      })
    )
  );
}
