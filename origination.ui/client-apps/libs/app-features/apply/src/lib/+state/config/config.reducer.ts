import { IConfig } from '@ua/shared/data-access';
import { createReducer, on } from '@ngrx/store';
import * as fromActions from './config.actions';

export function initConfig(overides: Partial<IConfig> = {}): IConfig {
  return {
    contentApiUrl: '',
    originApiUrl: '',
    eCommerceApiUrl: '',
    paymentServiceApiUrl: '',
    appInsightsKey: '',
    sessionTimeOutSeconds: 0,
    sessionTimeOutWarningSeconds: 0,
    storesBlockingUsedItems: [],
    ...overides,
  };
}

export const setConfigHandler = (state: IConfig, action): IConfig => ({
  ...state,
  ...action.config,
});

export const reducer = createReducer(
  initConfig(),
  on(fromActions.setConfig, setConfigHandler)
);
