import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import * as fromActions from './feature.flags.actions';

export interface FeatureFlagsState {
  isShortApp: boolean;
  isLoaded: boolean;
  monthlyPayFreqDisabled: boolean;
  skipCreditCard: boolean;
  error?: HttpErrorResponse;
}

export function initFeatureFlags(
  overrides: Partial<FeatureFlagsState> = {}
): FeatureFlagsState {
  return {
    isShortApp: false,
    skipCreditCard: false,
    monthlyPayFreqDisabled: false,
    isLoaded: false,
    ...overrides,
  };
}

export const getFeatureFlagsSuccessHandler = (
  state: FeatureFlagsState,
  action
): FeatureFlagsState => ({
  ...state,
  isShortApp: action.featureFlags.isShortApp,
  monthlyPayFreqDisabled: action.featureFlags.monthlyPayFreqDisabled,
  skipCreditCard: action.featureFlags.skipCreditCard,
  isLoaded: true,
});

export const getFeatureFlagsErrorHandler = (
  state: FeatureFlagsState,
  action
): FeatureFlagsState => ({
  ...state,
  error: action.error,
  isLoaded: true,
});

export const reducer = createReducer(
  initFeatureFlags(),
  on(fromActions.getFeatureFlagsSuccess, getFeatureFlagsSuccessHandler),
  on(fromActions.getFeatureFlagsError, getFeatureFlagsErrorHandler)
);
