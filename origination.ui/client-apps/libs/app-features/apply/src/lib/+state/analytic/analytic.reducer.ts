import { createReducer, on } from '@ngrx/store';
import * as fromActions from './analytic.actions';

const persistAnalyticEvents = 10;

const initialized = [];

export const reducer = createReducer(
  initialized,
  on(
    fromActions.alertEvent,
    fromActions.applicationExitEvent,
    fromActions.applicationResumeErrorEvent,
    fromActions.applicationResumeEvent,
    fromActions.applicationResumeSuccessEvent,
    (state: Array<{ [key: string]: any }>, action) => [
      ...(state.length >= persistAnalyticEvents ? state.slice(1) : state),
      action,
    ]
  ),
  on(
    fromActions.applicationStartErrorEvent,
    fromActions.applicationStartEvent,
    fromActions.applicationStartSuccessEvent,
    fromActions.applicationSubmissionErrorEvent,
    fromActions.applicationSubmissionEvent,
    (state: Array<{ [key: string]: any }>, action) => [
      ...(state.length >= persistAnalyticEvents ? state.slice(1) : state),
      action,
    ]
  ),
  on(
    fromActions.buttonClickEvent,
    fromActions.errorEvent,
    fromActions.formElementInteractionErrorEvent,
    fromActions.formElementInteractionEvent,
    fromActions.formFieldError,
    (state: Array<{ [key: string]: any }>, action) => [
      ...(state.length >= persistAnalyticEvents ? state.slice(1) : state),
      action,
    ]
  ),
  on(
    fromActions.leaseCreationEvent,
    fromActions.modalOpenEvent,
    fromActions.navigateLinkClickEvent,
    fromActions.otpCodeEntryEvent,
    fromActions.otpErrorEvent,
    (state: Array<{ [key: string]: any }>, action) => [
      ...(state.length >= persistAnalyticEvents ? state.slice(1) : state),
      action,
    ]
  ),
  on(
    fromActions.otpResendCodeEvent,
    fromActions.otpSendCodeEvent,
    fromActions.otpSsnEntryEvent,
    fromActions.pageViewEvent,
    fromActions.formFillEvent,
    (state: Array<{ [key: string]: any }>, action) => [
      ...(state.length >= persistAnalyticEvents ? state.slice(1) : state),
      action,
    ]
  ),
  on(
    fromActions.paymentEstimatorErrorEvent,
    fromActions.paymentEstimatorEvent,
    fromActions.paymentEstimatorFormFillEvent,
    fromActions.paymentEstimatorSliderChangeEvent,
    fromActions.paymentEstimatorSuccessEvent,
    (state: Array<{ [key: string]: any }>, action) => [
      ...(state.length >= persistAnalyticEvents ? state.slice(1) : state),
      action,
    ]
  ),
  on(
    fromActions.sessionTimeoutErrorEvent,
    fromActions.valuePropScrollEvent,
    (state: Array<{ [key: string]: any }>, action) => [
      ...(state.length >= persistAnalyticEvents ? state.slice(1) : state),
      action,
    ]
  )
);
