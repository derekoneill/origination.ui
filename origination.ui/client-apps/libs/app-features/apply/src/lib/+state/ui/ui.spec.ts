import * as fromUi from '.';

describe('ui state reducer', () => {
  let uiState: fromUi.UiState;
  beforeEach(() => {
    uiState = fromUi.initUi();
  });

  it('should handle the SetUi action correctly', () => {
    uiState = fromUi.reducer(
      uiState,
      fromUi.setUiState({
        windowType: fromUi.WindowType.WINDOW,
        ecommercePlatform: 'ecom',
        checkoutUrl: 'checkout-url',
        completeUrl: 'completed-url',
        returnToCheckoutUrl: 'returned-checkout-url',
        applyOnly: false,
        returnToMerchantUrl: 'returned-merchant-url',
        correlationSessionId: 'session-id',
        compositeId: 'composite-id',
        resultType: fromUi.ResultType.ApprovedNoItemsInCart,
        viewPort: fromUi.ViewPortType.XS,
      })
    );
    expect(uiState.applyOnly).toBeFalsy();
    expect(uiState.windowType).toEqual(fromUi.WindowType.WINDOW);
    expect(uiState.ecommercePlatform).toEqual('ecom');
    expect(uiState.checkoutUrl).toEqual('checkout-url');
    expect(uiState.completeUrl).toEqual('completed-url');
    expect(uiState.returnToCheckoutUrl).toEqual('returned-checkout-url');
    expect(uiState.returnToMerchantUrl).toEqual('returned-merchant-url');
    expect(uiState.compositeId).toEqual('composite-id');
    expect(uiState.resultType).toEqual(fromUi.ResultType.ApprovedNoItemsInCart);
    expect(uiState.viewPort).toEqual(fromUi.ViewPortType.XS);
  });
});
