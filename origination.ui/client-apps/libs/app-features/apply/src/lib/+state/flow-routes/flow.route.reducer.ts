import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import * as fromActions from './flow.route.actions';

export interface FlowRouteState {
  route: string;
  error?: HttpErrorResponse;
}

export function initFlowRoutes(
  overrides: Partial<FlowRouteState> = {}
): FlowRouteState {
  return {
    route: '',
    ...overrides,
  };
}

export const changeApplyFlowRouteSuccessHandler = (
  state: FlowRouteState,
  action
): FlowRouteState => ({
  route: action.route,
});

export const changeApplyFlowRouteErrorHandler = (
  state: FlowRouteState,
  action
): FlowRouteState => ({
  ...state,
  error: action.error,
});

export const reducer = createReducer(
  initFlowRoutes(),
  on(
    fromActions.changeApplyFlowRouteSuccess,
    changeApplyFlowRouteSuccessHandler
  ),
  on(fromActions.changeApplyFlowRouteError, changeApplyFlowRouteErrorHandler)
);
