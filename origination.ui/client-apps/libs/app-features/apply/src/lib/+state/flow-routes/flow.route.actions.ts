import { createAction, union } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { generateId } from '@ua/shared/utilities';

export enum RouteDirection {
  Back = 0,
  Next = 1,
}

export interface RoutePayload {
  from: string;
  direction: RouteDirection;
  params: { [key: string]: string };
}

const ApplyFlowPrefix = '[Apply-Flow]';
export const changeApplyFlowRoute = createAction(
  `${ApplyFlowPrefix} Change Route`,
  (routePayload: RoutePayload, correlationId: string = generateId()) => ({
    routePayload,
    correlationId,
  })
);

export const changeApplyFlowRouteSuccess = createAction(
  `${ApplyFlowPrefix} Change Route Success`,
  (route: string, correlationId: string = generateId()) => ({
    route,
    correlationId,
  })
);

export const changeApplyFlowRouteError = createAction(
  `${ApplyFlowPrefix} Change Route Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

const allActions = union({
  changeApplyFlowRoute,
  changeApplyFlowRouteSuccess,
  changeApplyFlowRouteError,
});
