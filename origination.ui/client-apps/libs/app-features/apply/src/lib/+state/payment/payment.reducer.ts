import { PayFrequency, PaymentInformation } from '@ua/shared/data-access';
import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import * as fromActions from './payment.actions';

export interface PaymentState {
  paymentInformation: PaymentInformation;
  isLoaded: boolean;
  error?: HttpErrorResponse;
}

export function initPayment(
  overrides: Partial<PaymentState> = {}
): PaymentState {
  return {
    paymentInformation: {
      lastPayDate: undefined,
      leaseId: 0,
      sessionId: '',
      nextPayDate: undefined,
      paymentFrequency: PayFrequency.EveryOtherWeek,
      street1: '',
      street2: '',
      state: '',
      city: '',
      zip: '',
    },
    isLoaded: false,
    ...overrides,
  };
}

export const getPaymentInformationBySessionIdSuccessHandler = (
  state: PaymentState,
  action
): PaymentState => ({
  ...state,
  paymentInformation: action.paymentInformation,
  isLoaded: true,
});

export const getPaymentInformationBySessionIdErrorHandler = (
  state: PaymentState,
  action
): PaymentState => ({
  ...state,
  error: action.error,
  isLoaded: true,
});

export const updatePaymentInformationSuccessHandler = (
  state: PaymentState,
  action
): PaymentState => ({
  ...state,
  paymentInformation: action.paymentInformation,
});

export const updatePaymentInformationErrorHandler = (
  state: PaymentState,
  action
): PaymentState => ({
  ...state,
  error: action.error,
});

export const reducer = createReducer(
  initPayment(),
  on(
    fromActions.getPaymentInformationBySessionIdSuccess,
    getPaymentInformationBySessionIdSuccessHandler
  ),
  on(
    fromActions.getPaymentInformationBySessionIdError,
    getPaymentInformationBySessionIdErrorHandler
  ),
  on(
    fromActions.updatePaymentInformationSuccess,
    updatePaymentInformationSuccessHandler
  ),
  on(
    fromActions.updatePaymentInformationError,
    updatePaymentInformationErrorHandler
  )
);
