import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { ResumeService } from '@ua/shared/data-access';
import * as fromResumeActions from './resume.actions';

@Injectable()
export class ResumeEffects {
  constructor(
    private actions$: Actions,
    private resumeService: ResumeService
  ) {}

  getResumableStateBySession$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromResumeActions.getResumableStateBySession),
      mergeMap((action) =>
        this.resumeService
          .getResumableStateBySession(action.getResumableStateBySessionRequest)
          .pipe(
            map((response) =>
              fromResumeActions.getResumableStateBySessionSuccess(
                response,
                action.correlationId
              )
            ),
            catchError((error) =>
              of(
                fromResumeActions.getResumableStateBySessionError(
                  error,
                  action.correlationId
                )
              )
            )
          )
      )
    )
  );

  getResumableStateByAuthenticationId$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromResumeActions.getResumableStateByAuthenticationId),
      mergeMap((action) =>
        this.resumeService
          .getResumableStateByAuthenticationId(
            action.getResumableStateByAuthenticationIdRequest
          )
          .pipe(
            map((response) =>
              fromResumeActions.getResumableStateByAuthenticationIdSuccess(
                response,
                action.correlationId
              )
            ),
            catchError((error) =>
              of(
                fromResumeActions.getResumableStateByAuthenticationIdError(
                  error,
                  action.correlationId
                )
              )
            )
          )
      )
    )
  );

  saveResumableState$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromResumeActions.saveResumableState),
      mergeMap((action) =>
        this.resumeService.saveResumableState(action.resumableState).pipe(
          map((_) =>
            fromResumeActions.saveResumableStateSuccess(
              action.resumableState,
              action.correlationId
            )
          ),
          catchError((error) =>
            of(
              fromResumeActions.saveResumableStateError(
                error,
                action.correlationId
              )
            )
          )
        )
      )
    )
  );
}
