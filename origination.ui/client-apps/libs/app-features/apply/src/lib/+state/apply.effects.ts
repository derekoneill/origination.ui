import { Injectable } from '@angular/core';
import { AuthenticationService, ResumeService } from '@ua/shared/data-access';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Observable, of, race, forkJoin, throwError } from 'rxjs';
import { Action } from '@ngrx/store';
import { catchError, map, filter, first, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import {
  AuthenticationMethod,
  BySessionId,
  Application,
  getFormattedContactInfo,
} from '@ua/shared/data-access';
import { CookieService } from 'ngx-cookie-service';
import { UnifiedApps } from '@ua/shared/state';
import * as fromActions from './apply.actions';
import * as fromApplyActions from './apply.actions';
import * as fromFeatureFlags from './feature-flags';
import * as fromFlowRoutes from './flow-routes';
import * as fromApplication from './application';
import * as fromResume from './resume';
import * as fromOrder from './order';
import * as fromSession from './session';
import * as fromPayment from './payment';
import * as fromApplicationResult from './application-result';
import * as fromSharedState from '@ua/shared/state';
import * as fromAuthentications from './authentication';

@Injectable()
export class ApplyEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private authenticationService: AuthenticationService,
    private resumeService: ResumeService,
    private cookieSession: CookieService
  ) {}

  walkBackInApplyFlow$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.walkBackInApplyFlow),
      switchMap((action) => {
        const actions = [];
        if (
          action.navigateRoute.route
            .toLowerCase()
            .startsWith(
              `/${UnifiedApps.apply.route}/${UnifiedApps.apply.children.getStarted.route}`
            )
        ) {
          actions.push(
            fromSharedState.navigateRoute({
              route: `${UnifiedApps.comprehension.route}/${UnifiedApps.comprehension.children.termsSheet.route}`,
              params: {},
            })
          );
        }
        return actions;
      })
    )
  );

  saveNewSessionInCookieForResumable$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromApplyActions.saveNewSessionInCookieForResumable),
      switchMap((action) => {
        this.cookieSession.set(
          'sessionId',
          action.bySessionId.sessionId,
          0,
          '/'
        );
        return [];
      })
    )
  );

  /**
   * now we get the resumable state successfully and know that the previous application is
   * resumable, so we will start the resuming process from here.
   */
  resumeApplicationByLoginSplitter: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromApplyActions.resumeApplicationByLoginSplitter),
      switchMap((action) => {
        const actions: Action[] = [];
        const bySessionId: BySessionId = {
          sessionId: action.resumeStateResponse.sessionId,
        };
        actions.push(fromSession.getSession(bySessionId, action.correlationId));
        actions.push(
          fromApplication.getApplicationBySessionId(
            bySessionId,
            action.correlationId
          )
        );
        actions.push(
          fromApplicationResult.getApplicationResultBySessionId(
            bySessionId,
            action.correlationId
          )
        );
        actions.push(
          fromPayment.getPaymentInformationBySessionId(
            bySessionId,
            action.correlationId
          )
        );
        actions.push(
          fromOrder.getOrderBySessionId(bySessionId, action.correlationId)
        );
        return actions;
      })
    )
  );

  resumeApplicationByLoginSplitterAggregator$: Observable<
    Action
  > = createEffect(() =>
    this.actions$.pipe(
      ofType(fromApplyActions.resumeApplicationByLoginSplitter),
      switchMap((action) => {
        const session$ = this.actions$.pipe(
          ofType(fromSession.getSessionSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const application$ = this.actions$.pipe(
          ofType(fromApplication.getApplicationBySessionIdSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const applicationResult$ = this.actions$.pipe(
          ofType(fromApplicationResult.getApplicationResultBySessionIdSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const payment$ = this.actions$.pipe(
          ofType(fromPayment.getPaymentInformationBySessionIdSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const order$ = this.actions$.pipe(
          ofType(fromOrder.getOrderBySessionIdSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const failed$ = this.actions$.pipe(
          ofType(
            fromSession.getSessionError,
            fromApplication.getApplicationBySessionIdError,
            fromApplicationResult.getApplicationResultBySessionIdError,
            fromPayment.getPaymentInformationBySessionIdError,
            fromOrder.getOrderBySessionIdError
          ),
          filter((a) => a.correlationId === action.correlationId),
          first(),
          switchMap((error) => throwError(error))
        );

        return race(
          forkJoin({
            session: session$,
            application: application$,
            applicationResult: applicationResult$,
            payment: payment$,
            order: order$,
          }),
          failed$
        ).pipe(
          map((_) =>
            fromApplyActions.resumeApplicationByLoginAggregatorSuccess(
              action.resumeStateResponse,
              action.correlationId
            )
          ),
          // There is something broken, let redirect to the broken page.
          catchError((error) =>
            of(
              fromApplyActions.resumeApplicationByLoginAggregatorError(
                error,
                action.correlationId
              )
            )
          )
        );
      })
    )
  );

  resumeApplicationByLoginSplitterAggregatorSuccess$: Observable<
    Action
  > = createEffect(() =>
    this.actions$.pipe(
      ofType(fromApplyActions.resumeApplicationByLoginAggregatorSuccess),
      switchMap((action) => {
        const actions: Action[] = [];
        if (
          action.resumeStateResponse.resumable &&
          action.resumeStateResponse.resumeData &&
          action.resumeStateResponse.resumeData.route
        ) {
          // application is resumable and resume data looks good, let navigate to where the user left off last time.
          actions.push(
            fromSharedState.navigateRoute(
              {
                route: action.resumeStateResponse.resumeData.route,
                params: { sid: action.resumeStateResponse.sessionId },
              },
              action.correlationId
            )
          );
        } else {
          // application is not resumable, notify the user there was something broken and ask the user
          // to restart the new application.
          actions.push(
            fromSharedState.navigateRoute(
              <fromSharedState.NavigateRoutePayload>{
                route: `${UnifiedApps.apply.route}/${UnifiedApps.apply.children.somethingBroke}`,
                params: {},
              },
              action.correlationId
            )
          );
        }
        return actions;
      })
    )
  );

  resumeApplicationByLoginSplitterAggregatorError$: Observable<
    Action
  > = createEffect(() =>
    this.actions$.pipe(
      ofType(fromApplyActions.resumeApplicationByLoginAggregatorError),
      map((action) =>
        fromSharedState.navigateRoute(
          <fromSharedState.NavigateRoutePayload>{
            route: `${UnifiedApps.apply.route}/${UnifiedApps.apply.children.somethingBroke}`,
            params: {},
          },
          action.correlationId
        )
      )
    )
  );

  fromInitSession$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromSession.initSession),
      switchMap((action) => {
        const success$ = this.actions$.pipe(
          ofType(fromSession.initSessionSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const failed$ = this.actions$.pipe(
          ofType(fromSession.initSessionError),
          filter((a) => a.correlationId === action.correlationId),
          first(),
          switchMap((error) => throwError(error))
        );

        return race(success$, failed$).pipe(
          switchMap((success) => {
            const actions: Action[] = [];

            // generate empty application for this new session and order.
            const newSessionAppSplitterPayload: fromAuthentications.VerifyAuthenticationCodeSplitterPayload = {
              sessionId: success.bySessionId.sessionId,
              authenticationId: action.initSessionRequest.authenticationId,
              authenticationCode: action.initSessionRequest.authenticationCode,
              storeId: action.initSessionRequest.storeId,
            };
            actions.push(
              fromApplyActions.startNewSessionAndApplicationSplitter(
                newSessionAppSplitterPayload,
                action.correlationId
              )
            );
            actions.push(
              fromApplyActions.saveNewSessionInCookieForResumable(
                success.bySessionId,
                action.correlationId
              )
            );

            return actions;
          }),
          catchError((error) =>
            of(
              fromApplyActions.startNewSessionAndApplicationAggregatorError(
                error,
                action.correlationId
              )
            )
          )
        );
      })
    )
  );

  startNewSessionAndApplicationSplitter$: Observable<Action> = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromApplyActions.startNewSessionAndApplicationSplitter),
        switchMap((action) => {
          const actions: Action[] = [];
          const bySessionId: BySessionId = {
            sessionId: action.verifyAuthCodeSplitterPayload.sessionId,
          };
          const { contactMethod, contactId } = getFormattedContactInfo(
            action.verifyAuthCodeSplitterPayload.authenticationId
          );
          const newApplication: Application = <Application>{
            sessionId: action.verifyAuthCodeSplitterPayload.sessionId,
            phoneNumber:
              contactMethod === AuthenticationMethod.SMS
                ? contactId.replace('+1', '')
                : '',
            emailAddress:
              contactMethod === AuthenticationMethod.Email ? contactId : '',
            defaultContactMechanism: contactMethod,
          };
          actions.push(
            fromApplication.updateApplication(
              newApplication,
              action.correlationId
            )
          );
          actions.push(
            fromOrder.getOrderBySessionId(bySessionId, action.correlationId)
          );
          return actions;
        })
      )
  );

  startNewSessionAndApplicationAggregator$: Observable<Action> = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromApplyActions.startNewSessionAndApplicationSplitter),
        switchMap((action) => {
          const updateApplication$ = this.actions$.pipe(
            ofType(fromApplication.updateApplicationSuccess),
            filter((a) => a.correlationId === action.correlationId),
            first()
          );

          const getOrder$ = this.actions$.pipe(
            ofType(fromOrder.getOrderBySessionIdSuccess),
            filter((a) => a.correlationId === action.correlationId),
            first()
          );

          const failed$ = this.actions$.pipe(
            ofType(
              fromApplication.updateApplicationError,
              fromOrder.getOrderBySessionIdError
            ),
            filter((a) => a.correlationId === action.correlationId),
            first(),
            switchMap((error) => throwError(error))
          );

          return race(
            forkJoin({
              application: updateApplication$,
              order: getOrder$,
            }),
            failed$
          ).pipe(
            map((_) =>
              fromApplyActions.startNewSessionAndApplicationAggregatorSuccess(
                <BySessionId>{
                  sessionId: action.verifyAuthCodeSplitterPayload.sessionId,
                },
                action.correlationId
              )
            ),
            catchError((error) =>
              of(
                fromApplyActions.startNewSessionAndApplicationAggregatorError(
                  error,
                  action.correlationId
                )
              )
            )
          );
        })
      )
  );

  startNewSessionAndApplicationAggregatorSuccess$: Observable<
    Action
  > = createEffect(() =>
    this.actions$.pipe(
      ofType(fromApplyActions.startNewSessionAndApplicationAggregatorSuccess),
      switchMap((action) => {
        const actions: Action[] = [];
        actions.push(
          fromFlowRoutes.changeApplyFlowRoute(
            {
              from: UnifiedApps.apply.children.verifyCode.route,
              direction: fromFlowRoutes.RouteDirection.Next,
              params: { sid: action.bySessionId.sessionId },
            },
            action.correlationId
          )
        );
        actions.push(
          fromAuthentications.resetAuthenticationCodeAndSsnWhenDoneMoveAwayFromVerifyCodePage(
            action.correlationId
          )
        );
        return actions;
      })
    )
  );

  saveApplicationResumableStateSplitter$: Observable<Action> = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromApplyActions.saveApplicationResumableStateSplitter),
        switchMap((action) => {
          const actions: Action[] = [];
          actions.push(
            fromApplication.updateApplication(
              action.saveApplicationResumableStateSplitterPayload.application,
              action.correlationId
            )
          );
          actions.push(
            fromResume.saveResumableState(
              action.saveApplicationResumableStateSplitterPayload
                .resumableState,
              action.correlationId
            )
          );
          return actions;
        })
      )
  );

  saveApplicationResumableStateAggregator$: Observable<Action> = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromApplyActions.saveApplicationResumableStateSplitter),
        switchMap((action) => {
          const updateApplication$ = this.actions$.pipe(
            ofType(fromApplication.updateApplicationSuccess),
            filter((a) => a.correlationId === action.correlationId),
            first()
          );
          const saveResumable$ = this.actions$.pipe(
            ofType(fromResume.saveResumableStateSuccess),
            filter((a) => a.correlationId === action.correlationId),
            first()
          );
          const failed$ = this.actions$.pipe(
            ofType(
              fromApplication.updateApplicationError,
              fromResume.saveResumableStateError
            ),
            filter((a) => a.correlationId === action.correlationId),
            first(),
            switchMap((error) => throwError(error))
          );
          return race(
            forkJoin({
              updateApplication: updateApplication$,
              saveResumable: saveResumable$,
            }),
            failed$
          ).pipe(
            map((_) =>
              fromApplyActions.saveApplicationResumableStateAggregatorSuccess(
                action.saveApplicationResumableStateSplitterPayload.nextRoute,
                action.correlationId
              )
            ),
            catchError((error) =>
              of(
                fromApplyActions.saveApplicationResumableStateAggregatorError(
                  error,
                  action.correlationId
                )
              )
            )
          );
        })
      )
  );

  saveApplicationResumableStateAggregatorSuccess$: Observable<
    Action
  > = createEffect(() =>
    this.actions$.pipe(
      ofType(fromApplyActions.saveApplicationResumableStateAggregatorSuccess),
      map((action) =>
        fromFlowRoutes.changeApplyFlowRoute(
          action.routePayload,
          action.correlationId
        )
      )
    )
  );

  resumeApplicationBySessionSplitter$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.resumeApplicationBySessionSplitter),
      switchMap((action) => {
        const actions: Action[] = [];
        const bySessionId: BySessionId = {
          sessionId:
            action.resumeApplicationBySessionPayload.application.sessionId,
        };
        const authId =
          action.resumeApplicationBySessionPayload.application
            .defaultContactMechanism === 0
            ? action.resumeApplicationBySessionPayload.application.phoneNumber
            : action.resumeApplicationBySessionPayload.application.emailAddress;
        actions.push(
          fromFeatureFlags.getFeatureFlags(
            <fromFeatureFlags.FeatureFlagsRequest>{
              storeId: action.resumeApplicationBySessionPayload.session.storeId,
              ldUserKey: authId,
            },
            action.correlationId
          )
        );
        actions.push(
          fromOrder.getOrderBySessionId(bySessionId, action.correlationId)
        );
        actions.push(
          fromApplicationResult.getApplicationResultBySessionId(
            bySessionId,
            action.correlationId
          )
        );
        actions.push(
          fromPayment.getPaymentInformationBySessionId(
            bySessionId,
            action.correlationId
          )
        );
        actions.push(
          fromResume.getResumableStateBySession(
            {
              sessionId: bySessionId.sessionId,
              originalSessionId:
                action.resumeApplicationBySessionPayload.session
                  .originalSessionId,
              shortAppEnabled: false,
            },
            action.correlationId
          )
        );
        return actions;
      })
    )
  );

  resumeApplicationBySessionAggregator$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.resumeApplicationBySessionSplitter),
      switchMap((action) => {
        const feature$ = this.actions$.pipe(
          ofType(fromFeatureFlags.getFeatureFlagsSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const order$ = this.actions$.pipe(
          ofType(fromOrder.getOrderBySessionIdSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const applicationResult$ = this.actions$.pipe(
          ofType(fromApplicationResult.getApplicationResultBySessionIdSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const payment$ = this.actions$.pipe(
          ofType(fromPayment.getPaymentInformationBySessionIdSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const resume$ = this.actions$.pipe(
          ofType(fromResume.getResumableStateBySessionSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const failed$ = this.actions$.pipe(
          ofType(
            fromFeatureFlags.getFeatureFlagsError,
            fromOrder.getOrderBySessionIdError,
            fromApplicationResult.getApplicationResultBySessionIdError,
            fromPayment.getPaymentInformationBySessionIdError,
            fromResume.getResumableStateBySessionError
          ),
          filter((a) => a.correlationId === action.correlationId),
          first(),
          switchMap((error) => throwError(error))
        );

        return race(
          forkJoin({
            feature: feature$,
            order: order$,
            applicationResult: applicationResult$,
            payment: payment$,
            resume: resume$,
          }),
          failed$
        ).pipe(
          map((aggregationAction) =>
            fromActions.resumeApplicationBySessionAggregatorSuccess(
              aggregationAction.resume.resumeStateResponse,
              action.correlationId
            )
          ),
          catchError((_) =>
            of(
              fromActions.resumeApplicationBySessionAggregatorError(
                <BySessionId>{
                  sessionId:
                    action.resumeApplicationBySessionPayload.session.sessionId,
                },
                action.correlationId
              )
            )
          )
        );
      })
    )
  );

  resumeApplicationBySessionAggregatorSuccess$: Observable<
    Action
  > = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.resumeApplicationBySessionAggregatorSuccess),
      switchMap((action) => {
        const actions: Action[] = [];
        if (
          action.resumeStateResponse.resumable &&
          action.resumeStateResponse.resumeData &&
          action.resumeStateResponse.resumeData.route
        ) {
          actions.push(
            fromSharedState.navigateRoute(
              {
                route: action.resumeStateResponse.resumeData.route,
                params: { sid: action.resumeStateResponse.sessionId },
              },
              action.correlationId
            )
          );
        } else {
          actions.push(
            fromSharedState.navigateRoute(
              {
                // TODO: need to think about it.
                route: UnifiedApps.apply.route,
                params: { sid: action.resumeStateResponse.sessionId },
              },
              action.correlationId
            )
          );
        }
        return actions;
      })
    )
  );

  resumeApplicationBySessionAggregatorError$: Observable<Action> = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fromActions.resumeApplicationBySessionAggregatorError),
        switchMap((action) => {
          const actions: Action[] = [];
          actions.push(
            fromSharedState.navigateRoute(
              {
                route: 'comprehension',
                params: { sid: action.bySessionId.sessionId },
              },
              action.correlationId
            )
          );
          return actions;
        })
      )
  );
}
