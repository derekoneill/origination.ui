import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { mergeMap, catchError } from 'rxjs/operators';
import { nextRoute, previousRoute } from './apply-flow-handlers';
import { HttpErrorResponse } from '@angular/common/http';
import * as fromFlowRouteActions from './flow.route.actions';

@Injectable()
export class FlowRouteEffects {
  constructor(private actions$: Actions, private router: Router) {}

  changeRoute$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromFlowRouteActions.changeApplyFlowRoute),
      mergeMap(async (action) => {
        const to =
          action.routePayload.direction ===
          fromFlowRouteActions.RouteDirection.Next
            ? nextRoute(action.routePayload)
            : previousRoute(action.routePayload);

        await this.router.navigate([to], {
          queryParamsHandling: 'merge',
          queryParams: action.routePayload.params,
        });
        return fromFlowRouteActions.changeApplyFlowRouteSuccess(
          this.router.url
        );
      }),
      catchError((error) =>
        of(
          fromFlowRouteActions.changeApplyFlowRouteError(<HttpErrorResponse>{
            status: 400,
            statusText: 'Could not navigate to the specified route',
            error: error,
          })
        )
      )
    )
  );
}
