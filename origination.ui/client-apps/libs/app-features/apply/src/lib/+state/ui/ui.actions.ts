import { createAction, union } from '@ngrx/store';
import { generateId } from '@ua/shared/utilities';

const UiPrefix = '[UI]';
export const setUiState = createAction(
  `${UiPrefix} Set UiState`,
  (uiStates: { [key: string]: any }, correlationId: string = generateId()) => ({
    uiStates,
    correlationId,
  })
);

const allActions = union({
  setUiState,
});

export type UiActions = typeof allActions;
