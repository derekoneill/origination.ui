import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { OrderService } from '@ua/shared/data-access';
import * as fromOrderActions from './order.actions';

@Injectable()
export class OrderEffects {
  constructor(private actions: Actions, private orderService: OrderService) {}

  getOrderBySessionId$: Observable<Action> = createEffect(() =>
    this.actions.pipe(
      ofType(fromOrderActions.getOrderBySessionId),
      mergeMap((action) =>
        this.orderService
          .getOrderBySessionId(action.bySessionId.sessionId)
          .pipe(
            map((response) =>
              fromOrderActions.getOrderBySessionIdSuccess(
                response,
                action.correlationId
              )
            ),
            catchError((error) =>
              of(
                fromOrderActions.getOrderBySessionIdError(
                  error,
                  action.correlationId
                )
              )
            )
          )
      )
    )
  );
}
