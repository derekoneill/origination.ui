import { HttpErrorResponse } from '@angular/common/http';
import { Session } from '@ua/shared/data-access';
import { createReducer, on } from '@ngrx/store';
import * as fromActions from './session.actions';

export interface SessionState {
  session: Session;
  isLoaded: boolean;
  isFound: boolean;
  error?: HttpErrorResponse;
}

export function initSessionState(
  overrides: Partial<SessionState> = {}
): SessionState {
  return {
    session: {
      sessionId: '',
      originalSessionId: '',
      storeId: 0,
      locale: 'us-en',
      orderToken: '',
      storeName: '',
    },
    isLoaded: false,
    isFound: false,
    ...overrides,
  };
}

export const foundSessionHandler = (state: SessionState): SessionState => ({
  ...state,
  isFound: true,
});

export const getSessionSuccessHandler = (
  state: SessionState,
  action
): SessionState => ({
  ...state,
  isLoaded: true,
  session: {
    ...state.session,
    ...action.session,
  },
});

export const initSessionSuccessHandler = (
  state: SessionState,
  action
): SessionState => ({
  ...state,
  session: {
    ...state.session,
    sessionId: action.payload.sessionId,
  },
  isLoaded: true,
  isFound: true,
});

export const initOrGetSessionErrorHandler = (
  state: SessionState,
  action
): SessionState => ({
  ...state,
  error: action.error,
  isLoaded: true,
  isFound: true,
});

export const reducer = createReducer(
  initSessionState(),
  on(fromActions.initSession, fromActions.getSession, foundSessionHandler),
  on(fromActions.getSessionSuccess, getSessionSuccessHandler),
  on(fromActions.initSessionSuccess, initSessionSuccessHandler),
  on(
    fromActions.initSessionError,
    fromActions.getSessionError,
    initOrGetSessionErrorHandler
  )
);
