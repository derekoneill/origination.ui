import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { SessionService } from '@ua/shared/data-access';
import { Observable, of } from 'rxjs';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { Session, BySessionId, HasNewSessionId } from '@ua/shared/data-access';
import * as fromSessionActions from './session.actions';

@Injectable()
export class SessionEffects {
  constructor(
    private actions$: Actions,
    private sessionService: SessionService
  ) {}

  getSession$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromSessionActions.getSession),
      mergeMap((action) =>
        this.sessionService.getSession(action.bySessionId.sessionId).pipe(
          map((res: Session & HasNewSessionId) => {
            if (res.newSessionId) {
              delete res.newSessionId;
            }
            const session: Session = {
              ...res,
              sessionId: res.newSessionId || res.sessionId,
              originalSessionId: res.sessionId,
            };
            return fromSessionActions.getSessionSuccess(
              session,
              action.correlationId
            );
          }),
          catchError((error) =>
            of(fromSessionActions.getSessionError(error, action.correlationId))
          )
        )
      )
    )
  );

  initSession$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromSessionActions.initSession),
      mergeMap((action) =>
        this.sessionService.initSession(action.initSessionRequest).pipe(
          map((sessionId: BySessionId) =>
            fromSessionActions.initSessionSuccess(
              sessionId,
              action.correlationId
            )
          ),
          catchError((error) =>
            of(fromSessionActions.initSessionError(error, action.correlationId))
          )
        )
      )
    )
  );
}
