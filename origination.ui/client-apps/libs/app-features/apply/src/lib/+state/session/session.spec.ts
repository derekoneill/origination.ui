import * as fromSession from '.';
import * as fromRoot from '..';
import { Session, BySessionId } from '@ua/shared/data-access';
import { HttpErrorResponse } from '@angular/common/http';

describe('session state reducer', () => {
  let sessionState: fromSession.SessionState;
  beforeEach(() => {
    sessionState = fromSession.initSessionState();
  });

  it('should handle the GetSession action correctly', () => {
    sessionState = fromSession.reducer(
      sessionState,
      fromSession.getSession(<BySessionId>{
        sessionId: '32c91be3-b225-4dbe-ba4f-71e75e4f5c30',
      })
    );
    expect(sessionState.isLoaded).toEqual(false);
  });

  it('should handle the GetSessionSuccess action correctly', () => {
    const session: Session = {
      sessionId: '32c91be3-b225-4dbe-ba4f-71e75e4f5c30',
      originalSessionId: '32c91be3-b225-4dbe-ba4f-71e75e4f5c30',
      locale: 'en_US',
      orderToken: 'order-token-id',
      storeId: 55555,
      storeName: 'Progressive Store',
    };
    sessionState = fromSession.reducer(
      sessionState,
      fromSession.getSessionSuccess(session)
    );
    expect(sessionState.isLoaded).toEqual(true);
    expect(sessionState.session).toEqual(session);
  });

  it('should handle the GetSessionError action correctly', () => {
    sessionState = fromSession.reducer(
      sessionState,
      fromSession.getSessionError(<HttpErrorResponse>{
        status: 500,
        statusText: 'Internal Server Error',
      })
    );
    expect(sessionState.error).toEqual(<HttpErrorResponse>{
      status: 500,
      statusText: 'Internal Server Error',
    });
  });
});
