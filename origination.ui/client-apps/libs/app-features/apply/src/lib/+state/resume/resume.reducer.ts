import { LoginStatusReason, ResumeStateResponse } from '@ua/shared/data-access';
import { HttpErrorResponse } from '@angular/common/http';
import * as fromActions from './resume.actions';
import { createReducer, on } from '@ngrx/store';

export interface ResumeState {
  resume: ResumeStateResponse;
  isLoaded: boolean;
  error?: HttpErrorResponse;
}

export function initResumeAppState(
  overrides: Partial<ResumeState> = {}
): ResumeState {
  return {
    isLoaded: false,
    resume: {
      sessionId: '',
      approvalLimit: 0,
      loginStatusReason: LoginStatusReason.ResumeFailedInvalidLogin,
      leaseId: 0,
      leaseStatus: '',
      resumeData: undefined,
      resumable: false,
    },
    ...overrides,
  };
}

export const saveResumeStateSuccessHandler = (
  state: ResumeState,
  action
): ResumeState => ({
  ...state,
  resume: {
    ...state.resume,
    resumeData: action.resumableState.data,
  },
});

export const getResumableStateBySessionOrByAuthenticationIdSuccessHandler = (
  state: ResumeState,
  action
): ResumeState => ({
  ...state,
  resume: action.resumeStateResponse,
  isLoaded: true,
});

export const saveOrGetResumableStateErrorHandler = (
  state: ResumeState,
  action
): ResumeState => ({
  ...state,
  error: action.error,
  isLoaded: true,
});

export const reducer = createReducer(
  initResumeAppState(),
  on(fromActions.saveResumableStateSuccess, saveResumeStateSuccessHandler),
  on(
    fromActions.getResumableStateByAuthenticationIdSuccess,
    fromActions.getResumableStateBySessionSuccess,
    getResumableStateBySessionOrByAuthenticationIdSuccessHandler
  ),
  on(
    fromActions.getResumableStateByAuthenticationIdError,
    fromActions.getResumableStateBySessionError,
    fromActions.saveResumableStateError,
    saveOrGetResumableStateErrorHandler
  )
);
