import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { PaymentService } from '@ua/shared/data-access';
import * as fromActions from './payment.actions';

@Injectable()
export class PaymentEffects {
  constructor(
    private actions$: Actions,
    private paymentService: PaymentService
  ) {}

  getPaymentInformation$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getPaymentInformationBySessionId),
      mergeMap((action) =>
        this.paymentService
          .getPaymentInformationBySessionId(action.bySessionId.sessionId)
          .pipe(
            map((response) =>
              fromActions.getPaymentInformationBySessionIdSuccess(
                response,
                action.correlationId
              )
            ),
            catchError((error) =>
              of(
                fromActions.getPaymentInformationBySessionIdError(
                  error,
                  action.correlationId
                )
              )
            )
          )
      )
    )
  );

  updatePaymentInformation$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.updatePaymentInformation),
      mergeMap((action) =>
        this.paymentService
          .updatePaymentInformation(action.paymentInformation)
          .pipe(
            map((response) =>
              fromActions.updatePaymentInformationSuccess(
                response,
                action.correlationId
              )
            ),
            catchError((error) =>
              of(
                fromActions.updatePaymentInformationError(
                  error,
                  action.correlationId
                )
              )
            )
          )
      )
    )
  );
}
