import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '@ua/shared/ui';
import { ApplyRoutingModule } from './apply.routing.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TextMaskModule } from 'angular2-text-mask';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslationService } from '@ua/shared/data-access';
import { UnifiedApps } from '@ua/shared/state';
import * as fromReducers from './+state';
import * as fromEffects from './+state/apply.effects';
import * as fromAnalyticEffects from './+state/analytic';
import * as fromApplicationEffects from './+state/application';
import * as fromApplicationResultEffects from './+state/application-result';
import * as fromAuthenticationEffects from './+state/authentication';
import * as fromFeatureFlagEffects from './+state/feature-flags';
import * as fromFlowRoutesEffects from './+state/flow-routes';
import * as fromOrderEffects from './+state/order';
import * as fromPaymentEffects from './+state/payment';
import * as fromResumeEffects from './+state/resume';
import * as fromSessionEffects from './+state/session';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    TranslateModule,
    ApplyRoutingModule,
    TextMaskModule,
    TranslateModule.forRoot({
      loader: { provide: TranslateLoader, useClass: TranslationService },
    }),
    StoreModule.forFeature(UnifiedApps.apply.route, fromReducers.reducer),
    EffectsModule.forFeature([
      fromEffects.ApplyEffects,
      fromAnalyticEffects.AnalyticEffects,
      fromApplicationEffects.ApplicationEffects,
      fromApplicationResultEffects.ApplicationResultEffects,
      fromAuthenticationEffects.AuthenticationEffects,
      fromFeatureFlagEffects.FeatureFlagsEffects,
      fromFlowRoutesEffects.FlowRouteEffects,
      fromOrderEffects.OrderEffects,
      fromPaymentEffects.PaymentEffects,
      fromResumeEffects.ResumeEffects,
      fromSessionEffects.SessionEffects,
    ]),
    ReactiveFormsModule,
  ],
  declarations: [ApplyRoutingModule.components],
})
export class ApplyModule {}
