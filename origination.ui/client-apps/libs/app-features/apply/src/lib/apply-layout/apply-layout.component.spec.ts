import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { Store } from '@ngrx/store';

import { ApplyLayoutComponent } from './apply-layout.component';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

describe('ApplyLayoutComponent', () => {
  let component: ApplyLayoutComponent;
  let mockStore: SubstituteOf<Store>;
  let mockRouter: SubstituteOf<Router>;
  let mockTranslateService: SubstituteOf<TranslateService>;

  beforeEach(() => {
    mockStore = Substitute.for<Store>();
    mockRouter = Substitute.for<Router>();
    mockTranslateService = Substitute.for<TranslateService>();

    component = new ApplyLayoutComponent(
      mockStore,
      mockRouter,
      mockTranslateService
    );
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
