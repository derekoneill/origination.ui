import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromSharedState from '@ua/shared/state';
import * as fromActions from '../+state/apply.actions';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'ua-apply-layout',
  templateUrl: './apply-layout.component.html',
  styleUrls: ['./apply-layout.component.scss'],
})
export class ApplyLayoutComponent implements OnInit {
  constructor(
    private readonly store$: Store,
    private readonly router: Router,
    private readonly translate: TranslateService
  ) {
    translate.use('en-US');
  }

  ngOnInit(): void {}

  onBack() {
    this.store$.dispatch(
      fromActions.walkBackInApplyFlow({
        route: this.router.url,
        params: {},
      })
    );
  }

  onClose() {
    this.store$.dispatch(
      fromSharedState.navigateRoute({
        route: 'comprehension',
        params: {},
      })
    );
  }
}
