import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromApply from '../+state';
import * as fromFlowRoutes from '../+state/flow-routes';
import { Session } from '@ua/shared/data-access';
import { Subscription } from 'rxjs';
import { UnifiedApps } from '@ua/shared/state';

@Component({
  selector: 'ua-submit-application',
  templateUrl: './submit-application.component.html',
  styleUrls: ['./submit-application.component.scss'],
})
export class SubmitApplicationComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  session: Session;
  constructor(private store$: Store<fromApply.ApplyState>) {}

  ngOnInit(): void {
    this.subs.push(
      this.store$
        .pipe(select(fromApply.getSessionSelector))
        .subscribe((session) => (this.session = session))
    );
  }

  ngOnDestroy(): void {
    this.subs.forEach((s) => s.unsubscribe());
  }

  onContinue() {
    this.store$.dispatch(
      fromFlowRoutes.changeApplyFlowRoute({
        from: UnifiedApps.apply.children.submitApplication.route,
        direction: fromFlowRoutes.RouteDirection.Next,
        params: { sid: this.session.sessionId },
      })
    );
  }
}
