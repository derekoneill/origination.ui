import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { ApplicationResult } from '@ua/shared/data-access';
import { separatePhoneNumberWithDash } from '@ua/shared/utilities';
import * as fromApplicationResult from '../+state/application-result';
import * as fromApply from '../+state/index';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'application-pending',
  templateUrl: './application-pending.component.html',
  styleUrls: ['./application-pending.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationPendingComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  applicationResult: ApplicationResult;
  supportPhoneNumber: string;

  constructor(
    private store$: Store<fromApplicationResult.ApplicationResultState>,
    translateService: TranslateService
  ) {
    this.supportPhoneNumber = separatePhoneNumberWithDash(
      translateService.instant('ApplicationPending.SupportPhoneNumber')
    );
  }

  ngOnInit(): void {
    this.subs.push(
      this.store$
        .pipe(select(fromApply.getApplicationResultSelector))
        .subscribe((appResult) => (this.applicationResult = appResult))
    );
  }

  ngOnDestroy(): void {
    this.subs.forEach((s) => s.unsubscribe());
  }
}
