import { TestBed, async } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { BasicInfoComponent } from './basic-info.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { provideMockStore, MockStoreConfig } from '@ngrx/store/testing';
import { Router } from '@angular/router';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { GritCoreModule } from '@progleasing/grit-core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { TextMaskModule } from 'angular2-text-mask';
import * as fromAuthentications from '../+state/authentication';
import * as fromApplications from '../+state/application';
import * as fromApply from '../+state';

xdescribe('BasicInfo Component', () => {
  let storeSpy;
  let routerSpy;
  let fixture;
  let component;
  let translateServiceSpy;
  let formBuilder;

  beforeEach(async(() => {
    routerSpy = jasmine.createSpyObj('window', ['navigate']);
    translateServiceSpy = jasmine.createSpyObj('translateService', ['']);

    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        ReactiveFormsModule,
        TranslateModule,
        // ApplyRoutingModule,
        MatDialogModule,
        GritCoreModule,
        TextMaskModule,
        BrowserAnimationsModule,
        // EffectsModule.forRoot([
        //   fromApply.ApplyEffects,
        //   fromRoot.RootEffects,
        // ])
        // StoreModule.forFeature(AppRoutes.apply.route, fromApply.reducer),
        // EffectsModule.forFeature(
        //   [fromApply.ApplyEffects])
      ],
      declarations: [BasicInfoComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        provideMockStore<fromApply.ApplyState>(<
          MockStoreConfig<fromApply.ApplyState>
        >{
          authInfo: fromAuthentications.initAuthenticationState(),
          application: fromApplications.initApplication(),
        }),
        { provide: FormBuilder, useValue: formBuilder },
        { provide: Router, useValue: routerSpy },
        { provide: TranslateService, useValue: translateServiceSpy },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    storeSpy = TestBed.get(Store);
    formBuilder = TestBed.get(FormBuilder);
    fixture = TestBed.createComponent(BasicInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should call ngOnInit once', () => {
    expect(component.ngOnInit).toHaveBeenCalledTimes(1);
  });

  it('should call ngAfterViewInit once', () => {
    expect(component.ngAfterViewInit).toHaveBeenCalledTimes(1);
  });

  it('should call ngOnDestroy once', () => {
    expect(component.ngOnDestroy).toHaveBeenCalledTimes(1);
  });

  it('should call onContinue when button clicked', () => {
    const button = fixture.debugElement.nativeElement.querySelector(
      'button-apply-basic-info'
    );
    button.click();
    fixture.whenStable().then(() => {
      expect(component.onContinue).toHaveBeenCalled();
    });
  });

  it('BasicInfoFormGroup invalid when empty', () => {
    component.basicInfoFormGroup.controls.firstName.setValue('');
    component.basicInfoFormGroup.controls.lastName.setValue('');
    component.basicInfoFormGroup.controls.dateOfBirth.setValue('');
    component.basicInfoFormGroup.controls.ssn.setValue('');
    expect(component.basicInfoFormGroup.valid).toBe(false);
  });

  it('firstName field validity', () => {
    const firstName = component.basicInfoFormGroup.controls.firstName;
    expect(firstName.valid).toBe(false);

    firstName.setValue('');
    expect(firstName.hasError('required')).toBe(true);

    firstName.setValue('+TestName');
    expect(firstName.hasError('nameFormat')).toBe(true);

    firstName.setValue('1TestName');
    expect(firstName.hasError('startsWithLetter')).toBe(true);

    firstName.setValue(' StartsWithSpace');
    expect(firstName.hasError('startsWithSpace')).toBe(true);

    firstName.setValue('Test');
    expect(firstName.valid).toBe(true);
  });

  it('lastName field validity', () => {
    const lastName = component.basicInfoFormGroup.controls.lastName;
    expect(lastName.valid).toBe(false);

    lastName.setValue('');
    expect(lastName.hasError('required')).toBe(true);

    lastName.setValue('+TestName');
    expect(lastName.hasError('nameFormat')).toBe(true);

    lastName.setValue('1TestName');
    expect(lastName.hasError('startsWithLetter')).toBe(true);

    lastName.setValue(' StartsWithSpace');
    expect(lastName.hasError('startsWithSpace')).toBe(true);

    lastName.setValue('Test');
    expect(lastName.valid).toBe(true);
  });

  it('dateOfBirth field validity', () => {
    const dateOfBirth = component.basicInfoFormGroup.controls.dateOfBirth;
    expect(dateOfBirth.valid).toBe(false);

    dateOfBirth.setValue('');
    expect(dateOfBirth.hasError('required')).toBe(true);

    const today = new Date();
    dateOfBirth.setValue(today);
    expect(dateOfBirth.hasError('ageRange')).toBe(true);

    const dob = today.setFullYear(today.getFullYear() - 101);
    dateOfBirth.setValue(dob);
    expect(dateOfBirth.hasError('ageRange')).toBe(true);

    const month = new Date('1989-00-12');
    dateOfBirth.setValue(month);
    expect(dateOfBirth.hasError('month')).toBe(true);

    const day = new Date('1989-01-40');
    dateOfBirth.setValue(day);
    expect(dateOfBirth.hasError('day')).toBe(true);

    const year = new Date('0000-01-12');
    dateOfBirth.setValue(year);
    expect(dateOfBirth.hasError('year')).toBe(true);

    const birthday = today.setFullYear(today.getFullYear() - 30);
    dateOfBirth.setValue(birthday);
    expect(dateOfBirth.valid).toBe(true);
  });

  it('ssn field validity', () => {
    component.shortAppEnabled = false;
    const ssn = component.basicInfoFormGroup.controls.ssn;
    expect(ssn.valid).toBe(false);

    ssn.setValue('');
    expect(ssn.hasError('required')).toBe(true);

    ssn.setValue('1234');
    expect(ssn.hasError('ssn')).toBe(true);

    ssn.setValue('12345678910');
    expect(ssn.hasError('ssn')).toBe(true);

    ssn.setValue('518841234');
    expect(ssn.valid).toBe(true);
  });

  it('toggleSsnVisibility called when ssn is valid input', () => {
    component.shortAppEnabled = false;
    component.maskSsn = false;
    component.basicInfoFormGroup.controls.ssn.setValue('518841234');
    component.ngDoCheck();
    expect(component.maskSsn).toBe(true);
    expect(component.toggleSsnVisibility).toHaveBeenCalled();
  });
});
