// tslint:disable:no-shadowed-variable
import { Subscription } from 'rxjs';
import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  ChangeDetectionStrategy,
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import {
  MIN_AGE,
  MAX_AGE,
  nameFormat,
  startsWithLetter,
  startsWithSpace,
  ageRange,
  month,
  day,
  year,
  ssn,
} from '@ua/shared/utilities';
import { Session, Application } from '@ua/shared/data-access';
import * as fromAuthentications from '../+state/authentication';
import * as fromAnalytic from '../+state/analytic';
import * as fromFlowRoutes from '../+state/flow-routes';
import * as fromApply from '../+state/index';

import { dateOfBirthTextMask, ssnTextMask } from '@ua/shared/utilities';
import { UnifiedApps } from '@ua/shared/state';

@Component({
  selector: 'ua-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['./basic-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BasicInfoComponent implements OnInit, OnDestroy, AfterViewInit {
  subs: Subscription[] = [];
  basicInfoFormGroup: FormGroup;
  application: Application;
  session: Session;
  authenticationState: fromAuthentications.AuthenticationState;

  dateOfBirthTextMask = dateOfBirthTextMask;
  ssnTextMask = ssnTextMask;
  isMasked = false;

  statuses = fromAuthentications.Status;
  saveResumableStatus: fromAuthentications.SavingResumableStatus;

  constructor(
    private store$: Store<fromApply.State>,
    public router: Router,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.subs.push(
      this.store$
        .pipe(select(fromApply.getSessionSelector))
        .subscribe((session) => (this.session = session)),
      this.store$
        .pipe(select(fromApply.getAuthenticationStateSelector))
        .subscribe(
          (authenticationState) =>
            (this.authenticationState = authenticationState)
        ),
      this.store$
        .pipe(select(fromApply.getApplicationSelector))
        .subscribe((application) => (this.application = application)),
      this.store$
        .pipe(select(fromApply.getSaveResumableStatusSelector))
        .subscribe(
          (saveResumableStatus) =>
            (this.saveResumableStatus = saveResumableStatus)
        )
    );

    this.basicInfoFormGroup = this.formBuilder.group({
      firstName: [
        this.application.firstName,
        [Validators.required, nameFormat, startsWithLetter, startsWithSpace],
      ],
      lastName: [
        this.application.lastName,
        [Validators.required, nameFormat, startsWithLetter, startsWithSpace],
      ],
      dateOfBirth: [
        this.application.dateOfBirth || '',
        [Validators.required, ageRange(MIN_AGE, MAX_AGE), month, day, year],
      ],
      ssn: [this.application.ssn, [Validators.required, ssn]],
    });

    this.basicInfoFormGroup.updateValueAndValidity();
  }

  ngAfterViewInit() {
    this.store$.dispatch(
      fromAnalytic.pageViewEvent(
        this.session,
        true,
        this.router.url,
        'PersonalInfo.Title',
        'SectionIdentifier.Apply.Title'
      )
    );
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => !sub.closed && sub.unsubscribe());
  }

  onMaskChanged(isMasked: boolean) {
    this.isMasked = isMasked;
  }

  getFormattedDate(date: Date): String {
    const year = date.getFullYear();
    let month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    let day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + '/' + day + '/' + year;
  }

  onContinue() {
    if (this.basicInfoFormGroup.valid) {
      const formValue = this.basicInfoFormGroup.value;
      const app = <Application>{
        ...this.application,
      };
      app.firstName = formValue.firstName;
      app.lastName = formValue.lastName;
      app.dateOfBirth = formValue.dateOfBirth;
      app.ssn = formValue.ssn;
      this.store$.dispatch(
        fromApply.saveApplicationResumableStateSplitter({
          resumableState: {
            sessionId: this.session.sessionId,
            resumeData: {
              authId: this.authenticationState.authenticationId,
              ssn: app.ssn,
              route: this.router.url,
            },
          },
          application: app,
          nextRoute: {
            from: UnifiedApps.apply.children.basicInfo.route,
            direction: fromFlowRoutes.RouteDirection.Next,
            params: { sid: this.session.sessionId },
          },
        })
      );
    }
  }
}
