import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Session, Application } from '@ua/shared/data-access';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as fromApply from '../+state';
import * as fromFlowRoutes from '../+state/flow-routes';
import * as fromAuthentications from '../+state/authentication';
import { routingNumber } from '@ua/shared/utilities';
import { UnifiedApps } from '@ua/shared/state';

enum BankInfoFormFields {
  BankRoutingNumber = 'bankRoutingNumber',
  BankAccountNumber = 'bankAccountNumber',
}

@Component({
  selector: 'ua-bank-info',
  templateUrl: './bank-info.component.html',
  styleUrls: ['./bank-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BankInfoComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  bankInfoForm: FormGroup;
  isMasked = false;

  session: Session;
  application: Application;
  authenticationState: fromAuthentications.AuthenticationState;

  statuses = fromAuthentications.Status;
  saveResumableStatus: fromAuthentications.SavingResumableStatus;

  constructor(
    private store$: Store<fromApply.ApplyState>,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.subs.push(
      this.store$
        .pipe(select(fromApply.getSessionSelector))
        .subscribe((session) => (this.session = session)),
      this.store$
        .pipe(select(fromApply.getApplicationSelector))
        .subscribe((application) => (this.application = application)),
      this.store$
        .pipe(select(fromApply.getAuthenticationStateSelector))
        .subscribe(
          (authenticationState) =>
            (this.authenticationState = authenticationState)
        ),
      this.store$
        .pipe(select(fromApply.getSaveResumableStatusSelector))
        .subscribe(
          (saveResumableStatus) =>
            (this.saveResumableStatus = saveResumableStatus)
        )
    );

    this.bankInfoForm = this.formBuilder.group({
      bankRoutingNumber: [
        this.application.bankRoutingNumber,
        [Validators.required, routingNumber()],
      ],
      bankAccountNumber: [
        this.application.bankAccountNumber,
        [Validators.required, Validators.pattern('^[0-9]*$')],
      ],
    });
  }

  ngOnDestroy(): void {
    this.subs.forEach((s) => s.unsubscribe());
  }

  onMaskChanged(isMasked: boolean) {
    this.isMasked = isMasked;
  }

  onContinue() {
    if (this.bankInfoForm.valid) {
      const application: Application = {
        ...this.application,
        bankRoutingNumber: this.bankInfoForm.get(
          BankInfoFormFields.BankRoutingNumber
        ).value,
        bankAccountNumber: this.bankInfoForm.get(
          BankInfoFormFields.BankAccountNumber
        ).value,
      };

      this.store$.dispatch(
        fromApply.saveApplicationResumableStateSplitter({
          resumableState: {
            sessionId: this.session.sessionId,
            resumeData: {
              authId: this.authenticationState.authenticationId,
              ssn: this.authenticationState.ssn,
              route: this.router.url,
            },
          },
          application: application,
          nextRoute: {
            from: UnifiedApps.apply.children.basicInfo.route,
            direction: fromFlowRoutes.RouteDirection.Next,
            params: { sid: this.session.sessionId },
          },
        })
      );
    }
  }

  disallowNonDigitInput(keyPressEvent) {
    const charCode = keyPressEvent.charCode || keyPressEvent.keyCode;
    const newChar = String.fromCharCode(charCode);
    const testRegex = /[^0-9]/g;
    if (testRegex.test(newChar)) {
      return false;
    }
  }
}
