import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UnifiedApps } from '@ua/shared/state';
import { BasicInfoComponent } from './basic-info/basic-info.component';
import { ContactInfoComponent } from './contact-info/contact-info.component';
import { BankInfoComponent } from './bank-info/bank-info.component';
import { IncomeInfoComponent } from './income-info/income-info.component';
import { OtpContactIdComponent } from './otp-info/otp-contact-id.component';
import { SubmitApplicationComponent } from './submit-application/submit-application.component';
import { SubmittedComponent } from './submitted/submitted.component';
import { OtpVerifyCodeComponent } from './otp-info/otp-verify-code.component';
import { ApplicationPendingComponent } from './application-pending/application-pending.component';
import { ApplyLayoutComponent } from './apply-layout/apply-layout.component';

const routes: Routes = [
  {
    path: '',
    component: ApplyLayoutComponent,
    children: [
      {
        path: UnifiedApps.apply.children.getStarted.route,
        component: OtpContactIdComponent,
      },
      {
        path: UnifiedApps.apply.children.verifyCode.route,
        component: OtpVerifyCodeComponent,
      },
      {
        path: UnifiedApps.apply.children.basicInfo.route,
        component: BasicInfoComponent,
      },
      {
        path: UnifiedApps.apply.children.contactInfo.route,
        component: ContactInfoComponent,
      },
      {
        path: UnifiedApps.apply.children.incomeInfo.route,
        component: IncomeInfoComponent,
      },
      {
        path: UnifiedApps.apply.children.basicInfo.route,
        component: BankInfoComponent,
      },
      {
        path: UnifiedApps.apply.children.submitApplication.route,
        component: SubmitApplicationComponent,
      },
      {
        path: UnifiedApps.apply.children.submittedApplication.route,
        component: SubmittedComponent,
      },
      {
        path: UnifiedApps.apply.children.applicationPending.route,
        component: ApplicationPendingComponent,
      },
      {
        path: '**',
        redirectTo: UnifiedApps.apply.children.getStarted.route,
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ApplyRoutingModule {
  static components = [
    ApplyLayoutComponent,
    OtpContactIdComponent,
    OtpVerifyCodeComponent,
    BasicInfoComponent,
    ContactInfoComponent,
    BankInfoComponent,
    IncomeInfoComponent,
    SubmitApplicationComponent,
    SubmittedComponent,
    ApplicationPendingComponent,
  ];
}
