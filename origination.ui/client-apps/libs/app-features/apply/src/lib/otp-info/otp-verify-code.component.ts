import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  ViewChild,
  ElementRef,
  OnDestroy,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { GetResumableStateByAuthenticationIdRequest } from '@ua/shared/data-access';
import * as fromApply from '../+state';
import * as fromSession from '../+state/session';
import * as fromAnalytic from '../+state/analytic';
import * as fromFlowRoutes from '../+state/flow-routes';
import * as fromAuthentications from '../+state/authentication';
import * as fromSharedState from '@ua/shared/state';
import { UnifiedApps } from '@ua/shared/state';
import {
  isValidEmail,
  isValidPhone,
  separatePhoneNumberWithDash,
  validatePhoneOrEmail,
  validateWithCallbackPredicate,
} from '@ua/shared/utilities';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'otp-verify-code',
  templateUrl: './otp-verify-code.component.html',
  styleUrls: ['./otp-verify-code.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OtpVerifyCodeComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  otpForm: FormGroup;
  authId: string;
  authCode: string;
  resendAuthCodeStatus: fromAuthentications.ResendAuthenticationCodeStatus;
  verifySsnStatus: fromAuthentications.VerifySsnStatus;
  verifyAuthCodeStatus: fromAuthentications.VerifyAuthenticationCodeStatus;
  isShortApp: boolean;
  authStatuses = fromAuthentications.Status;
  sessionState: fromSession.SessionState;
  phoneOrEmailLabel: string;

  @ViewChild('authCode') codeInput: ElementRef;
  @ViewChild('ssn') ssn: ElementRef;
  constructor(
    private formbuilder: FormBuilder,
    private translateService: TranslateService,
    private store$: Store<fromApply.State>,
    private changeDetector: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.subs.push(
      this.store$
        .pipe(select(fromApply.getSessionStateSelector))
        .subscribe((sessionState) => (this.sessionState = sessionState)),
      this.store$
        .pipe(select(fromApply.getShortAppSelector))
        .subscribe((isShortApp) => (this.isShortApp = isShortApp)),
      this.store$
        .pipe(select(fromApply.getAuthenticationIdSelector))
        .subscribe((authId) => (this.authId = authId)),
      this.store$
        .pipe(select(fromApply.getAuthenticationCodeSelector))
        .subscribe((authCode) => (this.authCode = authCode)),
      this.store$
        .pipe(select(fromApply.getResendCodeStatusSelector))
        .subscribe(
          (resendAuthCodeStatus) =>
            (this.resendAuthCodeStatus = resendAuthCodeStatus)
        )
    );

    this.subs.push(
      this.store$
        .pipe(select(fromApply.getVerifyAuthCodeStatusSelector))
        .subscribe((verifyAuthCodeStatus) => {
          this.verifyAuthCodeStatus = verifyAuthCodeStatus;

          if (this.otpForm) {
            if (
              this.verifyAuthCodeStatus.status ===
              fromAuthentications.Status.Error
            ) {
              this.otpForm.get('authCode').setErrors({
                error: this.translateService.instant('Error.InvalidAuthCode'),
              });
            } else {
              this.otpForm.get('authCode').setErrors(null);
            }
          }

          // Telling the grit system to update the error immediately.
          this.codeInput.nativeElement.blur();
          this.codeInput.nativeElement.focus();

          this.changeDetector.markForCheck();
        })
    );

    this.subs.push(
      this.store$
        .pipe(select(fromApply.getVerifySsnStatusSelector))
        .subscribe((verifySsn) => {
          this.verifySsnStatus = verifySsn;
          if (this.otpForm) {
            if (
              this.verifySsnStatus.status === fromAuthentications.Status.Error
            ) {
              if (this.verifySsnStatus.numberOfFailures >= 3) {
                // give user 3 changes and then route to broken page.
                this.store$.dispatch(
                  fromSharedState.navigateRoute({
                    route: UnifiedApps.apply.children.somethingBroke.route,
                    params: { sid: this.sessionState.session.sessionId },
                  })
                );
              }

              this.otpForm.get('ssn').setErrors({
                error: this.translateService.instant('Error.'),
              });
            } else {
              this.otpForm.get('ssn').setErrors(null);
            }

            // Telling the grit system to update the error immediately.
            this.ssn.nativeElement.blur();
            this.ssn.nativeElement.focus();

            this.changeDetector.markForCheck();
          }
        })
    );

    this.otpForm = this.formbuilder.group({
      authId: [
        this.authId,
        [
          validatePhoneOrEmail(
            this.translateService.instant('Error.ContactInfoRequired'),
            this.translateService.instant('Error.ContactInfoPhoneOrEmail')
          ),
        ],
      ],
      authCode: [
        this.authCode,
        [
          validateWithCallbackPredicate(
            () =>
              this.verifyAuthCodeStatus.status ===
              fromAuthentications.Status.Error,
            {
              error: this.translateService.instant('Error.InvalidAuthCode'),
            }
          ),
          Validators.minLength(6),
        ],
      ],
      ssn: [''],
    });

    const authCodeSubscriber = this.otpForm
      .get('authCode')
      .valueChanges.subscribe((val: string) => {
        if (val && val.length === 6) {
          if (!this.sessionState.error && this.sessionState.isLoaded) {
            this.store$.dispatch(
              fromAuthentications.verifyAuthenticationCodeSplitter(<
                fromAuthentications.VerifyAuthenticationCodeSplitterPayload
              >{
                authenticationId: this.otpForm.get('authId').value,
                authenticationCode: val,
                sessionId: this.sessionState.session.sessionId,
                storeId: this.sessionState.session.storeId,
              })
            );
          } else {
            this.store$.dispatch(
              fromAuthentications.verifyAuthenticationCodeAggregatorError(<
                HttpErrorResponse
              >{
                status: 404,
                statusText: 'No Session Id In Present',
              })
            );
          }
        } else {
          this.otpForm.get('authCode').setErrors(null);
        }
      });
    this.subs.push(authCodeSubscriber);

    const ssnSubscriber = this.otpForm
      .get('ssn')
      .valueChanges.subscribe((val: string) => {
        if (val && val.length === 4) {
          this.store$.dispatch(
            fromAuthentications.verifySsnSplitter(<
              GetResumableStateByAuthenticationIdRequest
            >{
              authId: this.otpForm.get('authId').value,
              authCode: this.otpForm.get('authCode').value,
              ssn: this.otpForm.get('ssn').value,
              originalSessionId: this.sessionState.session.sessionId,
              storeId: this.sessionState.session.storeId,
              shortAppEnabled: this.isShortApp,
            })
          );
        }
      });
    this.subs.push(ssnSubscriber);
  }

  ngOnDestroy(): void {
    this.subs.forEach((s) => s.unsubscribe());
  }

  onResendCode() {
    this.store$.dispatch(
      fromAuthentications.resendAuthenticationCode({
        authenticationId: this.otpForm.get('authId').value,
        sessionId: this.sessionState.session.sessionId,
      })
    );

    this.store$.dispatch(fromAnalytic.otpResendCodeEvent());
  }

  onClickToEdit() {
    this.otpForm.get('authId').enable();
  }

  formatingPhoneOrEmailValueAndLabel(isFocus: boolean): string {
    this.phoneOrEmailLabel = this.translateService.instant(
      'GetStarted.VerifyCode.PhoneOrEmail'
    );
    if (!isFocus) {
      const phoneOrEmail = this.otpForm.get('authId').value;
      if (phoneOrEmail && isValidPhone(phoneOrEmail)) {
        this.phoneOrEmailLabel = this.translateService.instant(
          'GetStarted.VerifyCode.Phone'
        );
        this.otpForm
          .get('authId')
          .setValue(separatePhoneNumberWithDash(phoneOrEmail));
      } else if (phoneOrEmail && isValidEmail(phoneOrEmail)) {
        this.phoneOrEmailLabel = this.translateService.instant(
          'GetStarted.VerifyCode.Email'
        );
      }
    }

    return this.phoneOrEmailLabel;
  }
}
