import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  AfterViewInit,
  ElementRef,
  OnDestroy,
} from '@angular/core';
import { Store, select } from '@ngrx/store';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { fromEvent, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { Session } from '@ua/shared/data-access';
import * as fromApply from '../+state';
import * as fromAnalytic from '../+state/analytic';
import * as fromAuthentications from '../+state/authentication';
import {
  isValidEmail,
  isValidPhone,
  separatePhoneNumberWithDash,
  validatePhoneOrEmail,
} from '@ua/shared/utilities';

@Component({
  selector: 'ua-otp-info',
  templateUrl: './otp-contact-id.component.html',
  styleUrls: ['./otp-contact-id.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OtpContactIdComponent implements OnInit, AfterViewInit, OnDestroy {
  subs: Subscription[] = [];
  otpForm: FormGroup;
  authId: string;
  consent: boolean;
  sendAuthCodeStatus: fromAuthentications.SendAuthenticationCodeStatus;
  session: Session;
  authStatuses = fromAuthentications.Status;
  isShortApp: boolean;
  phoneOrEmailLabel: string;

  constructor(
    private formbuilder: FormBuilder,
    private translateService: TranslateService,
    private router: Router,
    private store$: Store<fromApply.State>,
    private element: ElementRef
  ) {}

  ngOnInit(): void {
    this.subs.push(
      this.store$
        .pipe(select(fromApply.getAuthenticationIdSelector))
        .subscribe((authId) => (this.authId = authId)),
      this.store$
        .pipe(select(fromApply.getConsentSelector))
        .subscribe((consent) => (this.consent = consent)),
      this.store$
        .pipe(select(fromApply.getSendCodeStatusSelector))
        .subscribe(
          (sendAuthCodeStatus) => (this.sendAuthCodeStatus = sendAuthCodeStatus)
        ),
      this.store$
        .pipe(select(fromApply.getSessionSelector))
        .subscribe((session) => (this.session = session)),
      this.store$
        .pipe(select(fromApply.getShortAppSelector))
        .subscribe((isShortApp) => (this.isShortApp = isShortApp))
    );

    this.otpForm = this.formbuilder.group({
      authId: [
        this.authId,
        [
          validatePhoneOrEmail(
            this.translateService.instant('Error.ContactInfoRequired'),
            this.translateService.instant('Error.ContactInfoPhoneOrEmail')
          ),
        ],
      ],
      consent: [this.consent],
    });
  }

  ngAfterViewInit(): void {
    let title = this.translateService.instant('PersonalInfo.Title');
    let sectionTitle = this.translateService.instant(
      'SectionIdentifier.Resume.Title'
    );
    if (window.location.href.indexOf('login-resume')) {
      title = this.translateService.instant('Resume.Title');
      sectionTitle = this.translateService.instant('Resume.BodyTitle');
    }

    this.store$.dispatch(
      fromAnalytic.pageViewEvent(
        this.session,
        this.isShortApp,
        this.router.url,
        title,
        sectionTitle
      )
    );

    const disclosures = this.element.nativeElement.querySelectorAll('a');
    disclosures.forEach((element) => {
      this.subs.push(
        fromEvent(element, 'click').subscribe((e: any) => {
          this.store$.dispatch(
            fromAnalytic.buttonClickEvent(
              e.target.textContent,
              fromAnalytic.ButtonKind.LINK
            )
          );
        })
      );
    });

    this.subs.push(
      this.otpForm.get('consent').valueChanges.subscribe((_) => {
        this.store$.dispatch(
          fromAnalytic.buttonClickEvent(
            'disclosureCheck',
            fromAnalytic.ButtonKind.BUTTON
          )
        );
      })
    );
  }

  ngOnDestroy(): void {
    this.subs.forEach((s) => s.unsubscribe());
  }

  onContinue() {
    this.store$.dispatch(
      fromAuthentications.sendAuthenticationCode({
        authenticationId: this.otpForm.get('authId').value,
        consent: Boolean(this.otpForm.get('consent').value),
        sessionId: this.session.sessionId,
      })
    );

    // tracking send otp code.
    this.store$.dispatch(fromAnalytic.otpSendCodeEvent());
  }

  formatingPhoneOrEmailValueAndLabel(isFocus: boolean): string {
    this.phoneOrEmailLabel = this.translateService.instant(
      'GetStarted.VerifyCode.PhoneOrEmail'
    );
    if (!isFocus) {
      const phoneOrEmail = this.otpForm.get('authId').value;
      if (phoneOrEmail && isValidPhone(phoneOrEmail)) {
        this.phoneOrEmailLabel = this.translateService.instant(
          'GetStarted.VerifyCode.Phone'
        );
        this.otpForm
          .get('authId')
          .setValue(separatePhoneNumberWithDash(phoneOrEmail));
      } else if (phoneOrEmail && isValidEmail(phoneOrEmail)) {
        this.phoneOrEmailLabel = this.translateService.instant(
          'GetStarted.VerifyCode.Email'
        );
      }
    }

    return this.phoneOrEmailLabel;
  }
}
