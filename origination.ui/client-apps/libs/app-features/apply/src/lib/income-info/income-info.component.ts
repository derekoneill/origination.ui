import {
  Component,
  ChangeDetectionStrategy,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Subscription } from 'rxjs';
import {
  Session,
  Application,
  PaymentInformation,
  PayFrequency,
} from '@ua/shared/data-access';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DropDownItem } from '@progleasing/grit-core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import * as fromApply from '../+state';
import * as fromFlowRoutes from '../+state/flow-routes';
import * as fromPayment from '../+state/payment';
import * as fromAuthentications from '../+state/authentication';
import {
  addTrailingZerosToCurrency,
  formatFormDate,
  generateId,
  validateMonthlyGrossIncome,
} from '@ua/shared/utilities';
import { UnifiedApps } from '@ua/shared/state';

interface DropDownItemWithPayFrequency extends DropDownItem {
  enum: PayFrequency;
}

interface PayDateMinMaxRule {
  minDate: Date;
  maxDate: Date;
}

enum IncomeInfoFormFields {
  MonthlyIncome = 'monthlyIncome',
  LastPayDate = 'lastPayDate',
  NextPayDate = 'nextPayDate',
  PaymentFrequency = 'paymentFrequency',
}

@Component({
  selector: 'ua-income-info',
  templateUrl: './income-info.component.html',
  styleUrls: ['./income-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IncomeInfoComponent implements OnInit, OnDestroy {
  subs: Subscription[] = [];
  incomeForm: FormGroup;
  authenticationState: fromAuthentications.AuthenticationState;
  session: Session;
  application: Application;
  paymentInformation: PaymentInformation;
  payFrequency = PayFrequency;
  paymentOptions: DropDownItemWithPayFrequency[];
  lastPayDateMinMaxRule: PayDateMinMaxRule;
  nextPayDateMinMaxRule: PayDateMinMaxRule;

  statuses = fromAuthentications.Status;
  saveResumableStatus: fromAuthentications.SavingResumableStatus;

  constructor(
    private store$: Store<fromApply.ApplyState>,
    private router: Router,
    private formBuilder: FormBuilder,
    private translateService: TranslateService
  ) {
    this.lastPayDateMinMaxRule = this.nextPayDateMinMaxRule = {
      minDate: undefined,
      maxDate: undefined,
    };
  }

  ngOnInit(): void {
    this.subs.push(
      this.store$
        .pipe(select(fromApply.getAuthenticationStateSelector))
        .subscribe(
          (authenticationState) =>
            (this.authenticationState = authenticationState)
        ),
      this.store$
        .pipe(select(fromApply.getSessionSelector))
        .subscribe((session) => (this.session = session)),
      this.store$
        .pipe(select(fromApply.getApplicationSelector))
        .subscribe((application) => (this.application = application)),
      this.store$
        .pipe(select(fromApply.getPaymentInformationSelector))
        .subscribe(
          (paymentInformation) => (this.paymentInformation = paymentInformation)
        ),
      this.store$
        .pipe(select(fromApply.getSaveResumableStatusSelector))
        .subscribe(
          (saveResumableStatus) =>
            (this.saveResumableStatus = saveResumableStatus)
        )
    );

    const monthlyIncome: string =
      this.application.monthlyGrossIncome &&
      this.application.monthlyGrossIncome > 0
        ? this.application.monthlyGrossIncome.toString()
        : '';
    const lastPayDate: string = formatFormDate(
      this.paymentInformation.lastPayDate
    );
    const nextPayDate: string = formatFormDate(
      this.paymentInformation.nextPayDate
    );
    const paymentFrequency = this.paymentInformation.paymentFrequency
      ? this.translateService.instant(
          `IncomeInfo.PaymentOptions.${this.paymentInformation.paymentFrequency}`
        )
      : '';

    this.paymentOptions = this.buildPaymentFrequencyOptions();
    this.incomeForm = this.formBuilder.group({
      monthlyIncome: [
        monthlyIncome,
        validateMonthlyGrossIncome(
          this.translateService.instant(
            'IncomeInfo.Errors.MonthlyIncomeRequired'
          ),
          this.translateService.instant('IncomeInfo.Errors.MonthlyIncomeMin'),
          this.translateService.instant('IncomeInfo.Errors.MonthlyIncomeMax')
        ),
      ],
      lastPayDate: [lastPayDate, Validators.required],
      nextPayDate: [nextPayDate, Validators.required],
      paymentFrequency: [paymentFrequency, Validators.required],
    });

    this.calculateMinAndMaxPayDateRule();
  }

  ngOnDestroy(): void {
    this.subs.forEach((s) => s.unsubscribe());
  }

  disallowNonDigitInput(keyPressEvent) {
    const charCode = keyPressEvent.charCode || keyPressEvent.keyCode;
    const newChar = String.fromCharCode(charCode);
    const testRegex = /[^0-9]/g;
    if (testRegex.test(newChar)) {
      return false;
    }
  }

  addTrailingZeros(value: string): void {
    if (value) {
      this.incomeForm
        .get(IncomeInfoFormFields.MonthlyIncome)
        .setValue(addTrailingZerosToCurrency(value));
    }
  }

  onContinue() {
    if (this.incomeForm.valid) {
      const correlationId = generateId();
      const freqDisplayText = this.incomeForm.get(
        IncomeInfoFormFields.PaymentFrequency
      ).value;
      const paymentInfo: PaymentInformation = {
        ...this.paymentInformation,
        sessionId: this.session.sessionId,
        paymentFrequency: this.convertPayFrequencyDisplayTextToPayFrequencyEnum(
          freqDisplayText
        ),
        lastPayDate: this.incomeForm.get(IncomeInfoFormFields.LastPayDate)
          .value,
        nextPayDate: this.incomeForm.get(IncomeInfoFormFields.NextPayDate)
          .value,
      };
      this.store$.dispatch(
        fromPayment.updatePaymentInformation(paymentInfo, correlationId)
      );

      const mgi = this.incomeForm
        .get(IncomeInfoFormFields.MonthlyIncome)
        .value.replace(/,/gi, '');
      const application: Application = {
        ...this.application,
        sessionId: this.session.sessionId,
        monthlyGrossIncome: parseFloat(mgi),
      };

      this.store$.dispatch(
        fromApply.saveApplicationResumableStateSplitter(
          {
            resumableState: {
              sessionId: this.session.sessionId,
              resumeData: {
                authId: this.authenticationState.authenticationId,
                ssn: this.authenticationState.ssn,
                route: this.router.url,
              },
            },
            application: application,
            nextRoute: {
              from: UnifiedApps.apply.children.incomeInfo.route,
              direction: fromFlowRoutes.RouteDirection.Next,
              params: { sid: this.session.sessionId },
            },
          },
          correlationId
        )
      );
    }
  }

  private buildPaymentFrequencyOptions(): DropDownItemWithPayFrequency[] {
    const payOptions: DropDownItemWithPayFrequency[] = [];
    for (const payOption in PayFrequency) {
      if (isNaN(Number(payOption))) {
        const item: DropDownItemWithPayFrequency = {
          id: `${payOption}`,
          value: this.translateService.instant(
            `IncomeInfo.PaymentOptions.${payOption}`
          ),
          displayText: this.translateService.instant(
            `IncomeInfo.PaymentOptions.${payOption}`
          ),
          searchableTerms: this.translateService.instant(
            'IncomeInfo.PaymentSearchTerms'
          ),
          enum: PayFrequency[payOption as keyof typeof PayFrequency],
        };
        payOptions.push(item);
      }
    }
    return payOptions;
  }

  private convertPayFrequencyDisplayTextToPayFrequencyEnum(
    payFrequencyDispayText: string
  ): PayFrequency {
    for (const item of this.paymentOptions) {
      if (item.displayText === payFrequencyDispayText) {
        return item.enum;
      }
    }
    return PayFrequency.EveryOtherWeek;
  }

  private calculateMinAndMaxPayDateRule(): void {
    let minDate = new Date();
    let maxDate = new Date();
    const today = new Date();

    minDate = new Date(minDate.setDate(minDate.getDate() - 35));
    maxDate = new Date(maxDate.setDate(new Date().getDate() + 35));
    this.lastPayDateMinMaxRule = {
      minDate: minDate,
      maxDate: today,
    };

    const nextMinDate = today;
    if (this.incomeForm) {
      const selectedDate = this.incomeForm.get(IncomeInfoFormFields.LastPayDate)
        .value;
      if (selectedDate) {
        const selected = moment(new Date(selectedDate)).format('MM DD YYYY');
        const now = moment().format('MM DD YYYY');
        if (selected === now) {
          nextMinDate.setDate(today.getDate() + 1);
        }
      }
    }

    this.nextPayDateMinMaxRule = {
      minDate: nextMinDate,
      maxDate: maxDate,
    };
  }
}
