import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { SlideshowComponent } from './slideshow/slideshow.component';
import { SlideshowManager } from './slideshow-manager/slideshow-manager';
import { TermsSheetComponent } from './terms-sheet/terms-sheet.component';
import { TermssheetManager } from './termssheet-manager/termssheet-manager';
import { ComprehensionLayoutComponent } from './comprehension-layout/comprehension-layout.component';
import { UnifiedApps } from '@ua/shared/state';

const routes: Routes = [
  {
    path: '',
    component: ComprehensionLayoutComponent,
    children: [
      {
        path: UnifiedApps.comprehension.children.slideShow.route,
        component: SlideshowManager,
      },
      {
        path: UnifiedApps.comprehension.children.termsSheet.route,
        component: TermssheetManager,
      },
      {
        path: '**',
        redirectTo: UnifiedApps.comprehension.children.slideShow.route,
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComprehensionRoutingModule {
  constructor() {}
  static components = [
    ComprehensionLayoutComponent,
    SlideshowManager,
    SlideshowComponent,
    TermssheetManager,
    TermsSheetComponent,
  ];
}
