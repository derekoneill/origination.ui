import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { select, Store } from '@ngrx/store';
import * as fromSelectors from '../+state/comprehension.selectors';
import * as fromActions from '../+state/comprehension.actions';
import * as fromSharedState from '@ua/shared/state';
import { Router } from '@angular/router';

@Component({
  selector: 'ua-comprehension-layout',
  templateUrl: './comprehension-layout.component.html',
  styleUrls: ['./comprehension-layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComprehensionLayoutComponent implements OnInit, OnDestroy {
  readonly subs: Subscription[] = [];
  initializeComprehensionContent$: Observable<boolean>;
  constructor(
    private readonly store$: Store,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.subs.push(
      this.store$
        .pipe(select(fromSelectors.comprehensionContentInitializedSelector))
        .subscribe((initialized) => {
          if (!initialized) {
            this.store$.dispatch(fromActions.initComprehensionContent());
          }
        })
    );
  }

  onBack() {
    this.store$.dispatch(
      fromActions.walkBackInComprehensionFlow({
        route: this.router.url,
        params: {},
      })
    );
  }

  onClose() {
    this.store$.dispatch(
      fromSharedState.navigateRoute({
        route: fromSharedState.UnifiedApps.comprehension.route,
        params: {},
      })
    );
  }

  ngOnDestroy(): void {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
