import { Router } from '@angular/router';
import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { Store } from '@ngrx/store';
import { ComprehensionLayoutComponent } from './comprehension-layout.component';

describe('ComprehensionLayoutComponent', () => {
  let component: ComprehensionLayoutComponent;
  let mockStore: SubstituteOf<Store>;
  let mockRouter: SubstituteOf<Router>;

  beforeEach(() => {
    mockStore = Substitute.for<Store>();
    mockRouter = Substitute.for<Router>();
    component = new ComprehensionLayoutComponent(mockStore, mockRouter);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
