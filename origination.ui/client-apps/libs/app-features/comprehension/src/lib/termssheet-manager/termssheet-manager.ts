import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import * as fromSelectors from '../+state/comprehension.selectors';
import * as fromActions from '../+state/comprehension.actions';

@Component({
  selector: 'ua-termssheet-manager',
  templateUrl: './termssheet-manager.html',
  styleUrls: ['./termssheet-manager.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TermssheetManager implements OnInit {
  agreementLabel$: Observable<string>;
  bodyCopy$: Observable<string>;
  heading$: Observable<string>;
  terms$: Observable<string[]>;

  constructor(private readonly store$: Store) {}

  ngOnInit(): void {
    this.agreementLabel$ = this.store$.pipe(
      select(fromSelectors.termsSheetAgreementLabelSelector)
    );
    this.bodyCopy$ = this.store$.pipe(
      select(fromSelectors.termsSheetBodyCopySelector)
    );
    this.heading$ = this.store$.pipe(
      select(fromSelectors.termsSheetHeaderSelector)
    );
    this.terms$ = this.store$.pipe(
      select(fromSelectors.termsSheetTermsSelector)
    );
  }

  onTermsSheetAgreement() {
    this.store$.dispatch(fromActions.acceptTermsAgreement());
  }
}
