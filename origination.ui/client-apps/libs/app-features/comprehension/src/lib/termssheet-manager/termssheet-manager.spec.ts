import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { Store } from '@ngrx/store';
import { TermssheetManager } from './termssheet-manager';

describe('TermssheetManager', () => {
  let component: TermssheetManager;
  let mockStore: SubstituteOf<Store>;

  beforeEach(() => {
    mockStore = Substitute.for<Store>();
    component = new TermssheetManager(mockStore);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
