import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromSelectors from '../+state/comprehension.selectors';
import * as fromActions from '../+state/comprehension.actions';
import * as fromReducers from '../+state/comprehension.reducers';

@Component({
  selector: 'ua-comprehension-layout',
  templateUrl: './slideshow-manager.html',
  styleUrls: ['./slideshow-manager.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SlideshowManager implements OnInit {
  slideControl$: Observable<fromReducers.SlideControl>;
  canGoPrevious$: Observable<boolean>;
  currentSlide$: Observable<fromReducers.Slide>;
  slideIndex$: Observable<number>;
  totalOfSlides$: Observable<number>;

  constructor(private readonly store$: Store) {}

  ngOnInit(): void {
    this.slideControl$ = this.store$.pipe(
      select(fromSelectors.slideControlSelector)
    );
    this.canGoPrevious$ = this.store$.pipe(
      select(fromSelectors.canGoPreviousSelector)
    );
    this.currentSlide$ = this.store$.pipe(
      select(fromSelectors.currentSlideSelector)
    );
    this.slideIndex$ = this.store$.pipe(
      select(fromSelectors.slideIndexSelector)
    );
    this.totalOfSlides$ = this.store$.pipe(
      select(fromSelectors.totalOfSlidesSelector)
    );
  }

  gotoSlide(event: { slideIndex: number; totalOfSlides: number }) {
    this.store$.dispatch(
      fromActions.gotoSlide(event.slideIndex, event.totalOfSlides)
    );
  }
}
