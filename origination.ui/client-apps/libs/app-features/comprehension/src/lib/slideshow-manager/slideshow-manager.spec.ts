import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { Store } from '@ngrx/store';
import { SlideshowManager } from './slideshow-manager';

describe('Slideshow Manager', () => {
  let component: SlideshowManager;
  let mockStore: SubstituteOf<Store>;

  beforeEach(() => {
    mockStore = Substitute.for<Store>();
    component = new SlideshowManager(mockStore);
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });
});
