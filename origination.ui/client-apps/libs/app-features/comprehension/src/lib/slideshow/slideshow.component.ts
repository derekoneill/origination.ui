import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import * as fromReducers from '../+state/comprehension.reducers';

interface GoToSlideEvent {
  slideIndex: number;
  totalOfSlides: number;
}

@Component({
  selector: 'ua-comprehension-slideshow',
  templateUrl: './slideshow.component.html',
  styleUrls: ['./slideshow.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SlideshowComponent implements OnInit {
  @Input() slideControl: fromReducers.SlideControl;
  @Input() canGoPrevious: boolean;
  @Input() currentSlide: fromReducers.Slide;
  @Input() slideIndex: number;
  @Input() totalOfSlides: number;

  @Output() gotoSlide: EventEmitter<GoToSlideEvent> = new EventEmitter<
    GoToSlideEvent
  >();

  constructor() {}

  ngOnInit(): void {}

  onPrevious() {
    this.gotoSlide.emit({
      slideIndex: this.slideIndex - 1,
      totalOfSlides: this.totalOfSlides,
    });
  }

  onNext() {
    this.gotoSlide.emit({
      slideIndex: this.slideIndex + 1,
      totalOfSlides: this.totalOfSlides,
    });
  }
}
