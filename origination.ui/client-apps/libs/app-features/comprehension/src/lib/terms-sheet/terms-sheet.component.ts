import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ua-terms-sheet',
  templateUrl: './terms-sheet.component.html',
  styleUrls: ['./terms-sheet.component.scss'],
})
export class TermsSheetComponent implements OnInit {
  @Input() agreementLabel: string;
  @Input() bodyCopy: string;
  @Input() heading: string;
  @Input() terms: string[];

  @Output() agreementEvent: EventEmitter<void> = new EventEmitter<void>();

  constructor() {}

  public ngOnInit(): void {}

  public agreeToTerms(): void {
    this.agreementEvent.emit();
  }
}
