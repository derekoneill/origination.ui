import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComprehensionRoutingModule } from './comprehension.routing.module';
import { ComponentsModule } from '@ua/shared/ui';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromComprehensionReducers from './+state/comprehension.reducers';
import * as fromEffects from './+state/comprehension.effects';
import { UnifiedApps } from '@ua/shared/state';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    StoreModule.forFeature(
      UnifiedApps.comprehension.route,
      fromComprehensionReducers.reducer
    ),
    EffectsModule.forFeature([fromEffects.ComprehensionEffects]),
    ComprehensionRoutingModule,
  ],
  declarations: [ComprehensionRoutingModule.components],
})
export class ComprehensionModule {}
