import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { forkJoin, Observable, of, race } from 'rxjs';
import { Action } from '@ngrx/store';
import {
  catchError,
  filter,
  first,
  map,
  mergeMap,
  switchMap,
} from 'rxjs/operators';
import { ContentService } from '@ua/shared/data-access';
import * as fromActions from './comprehension.actions';
import * as fromSharedState from '@ua/shared/state';
import { UnifiedApps } from '@ua/shared/state';

@Injectable()
export class ComprehensionEffects {
  initComprehensionContent$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.initComprehensionContent),
      switchMap((action) => [
        fromActions.getTermsSheet(
          {
            contentPageKey: 'terms-sheets',
            instanceType: 'lease-cost-terms',
          },
          action.correlationId
        ),
        fromActions.getSlides(
          {
            contentPageKey: 'comprehension-slideshow',
          },
          action.correlationId
        ),
      ])
    )
  );

  initComprehensionContentSuccess$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.initComprehensionContent),
      switchMap((action) => {
        const getSlides$ = this.actions$.pipe(
          ofType(fromActions.getSlidesSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const getTermsSheet$ = this.actions$.pipe(
          ofType(fromActions.getTermsSheetSuccess),
          filter((a) => a.correlationId === action.correlationId),
          first()
        );

        const errors$ = this.actions$.pipe(
          ofType(fromActions.getSlidesError, fromActions.getTermsSheetError),
          filter((a) => a.correlationId === action.correlationId)
        );

        return race(
          forkJoin({
            getSlides: getSlides$,
            getTermsSheet: getTermsSheet$,
          }),
          errors$
        ).pipe(
          map((res) => {
            return fromActions.initComprehensionContentSuccess(
              action.correlationId
            );
          })
        );
      })
    )
  );

  initSlideshows$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getSlides),
      mergeMap((action) =>
        this.contentService
          .getContent<fromActions.SlideshowModel[]>(
            action.contentRequest.contentPageKey
          )
          .pipe(
            map((res) =>
              fromActions.getSlidesSuccess(res[0], action.correlationId)
            ),
            catchError((error) =>
              of(fromActions.getSlidesError(error, action.correlationId))
            )
          )
      )
    )
  );

  getTermsSheet$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.getTermsSheet),
      mergeMap((action) =>
        this.contentService
          .getContent<fromActions.TermsSheetModel>(
            action.contentRequest.contentPageKey,
            action.contentRequest.instanceType
          )
          .pipe(
            map((res) =>
              fromActions.getTermsSheetSuccess(res[0], action.correlationId)
            ),
            catchError((error) =>
              of(fromActions.getTermsSheetError(error, action.correlationId))
            )
          )
      )
    )
  );

  gotoTermsSheetAfterViewingAllSlide: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.gotoSlide),
      switchMap((action) => {
        const actions = [];
        if (action.slideIndex === action.totalOfSlides) {
          actions.push(
            fromSharedState.navigateRoute(
              {
                route: `${UnifiedApps.comprehension.route}/${UnifiedApps.comprehension.children.termsSheet.route}`,
                params: {},
              },
              action.correlationId
            )
          );
        }

        return actions;
      })
    )
  );

  gotoApplyFlowAfterAcceptTerms: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.acceptTermsAgreement),
      switchMap((action) => {
        const actions = [];
        actions.push(
          fromSharedState.navigateRoute(
            {
              route: `${UnifiedApps.apply.route}/${UnifiedApps.apply.children.getStarted.route}`,
              params: {},
            },
            action.correlationId
          )
        );
        return actions;
      })
    )
  );

  walkBackInComprehensionFlow$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromActions.walkBackInComprehensionFlow),
      switchMap((action) => {
        const actions = [];
        if (
          action.navigateRoute.route
            .toLowerCase()
            .startsWith(
              `/${UnifiedApps.comprehension.route}/${UnifiedApps.comprehension.children.termsSheet.route}`
            )
        ) {
          actions.push(
            fromSharedState.navigateRoute(
              {
                route: `${UnifiedApps.comprehension.route}/${UnifiedApps.comprehension.children.slideShow.route}`,
                params: {},
              },
              action.correlationId
            )
          );
        }

        return actions;
      })
    )
  );

  constructor(
    private readonly actions$: Actions,
    private readonly contentService: ContentService
  ) {}
}
