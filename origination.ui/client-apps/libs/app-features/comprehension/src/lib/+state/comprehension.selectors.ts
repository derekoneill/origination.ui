import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UnifiedApps } from '@ua/shared/state';
import * as fromSharedState from '@ua/shared/state';
import * as fromComprehensionReducers from './comprehension.reducers';

export interface State extends fromSharedState.State {
  comprehension: fromComprehensionReducers.ComprehensionState;
}

const comprehensionSelectors = createFeatureSelector<
  fromComprehensionReducers.ComprehensionState
>(UnifiedApps.comprehension.route);

export const comprehensionContentInitializedSelector = createSelector(
  comprehensionSelectors,
  (state) => state.initialized
);

export const slideshowSelector = createSelector(
  comprehensionSelectors,
  (state) => state.slideshow
);

export const slideControlSelector = createSelector(
  slideshowSelector,
  (state) => state.slideControl
);

export const currentSlideSelector = createSelector(
  slideshowSelector,
  (state) => state.currentSlide
);

export const slideIndexSelector = createSelector(
  slideshowSelector,
  (state) => state.slideIndex
);

export const canGoPreviousSelector = createSelector(
  slideshowSelector,
  (state) => state.canGoPrevious
);

export const totalOfSlidesSelector = createSelector(
  slideshowSelector,
  (state) => state.totalOfSlides
);

export const termsSheetSelector = createSelector(
  comprehensionSelectors,
  (state) => state.termsSheet
);

export const termsSheetAgreementLabelSelector = createSelector(
  termsSheetSelector,
  (state) => state.agreementLabel
);

export const termsSheetHeaderSelector = createSelector(
  termsSheetSelector,
  (state) => state.heading
);

export const termsSheetBodyCopySelector = createSelector(
  termsSheetSelector,
  (state) => state.bodyCopy
);

export const termsSheetTermsSelector = createSelector(
  termsSheetSelector,
  (state) => state.terms
);
