import { createAction, union } from '@ngrx/store';
import { generateId } from '@ua/shared/utilities';
import { SlideControl, Slide } from './comprehension.reducers';
import { HttpErrorResponse } from '@angular/common/http';
import * as fromSharedState from '@ua/shared/state';

export interface ContentRequest {
  contentPageKey: string;
  instanceType?: string;
}

const SlideshowPrefix = '[Comprehension-Slideshow]';

export interface SlideshowModel {
  slideControls: SlideControl;
  slides: Slide[];
}

export const getSlides = createAction(
  `${SlideshowPrefix} Get Sides`,
  (contentRequest: ContentRequest, correlationId: string = generateId()) => ({
    contentRequest,
    correlationId,
  })
);

export const getSlidesSuccess = createAction(
  `${SlideshowPrefix} Get Slides Success`,
  (slideshowModel: SlideshowModel, correlationId: string = generateId()) => ({
    slideshowModel,
    correlationId,
  })
);

export const getSlidesError = createAction(
  `${SlideshowPrefix} Get Slides Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

export const gotoSlide = createAction(
  `${SlideshowPrefix} Get To Nth Slide`,
  (
    slideIndex: number,
    totalOfSlides: number,
    correlationId: string = generateId()
  ) => ({
    slideIndex,
    totalOfSlides,
    correlationId,
  })
);

const TermsSheetPrefix = '[Comprehension-TermsSheet]';

export interface TermsSheetModel {
  agreementLabel: string;
  bodyCopy: string;
  heading: string;
  terms: string[];
}

export const getTermsSheet = createAction(
  `${TermsSheetPrefix} Get TermsSheet`,
  (contentRequest: ContentRequest, correlationId: string = generateId()) => ({
    contentRequest,
    correlationId,
  })
);

export const getTermsSheetSuccess = createAction(
  `${TermsSheetPrefix} Get TermsSheet Success`,
  (termsSheetModel: TermsSheetModel, correlationId: string = generateId()) => ({
    termsSheetModel,
    correlationId,
  })
);

export const getTermsSheetError = createAction(
  `${TermsSheetPrefix} Get TermsSheet Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

const ComprehensionContentPrefix = '[Comprehension-Content]';

export const initComprehensionContent = createAction(
  `${ComprehensionContentPrefix} Initialize Comprehension Content`,
  (correlationId: string = generateId()) => ({ correlationId })
);

export const initComprehensionContentSuccess = createAction(
  `${ComprehensionContentPrefix} Initialize Comprehension Content Success`,
  (correlationId: string = generateId()) => ({ correlationId })
);

export const acceptTermsAgreement = createAction(
  '[TermsSheet-Agreement] Accept TermsSheet Agreement',
  (correlationId: string = generateId()) => ({ correlationId })
);

const ComprehensionFlowPrefix = '[Comprehension-Flow]';
export const walkBackInComprehensionFlow = createAction(
  `${ComprehensionFlowPrefix} Walk Back In Comprehension Flow`,
  (
    navigateRoute: fromSharedState.NavigateRoutePayload,
    correlationId: string = generateId()
  ) => ({
    navigateRoute,
    correlationId,
  })
);

const allActions = union({
  initComprehensionContent,
  initComprehensionContentSuccess,
  getSlides,
  getSlidesSuccess,
  getSlidesError,
  gotoSlide,
  getTermsSheet,
  getTermsSheetSuccess,
  getTermsSheetError,
  acceptTermsAgreement,
  walkBackInComprehensionFlow,
});

export type ComprehensionActions = typeof allActions;
