import { createReducer, on } from '@ngrx/store';
import * as fromComprehensionActions from './comprehension.actions';
import * as fromSharedState from '@ua/shared/state';

export interface SlideControl {
  previousLabel: string;
  nextLabel: string;
}

export interface Slide {
  heading: string;
  bodyCopy: string;
  icon: string;
}

export interface SlideshowState extends fromSharedState.State {
  initialized: boolean;
  slideControl: SlideControl;
  slides: { [key: number]: Slide };
  totalOfSlides: number;
  currentSlide: Slide;
  slideIndex: number;
  canGoPrevious: boolean;
}

export interface TermsSheetState extends fromSharedState.State {
  initialized: boolean;
  agreementLabel: string;
  bodyCopy: string;
  heading: string;
  terms: string[];
}

export interface ComprehensionState extends fromSharedState.State {
  initialized: boolean;
  slideshow: SlideshowState;
  termsSheet: TermsSheetState;
}

export const initSlideshowState = (
  overrides: Partial<SlideshowState> = {}
): SlideshowState => ({
  initialized: false,
  slideControl: {
    previousLabel: undefined,
    nextLabel: undefined,
  },
  slides: {},
  totalOfSlides: 0,
  slideIndex: -1,
  currentSlide: undefined,
  canGoPrevious: false,
  error: undefined,
  ...overrides,
});

export const initTermsSheetState = (
  overrides: Partial<TermsSheetState> = {}
): TermsSheetState => ({
  initialized: false,
  agreementLabel: '',
  bodyCopy: '',
  heading: '',
  terms: [],
  ...overrides,
});

export const initState = (
  overrides: Partial<ComprehensionState> = {}
): ComprehensionState => ({
  initialized: false,
  slideshow: initSlideshowState(),
  termsSheet: initTermsSheetState(),
  ...overrides,
});

export const initComprehensionContentSuccessHandler = (
  state: ComprehensionState
): ComprehensionState => {
  return {
    ...state,
    initialized: true,
  };
};

export const getSlidesSuccessHandler = (
  state: ComprehensionState,
  action
): ComprehensionState => {
  const slideshowModel = action.slideshowModel;
  const newState = {
    ...state,
    slideshow: {
      ...state.slideshow,
      initialized: true,
      slideControl: slideshowModel.slideControls,
      totalOfSlides: slideshowModel.slides.length,
      slides: slideshowModel.slides.reduce((accumulator, slide, index) => {
        const currentField = {
          [index]: slide,
        };
        return { ...accumulator, ...currentField };
      }, {}),
      canGoPrevious: false,
      slideIndex: 0,
    },
  };
  newState.slideshow.currentSlide =
    newState.slideshow.slides[newState.slideshow.slideIndex];
  return newState;
};

export const getSlidesErrorHandler = (
  state: ComprehensionState,
  action
): ComprehensionState => ({
  ...state,
  slideshow: {
    ...state.slideshow,
    error: action.error,
  },
});

export const moveToSlideIndexHandler = (
  state: ComprehensionState,
  action
): ComprehensionState => {
  const newState = { ...state };
  if (
    action.slideIndex >= 0 &&
    action.slideIndex <= state.slideshow.totalOfSlides - 1
  ) {
    newState.slideshow = {
      ...newState.slideshow,
      slideIndex: action.slideIndex,
      currentSlide: newState.slideshow.slides[action.slideIndex],
      canGoPrevious: action.slideIndex > 0,
    };
  }
  return newState;
};

export const getTermsSheetSuccessHandler = (
  state: ComprehensionState,
  action
): ComprehensionState => {
  const termsSheetModel = action.termsSheetModel;
  const newState = {
    ...state,
    termsSheet: {
      ...state.termsSheet,
      initialized: true,
      agreementLabel: termsSheetModel.agreementLabel,
      bodyCopy: termsSheetModel.bodyCopy,
      heading: termsSheetModel.heading,
      terms: termsSheetModel.terms,
    },
  };
  return newState;
};

export const getTermsSheetErrorHandler = (
  state: ComprehensionState,
  action
): ComprehensionState => ({
  ...state,
  termsSheet: {
    ...state.termsSheet,
    error: action.error,
  },
});

export const walkBackInComprehensionFlowHandler = (
  state: ComprehensionState,
  action
): ComprehensionState => {
  const newState = { ...state };
  if (
    action.navigateRoute.route
      .toLowerCase()
      .startsWith('/comprehension/slideshow')
  ) {
    const index = state.slideshow.slideIndex - 1;
    if (index >= 0) {
      newState.slideshow = {
        ...state.slideshow,
        slideIndex: index,
        currentSlide: state.slideshow.slides[index],
        canGoPrevious: index > 0,
      };
    }
  }

  return newState;
};

export const reducer = createReducer(
  initState(),
  on(
    fromComprehensionActions.initComprehensionContentSuccess,
    initComprehensionContentSuccessHandler
  ),
  on(fromComprehensionActions.getSlidesSuccess, getSlidesSuccessHandler),
  on(fromComprehensionActions.getSlidesError, getSlidesErrorHandler),
  on(fromComprehensionActions.gotoSlide, moveToSlideIndexHandler),
  on(
    fromComprehensionActions.getTermsSheetSuccess,
    getTermsSheetSuccessHandler
  ),
  on(fromComprehensionActions.getTermsSheetError, getTermsSheetErrorHandler),
  on(
    fromComprehensionActions.walkBackInComprehensionFlow,
    walkBackInComprehensionFlowHandler
  )
);
