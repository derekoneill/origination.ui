export const Constants = {
  UIRenderType: {
    IFRAME: 'IFRAME',
    WINDOW: 'WINDOW',
    REDIRECT: 'REDIRECT',
  },
  Messages: {
    CLOSE_MODAL: '__PROGRESSIVE_UI_CLOSE__',
  },
  ECommercePlatform: {
    MAGENTO: 'MAGENTO',
    SHOPIFY: 'SHOPIFY',
    DEMANDWARE: 'DEMANDWARE',
    WOOCOMMERCE: 'WOOCOMMERCE',
    CUSTOM: 'CUSTOM',
  },
  StatesMap: {
    AL: 'Alabama',
    AK: 'Alaska',
    AS: 'American Samoa',
    AZ: 'Arizona',
    AR: 'Arkansas',
    CA: 'California',
    CO: 'Colorado',
    CT: 'Connecticut',
    DE: 'Delaware',
    DC: 'District Of Columbia',
    FM: 'Federated States Of Micronesia',
    FL: 'Florida',
    GA: 'Georgia',
    GU: 'Guam',
    HI: 'Hawaii',
    ID: 'Idaho',
    IL: 'Illinois',
    IN: 'Indiana',
    IA: 'Iowa',
    KS: 'Kansas',
    KY: 'Kentucky',
    LA: 'Louisiana',
    ME: 'Maine',
    MH: 'Marshall Islands',
    MD: 'Maryland',
    MA: 'Massachusetts',
    MI: 'Michigan',
    MN: 'Minnesota',
    MS: 'Mississippi',
    MO: 'Missouri',
    MT: 'Montana',
    NE: 'Nebraska',
    NV: 'Nevada',
    NH: 'New Hampshire',
    NJ: 'New Jersey',
    NM: 'New Mexico',
    NY: 'New York',
    NC: 'North Carolina',
    ND: 'North Dakota',
    MP: 'Northern Mariana Islands',
    OH: 'Ohio',
    OK: 'Oklahoma',
    OR: 'Oregon',
    PW: 'Palau',
    PA: 'Pennsylvania',
    PR: 'Puerto Rico',
    RI: 'Rhode Island',
    SC: 'South Carolina',
    SD: 'South Dakota',
    TN: 'Tennessee',
    TX: 'Texas',
    UT: 'Utah',
    VT: 'Vermont',
    VI: 'Virgin Islands',
    VA: 'Virginia',
    WA: 'Washington',
    WV: 'West Virginia',
    WI: 'Wisconsin',
    WY: 'Wyoming',
  },
  StateSearch: [
    {
      value: 'AL',
      searchTerms: ['AL', 'Alabama'],
      display: 'Alabama',
    },
    {
      value: 'AK',
      searchTerms: ['AK', 'Alaska'],
      display: 'Alaska',
    },
    // {
    //     'value': 'AS',
    //     'searchTerms': ['AS', 'American Samoa'],
    //     'display': 'American Samoa'
    // },
    {
      value: 'AZ',
      searchTerms: ['AZ', 'Arizona'],
      display: 'Arizona',
    },
    {
      value: 'AR',
      searchTerms: ['AR', 'Arkansas'],
      display: 'Arkansas',
    },
    {
      value: 'CA',
      searchTerms: ['CA', 'California'],
      display: 'California',
    },
    {
      value: 'CO',
      searchTerms: ['CO', 'Colorado'],
      display: 'Colorado',
    },
    {
      value: 'CT',
      searchTerms: ['CT', 'Connecticut'],
      display: 'Connecticut',
    },
    {
      value: 'DE',
      searchTerms: ['DE', 'Delaware'],
      display: 'Delaware',
    },
    // {
    //     'value': 'DC',
    //     'searchTerms': ['DC', 'District Of Columbia'],
    //     'display': 'District Of Columbia'
    // },
    // {
    //     'value': 'FM',
    //     'searchTerms': ['FM', 'Federated States Of Micronesia'],
    //     'display': 'Federated States Of Micronesia'
    // },
    {
      value: 'FL',
      searchTerms: ['FL', 'Florida'],
      display: 'Florida',
    },
    {
      value: 'GA',
      searchTerms: ['GA', 'Georgia'],
      display: 'Georgia',
    },
    // {
    //     'value': 'GU',
    //     'searchTerms': ['GU', 'Guam'],
    //     'display': 'Guam'
    // },
    {
      value: 'HI',
      searchTerms: ['HI', 'Hawaii'],
      display: 'Hawaii',
    },
    {
      value: 'ID',
      searchTerms: ['ID', 'Idaho'],
      display: 'Idaho',
    },
    {
      value: 'IL',
      searchTerms: ['IL', 'Illinois'],
      display: 'Illinois',
    },
    {
      value: 'IN',
      searchTerms: ['IN', 'Indiana'],
      display: 'Indiana',
    },
    {
      value: 'IA',
      searchTerms: ['IA', 'Iowa'],
      display: 'Iowa',
    },
    {
      value: 'KS',
      searchTerms: ['KS', 'Kansas'],
      display: 'Kansas',
    },
    {
      value: 'KY',
      searchTerms: ['KY', 'Kentucky'],
      display: 'Kentucky',
    },
    {
      value: 'LA',
      searchTerms: ['LA', 'Louisiana'],
      display: 'Louisiana',
    },
    {
      value: 'ME',
      searchTerms: ['ME', 'Maine'],
      display: 'Maine',
    },
    // {
    //     'value': 'MH',
    //     'searchTerms': ['MH', 'Marshall Islands'],
    //     'display': 'Marshall Islands'
    // },
    {
      value: 'MD',
      searchTerms: ['MD', 'Maryland'],
      display: 'Maryland',
    },
    {
      value: 'MA',
      searchTerms: ['MA', 'Massachusetts'],
      display: 'Massachusetts',
    },
    {
      value: 'MI',
      searchTerms: ['MI', 'Michigan'],
      display: 'Michigan',
    },
    {
      value: 'MN',
      searchTerms: ['MN', 'Minnesota'],
      display: 'Minnesota',
    },
    {
      value: 'MS',
      searchTerms: ['MS', 'Mississippi'],
      display: 'Mississippi',
    },
    {
      value: 'MO',
      searchTerms: ['MO', 'Missouri'],
      display: 'Missouri',
    },
    {
      value: 'MT',
      searchTerms: ['MT', 'Montana'],
      display: 'Montana',
    },
    {
      value: 'NE',
      searchTerms: ['NE', 'Nebraska'],
      display: 'Nebraska',
    },
    {
      value: 'NV',
      searchTerms: ['NV', 'Nevada'],
      display: 'Nevada',
    },
    {
      value: 'NH',
      searchTerms: ['NH', 'New Hampshire'],
      display: 'New Hampshire',
    },
    {
      value: 'NJ',
      searchTerms: ['NJ', 'New Jersey'],
      display: 'New Jersey',
    },
    {
      value: 'NM',
      searchTerms: ['NM', 'New Mexico'],
      display: 'New Mexico',
    },
    {
      value: 'NY',
      searchTerms: ['NY', 'New York'],
      display: 'New York',
    },
    {
      value: 'NC',
      searchTerms: ['NC', 'North Carolina'],
      display: 'North Carolina',
    },
    {
      value: 'ND',
      searchTerms: ['ND', 'North Dakota'],
      display: 'North Dakota',
    },
    // {
    //     'value': 'MP',
    //     'searchTerms': ['MP', 'Northern Mariana Islands'],
    //     'display': 'Northern Mariana Islands'
    // },
    {
      value: 'OH',
      searchTerms: ['OH', 'Ohio'],
      display: 'Ohio',
    },
    {
      value: 'OK',
      searchTerms: ['OK', 'Oklahoma'],
      display: 'Oklahoma',
    },
    {
      value: 'OR',
      searchTerms: ['OR', 'Oregon'],
      display: 'Oregon',
    },
    // {
    //     'value': 'PW',
    //     'searchTerms': ['PW', 'Palau'],
    //     'display': 'Palau'
    // },
    {
      value: 'PA',
      searchTerms: ['PA', 'Pennsylvania'],
      display: 'Pennsylvania',
    },
    // {
    //     'value': 'PR',
    //     'searchTerms': ['PR', 'Puerto Rico'],
    //     'display': 'Puerto Rico'
    // },
    {
      value: 'RI',
      searchTerms: ['RI', 'Rhode Island'],
      display: 'Rhode Island',
    },
    {
      value: 'SC',
      searchTerms: ['SC', 'South Carolina'],
      display: 'South Carolina',
    },
    {
      value: 'SD',
      searchTerms: ['SD', 'South Dakota'],
      display: 'South Dakota',
    },
    {
      value: 'TN',
      searchTerms: ['TN', 'Tennessee'],
      display: 'Tennessee',
    },
    {
      value: 'TX',
      searchTerms: ['TX', 'Texas'],
      display: 'Texas',
    },
    {
      value: 'UT',
      searchTerms: ['UT', 'Utah'],
      display: 'Utah',
    },
    {
      value: 'VT',
      searchTerms: ['VT', 'Vermont'],
      display: 'Vermont',
    },
    // {
    //     'value': 'VI',
    //     'searchTerms': ['VI', 'Virgin Islands'],
    //     'display': 'Virgin Islands'
    // },
    {
      value: 'VA',
      searchTerms: ['VA', 'Virginia'],
      display: 'Virginia',
    },
    {
      value: 'WA',
      searchTerms: ['WA', 'Washington'],
      display: 'Washington',
    },
    {
      value: 'WV',
      searchTerms: ['WV', 'West Virginia'],
      display: 'West Virginia',
    },
    {
      value: 'WI',
      searchTerms: ['WI', 'Wisconsin'],
      display: 'Wisconsin',
    },
    {
      value: 'WY',
      searchTerms: ['WY', 'Wyoming'],
      display: 'Wyoming',
    },
  ],

  StatesArray: () => {
    return Object.keys(Constants.StatesMap);
  },

  StatesWeDontDoBusinessIn: {
    MN: 'Minnesota',
    NJ: 'New Jersey',
    VT: 'Vermont',
    WI: 'Wisconsin',
    WY: 'Wyoming',
  },
  UsedItemStatesBlocked: {
    AL: 'Alabama',
    DC: 'District of Columbia',
    GA: 'Georgia',
    PA: 'Pennsylvania',
  },
  StatesWeDontShipTo: {
    CO: 'Colorado',
    HI: 'Hawaii',
    IA: 'Iowa',
    NE: 'Nebraska',
    SC: 'South Carolina',
    ME: 'Maine',
  },
};

export enum PgButtonType {
  Primary = 'primary',
  Secondary = 'secondary',
  Subtle = 'subtle',
  Destroy = 'destroy',
  Success = 'success',
  PrimaryLight = 'primary-light',
  SecondaryLight = 'secondary-light',
  SubtleLight = 'subtle-light',
  DestroyLight = 'destroy-light',
  SuccessLight = 'success-light',
}

export enum ApplicationStatus {
  APPROVED = 'Approved',
  PREAPPROVED = 'PreApproved',
  IN_PROCESS = 'InProcess',
  PENDING = 'Pending',
  DENIED = 'Denied',
}
