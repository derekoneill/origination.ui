// tslint:disable:no-shadowed-variable
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { Constants } from '../constants';

export const MIN_ITEM_COST = 149.99;
export const MAX_ITEM_COST = 4000;
export const MIN_AGE = 18;
export const MAX_AGE = 99;
export const MIN_MONTHLY_INCOME = 1;
export const MAX_MONTHLY_INCOME = 99999.98;

const dobRegex = /^(0[1-9]|1[012]|[0-9])\/(0[1-9]|[12][0-9]|3[01]|[0-9])\/(\d{4})$/;
const emailRegex = /(.+)@(.+)\.(.{2,})/;
const stateCodes = Object.keys(Constants.StatesMap);

export function rangeValidator(minValue: number, maxValue: number) {
  return (control: AbstractControl) => {
    const minAmountValidator = this.minAmount(minValue);
    const maxAmountValidator = this.maxAmount(maxValue);

    if (
      minAmountValidator(control) !== null ||
      maxAmountValidator(control) !== null
    ) {
      return { rangeValidator: true };
    }
    return null;
  };
}

export function minAmount(amount: number) {
  return (control: AbstractControl) => {
    if (control && control.value) {
      const value = parseFloat(control.value.replace(/([^0-9\.])/g, ''));
      if (isNaN(value)) {
        return null;
      }
      if (value < amount) {
        return { minAmount: true };
      }
    }
    return null;
  };
}

export function maxAmount(amount: number) {
  return (control: AbstractControl) => {
    if (control && control.value) {
      const value = parseFloat(control.value.replace(/([^0-9\.])/g, ''));
      if (isNaN(value)) {
        return null;
      }
      if (value > amount) {
        return { maxAmount: true };
      }
    }
    return null;
  };
}

export function ageRange(minAge: number, maxAge: number) {
  return (control: AbstractControl) => {
    if (control && control.value) {
      const value = control.value.replace(/\s/g, '');
      if (dobRegex.test(value)) {
        const calcAge = this.calculateAge(value);
        if (calcAge < minAge) {
          return { minAge: true };
        } else if (calcAge > maxAge) {
          return { maxAge: true };
        }
      }
    }
    return null;
  };
}

export function month(control: AbstractControl) {
  if (control && control.value) {
    const value = control.value;
    const month = parseInt(value.split('/')[0], 10);
    if (isNaN(month) || month > 12 || month < 1) {
      return { month: true };
    }
  }
  return null;
}

export function day(control: AbstractControl) {
  if (control && control.value) {
    const value = control.value;
    const dateElements = value.split('/');
    const month = parseInt(dateElements[0], 10);
    const day = parseInt(dateElements[1], 10);
    const year = parseInt(dateElements[2], 10);
    if (isNaN(day) && !isNaN(month)) {
      return { day: true };
    } else if (isNaN(day)) {
      return null;
    } else if (day === 0 || day > 31) {
      return { day: true };
    }
    const daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    if (
      !isNaN(year) &&
      (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0))
    ) {
      // Account for leap years
      daysInMonth[1] = 29;
    }
    if (!isNaN(year) && day > daysInMonth[month - 1]) {
      return { day: true };
    }
    return null;
  }
}

export function year(control: AbstractControl) {
  if (control && control.value) {
    const value = control.value;
    const dateElements = value.split('/');
    const month = parseInt(dateElements[0], 10);
    const day = parseInt(dateElements[1], 10);
    const year = parseInt(dateElements[2], 10);
    if (!isNaN(month) && !isNaN(day) && isNaN(year)) {
      return { year: true };
    } else if (!isNaN(year) && (year < 1900 || year > 3000)) {
      return { year: true };
    }
  }
  return null;
}

export function atMostAge(maxAge) {
  return (control: AbstractControl) => {
    if (control && control.value) {
      if (dobRegex.test(control.value)) {
        const calcAge = this.calculateAge(control.value);
        if (calcAge >= maxAge) {
          return {
            atMostAge: true,
          };
        }
      }
    }
    return null;
  };
}

export function phoneOrEmail(control: AbstractControl) {
  if (!control || !control.value) {
    return null;
  }
  if (control.value) {
    if (isValidEmail(control.value) || isValidPhone(control.value)) {
      return null;
    }
    return {
      phoneOrEmail: true,
    };
  }
}

export function isValidPhone(val: string) {
  return val.replace(/\D/gi, '').length === 10;
}

export function isValidEmail(val: string) {
  return val.indexOf('@') > -1 && val.indexOf('.') > -1;
}

export function validatePhoneOrEmail(
  requiredError: string,
  phoneOrEmailError: string
): ValidatorFn {
  return (control: AbstractControl): { [key: string]: string } => {
    if (!control.value) {
      return {
        error: requiredError,
      };
    }

    if (phoneOrEmail(control)) {
      return {
        error: phoneOrEmailError,
      };
    }
    return null;
  };
}

export function validateWithCallbackPredicate(
  hasError: () => boolean,
  errorMesage: { [key: string]: string }
): ValidatorFn {
  return (_: AbstractControl): { [key: string]: string } => {
    if (hasError && hasError()) {
      return errorMesage;
    }
    return null;
  };
}

export function validateMonthlyGrossIncome(
  requiredError: string,
  minValueError: string,
  maxValueError: string
): ValidatorFn {
  return (ctrl: AbstractControl): { [key: string]: string } => {
    if (ctrl) {
      let value = ctrl.value;
      if (!value) {
        return {
          error: requiredError,
        };
      }

      value = parseFloat(value.replace(/([^0-9\.])/g, ''));
      if (value < MIN_MONTHLY_INCOME) {
        return {
          error: minValueError,
        };
      }

      if (value > MAX_MONTHLY_INCOME) {
        return {
          error: maxValueError,
        };
      }
    }
    return null;
  };
}

export function bankRouting(control: AbstractControl) {
  const routingNumber = control.value;

  if (!routingNumber) {
    return {
      bankRouting: true,
    };
  }

  // Has to be 9 digits
  const match = routingNumber.match('^\\d{9}$');
  if (!match) {
    return {
      bankRouting: true,
    };
  }

  // The values we will use to multiply the routing values to calculate the check digit
  const weights = [3, 7, 1];
  let sum = 0;

  for (let i = 0; i < 8; i++) {
    sum += parseInt(routingNumber[i], 10) * weights[i % 3];
  }

  if (!((10 - (sum % 10)) % 10 === parseInt(routingNumber[8], 10))) {
    return {
      bankRouting: true,
    };
  }

  return null;
}

export function ssn(control: AbstractControl) {
  const val: string = control.value;
  const testRegex = /^(?!219099999|078051120)(?!666|000)\d{3}\d{2}\d{4}$/;
  if (val) {
    const testSSN = val.replace(/\D/g, '');
    if (testSSN.length !== 9) {
      return { length: true };
    }
    if (!testRegex.test(testSSN)) {
      return { ssn: true };
    } else {
    }
  }

  return null;
}

export function stateCode(control: AbstractControl) {
  const stateCode = control.value;
  if (stateCodes.indexOf(stateCode) >= 0 || stateCode === 'N/A') {
    return null;
  } else {
    return { state: true };
  }
}

export function startsWithLetter(control: AbstractControl) {
  const val: string = control.value;
  const testRegex = /^[a-zA-Z]/;
  if (val.length > 0 && !testRegex.test(val)) {
    return { startsWithLetter: true };
  }
  return null;
}

export function nameFormat(control: AbstractControl) {
  const val: string = control.value;
  const testRegex = /[^a-zA-Z\s-']/g;
  if (testRegex.test(val)) {
    return { nameFormat: true };
  }
  return null;
}

export function startsWithSpace(control: AbstractControl) {
  const val: string = control.value;
  const testRegex = /^\s/;
  if (testRegex.test(val)) {
    return { startsWithSpace: true };
  }
  return null;
}

export function emailFormat(control: AbstractControl) {
  if (control && control.value) {
    const value = control.value;
    if (!emailRegex.test(value)) {
      return { emailFormat: true };
    }
  }
  return null;
}

export function contains(str: string, identifier: string) {
  return (control: AbstractControl) => {
    if (control && control.value) {
      const value = control.value;
      if (value.indexOf(str) < 0) {
        const obj = {};
        obj[identifier] = true;
        return obj;
      }
    }
    return null;
  };
}

export function containsPattern(pattern: RegExp, identifier: string) {
  return (control: AbstractControl) => {
    if (control && control.value) {
      const value = control.value;
      if (!pattern.test(value)) {
        const obj = {};
        obj[identifier] = true;
        return obj;
      }
    }
    return null;
  };
}

export function emailCharacters(control: AbstractControl) {
  if (control && control.value) {
    const value = control.value;
    const emailSpecialCharsRegex = /[^a-zA-Z0-9\.\_@\+\-]/g;
    if (emailSpecialCharsRegex.test(value)) {
      return { emailCharacters: true };
    }
  }
  return null;
}

export function phoneLength(control: AbstractControl) {
  if (control && control.value) {
    const value = control.value;
    const digits = value.replace(/[^\d]/g, '');
    if (digits.length !== 10) {
      return { length: true };
    }
  }
  return null;
}

/*
 * This was taken from https://github.com/DrShaffopolis/bank-routing-number-validator/blob/master/index.js
 */
export function isValidRoutingNumber(routingNumber: string) {
  if (!routingNumber) {
    // all 0's is technically a valid routing number, but it's inactive
    return false;
  }

  let routing = routingNumber.toString();
  while (routing.length < 9) {
    routing = '0' + routing;
  }

  // Has to be 9 digits
  const match = routing.match('^\\d{9}$');
  if (!match) {
    return false;
  }

  // The first two digits of the nine digit RTN must be in the ranges 00 through 12, 21 through 32, 61 through 72, or 80.
  // https://en.wikipedia.org/wiki/Routing_transit_number
  const firstTwo = parseInt(routing.substring(0, 2), 10);
  const firstTwoValid =
    (0 <= firstTwo && firstTwo <= 12) ||
    (21 <= firstTwo && firstTwo <= 32) ||
    (61 <= firstTwo && firstTwo <= 72) ||
    firstTwo === 80;
  if (!firstTwoValid) {
    return false;
  }

  // This is the checksum
  // http://www.siccolo.com/Articles/SQLScripts/how-to-create-sql-to-calculate-routing-check-digit.html
  const weights = [3, 7, 1];
  let sum = 0;
  for (let i = 0; i < 8; i++) {
    sum += parseInt(routing[i], 10) * weights[i % 3];
  }

  return (10 - (sum % 10)) % 10 === parseInt(routing[8], 10);
}

export function routingNumber() {
  return (control: AbstractControl) => {
    if (control && control.value) {
      const input = control.value;
      // Testing Numbers
      if (
        input === '000000001' ||
        input === '000000002' ||
        input === '000000003'
      ) {
        return null; // Allow Testing numbers through
      }

      if (!/^[0-9]{9}$/.test(input)) {
        return { lengthRequirement: true };
      } else if (!this.isValidRoutingNumber(input)) {
        return { invalidRoutingNumber: true };
      }
    }
    return null;
  };
}

export function creditCard() {
  return (control: AbstractControl) => {
    if (control && control.value) {
      if (!this.doesCardMeetFormattingRequirement(control.value)) {
        return { creditCardFormatting: true };
      } else if (!this.luhnAlgorithmCheck(control.value)) {
        return { invalidCreditCard: true };
      }
    }
    return null;
  };
}

export function expirationDate(control: AbstractControl) {
  if (control && control.value) {
    let value = <string>control.value;
    value = value.replace(/[^0-9]/gi, '');
    const monthPart = value.substr(0, 2);
    const yearPart = value.substr(2, 2);
    const monthNumber = parseInt(monthPart, 10);
    if (monthNumber > 12 || monthNumber < 1) {
      return { invalidExpirationDate: true };
    }

    // today's date is in the year 2019, make sure in 81 years this is updated to pre-pend 21
    // you have been warned.
    const yearNumber = parseInt('20' + yearPart, 10);
    const today = new Date();
    if (yearNumber < today.getFullYear()) {
      return { invalidExpirationDate: true };
    } else if (
      yearNumber === today.getFullYear() &&
      monthNumber <= today.getMonth() + 1
    ) {
      return { invalidExpirationDate: true };
    }
  }

  return null;
}

function calculateAge(dateString) {
  const birth = new Date(dateString);
  const now = new Date();

  let age = now.getFullYear() - birth.getFullYear();

  // If now is JAN and birth is FEB we are still a year younger
  if (now.getMonth() < birth.getMonth()) {
    age -= 1;
  } else if (
    // else if Now is JAN 1 and BIRTH is JAN 2
    // We are still a year younger.
    now.getMonth() === birth.getMonth() &&
    now.getDate() < birth.getDate()
  ) {
    age -= 1;
  }
  return age;
}

function doesCardMeetFormattingRequirement(value: string) {
  if (!value || !value.length) {
    return false;
  }
  value = value.replace(/[^0-9]/gi, '');
  if (value && value.length > 14) {
    return true;
  }
  return false;
}

function luhnAlgorithmCheck(cardNumber: string) {
  // Clean the credit card input
  const cardNumberArray = cardNumber.replace(/\D/gi, '').split('');

  // The result of the algorithm
  let digitSum = 0;

  // Toggle for doubling every other digit
  let everyOtherToggle = false;

  // from right to left
  for (let i = cardNumberArray.length - 1; i >= 0; i--) {
    let numberDigit = parseInt(cardNumberArray[i], 10);

    if (everyOtherToggle) {
      // Every other digit is doubled
      numberDigit *= 2;

      // if the result of doubling greater than 9
      // use the sum of both digits (in this case we can subtract 9)
      if (numberDigit > 9) {
        numberDigit -= 9;
      }
    }

    // Add the result to the total algorithm sum;
    digitSum += numberDigit;

    // Flip the toggle
    everyOtherToggle = !everyOtherToggle;
  }

  // If digits sum is greater than 0
  // and divisible by 10, we are good.
  return digitSum > 0 && digitSum % 10 === 0;
}
