import { FormControl } from '@angular/forms';
import { Constants } from '@ua/shared/utilities';

export function cityValidator() {
  return (c: FormControl) => {
    const value = c.value;
    const numberRegex = /[0-9]/;
    const specialCharacterRegex = /[\!\@\#\$\%\^\&\*\_\<\>,\\\/\+\=\'\"\(\)\{\}\[\]\~`\?|\;\:]/;
    if (value && numberRegex.test(value)) {
      return { number: true };
    }
    if (specialCharacterRegex.test(value)) {
      return { specialChar: true };
    }

    if (value && value[0] === ' ') {
      return {
        spaceFormat: true,
      };
    }

    return null;
  };
}

export function stateValidator() {
  return (c: FormControl) => {
    const value = c.value;

    if (!value) {
      return;
    }

    const selectedValue = Constants.StateSearch.filter(
      (x) => x.value === value
    );
    if (value[0] === ' ') {
      return {
        spaceFormat: true,
      };
    } else if (!selectedValue.length) {
      return {
        selection: true,
      };
    }
    return null;
  };
}

export function zipValidator() {
  return (c: FormControl) => {
    const value = c.value;
    if (value && value.length < 5) {
      return {
        pattern: true,
      };
    } else if (value && value.length > 5 && value.length !== 10) {
      return {
        sixToEightDigits: true,
      };
    }
    return null;
  };
}

export function street1Validator() {
  return (c: FormControl) => {
    const value = c.value;

    if (
      value &&
      (value.match(/POB/gi) ||
        value.match(/P.O.B./gi) ||
        value.match(/PO Box/gi) ||
        value.match(/P.O. Box/gi) ||
        value.match(/Post Office/gi) ||
        value.match(/Post Office Box/gi))
    ) {
      return {
        poBox: true,
      };
    } else if (value && value[0] === ' ') {
      return {
        spaceFormat: true,
      };
    }
    return null;
  };
}

export function street2Validator() {
  return (c: FormControl) => {
    const value = c.value;

    if (value && value[0] === ' ') {
      return {
        spaceFormat: true,
      };
    }
    return null;
  };
}
