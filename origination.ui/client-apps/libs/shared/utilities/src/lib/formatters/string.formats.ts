import * as moment from 'moment';

export const dateOfBirthTextMask = [
  /\d/,
  /\d/,
  '/',
  /\d/,
  /\d/,
  '/',
  /\d/,
  /\d/,
  /\d/,
  /\d/,
];
export const ssnTextMask = [
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  /\d/,
  /\d/,
];
export const phoneTextMask = [
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  /\d/,
  /\d/,
];
export const expirationDateTextMask = [/\d/, /\d/, '/', /\d/, /\d/];

export function padLeft(str: string, strLen: number, padChar: string = '0') {
  return (padChar.repeat(strLen) + str).slice(-strLen);
}

export function padRight(str: string, strLen: number, padChar: string = '0') {
  return (str + padChar.repeat(strLen)).slice(0, strLen);
}

export function formatMoney(value: number | string): string {
  let strValue: string;
  if (typeof value === 'number') {
    strValue = value.toString();
  } else if (typeof value === 'string') {
    strValue = value;
  }
  let decimal = strValue.slice(-2);
  decimal = this.padLeft(decimal, 2);

  let theRest = '';
  if (strValue.length > 2) {
    theRest = strValue.substr(0, strValue.length - 2);
  }

  if (theRest.length) {
    for (let i = 0; i < theRest.length; i++) {
      if (theRest[i] !== '0') {
        break;
      } else {
        theRest = theRest.substr(1);
      }
    }
  }

  return `$ ${theRest || ''}.${decimal}`;
}

export function addTrailingZerosToCurrency(value: number | string): string {
  let strValue: string;
  if (typeof value === 'number') {
    strValue = value.toString();
  } else if (typeof value === 'string') {
    strValue = value;
  }

  if (strValue && strValue.length > 0) {
    if (strValue.indexOf('.') >= 0) {
      const decimalValue = strValue.split('.')[1];
      if (!decimalValue || decimalValue.length < 2) {
        const zerosToAppend = decimalValue ? '0' : '00';
        return strValue + zerosToAppend;
      }
    } else {
      return strValue + '.00';
    }
  }

  return strValue;
}

export function formatFormDate(date: Date | string): string {
  if (!date) {
    return null;
  }
  let formatDate;
  if (date instanceof Date) {
    if (!date.getFullYear()) {
      return null;
    }
    formatDate = new Date(<any>date);
  } else if (typeof date === 'string') {
    formatDate = moment(date).toDate();
    if (!formatDate.getFullYear()) {
      return null;
    }
  }
  const month = ('00' + (formatDate.getMonth() + 1).toString()).slice(-2);
  const day = ('00' + formatDate.getDate().toString()).slice(-2);
  const year = ('0000' + formatDate.getFullYear()).slice(-4);
  return `${month}/${day}/${year}`;
}

/*
  NOTE: SSN testing regex obtained from this source:
  https://www.codeproject.com/Articles/651609/Validating-Social-Security-Numbers-through-Regular

  Validated through unit tests

  this only deals with the 9 numbers, not with the formatting of the SSN
  if the SSN doesn't contain exactly 9 numbers, this will always return false
*/
export function validateSsn(ssn: string) {
  const testRegex = /^(?!219099999|078051120)(?!666|000)\d{3}\d{2}\d{4}$/;
  if (ssn) {
    const testSSN = ssn.replace(/\D/g, '');
    if (testSSN.length !== 9) {
      return { length: true };
    }
    if (!testRegex.test(testSSN)) {
      return { valid: true };
    } else {
    }
  }

  return null;
}

export function separatePhoneNumberWithDash(phoneNumber: string): string {
  let formatedPhoneNumber = phoneNumber;
  if (phoneNumber) {
    const onlyPhoneNumber = phoneNumber.replace(/\D/g, '');
    formatedPhoneNumber =
      onlyPhoneNumber.slice(0, 3) +
      '-' +
      onlyPhoneNumber.slice(3, 6) +
      '-' +
      onlyPhoneNumber.slice(6);
  }
  return formatedPhoneNumber;
}

export function getPayFrequencyText(frequency: string) {
  const freqText = frequency.split(/(?=[A-Z])/).join(' ');
  return freqText.toLowerCase();
}

export function normalizePhoneNumber(phoneNumber: string): string {
  const digits = phoneNumber.replace(/[^\d]/g, '');
  if (digits.length === 10) {
    return '+1' + digits;
  } else if (digits.length === 11) {
    return '+' + digits;
  }

  throw new Error('Invalid phone number, cannot normalize.');
}
