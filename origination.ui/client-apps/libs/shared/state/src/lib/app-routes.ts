export const UnifiedApps = {
  comprehension: {
    route: 'comprehension',
    children: {
      slideShow: {
        route: 'slideshow',
      },
      termsSheet: {
        route: 'terms-sheet',
      },
    },
  },

  apply: {
    route: 'apply',
    children: {
      getStarted: {
        route: 'get-started',
      },
      verifyCode: {
        route: 'verify-code',
      },
      basicInfo: {
        route: 'basic-info',
      },
      contactInfo: {
        route: 'contact-info',
      },
      incomeInfo: {
        route: 'income-info',
      },
      bankInfo: {
        route: 'bank-info',
      },
      submitApplication: {
        route: 'submit-application',
      },
      submittedApplication: {
        route: 'submitted-application',
      },
      loginResume: {
        route: 'login-resume',
      },
      somethingBroke: {
        route: 'something-broke',
      },
      applicationPending: {
        route: 'application-pending',
      },
      // define more children routes.
    },
  },

  problem: {
    route: 'problem',
    children: {
      unableToApprove: {
        route: 'unable-to-approve',
      },
    },
  },
};
