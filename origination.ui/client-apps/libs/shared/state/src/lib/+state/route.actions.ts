import { HttpErrorResponse } from '@angular/common/http';
import { Action, createAction, union } from '@ngrx/store';
import { generateId } from '@ua/shared/utilities';

export interface NavigateRoutePayload {
  route: string;
  params: { [key: string]: string };
}

export interface NavigateRouteAction extends Action {
  routePayload: NavigateRoutePayload;
  correlationId: string;
  error: HttpErrorResponse;
}

const ActionPrefix = '[Navigate-Route]';

export const navigateRoute = createAction(
  `${ActionPrefix} Navigate To Route`,
  (
    routePayload: NavigateRoutePayload,
    correlationId: string = generateId()
  ) => ({
    routePayload,
    correlationId,
  })
);

export const navigateRouteSuccess = createAction(
  `${ActionPrefix} Navigate To Route Success`,
  (
    routePayload: NavigateRoutePayload,
    correlationId: string = generateId()
  ) => ({
    routePayload,
    correlationId,
  })
);

export const navigateRouteError = createAction(
  `${ActionPrefix} Navigate To Route Error`,
  (error: HttpErrorResponse, correlationId: string = generateId()) => ({
    error,
    correlationId,
  })
);

const allActions = union({
  navigateRoute,
  navigateRouteSuccess,
  navigateRouteError,
});

export type RouteActions = typeof allActions;
