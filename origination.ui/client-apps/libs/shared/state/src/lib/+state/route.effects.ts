import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import {
  Router,
  UrlTree,
  UrlSegmentGroup,
  PRIMARY_OUTLET,
} from '@angular/router';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import * as fromRouteActions from './route.actions';

@Injectable()
export class RouteEffects {
  navigateToRoute$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(fromRouteActions.navigateRoute),
      mergeMap(async (action) => {
        const tree: UrlTree = this.router.parseUrl(action.routePayload.route);
        const params = {
          ...tree.queryParams,
          ...action.routePayload.params,
        };

        const g: UrlSegmentGroup = tree.root.children[PRIMARY_OUTLET];
        const segments = g.segments.map((seg) => seg.path);
        if (segments.length > 0) {
          await this.router.navigate(segments, {
            queryParamsHandling: 'merge',
            queryParams: params,
          });
        } else {
          await this.router.navigate([action.routePayload.route], {
            queryParamsHandling: 'merge',
            queryParams: params,
          });
        }
        return fromRouteActions.navigateRouteSuccess(
          {
            route: this.router.url,
            params: params,
          },
          action.correlationId
        );
      })
    )
  );

  constructor(private actions$: Actions, private router: Router) {}
}
