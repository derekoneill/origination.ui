import { HttpErrorResponse } from '@angular/common/http';
import { createReducer, on } from '@ngrx/store';
import * as fromRouteActions from './route.actions';

export interface State {
  error?: HttpErrorResponse;
}

export interface RouteState extends State {
  route: string;
  params: { [key: string]: string };
}

export function initState(overrides: Partial<RouteState> = {}): RouteState {
  return {
    route: '',
    params: {},
    ...overrides,
  };
}

const navigateRouteSuccessReducer = (state: RouteState, action): RouteState => {
  return {
    ...state,
    route: action.routePayload.route,
    params: action.routePayload.params,
  };
};

const navigateRouteErrorReducer = (state: RouteState, action): RouteState => {
  return {
    ...state,
    error: action.error,
  };
};

export const reducer = createReducer(
  initState(),
  on(fromRouteActions.navigateRouteSuccess, navigateRouteSuccessReducer),
  on(fromRouteActions.navigateRouteError, navigateRouteErrorReducer)
);
