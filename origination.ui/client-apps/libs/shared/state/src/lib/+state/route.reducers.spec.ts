import { reducer } from './route.reducers';
import * as fromRouteActions from './route.actions';
import { HttpErrorResponse } from '@angular/common/http';

describe('route.reducer', () => {
  it('should handler navigate route successfully, correctly', () => {
    const routeToNavigate = 'route/sub-route';
    const initState = reducer(
      undefined,
      fromRouteActions.navigateRouteSuccess({
        route: routeToNavigate,
        params: {},
      })
    );
    expect(initState.route).toBe(routeToNavigate);
  });

  it('should handle navigate route error, correctly', () => {
    const error: HttpErrorResponse = <HttpErrorResponse>{
      error: 'cannot navigate to the non-existing route',
    };
    const initState = reducer(
      undefined,
      fromRouteActions.navigateRouteError(error)
    );
    expect(initState.error).toEqual(error);
  });
});
