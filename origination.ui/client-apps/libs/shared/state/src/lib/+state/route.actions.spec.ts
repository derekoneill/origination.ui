import {
  navigateRoute,
  NavigateRoutePayload,
  navigateRouteSuccess,
} from './route.actions';

describe('Route Actions', () => {
  describe('Navigate Route', () => {
    const action = navigateRoute;
    let payload: NavigateRoutePayload;
    let correlationId: string;

    beforeEach(() => {
      payload = {
        route: 'wherever',
        params: {},
      };

      correlationId = 'some id';
    });

    it('should create the action', () => {
      const result = action(payload, correlationId);

      expect(result.correlationId).toBe(correlationId);
      expect(result.type).toContain('Navigate To Route');
      expect(result.routePayload).toBe(payload);
    });

    it('should generate a correlationId', () => {
      const result = action(payload);

      expect(result.correlationId).toBeDefined();
    });
  });

  describe('Navigate Route Success', () => {
    const action = navigateRouteSuccess;
    let payload: NavigateRoutePayload;
    let correlationId: string;

    beforeEach(() => {
      payload = {
        route: 'wherever',
        params: {},
      };

      correlationId = 'some id';
    });

    it('should create the action', () => {
      const result = action(payload, correlationId);

      expect(result.correlationId).toBe(correlationId);
      expect(result.type).toContain('Navigate To Route');
      expect(result.routePayload).toBe(payload);
    });

    it('should generate a correlationId', () => {
      const result = action(payload);

      expect(result.correlationId).toBeDefined();
    });
  });
});
