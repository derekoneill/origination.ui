import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromReducers from './+state/route.reducers';
import * as fromEffects from './+state/route.effects';

@NgModule({
  imports: [
    StoreModule.forFeature('routeState', fromReducers.reducer),
    EffectsModule.forFeature([fromEffects.RouteEffects]),
  ],
})
export class SharedStateModule {}
