import { Injectable } from '@angular/core';
import {
  AnimationBuilder,
  trigger,
  style,
  animate,
  transition,
  AnimationFactory,
} from '@angular/animations';
import { BaseAnimation } from './base.animation';
@Injectable()
export class HideFieldMessageAnimation extends BaseAnimation {
  constructor(private builder: AnimationBuilder) {
    super();
    this.duration = 100;
    this.animationFactory = this.builder.build([
      animate(
        '100ms ease-out',
        style({
          fontSize: '0px',
          maxHeight: '0px',
          lineHeight: '0px',
          opacity: '0',
          top: '0px',
          marginTop: '0px',
        })
      ),
    ]);
  }
}
