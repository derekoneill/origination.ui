import { Injectable } from '@angular/core';
import {
  AnimationBuilder,
  trigger,
  style,
  animate,
  transition,
  AnimationFactory,
} from '@angular/animations';
import { BaseAnimation } from './base.animation';

@Injectable()
export class ShowFieldMessageAnimation extends BaseAnimation {
  constructor(private builder: AnimationBuilder) {
    super();
    this.duration = 100;
    this.animationFactory = this.builder.build([
      animate(
        '100ms ease-out',
        style({
          fontSize: '13px',
          maxHeight: '100px',
          lineHeight: '19px',
          opacity: '1',
          top: '6px',
          marginTop: '8px',
        })
      ),
    ]);
  }
}
