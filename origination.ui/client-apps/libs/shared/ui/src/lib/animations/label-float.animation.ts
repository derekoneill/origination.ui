import { Injectable } from '@angular/core';
import {
  AnimationBuilder,
  trigger,
  style,
  animate,
  transition,
  AnimationFactory,
} from '@angular/animations';
import { BaseAnimation } from './base.animation';

@Injectable()
export class LabelFloatAnimation extends BaseAnimation {
  constructor(private builder: AnimationBuilder) {
    super();
    this.duration = 100;
    this.animationFactory = this.builder.build([
      animate(
        '100ms ease-out',
        style({
          fontSize: '13px',
          top: '8px',
          lineHeight: '19px',
        })
      ),
    ]);
  }
}

@Injectable()
export class LabelSinkAnimation extends BaseAnimation {
  constructor(private builder: AnimationBuilder) {
    super();
    this.duration = 100;
    this.animationFactory = this.builder.build([
      animate(
        '100ms ease-out',
        style({
          fontSize: '16px',
          top: '20px',
          lineHeight: '24px',
        })
      ),
    ]);
  }
}
