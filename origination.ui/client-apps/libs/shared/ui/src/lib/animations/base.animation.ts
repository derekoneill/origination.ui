import { Injectable } from '@angular/core';
import {
  AnimationBuilder,
  trigger,
  style,
  animate,
  transition,
  AnimationFactory,
} from '@angular/animations';

@Injectable()
export class BaseAnimation {
  protected animationFactory: AnimationFactory;
  protected duration: number;

  playAnimation(element, promiseTimeout: number = null) {
    return new Promise((res, rej) => {
      const player = this.animationFactory.create(element);
      player.play();
      let duration = this.duration;
      if (promiseTimeout !== null) {
        duration = promiseTimeout;
      }
      setTimeout(() => {
        res();
      }, duration);
    });
  }
}
