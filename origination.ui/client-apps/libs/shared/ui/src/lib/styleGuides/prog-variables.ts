const progVariables = {
  accelerationCurve: 'cubic-bezier(0.4, 0.0, 1, 1)',
  deccelerationCurve: 'cubic-bezier(0.0, 0.0, 0.2, 1)',
  standardCurve: 'cubic-bezier(0.4, 0.0, 0.2, 1)',
  sharpCurve: 'cubic-bezier(0.4, 0.0, 0.6, 1)',

  accelerationCurveArray: [0.4, 0.0, 1, 1],
  deccelerationCurveArray: [0.0, 0.0, 0.2, 1],
  standardCurveArray: [0.4, 0.0, 0.2, 1],
  sharpCurveArray: [0.4, 0.0, 0.6, 1],

  // Colors
  colorNeutralWhite: '#DFE3E8',
  colorNeutralWhiteDark: '#C4CDD5',
  colorNeutralWhiteLight: '#F4F6F8',
  colorNeutralWhiteLighter: '#F9FAFC',
  colorNeutralWhiteLightest: '#FFFFFF',

  // NEUTRAL BLACK
  colorNeutralBlack: '#454f5b',
  colorNeutralBlackDark: '#212b36',
  colorNeutralBlackLight: '#637381',
  colorNeutralBlackLighter: '#919eab',

  colorCaution: '#f0ab55',
  colorCautionDark: '#EF9E38',
  colorCautionLight: '#F6D9B8',
  colorCautionLighter: '#F7ECDE',

  colorError: '#f05f55',
  colorErrorDark: '#ef4438',
  colorErrorLight: '#F6BAB8',
  colorErrorLighter: '#F7DEDE',

  colorSuccess: '#32BE92',
  colorSuccessDark: '#0FB480',
  colorSuccessLight: '#A7E1D1',
  colorSuccessLighter: '#D5EFE9',

  colorPrimary: '#3185FC',
  colorPrimaryDarkAccent: '#3080F1',
  colorPrimaryDark: '#2F77DD',
  colorPrimaryDarker: '#2E6FCA',
  colorPrimaryLightAccent: '#3B8AFB',
  colorPrimaryLight: '#4F96FB',
  colorPrimaryLighter: '#63A2FC',
  colorPrimaryLightest: '#DBE8FC',
  colorPrimaryLighterAccent: '#9EC5FB',
};

export { progVariables };
