import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { GritCoreModule } from '@progleasing/grit-core';
import { NativeService } from '@ua/shared/data-access';
import { UaLayoutComponent } from './components/ua-layout/ua-layout.component';
import { TimerTextMaskDirective } from './directives/text-mask/text-mask.directive';
import { SingleConsecutiveSpaceDirective } from './directives/single-space/single-space.directive';
import { RestrictPatternDirective } from './directives/restrict-pattern/restrict-pattern.directive';

import { PgButtonComponent } from './components/pg-button/pg-button.component';
import { PgHeaderComponent } from './components/pg-header/pg-header.component';
import { PgHeaderProgressComponent } from './components/pg-header-progress/pg-header-progress.component';
import { PgAutocompleteListItemComponent } from './components/pg-autocomplete-list-item/pg-autocomplete-list-item.component';
import { PgAddressErrorMessageDirective } from './directives/pg-address-error-message/pg-address-error-message.directive';
import { PgAddressWrapperComponent } from './components/pg-address-wrapper/pg-address-wrapper.component';
import { PgAddressLabelDirective } from './directives/pg-address-label/pg-address-label.directive';
import { PgAddressInputDirective } from './directives/pg-address-input/pg-address-input.directive';
import { PgAddressErrorDirective } from './directives/pg-address-error/pg-address-error.directive';
import { PgAddressErrorIconDirective } from './directives/pg-address-error-icon/pg-address-error-icon.directive';
import { OldPgCheckboxComponent } from './components/pg-checkbox/pg-checkbox.component';
import { PgSpinnerShortComponent } from './components/pg-spinner-short/pg-spinner-short.component';
import { PgInvertedHeaderComponent } from './components/pg-inverted-header/pg-inverted-header.component';
import { PgCheckboxListComponent } from './components/pg-checkbox-list/pg-checkbox-list.component';
import { PgCheckboxListItemComponent } from './components/pg-checkbox-list-item/pg-checkbox-list-item.component';

import { PgSelectComponent } from './components/pg-select/pg-select.component';
import { PgOptionComponent } from './components/pg-option/pg-option.component';
import { PgOverlayOriginDirective } from './directives/pg-overlay-origin/pg-overlay-origin.directive';
import { PgConnectedOverlayComponent } from './components/pg-connected-overlay/pg-connected-overlay.component';
import { PgAddressSuccessIconDirective } from './directives/pg-address-success-icon/pg-address-success-icon.directive';
import { LabelSinkAnimation, LabelFloatAnimation } from './animations';
import { PgExpandedAddressWrapperComponent } from './components/pg-expanded-address-wrapper/pg-expanded-address-wrapper.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    GritCoreModule,
    RouterModule,
  ],
  providers: [NativeService, LabelFloatAnimation, LabelSinkAnimation],
  entryComponents: [],
  declarations: [
    PgButtonComponent,
    PgHeaderComponent,
    PgHeaderProgressComponent,
    PgAutocompleteListItemComponent,
    PgAddressErrorMessageDirective,
    PgAddressWrapperComponent,
    PgExpandedAddressWrapperComponent,
    PgAddressLabelDirective,
    PgAddressInputDirective,
    PgAddressErrorDirective,
    PgAddressErrorIconDirective,
    OldPgCheckboxComponent,
    PgSpinnerShortComponent,
    PgInvertedHeaderComponent,
    PgCheckboxListComponent,
    PgCheckboxListItemComponent,
    PgSelectComponent,
    PgOptionComponent,
    PgOverlayOriginDirective,
    PgConnectedOverlayComponent,
    PgAddressSuccessIconDirective,
    TimerTextMaskDirective,
    RestrictPatternDirective,
    SingleConsecutiveSpaceDirective,
    HeaderComponent,
    UaLayoutComponent,
  ],

  exports: [
    GritCoreModule,
    PgButtonComponent,
    PgHeaderProgressComponent,
    PgAddressErrorMessageDirective,
    PgAddressLabelDirective,
    PgAddressInputDirective,
    PgAddressErrorDirective,
    PgAddressWrapperComponent,
    PgExpandedAddressWrapperComponent,
    OldPgCheckboxComponent,
    PgSpinnerShortComponent,
    PgInvertedHeaderComponent,
    PgOptionComponent,
    PgSelectComponent,
    PgOverlayOriginDirective,
    PgConnectedOverlayComponent,
    PgCheckboxListComponent,
    PgCheckboxListItemComponent,
    TimerTextMaskDirective,
    RestrictPatternDirective,
    SingleConsecutiveSpaceDirective,
    HeaderComponent,
    UaLayoutComponent,
  ],
})
export class ComponentsModule {}
