import {
  Directive,
  HostBinding,
  ElementRef,
  Input,
  Renderer2,
  HostListener,
} from '@angular/core';

let uid = 0;

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[grDropDownItem], gr-drop-down-item',
  exportAs: 'grDropDownItem',
})
export class GrDropDownItemDirective {
  @HostBinding('class.gr-drop-down-item')
  _classDropDown = true;

  @HostBinding('class.is-focused')
  isFocused = false;

  @Input()
  value: any;
  @Input()
  enumValue: any;
  @Input()
  searchableTerms: string[] = [];

  @Input()
  id = `gr-drop-down-${uid++}`;

  get displayText() {
    return this.elementRef.nativeElement.textContent;
  }

  constructor(public elementRef: ElementRef, private _renderer: Renderer2) {}

  @HostListener('click')
  onClick() {}

  @HostListener('mouseover')
  onMouseOver() {}
}
