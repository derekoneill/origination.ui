import {
  Component,
  OnInit,
  Input,
  AfterViewInit,
  OnChanges,
  SimpleChanges,
  ElementRef,
  OnDestroy,
  ViewChild,
  Renderer2,
} from '@angular/core';

import { PgOverlayOriginDirective } from '../../directives/pg-overlay-origin/pg-overlay-origin.directive';
import { NativeService } from '@ua/shared/data-access';
import { progVariables } from '../../styleGuides/prog-variables';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pg-connected-overlay',
  templateUrl: './pg-connected-overlay.component.html',
  styleUrls: ['./pg-connected-overlay.component.scss'],
})
export class PgConnectedOverlayComponent
  implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  @Input() styles: any;
  @Input() origin: PgOverlayOriginDirective;
  @Input() visible = false;

  @Input() positionAdjustments: any;
  @Input() forcePosition: any;

  @ViewChild('overlayEl') overlayEl: ElementRef;
  @ViewChild('overlayContentEl') overlayContentEl: ElementRef;
  @ViewChild('overlayContentOpacityWrapper')
  overlayContentOpacityWrapper: ElementRef;

  private nativeNode: HTMLElement;

  public get originPosition() {
    return this.origin.getOriginBoundingBox();
  }

  constructor(
    private el: ElementRef,
    private nativeService: NativeService,
    private renderer: Renderer2
  ) {}

  ngOnInit() {}

  ngAfterViewInit() {
    this.el.nativeElement.parentElement.removeChild(this.el.nativeElement);
    this.nativeNode = this.nativeService.document.body.appendChild(
      this.el.nativeElement
    );
    // Force styles early on to allow for animation
    this.setJSStyles();
    this.calculateLayoutPositions();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.visible) {
      if (!changes.visible.previousValue && changes.visible.currentValue) {
        this.show();
      } else if (
        changes.visible.previousValue &&
        !changes.visible.currentValue
      ) {
        this.hide();
      }
    }
  }

  ngOnDestroy() {
    this.nativeService.document.body.removeChild(this.nativeNode);
  }

  hide() {
    return new Promise((res, rej) => {
      const overlay = this.overlayEl.nativeElement;
      const overlayContent = this.overlayContentEl.nativeElement;
      const animationOptions = {
        duration: 150,
        easing: progVariables.deccelerationCurveArray,
      };
      const promises = [
        this.nativeService.animate(
          overlay,
          {
            opacity: 0,
          },
          animationOptions
        ),
        this.nativeService.animate(
          overlayContent,
          {
            opacity: 0,
          },
          animationOptions
        ),
      ];

      Promise.all(promises).then(() => {
        this.renderer.setStyle(overlay, 'visibility', 'hidden');
        this.renderer.setStyle(overlayContent, 'visibility', 'hidden');
      });
    });
  }

  show() {
    this.calculateLayoutPositions();
    const overlay = this.overlayEl.nativeElement;
    const overlayContent = this.overlayContentEl.nativeElement;
    const overlayContentOpacityWrapper = this.overlayContentOpacityWrapper
      .nativeElement;
    const overlayContentStyles = this.nativeService.window.getComputedStyle(
      overlayContent
    );
    const targetWidth = overlayContentStyles.width;
    const targetHeight = overlayContentStyles.height;
    this.renderer.setStyle(overlay, 'visibility', 'visible');
    this.renderer.setStyle(overlayContent, 'visibility', 'visible');
    this.renderer.setStyle(overlayContent, 'opacity', '0');
    this.renderer.setStyle(overlayContentOpacityWrapper, 'opacity', '1');

    // const widthAnimation =
    //   this.nativeService.animate(
    //     overlayContent,
    //     { scaleX: [1, 0], scaleY: [0, 0] },
    //     { duration: 300, easing: progVariables.standardCurveArray, queue: false });

    // const heightAnimation =
    //   this.nativeService.animate(
    //     overlayContent,
    //     { scaleY: 1, scaleX: 1 },
    //     { duration: 300, easing: progVariables.standardCurveArray, delay: 50, queue: false });

    // const opacityWrapperAnimation = this.nativeService.animate(
    //   overlayContentOpacityWrapper,
    //   { opacity: 1 },
    //   { duration: 150, easing: progVariables.standardCurveArray, delay: 100, queue: false });

    // const promises = [
    //   widthAnimation,
    //   heightAnimation,
    //   opacityWrapperAnimation
    // ];
    const animationOptions = {
      duration: 150,
      easing: progVariables.deccelerationCurveArray,
    };

    const promises = [
      this.nativeService.animate(
        overlay,
        {
          opacity: 1,
        },
        animationOptions
      ),
      this.nativeService.animate(
        overlayContent,
        {
          opacity: 1,
        },
        animationOptions
      ),
    ];

    return Promise.all(promises);
  }

  /*
    Calculation precedence.

    highest -> window boundaries
    ......  -> forced position
    ......  -> position delta
    lowest  -> default positioning to origin component.
  */
  private calculateLayoutPositions() {
    const window = this.nativeService.window;
    const overlayContent = this.overlayContentEl.nativeElement;
    const origin = this.originPosition;
    let top = origin.top;
    let left = origin.left;

    // Forced position
    if (this.forcePosition) {
      top = this.forcePosition.top;
      left = this.forcePosition.left;
      // Position deltas
    } else if (this.positionAdjustments) {
      if (
        this.positionAdjustments.topDelta ||
        this.positionAdjustments.topDelta === 0
      ) {
        top += this.positionAdjustments.topDelta;
      }
      if (
        this.positionAdjustments.leftDelta ||
        this.positionAdjustments.leftDelta === 0
      ) {
        left += this.positionAdjustments.leftDelta;
      }
    }

    const height = overlayContent.clientHeight;
    const width = overlayContent.clientWidth;

    // Window boundaries
    if (top < 0) {
      top = 0;
    } else if (top + height > window.innerHeight) {
      top = window.innerHeight - height;
    }

    if (left < 0) {
      left = 0;
    } else if (left + width > window.innerWidth) {
      left = window.innerWidth - width;
    }

    this.renderer.setStyle(overlayContent, 'top', `${top}px`);
    this.renderer.setStyle(overlayContent, 'left', `${left}px`);
  }

  private setJSStyles() {
    // this.overlayContentEl.nativeElement.style.transform = 'scaleX(0) scaleY(0)';
  }
}
