import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pg-inverted-header',
  templateUrl: './pg-inverted-header.component.html',
  styleUrls: ['./pg-inverted-header.component.scss'],
})
export class PgInvertedHeaderComponent implements OnInit {
  @Output() exitClicked: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}
}
