// tslint:disable:no-input-rename
import {
  Component,
  OnInit,
  ViewChild,
  AfterViewInit,
  Input,
  ContentChildren,
  QueryList,
  AfterContentInit,
  ElementRef,
  Renderer2,
  OnChanges,
  SimpleChanges,
  HostBinding,
} from '@angular/core';
import { PgConnectedOverlayComponent } from '../pg-connected-overlay/pg-connected-overlay.component';
import { PgOptionComponent } from '../pg-option/pg-option.component';
import { NativeService } from '@ua/shared/data-access';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pg-select',
  templateUrl: './pg-select.component.html',
  styleUrls: ['./pg-select.component.scss'],
})
export class PgSelectComponent
  implements OnInit, AfterViewInit, AfterContentInit, OnChanges {
  @Input('value') value: any;

  @Input('optionsMaxWidth') optionsMaxWidth = 144;
  @Input('optionsMaxHeight') optionsMaxHeight = 224;

  @ViewChild(PgConnectedOverlayComponent)
  overlayContent: PgConnectedOverlayComponent;
  @ViewChild('optionsWrapper') optionsWrapper: ElementRef;
  @ContentChildren(PgOptionComponent) optionChildrenElements: QueryList<
    PgOptionComponent
  >;

  selectedOptionIndex: number;
  selectedOptionComponent: PgOptionComponent;

  @Input('valueCompare') valueCompare = (a, b) => a === b;
  @Input('positionAdjustmentCalculation')
  positionAdjustmentCalculation: Function = () => {};

  constructor(private native: NativeService, private renderer: Renderer2) {}

  ngOnInit() {}

  ngAfterViewInit() {}

  ngAfterContentInit() {
    this.calculateSelectedOption();
    this.optionChildrenElements.forEach((option) =>
      option.optionSelected.subscribe((x) => {
        if (!x.disabled) {
          this.overlayContent.hide();
        }
      })
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.value) {
      if (
        !this.valueCompare(
          changes.value.currentValue,
          changes.value.previousValue
        )
      ) {
        this.calculateSelectedOption();
        this.overlayContent.hide();
      }
    }
  }

  handleCaretClicked() {
    this.calculateSelectedOption();
    this.overlayContent.show();
  }

  getSelectDisplayValue() {
    if (this.selectedOptionIndex || this.selectedOptionIndex === 0) {
      const optArray = this.optionChildrenElements.toArray();
      const option = optArray[this.selectedOptionIndex];
      return option.getDisplayValue();
    }
    return null;
  }

  private calculateSelectedOption() {
    if (!this.optionChildrenElements) {
      return;
    }
    const window = this.native.window;
    const optArray = this.optionChildrenElements.toArray();
    let selectedOptionIndex = null;
    let selectedOptionComponent: PgOptionComponent = null;
    for (let i = 0; i < optArray.length; i++) {
      const option = optArray[i];
      if (this.valueCompare(option.value, this.value)) {
        selectedOptionIndex = i;
        selectedOptionComponent = option;
        break;
      }
    }

    if (selectedOptionComponent) {
      const optionsWrapperEl = this.optionsWrapper.nativeElement;
      const selectedOptionEl = selectedOptionComponent.el.nativeElement;
      const optionsWrapperStyles = window.getComputedStyle(optionsWrapperEl);
      const selectedOptionStyles = window.getComputedStyle(selectedOptionEl);

      const optionHeight = parseInt(selectedOptionStyles.height, 10);
      const offsetTop = Math.max(selectedOptionEl.offsetTop - optionHeight, 0);
      const positionAdjustments = { topDelta: -optionHeight, leftDelta: 0 };
      const maxScrollableHeight =
        optionsWrapperEl.scrollHeight - optionsWrapperEl.clientHeight;
      if (offsetTop > maxScrollableHeight) {
        positionAdjustments.topDelta = -(
          offsetTop -
          maxScrollableHeight +
          optionHeight
        );
      } else if (selectedOptionEl.offsetTop === 0) {
        positionAdjustments.topDelta = 0;
      }
      // account for border widths
      const borderWidth = parseInt(optionsWrapperStyles.borderWidth, 10);
      positionAdjustments.topDelta -= borderWidth;
      positionAdjustments.leftDelta -= borderWidth;

      if (this.positionAdjustmentCalculation) {
        // TODO: add a consumer hook to allow a post delta calculation
        // so we can adjust from the consumer as well.
      }

      this.overlayContent.positionAdjustments = positionAdjustments;
      this.optionsWrapper.nativeElement.scrollTop = offsetTop;
    }
    this.selectedOptionComponent = selectedOptionComponent;
    this.selectedOptionIndex = selectedOptionIndex;
  }

  private clamp(min, n, max) {
    return Math.min(Math.max(n, min), max);
  }
}
