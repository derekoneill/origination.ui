import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ua-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Output() back: EventEmitter<void> = new EventEmitter();
  @Output() closeApp: EventEmitter<void> = new EventEmitter();

  public ngOnInit(): void {}

  public previous(): void {
    this.back.emit();
  }

  public exit(): void {
    this.closeApp.emit();
  }
}
