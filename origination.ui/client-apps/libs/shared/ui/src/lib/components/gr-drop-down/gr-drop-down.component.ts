import {
  Component,
  OnInit,
  HostBinding,
  ViewEncapsulation,
  ContentChildren,
  QueryList,
  AfterContentInit,
  ElementRef,
  ViewChildren,
  Renderer2,
  Optional,
  Input,
} from '@angular/core';
import { GrDropDownItemDirective } from './gr-drop-down-item.directive';
import { PgFieldComponent } from '@progleasing/grit-core';
import { NgControl, FormGroupDirective } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { AnimationService } from '@progleasing/grit-core';
import {
  animateCollapseDropDown,
  animateExpandDropDown,
  animateUpdateDropDown,
} from './gr-drop-down.animations';

export interface DropDownItem {
  displayText: string;
  searchableTerms: string[];
  value: any;
  id: string;
  enumValue?: any;
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gr-drop-down',
  templateUrl: './gr-drop-down.component.html',
  styleUrls: ['./gr-drop-down.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class GrDropDownComponent implements OnInit, AfterContentInit {
  @HostBinding('class.gr-drop-down')
  _classGrDropDown = true;

  // The content children passed in
  @ContentChildren(GrDropDownItemDirective)
  _itemChildren: QueryList<GrDropDownItemDirective>;

  // The view children we render
  @ViewChildren('dropDownItem')
  _dropDownItemViewChildren: QueryList<ElementRef>;

  _expanded = false;

  _globalPromise = Promise.resolve();

  ngControl: NgControl;

  dropDownItems: DropDownItem[] = [];

  @Input()
  displayElement: HTMLInputElement;

  get controlValue() {
    if (!this.ngControl) {
      return '';
    }
    return this.ngControl.value || '';
  }

  // Flag to indicate one of the item click events happened
  protected _valueSelected = false;

  // maxHeight of the element
  // private readonly maxHeight = 134;
  private readonly maxHeight = 240;

  constructor(
    public pgField: PgFieldComponent,
    private _animate: AnimationService,
    private _elementRef: ElementRef,
    private _renderer: Renderer2,
    @Optional() public formGroup: FormGroupDirective
  ) {}

  ngOnInit() {}

  ngAfterContentInit() {
    this._reconcileDropDownDisplayState();
    this.dropDownItems = this._getDropDownItems();
    this.ngControl = this.pgField.control.ngControl;

    // Watch the state of the field for changes
    this.pgField.control.stateChanges.pipe(debounceTime(100)).subscribe(() => {
      this._reconcileDropDownDisplayState();
    });

    // Watch the value of the control for changes.
    this.pgField.control.ngControl.valueChanges.subscribe((chgs) => {
      // Run an update cycle on the visible list items
      if (this._expanded) {
        this._updateDropDownItems(this.controlValue.toString());
      }
    });
  }

  formatDisplayText(display) {
    if (!this.controlValue) {
      return display;
    }
    const searchTerm = this.controlValue.toString();
    if (!searchTerm || !searchTerm.length) {
      return display;
    }

    const result = Array(display.length);
    let displayIndex = 0;
    for (const character of searchTerm) {
      for (displayIndex; displayIndex < display.length; displayIndex++) {
        if (character === display[displayIndex]) {
          result.push(
            `<span class="search-term-bold-letter">${character}</span>`
          );
          displayIndex++;
          break;
        } else {
          result.push(display[displayIndex]);
        }
      }
    }
    result.push(display.substr(displayIndex));

    // use this funtion to bold specific letters
    return result.join('');
  }

  handleItemClicked(item: DropDownItem) {
    this._valueSelected = true;
    this._setValue(item.value || item.displayText, item.displayText);
  }

  private _setValue(value, display?) {
    // @ts-ignore
    this.formGroup.form.get(this.ngControl.name).setValue(value);
    if (this.displayElement && display) {
      this.displayElement.value = display;
    }
  }

  private _isMatch(
    matchable: {
      searchableTerms: string[];
      displayText: string;
      value?: string;
    },
    searchTerm: string
  ) {
    return (
      !searchTerm ||
      matchable.searchableTerms.some(
        (st) => st.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
      ) ||
      matchable.displayText.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
    );
  }

  private _isExactMatch(
    matchable: {
      searchableTerms: string[];
      displayText: string;
      value?: any;
    },
    searchTerm: string
  ) {
    if (!searchTerm) {
      return false;
    }
    return (
      searchTerm.toLowerCase() === matchable.displayText.toLowerCase() ||
      (matchable.value &&
        matchable.value.toString().toLowerCase() === searchTerm.toLowerCase())
    );
  }

  private _getDropDownItems(fieldValue = '') {
    return (
      this._itemChildren
        // .filter(ic => this._isMatch(ic, fieldValue))
        .map((ic) => {
          return {
            displayText: ic.displayText,
            searchableTerms: ic.searchableTerms,
            value: ic.value,
            id: ic.id,
            enumValue: ic.enumValue,
          };
        })
    );
  }

  _reconcileDropDownDisplayState() {
    const control = this.pgField.control;
    if (control.isFocused && !this._expanded) {
      this._expanded = true;
      this.pgField.control.isErrorStateFrozen = true;
      return this._globalPromise.then(
        () => (this._globalPromise = this._expandDropdown())
      );
    } else if (!control.isFocused && this._expanded) {
      this._expanded = false;

      setTimeout(() => {
        if (!this._valueSelected) {
          this._selectValueFromRemaining();
        }
        this._valueSelected = false;

        this._globalPromise = this._collapseDropdown().then(() => {
          this.pgField.control.isErrorStateFrozen = false;
          this.dropDownItems = this._getDropDownItems(this.controlValue);
        });
      }, 0);
    }
  }

  private _selectValueFromRemaining() {
    // TODO: Do a better job of abstracting this logic
    const exactMatch = this.dropDownItems.filter((ddi) =>
      this._isExactMatch(ddi, this.controlValue)
    );
    if (exactMatch && exactMatch.length) {
      this._setValue(
        exactMatch[0].value || exactMatch[0].displayText,
        exactMatch[0].displayText
      );
    } else if (this.dropDownItems.length === 1) {
      this._setValue(
        this.dropDownItems[0].value || this.dropDownItems[0].displayText,
        this.dropDownItems[0].displayText
      );
    }
  }

  private _updateDropDownItems(fieldValue: string) {
    if (this._valueSelected) {
      return;
    }
    const newDropDownItems: DropDownItem[] = this._getDropDownItems();

    this.dropDownItems = newDropDownItems;

    return this._globalPromise.then(() => {
      this._globalPromise = new Promise((res, rej) => {
        // Put this in an async context
        // to allow browser to finish rendering.
        setTimeout(() => {
          animateUpdateDropDown(
            this._animate,
            this._renderer,
            this._elementRef.nativeElement,
            this._dropDownItemViewChildren.map((child) => child.nativeElement),
            this.maxHeight
          ).then(() => res());
        }, 0);
      });
    });
  }

  private _expandDropdown() {
    return animateExpandDropDown(
      this._animate,
      this._renderer,
      this._elementRef.nativeElement,
      this._dropDownItemViewChildren.map((child) => child.nativeElement),
      this.maxHeight
    );
  }

  private _collapseDropdown() {
    const currentHeight = this._elementRef.nativeElement.clientHeight;
    return animateCollapseDropDown(
      this._animate,
      this._renderer,
      this._elementRef.nativeElement,
      this._dropDownItemViewChildren.map((child) => child.nativeElement)
    );
  }
}
