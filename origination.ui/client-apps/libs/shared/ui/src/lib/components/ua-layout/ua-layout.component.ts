import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ua-layout',
  templateUrl: './ua-layout.component.html',
  styleUrls: ['./ua-layout.component.scss'],
})
export class UaLayoutComponent {
  @Output() back: EventEmitter<void> = new EventEmitter<void>();
  @Output() closeApp: EventEmitter<void> = new EventEmitter<void>();
  onBack() {
    this.back.emit();
  }

  onClose() {
    this.closeApp.emit();
  }
}
