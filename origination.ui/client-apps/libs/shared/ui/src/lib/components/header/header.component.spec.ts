import { Spectator, createComponentFactory } from '@ngneat/spectator';
import { HeaderComponent } from './header.component';
import { GritCoreModule } from '@progleasing/grit-core';

describe('HeaderComponent', () => {
  let spectator: Spectator<HeaderComponent>;
  const createComponent = createComponentFactory({
    component: HeaderComponent,
    imports: [GritCoreModule],
  });

  beforeEach(() => (spectator = createComponent({ detectChanges: true })));

  it('should emit back event when the arrow back icon being clicked.', () => {
    jest.spyOn(spectator.component.back, 'emit');
    spectator.component.previous();
    expect(spectator.component.back.emit).toBeCalledTimes(1);
  });

  it('should emit closeApp event when the close icon being clicked.', () => {
    jest.spyOn(spectator.component.closeApp, 'emit');
    spectator.component.exit();
    expect(spectator.component.closeApp.emit).toBeCalledTimes(1);
  });
});
