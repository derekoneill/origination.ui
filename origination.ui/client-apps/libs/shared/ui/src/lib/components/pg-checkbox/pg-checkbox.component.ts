import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

let ckId = 0;

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pg-checkbox-old',
  templateUrl: './pg-checkbox.component.html',
  styleUrls: ['./pg-checkbox.component.scss'],
})
export class OldPgCheckboxComponent implements OnInit {
  @Input() parentForm: FormGroup;
  @Input() controlName: string;
  @Input() type = 'primary';

  @Input() id: string;

  constructor() {
    if (!this.id) {
      this.id = (ckId++).toString();
    }
  }

  ngOnInit() {}
}
