import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  ElementRef,
  ViewChild,
} from '@angular/core';
import { NativeService } from '@ua/shared/data-access';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pg-header',
  templateUrl: './pg-header.component.html',
  styleUrls: ['./pg-header.component.scss'],
})
export class PgHeaderComponent implements OnInit {
  @ViewChild('headerWrapper') headerWrapper: ElementRef;
  @Output() exitClicked = new EventEmitter<boolean>();
  constructor(private el: ElementRef, private native: NativeService) {}

  ngOnInit() {}

  onExitButtonClicked() {
    this.exitClicked.emit(true);
  }

  hide(animated: boolean = false) {
    if (!animated) {
      this.headerWrapper.nativeElement.style.top = '-84px';
      return Promise.resolve();
    } else {
      return this.animateTop(-84);
    }
  }

  show(animated: boolean = false) {
    if (!animated) {
      this.headerWrapper.nativeElement.style.top = '0px';
      return Promise.resolve();
    } else {
      return this.animateTop(0);
    }
  }

  private animateTop(top) {
    return this.native.animate(
      this.headerWrapper.nativeElement,
      {
        top: top,
      },
      {
        duration: 150,
        easing: [0.0, 0.0, 0.2, 1],
        queue: false,
      }
    );
  }
}
