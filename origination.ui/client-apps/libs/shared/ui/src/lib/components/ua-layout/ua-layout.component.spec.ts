import { EventEmitter } from '@angular/core';
import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { UaLayoutComponent } from './ua-layout.component';

describe('UaLayoutComponent', () => {
  let component: UaLayoutComponent;

  beforeEach(() => {
    component = new UaLayoutComponent();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('On Back', () => {
    let mockEmitter: SubstituteOf<EventEmitter<void>>;

    beforeEach(() => {
      mockEmitter = Substitute.for<EventEmitter<void>>();
    });

    it('should emit the back event', () => {
      component.back = mockEmitter;

      component.onBack();

      mockEmitter.received().emit();
    });
  });

  describe('On Close', () => {
    let mockEmitter: SubstituteOf<EventEmitter<void>>;

    beforeEach(() => {
      mockEmitter = Substitute.for<EventEmitter<void>>();
    });

    it('should emit the back event', () => {
      component.closeApp = mockEmitter;

      component.onClose();

      mockEmitter.received().emit();
    });
  });
});
