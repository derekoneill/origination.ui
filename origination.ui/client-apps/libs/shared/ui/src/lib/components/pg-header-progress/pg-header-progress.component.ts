import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { NativeService } from '@ua/shared/data-access';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pg-header-progress',
  templateUrl: './pg-header-progress.component.html',
  styleUrls: ['./pg-header-progress.component.scss'],
})
export class PgHeaderProgressComponent implements OnInit {
  @ViewChild('progressWrapper') progressWrapper: ElementRef;
  @Input() progress = 0;

  constructor(private native: NativeService) {}

  ngOnInit() {}

  setProgress(val) {
    this.progress = val;
  }

  hide(animated: boolean = false) {
    if (!animated) {
      this.progressWrapper.nativeElement.style.top = '-84px';
      return Promise.resolve();
    } else {
      return this.animateTop(-4);
    }
  }

  show(animated: boolean = false) {
    const amount = this.native.window.innerWidth > 768 ? 84 : 47;
    if (!animated) {
      this.progressWrapper.nativeElement.style.top = amount + 'px';
      return Promise.resolve();
    } else {
      return this.animateTop(amount);
    }
  }

  private animateTop(top) {
    return this.native.animate(
      this.progressWrapper.nativeElement,
      {
        top: top,
      },
      {
        duration: 150,
        easing: [0.0, 0.0, 0.2, 1],
        queue: false,
      }
    );
  }
}
