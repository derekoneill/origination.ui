import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

/*
  DEPRECATED:
  Find all of the places this is being used and replace with the directive.
*/
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pg-button',
  templateUrl: './pg-button.component.html',
  styleUrls: ['./pg-button.component.scss'],
})
export class PgButtonComponent implements OnInit {
  @Input() type: string;
  @Input() label: string;
  @Input() leftIcon: string;
  @Input() rightIcon: string;
  @Input() disabled: boolean;
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onClick: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() loading: boolean;
  @Input() emitWhenDisabled = false;
  @Input() transparent = false;
  constructor() {}

  ngOnInit() {}

  handleClick(event: MouseEvent) {
    event.preventDefault();
    // if (!this.disabled || this.emitWhenDisabled) {
    //   this.onClick.emit(this.disabled);
    // } else {}
    this.onClick.emit(this.disabled);
  }
}
