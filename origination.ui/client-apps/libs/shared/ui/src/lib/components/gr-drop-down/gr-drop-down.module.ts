import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GrDropDownComponent } from './gr-drop-down.component';
import { GrDropDownItemDirective } from './gr-drop-down-item.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [GrDropDownComponent, GrDropDownItemDirective],
  exports: [GrDropDownComponent, GrDropDownItemDirective],
})
export class GrDropDownModule {}
