import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pg-autocomplete-list-item',
  templateUrl: './pg-autocomplete-list-item.component.html',
  styleUrls: ['./pg-autocomplete-list-item.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PgAutocompleteListItemComponent implements OnInit {
  @Input() line1: string;
  @Input() line2: string;
  @Input() matchingString: string;
  @Output() selected: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() {}

  ngOnInit() {}

  formatMatchingLetters() {
    if (!this.line1) {
      return '';
    }
    if (!this.matchingString) {
      return `${this.line1} <span class="line2">${this.line2 || ''}</span>`;
    }
    const searchChars = this.matchingString.split('');
    let result = '';
    for (let i = 0; i < this.line1.length; i++) {
      if (
        searchChars.length > 0 &&
        this.line1.toLowerCase()[i] === searchChars[0].toLowerCase()
      ) {
        result += `<span class="bold-letter">${this.line1[i]}</span>`;
        searchChars.shift();
      } else {
        result += this.line1[i];
      }
    }
    return result + ` <span class="line2">${this.line2 || ''}</span>`;
  }
}
