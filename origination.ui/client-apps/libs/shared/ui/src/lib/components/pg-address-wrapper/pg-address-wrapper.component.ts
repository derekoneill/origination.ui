import { debounceTime, skip } from 'rxjs/operators';
import {
  Component,
  OnInit,
  ContentChildren,
  ViewChildren,
  AfterContentInit,
  QueryList,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
  Input,
  EventEmitter,
  Output,
  OnChanges,
  OnDestroy,
} from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { PgAddressService, NativeService } from '@ua/shared/data-access';
import { PgAddressErrorDirective } from '../../directives/pg-address-error/pg-address-error.directive';
import { PgAddressLabelDirective } from '../../directives/pg-address-label/pg-address-label.directive';
import { PgAddressInputDirective } from '../../directives/pg-address-input/pg-address-input.directive';
import { PgAddressErrorIconDirective } from '../../directives/pg-address-error-icon/pg-address-error-icon.directive';

import { Subscription, BehaviorSubject } from 'rxjs';

import scrollToElement from 'scroll-to-element';
import { PgAddressSuccessIconDirective } from '../../directives/pg-address-success-icon/pg-address-success-icon.directive';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pg-address-wrapper',
  templateUrl: './pg-address-wrapper.component.html',
  styleUrls: ['./pg-address-wrapper.component.scss'],
  providers: [PgAddressService, NativeService],
  encapsulation: ViewEncapsulation.None,
})
export class PgAddressWrapperComponent
  implements OnInit, AfterContentInit, OnChanges, OnDestroy {
  @ContentChildren(PgAddressInputDirective)
  inputDirectives: QueryList<PgAddressInputDirective>;
  @ContentChildren(PgAddressLabelDirective)
  labelDirectives: QueryList<PgAddressLabelDirective>;
  @ContentChildren(PgAddressErrorDirective)
  errorDirectives: QueryList<PgAddressErrorDirective>;
  @ViewChildren(PgAddressErrorIconDirective)
  errorIconDirectives: QueryList<PgAddressErrorIconDirective>;
  @ViewChildren(PgAddressSuccessIconDirective)
  successIconDirectives: QueryList<PgAddressErrorIconDirective>;

  @ViewChild('street1Wrapper') street1Wrapper: ElementRef;
  @ViewChild('stateWrapper') stateWrapper: ElementRef;
  @ViewChild('autocompleteListEl') autocompleteListEl: ElementRef;
  @ViewChild('stateAutocompleteListEl') stateAutocompleteListEl: ElementRef;

  subs: Subscription[] = [];

  @Input() addressAutocompleteList;
  @Input() stateAutocompletelist;

  @Input() complete;

  @Output() addressItemSelected = new EventEmitter<any>();
  @Output() stateSelected = new EventEmitter<any>();

  inputState: any = {
    hadInitialBlur: false,
    focus: false,
    error: false,
    hover: false,
  };
  errorState: any = {};
  focusState: any = {};
  focusEventBehavior = new BehaviorSubject<any>(null);
  isAddressListExpanded = false;
  isStateListExpanded = false;

  constructor(
    private addressService: PgAddressService,
    private formGroup: FormGroupDirective,
    private native: NativeService
  ) {}

  ngOnInit() {
    this.focusEventBehavior
      .asObservable()
      .pipe(skip(1), debounceTime(200))
      .subscribe(() => {
        this.inputState.focus =
          this.focusState.street1 ||
          this.focusState.street2 ||
          this.focusState.city ||
          this.focusState.state ||
          this.focusState.zip ||
          this.focusState.temp;
        this.focusState.temp = false;
        if (!this.inputState.focus) {
          this.inputState.hadInitialBlur = true;
        }

        Object.keys(this.focusState).forEach((key) => {
          if (this.errorState[key]) {
          } else if (this.focusState[key]) {
            this.colorFieldLabel(key, 'focus');
          } else if (this.inputState.hover) {
            this.colorFieldLabel(key, 'hover');
          } else {
            this.colorFieldLabel(key, 'idle');
          }
        });

        if (this.focusState.street1) {
          this.expandAutocompleteList();
        } else {
          this.hideAutocompleteList();
        }

        if (this.focusState.state) {
          this.expandStateAutocompleteList();
        } else {
          this.hideStateAutocompleteList();
        }

        this.errorCheck();
      });
  }

  ngOnChanges() {
    if (
      this.addressAutocompleteList &&
      this.addressAutocompleteList.length &&
      !this.isAddressListExpanded &&
      this.focusState.street1
    ) {
      this.expandAutocompleteList();
    }
  }

  ngAfterContentInit() {
    const addressToControlMap = {};
    this.inputDirectives.forEach((x) => {
      const control = this.formGroup.form.get(x.formControlName);
      addressToControlMap[x.pgAddressInput] = control;
      this.subs.push(
        x.valueChange.subscribe((val) =>
          this.handleFieldValueChanged(x.addressFieldName, val)
        ),
        x.focusChange.subscribe((isFocused) =>
          this.handleFieldFocusChanged(x.addressFieldName, isFocused)
        ),
        x.hoverChange.subscribe((isHovering) =>
          this.handleFieldHoverChanged(x, isHovering)
        )
      );
      if (control.value && control.value.length) {
        this.floatFieldLabel(x.pgAddressInput);
      }
    });
    this.addressService.setAddressFieldToFormControlMap(addressToControlMap);
    this.subs.push(
      this.formGroup.statusChanges.subscribe(() => {
        this.errorCheck();
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach((x) => !x.closed && x.unsubscribe());
  }

  scrollToField(fieldName = 'street1', offset = -100) {
    const field = this.inputDirectives.find(
      (x) => x.addressFieldName === fieldName
    );
    if (field) {
      scrollToElement(field.el.nativeElement, {
        offset: offset,
        ease: 'out-quad',
        duration: 300,
      });
    }
  }

  handleFieldValueChanged(fieldName: string, value: string) {
    if (value && value.length) {
      this.floatFieldLabel(fieldName);
    }
    this.errorCheck();
  }

  handleFieldFocusChanged(fieldName: string, isFocused: boolean) {
    this.focusState[fieldName] = isFocused;
    this.focusEventBehavior.next(null);
    if (isFocused) {
      this.floatFieldLabel(fieldName);
    }
    if (fieldName === 'state' && !isFocused) {
      this.pickState();
    }
  }

  handleFieldHoverChanged(childDirective, isHovering) {
    const key = childDirective.addressFieldName;
    if (!this.focusState[key] && !this.errorState[key]) {
      if (isHovering) {
        this.colorFieldLabel(key, 'hover');
      } else {
        this.colorFieldLabel(key, 'idle');
      }
    }
  }

  errorCheck() {
    const errorMap = {
      street1: null,
      street2: null,
      city: null,
      state: null,
      zip: null,
    };
    this.inputState.error = false;
    this.errorState = {};
    const errorDirectiveArray = this.errorDirectives.toArray();
    for (let i = 0; i < errorDirectiveArray.length; i++) {
      const err = errorDirectiveArray[i];
      // Field is in error state.
      // But an error for this specific field is showing
      if (errorMap[err.addressFieldName] && err.isActive) {
        err.hide();

        // Field is in error state and it is not showing.
      } else if (this.inputState.hadInitialBlur && err.isActive) {
        this.inputState.error = true;
        this.colorFieldLabel(err.addressFieldName, 'error');
        this.showFieldErrorIcon(err.addressFieldName);
        err.show();
        errorMap[err.addressFieldName] = err;
        this.errorState[err.addressFieldName] = true;

        // Field is not in error state
      } else if (!err.isActive) {
        err.hide();
      }
    }
    Object.keys(errorMap).forEach((key) => {
      if (!errorMap[key]) {
        this.hideFieldErrorIcon(key);
        if (this.focusState[key]) {
          this.colorFieldLabel(key, 'focus');
        } else {
          this.colorFieldLabel(key, 'idle');
        }
      }
      if (this.formGroup.form.get(key).valid && !this.focusState[key]) {
        this.showSuccessIconForField(key);
      } else {
        this.hideSuccessIconForField(key);
      }
    });
  }

  resetErrorThreshold() {
    this.inputState.hadInitialBlur = false;
    this.errorDirectives.forEach((err) => err.hide());
  }

  // HELPERS
  getStreet1Value() {
    return this.inputDirectives
      .find((x) => x.addressFieldName === 'street1')
      .getValue();
  }

  getStreet2Value() {
    return this.inputDirectives
      .find((x) => x.addressFieldName === 'street2')
      .getValue();
  }

  getStateValue() {
    return this.inputDirectives
      .find((x) => x.addressFieldName === 'state')
      .getValue();
  }

  pickState() {
    const list = this.getMatchingStateAutocompleteList();
    if (list.length === 1) {
      const stateControl = this.inputDirectives.find(
        (x) => x.addressFieldName === 'state'
      );
      stateControl.setValue(list[0].value);
    }
  }

  getMatchingStateAutocompleteList() {
    const stateValue = this.getStateValue();
    if (!stateValue || !stateValue.length) {
      return this.stateAutocompletelist || [];
    } else {
      const searchString = stateValue.toLowerCase();
      let list = [];
      outerLoop: for (let i = 0; i < this.stateAutocompletelist.length; i++) {
        const currentItem = this.stateAutocompletelist[i];
        for (let j = 0; j < currentItem.searchTerms.length; j++) {
          const currentSearchTerm = currentItem.searchTerms[j].toLowerCase();
          if (searchString === currentItem.value.toLowerCase()) {
            list = [currentItem];
            break outerLoop;
          } else if (currentSearchTerm.includes(searchString)) {
            list.push(currentItem);
            break;
          }
        }
      }
      return list;
    }
  }

  // EVENT HANDLERS
  onMouseEvent(hovering) {
    this.inputState.hover = hovering;
  }

  // PRIVATE
  private handleAddressItemSelected(item) {
    this.focusState.temp = true;
    this.focusState.street2 = true;
    this.focusEventBehavior.next({});
    const street2Control = this.inputDirectives.find(
      (x) => x.addressFieldName === 'street2'
    );
    if (street2Control) {
      street2Control.el.nativeElement.focus();
    }
    this.addressItemSelected.emit(item);
  }

  private handleStateItemSelected(state) {
    this.focusState.temp = true;
    this.focusState.zip = true;
    this.focusEventBehavior.next({});
    const zipControl = this.inputDirectives.find(
      (x) => x.addressFieldName === 'zip'
    );
    if (zipControl) {
      zipControl.el.nativeElement.focus();
    }
    this.stateSelected.emit(state);
  }

  private floatFieldLabel(fieldName) {
    const labels = this.labelDirectives.filter(
      (dir) => dir.addressFieldName === fieldName
    );
    if (labels.length) {
      labels.forEach((label) => !label.isFloating && label.float());
    }
  }

  private showFieldErrorIcon(fieldName) {
    const icons = this.errorIconDirectives.filter(
      (dir) => dir.addressFieldName === fieldName
    );
    if (icons.length) {
      icons.forEach((label) => !label.isVisible && label.show());
    }
  }
  private hideFieldErrorIcon(fieldName) {
    const icons = this.errorIconDirectives.filter(
      (dir) => dir.addressFieldName === fieldName
    );
    if (icons.length) {
      icons.forEach((label) => label.isVisible && label.hide());
    }
  }

  private showSuccessIconForField(fieldName) {
    const arr = this.successIconDirectives.toArray();
    if (!arr) {
      return;
    }

    const successIcon = arr.find((x) => x.addressFieldName === fieldName);

    if (!successIcon) {
      return;
    }

    // Since street2 is not required we didn't want the check mark to appear when it was empty
    if (
      fieldName === 'street2' &&
      (!this.getStreet2Value() || this.getStreet2Value() === '')
    ) {
      return;
    }

    successIcon.show();
  }

  private hideSuccessIconForField(fieldName) {
    const arr = this.successIconDirectives.toArray();
    if (!arr) {
      return;
    }

    const successIcon = arr.find((x) => x.addressFieldName === fieldName);

    if (!successIcon) {
      return;
    }

    successIcon.hide();
  }

  private colorFieldLabel(fieldName, state) {
    const labels = this.labelDirectives.filter(
      (dir) => dir.addressFieldName === fieldName
    );
    if (labels.length) {
      labels.forEach((label) => {
        switch (state) {
          case 'idle':
            label.setIdleColor();
            break;
          case 'hover':
            label.setHoverColor();
            break;
          case 'focus':
            label.setFocusColor();
            break;
          case 'error':
            label.setErrorColor();
            break;
        }
      });
    }
  }

  private expandAutocompleteList() {
    if (!this.addressAutocompleteList || !this.addressAutocompleteList.length) {
      return;
    }
    this.animateExpandListHelper(
      this.street1Wrapper.nativeElement,
      this.autocompleteListEl.nativeElement
    );
    this.isAddressListExpanded = true;
  }

  private hideAutocompleteList() {
    if (this.isAddressListExpanded) {
      this.animateHideListHelper(
        this.street1Wrapper.nativeElement,
        this.autocompleteListEl.nativeElement
      );
      this.isAddressListExpanded = false;
    }
  }

  private expandStateAutocompleteList() {
    this.animateExpandListHelper(
      this.stateWrapper.nativeElement,
      this.stateAutocompleteListEl.nativeElement
    );
    this.isStateListExpanded = true;
  }

  private hideStateAutocompleteList() {
    if (this.isStateListExpanded) {
      this.animateHideListHelper(
        this.stateWrapper.nativeElement,
        this.stateAutocompleteListEl.nativeElement
      );
      this.isStateListExpanded = false;
    }
  }

  private animateExpandListHelper(wrapperEl, listEl) {
    this.native
      .animate(
        wrapperEl,
        {
          maxHeight: '208px',
          minHeight: '64px',
          height: '208px',
        },
        {
          duration: 200,
        }
      )
      .then(() => {
        return (listEl.style.display = 'block');
      })
      .then(() => {
        return this.native.animate(
          listEl,
          {
            opacity: 1,
            left: 64,
          },
          {
            delay: 100,
            duration: 200,
            easing: 'easeOutQuart',
            queue: false,
          }
        );
      });
  }

  private animateHideListHelper(wrapperEl, listEl) {
    this.native
      .animate(
        listEl,
        {
          opacity: 0,
          left: 128,
        },
        {
          duration: 200,
          easing: 'easeOutQuart',
        }
      )
      .then(() => {
        return this.native.animate(
          wrapperEl,
          {
            maxHeight: '64px',
            minHeight: '64px',
            height: '64px',
          },
          {
            delay: 100,
            duration: 150,
            queue: false,
          }
        );
      })
      .then(() => {
        return (listEl.style.display = 'none');
      });
  }
}
