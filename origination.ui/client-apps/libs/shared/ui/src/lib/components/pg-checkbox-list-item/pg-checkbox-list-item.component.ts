import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ContentChild,
  Renderer2,
  EventEmitter,
  Input,
  Output,
  HostBinding,
  HostListener,
} from '@angular/core';
import { progVariables } from '../../styleGuides/prog-variables';
import { NativeService } from '@ua/shared/data-access';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pg-checkbox-list-item',
  templateUrl: './pg-checkbox-list-item.component.html',
  styleUrls: ['./pg-checkbox-list-item.component.scss'],
})
export class PgCheckboxListItemComponent implements OnInit {
  @ViewChild('itemWrapper') checkItemWrapper: ElementRef;
  @ViewChild('defaultStateIcon') unCheckedIcon: ElementRef;
  @ViewChild('checkedIcon') checkedIcon: ElementRef;
  @ViewChild('helperText') helperText: ElementRef;

  // NOTE: hacky work around to pass responsive. this control should
  // eventually end up in design system anyways
  smallViewportHeightContracted = '56px';
  smallViewportHeightExpanded = '80px';
  largeViewportHeightContracted = '72px';
  largeViewportHeightExpanded = '100px';

  isChecked = false;

  @Input() checkedItemValue: any;

  @Output() checkedEmitter: EventEmitter<any> = new EventEmitter<any>();

  @Output() unCheckedEmitter: EventEmitter<any> = new EventEmitter<any>();

  constructor(private native: NativeService, private renderer: Renderer2) {}

  ngOnInit() {}

  @HostListener('click')
  toggleCheck() {
    this.isChecked = !this.isChecked;
    if (this.isChecked) {
      this.handleItemChecked(this.checkItemWrapper.nativeElement);
      this.checkedEmitter.emit(this.checkedItemValue);
    } else {
      this.handleItemUnChecked();
      this.unCheckedEmitter.emit(this.checkedItemValue);
    }
  }

  handleItemChecked(wrapperEl) {
    const promises = [];
    this.renderer.setStyle(this.helperText.nativeElement, 'display', 'block');
    this.renderer.setStyle(this.helperText.nativeElement, 'opacity', 0);

    promises.push(
      this.native
        .animate(
          wrapperEl,
          {
            backgroundColor: progVariables.colorPrimary,
            color: progVariables.colorNeutralWhiteLighter,
          },
          {
            duration: 150,
            easing: progVariables.standardCurveArray,
            queue: false,
          }
        )
        .then(() => {
          this.renderer.setStyle(
            this.unCheckedIcon.nativeElement,
            'display',
            'none'
          );
          this.renderer.setStyle(
            this.checkedIcon.nativeElement,
            'opacity',
            '1'
          );
          this.renderer.setStyle(
            this.checkedIcon.nativeElement,
            'display',
            'block'
          );
        })
    );

    promises.push(
      this.native.animate(
        wrapperEl,
        {
          height: this.getExpandedHeight(),
          boxShadowX: '0px',
          boxShadowY: '2px',
          boxShadowBlur: '4px',
        },
        {
          duration: 150,
          easing: progVariables.standardCurveArray,
          queue: false,
        }
      )
    );

    promises.push(
      this.native.animate(
        this.helperText.nativeElement,
        {
          opacity: 1,
          translateY: [0, -24],
        },
        {
          duration: 100,
          delay: 50,
          easing: progVariables.standardCurveArray,
          queue: false,
        }
      )
    );

    return Promise.all(promises);
  }

  getExpandedHeight() {
    if (window.innerWidth >= 600) {
      return this.largeViewportHeightExpanded;
    }
    return this.smallViewportHeightExpanded;
  }

  getContractedHeight() {
    if (window.innerWidth >= 600) {
      return this.largeViewportHeightContracted;
    }
    return this.smallViewportHeightContracted;
  }

  handleItemUnChecked() {
    const promises = [];

    promises.push(
      this.native
        .animate(
          this.checkItemWrapper.nativeElement,
          {
            height: this.getContractedHeight(),
          },
          {
            duration: 150,
            easing: progVariables.standardCurveArray,
            queue: false,
          }
        )
        .then(() => {
          this.checkItemWrapper.nativeElement.style.height = null;
          this.renderer.setStyle(
            this.checkedIcon.nativeElement,
            'display',
            'none'
          );
          this.renderer.setStyle(
            this.checkedIcon.nativeElement,
            'opacity',
            '0'
          );
          this.renderer.setStyle(
            this.unCheckedIcon.nativeElement,
            'opacity',
            '1'
          );
          this.renderer.setStyle(
            this.unCheckedIcon.nativeElement,
            'display',
            'block'
          );
        }),
      this.native
        .animate(
          this.helperText.nativeElement,
          {
            opacity: 0,
            translateY: [-24, 0],
          },
          {
            duration: 100,
            queue: false,
          }
        )
        .then(() => {
          this.renderer.setStyle(
            this.helperText.nativeElement,
            'display',
            'none'
          );
        })
    );

    promises.push(
      this.native
        .animate(
          this.checkItemWrapper.nativeElement,
          {
            backgroundColor: progVariables.colorNeutralWhiteLightest,
            color: progVariables.colorNeutralBlack,
            boxShadowX: '0px',
            boxShadowY: '0px',
            boxShadowBlur: '0px',
          },
          {
            duration: 150,
            easing: progVariables.standardCurveArray,
            queue: false,
          }
        )
        .then(() => {
          this.renderer.setStyle(
            this.checkedIcon.nativeElement,
            'display',
            'none'
          );
          this.renderer.setStyle(
            this.checkedIcon.nativeElement,
            'opacity',
            '0'
          );
          this.renderer.setStyle(
            this.unCheckedIcon.nativeElement,
            'opacity',
            '1'
          );
          this.renderer.setStyle(
            this.unCheckedIcon.nativeElement,
            'display',
            'block'
          );
          this.renderer.setStyle(
            this.checkItemWrapper.nativeElement,
            'box-shadow',
            null
          );
        })
    );

    return Promise.all(promises).then(() => {
      this.checkItemWrapper.nativeElement.style.height = null;
    });
  }
}
