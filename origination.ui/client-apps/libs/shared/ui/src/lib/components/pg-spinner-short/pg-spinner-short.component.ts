// tslint:disable:no-input-rename
import { Component, OnInit, Input } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pg-spinner-short',
  templateUrl: './pg-spinner-short.component.html',
  styleUrls: ['./pg-spinner-short.component.scss'],
})
export class PgSpinnerShortComponent implements OnInit {
  @Input('size') size = 64;
  @Input() type: 'BLUE' | 'WHITE' = 'BLUE';

  constructor() {}

  ngOnInit() {}
}
