import {
  Component,
  OnInit,
  Input,
  Output,
  ElementRef,
  Renderer2,
  ViewChild,
  EventEmitter,
  HostListener,
  HostBinding,
  OnChanges,
} from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pg-option',
  templateUrl: './pg-option.component.html',
  styleUrls: ['./pg-option.component.scss'],
})
export class PgOptionComponent implements OnInit, OnChanges {
  // tslint:disable-next-line:no-input-rename
  @Input('disabled') disabled = false;
  // tslint:disable-next-line:no-input-rename
  @Input('value') value: any;

  // tslint:disable-next-line:no-output-rename
  @Output('optionSelected') optionSelected: EventEmitter<
    any
  > = new EventEmitter<any>();

  @ViewChild('contentEl') contentEl: ElementRef;

  @HostBinding('class.disabled') class_disabled = false;

  constructor(public el: ElementRef) {}

  ngOnInit() {}

  ngOnChanges() {
    this.class_disabled = this.disabled;
  }

  getDisplayValue() {
    if (this.contentEl) {
      return this.contentEl.nativeElement.textContent.trim();
    }
    return '';
  }
  @HostListener('click', ['$event'])
  handleItemClicked() {
    this.optionSelected.emit({
      disabled: this.disabled,
      value: this.value,
      display: this.getDisplayValue(),
    });
  }
}
