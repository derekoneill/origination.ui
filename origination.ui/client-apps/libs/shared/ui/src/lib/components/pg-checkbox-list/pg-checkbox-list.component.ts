import {
  Component,
  OnInit,
  ContentChildren,
  QueryList,
  Input,
  Output,
  AfterViewInit,
  OnDestroy,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { PgCheckboxListItemComponent } from '../pg-checkbox-list-item/pg-checkbox-list-item.component';
import { EventEmitter } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'pg-checkbox-list',
  templateUrl: './pg-checkbox-list.component.html',
  styleUrls: ['./pg-checkbox-list.component.scss'],
})
export class PgCheckboxListComponent
  implements OnInit, AfterViewInit, OnDestroy {
  @ContentChildren(PgCheckboxListItemComponent) checkboxItems: QueryList<
    PgCheckboxListItemComponent
  >;

  @Output()
  checkedItemList: any[] = [];

  // tslint:disable-next-line:no-output-native
  @Output()
  change: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  allowMultiSelect = false;

  subs: Subscription[] = [];

  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {
    this.checkboxItems.forEach((element) => {
      this.subs.push(
        element.checkedEmitter.subscribe((checkedItem) => {
          this.addCheckedItem(checkedItem);
          this.change.emit(
            this.checkboxItems.filter((x) => x.isChecked === true)
          );
        })
      );
    });

    this.checkboxItems.forEach((element) => {
      this.subs.push(
        element.unCheckedEmitter.subscribe((removedItem) => {
          this.removeCheckedItem(removedItem);
          this.change.emit(
            this.checkboxItems.filter((x) => x.isChecked === true)
          );
        })
      );
    });
  }

  // Mark specific checkbox item as checked
  markItemAsChecked(itemKey) {
    if (this.checkboxItems) {
      this.checkboxItems.forEach((checkboxitem) => {
        if (checkboxitem.checkedItemValue === itemKey) {
          checkboxitem.toggleCheck();
        }
      });
    }
  }

  private addCheckedItem(item) {
    if (!this.allowMultiSelect) {
      // If this is not a multi select lets remove the previous item and unselect previous values
      this.checkboxItems.forEach((checkboxitem) => {
        if (checkboxitem.isChecked && checkboxitem.checkedItemValue !== item) {
          checkboxitem.toggleCheck();
        }
      });
    }

    this.checkedItemList.push(item); // TODO: Make this more of a robust with check
  }

  private removeCheckedItem(item) {
    this.checkedItemList.splice(this.checkedItemList.indexOf(item), 1); // TODO: Make this of a more robust with check
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => !sub.closed && sub.unsubscribe());
  }
}
