import { AnimationService } from '@progleasing/grit-core';
import { styleVariables } from '@progleasing/grit-core';
import { Renderer2 } from '@angular/core';

export function animateExpandDropDown(
  animationService: AnimationService,
  renderer: Renderer2,
  hostElement: HTMLElement,
  dropDownChildrenElements: HTMLElement[],
  // maxHeight = 134
  maxHeight = 240
) {
  // we declare the max height for the parent
  // this is necessary to know how far to animate towards.

  let totalItemHeight = 0;

  // set the parent height to a static 0
  renderer.setStyle(hostElement, 'height', '0px');
  // set all of the list item children styles prior to animation
  dropDownChildrenElements.forEach((el) => {
    renderer.setStyle(el, 'opacity', 0);
    renderer.setStyle(el, 'visibility', 'visible');

    // Translate them to the right in preparation for the shift animation
    renderer.setStyle(el, 'transform', 'translateX(64px)');

    totalItemHeight += el.clientHeight;
  });

  // now either we animate the parent to the maxHeight or the totalItemHeight,
  // whichever is smaller.
  const targetHeight = Math.min(totalItemHeight, maxHeight);

  // The height animation
  return animationService
    .animate({
      targets: hostElement,
      height: [0, targetHeight],
      duration: 150,
      easing: styleVariables.transitionCurveDeceleration,
    })
    .then(() => {
      renderer.setStyle(hostElement, 'height', `${targetHeight}px`);
    })
    .then(() => {
      return animationService.animate({
        transform: 'translateX(0)',
        opacity: 1,
        targets: dropDownChildrenElements,
        delay: (item, index) => Math.min(index * 50 + 50, 250),
        duration: 150,
        easing: styleVariables.transitionCurveDeceleration,
        fillMode: 'forwards',
      });
    })
    .then((data) => {
      data.forEach((c: any) => {
        renderer.setStyle(c.element, 'opacity', 1);
        renderer.setStyle(c.element, 'transform', 'translateX(0px)');
        c.animation.destroy();
      });
    });
}

export function animateCollapseDropDown(
  animationService: AnimationService,
  renderer: Renderer2,
  hostElement: HTMLElement,
  dropDownChildrenElements: HTMLElement[]
) {
  const startHeight = hostElement.offsetHeight;
  return animationService
    .animate({
      transform: 'translateX(64px)',
      opacity: 0,
      targets: dropDownChildrenElements,
      delay: (item, index) => Math.max(250 - index * 50, 0),
      duration: 150,
      easing: styleVariables.transitionCurveAcceleration,
      fillMode: 'forwards',
    })
    .then((data) => {
      data.forEach((c: any) => {
        renderer.setStyle(c.element, 'opacity', 0);
        renderer.setStyle(c.element, 'transform', 'translateX(64px)');
        renderer.setStyle(c.element, 'visibility', 'hidden');
        c.animation.destroy();
      });
    })
    .then(() => {
      return animationService.animate({
        targets: hostElement,
        height: [startHeight, 0],
        duration: 150,
        easing: styleVariables.transitionCurveAcceleration,
      });
    })
    .then(() => {
      renderer.setStyle(hostElement, 'height', '0px');
    });
}

export function animateUpdateDropDown(
  animationService: AnimationService,
  renderer: Renderer2,
  hostElement: HTMLElement,
  dropDownChildrenElements: HTMLElement[],
  //maxHeight = 134
  maxHeight = 240
) {
  const childArray = dropDownChildrenElements;
  let targetHeight = 0;
  for (let i = 0; i < childArray.length; i++) {
    targetHeight += childArray[i].clientHeight;
    if (targetHeight >= maxHeight) {
      break;
    }
  }

  targetHeight = Math.min(targetHeight, maxHeight);

  return animationService
    .animate({
      targets: hostElement,
      height: targetHeight,
      duration: 150,
      easing: styleVariables.transitionCurveAcceleration,
    })
    .then(() => {
      renderer.setStyle(hostElement, 'height', `${targetHeight}px`);
    });
}
