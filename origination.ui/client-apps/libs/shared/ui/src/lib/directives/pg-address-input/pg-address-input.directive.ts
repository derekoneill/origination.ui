import {
  Directive,
  ElementRef,
  Input,
  Self,
  EventEmitter,
  HostBinding,
  OnInit,
  OnDestroy,
  HostListener,
} from '@angular/core';
import { FormGroupDirective, NgControl } from '@angular/forms';
import { Subscription } from 'rxjs';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[pgAddressInput]',
  // tslint:disable-next-line:no-host-metadata-property
  host: {
    '[placeholder]': 'placeholder',
    '[disabled]': 'disabled',
    '[required]': 'required',
  },
})
export class PgAddressInputDirective implements OnInit, OnDestroy {
  @Input() pgAddressInput: string;
  @Input() formControlName: string;
  @HostBinding('class') className: any;
  @HostBinding('class.font-body-l') _classNameFontBodyL = true;

  @Input() public placeholder = '';
  valueChange = new EventEmitter<string>();
  focusChange = new EventEmitter<boolean>();
  hoverChange = new EventEmitter<boolean>();

  subs: Subscription[] = [];
  get addressFieldName() {
    return this.pgAddressInput;
  }

  constructor(
    public el: ElementRef,
    @Self() public control: NgControl,
    public formGroup: FormGroupDirective
  ) {}

  // LIFE CYCLE
  ngOnInit() {
    this.className = `pg-address-input ${this.addressFieldName}-input`;
    if (this.control.value) {
      this.valueChange.emit(this.control.value);
    }
    this.subs.push(
      this.control.valueChanges.subscribe((value) => {
        this.valueChange.emit(value);
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => !sub.closed && sub.unsubscribe());
  }

  // HOST LISTENERS
  @HostListener('focus', ['$event'])
  onFocus() {
    this.focusChange.emit(true);
    this.el.nativeElement.classList.add('show-placeholder');
  }

  @HostListener('blur', ['$event'])
  onBlur() {
    this.focusChange.emit(false);
  }

  @HostListener('mouseover', [])
  onHover() {
    this.hoverChange.emit(true);
  }

  @HostListener('mouseout', [])
  onMouseOut() {
    this.hoverChange.emit(false);
  }

  setValue(value: string) {
    // @ts-ignore
    this.formGroup.form.get(this.control.name).setValue(value);
  }

  getValue(): string {
    return this.control.value;
  }
}
