import { Input, ElementRef, Directive } from '@angular/core';
import { PgFormatBaseDirective } from '../pg-format/pg-format.directive';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[restrictPattern]',
})
export class RestrictPatternDirective extends PgFormatBaseDirective {
  @Input() singleSpace = false;
  @Input() restrictPattern: RegExp;

  constructor(private e: ElementRef) {
    super(e);
  }

  format(value: string): string {
    if (!value) {
      return null;
    }
    const matches = value.match(this.restrictPattern);
    let result = '';
    if (matches && matches.length) {
      result = matches.join('');
    }
    if (this.singleSpace) {
      result = result.replace(/\s+/gi, ' ');
    }
    return result;
  }
}
