import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { NativeService, PgAddressService } from '@ua/shared/data-access';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[pgAddressError]',
  // tslint:disable-next-line:no-host-metadata-property
  host: {
    class: 'pg-address-error',
  },
})
export class PgAddressErrorDirective implements OnInit {
  @Input() pgAddressError: string;
  @Input() errorKey: string;

  get addressFieldName() {
    return this.pgAddressError;
  }

  // Denotes whether the formgroup error
  get isActive() {
    const control = this.addressService.getFormControlByAddressFieldName(
      this.addressFieldName
    );
    if (!control) {
      return false;
    }
    return control.hasError(this.errorKey);
  }

  private _isVisible = true;

  constructor(
    public el: ElementRef,
    public formGroup: FormGroupDirective,
    private addressService: PgAddressService,
    private native: NativeService
  ) {}

  ngOnInit() {
    // initialize to invisible
    this.hide({ duration: 0 });
  }

  public setVisiblityBasedOnControl(allowErrorToShow = true) {
    if (this.isActive && allowErrorToShow) {
      this.show();
    } else {
      this.hide();
    }
  }

  public show(animationOptions: any = null): Promise<any> {
    if (this._isVisible) {
      return Promise.resolve();
    }
    const opts = animationOptions || {
      duration: 150,
      easing: 'easeOutQuart',
      delay: 0,
    };
    this.el.nativeElement.display = 'block';
    return this.native
      .animate(
        this.el.nativeElement,
        {
          marginTop: '8px',
          fontSize: '13px',
          opacity: 1,
          lineHeight: '19px',
        },
        opts
      )
      .then((x) => {
        this._isVisible = true;
      });
  }

  public hide(animationOptions: any = null): Promise<any> {
    if (!this._isVisible) {
      return Promise.resolve();
    }
    const opts = animationOptions || {
      duration: 150,
      easing: 'easeOutQuart',
      delay: 0,
    };
    return this.native
      .animate(
        this.el.nativeElement,
        {
          marginTop: '0px',
          fontSize: '0px',
          opacity: 0,
          lineHeight: '0px',
        },
        opts
      )
      .then(() => {
        this.el.nativeElement.display = 'none';
        this._isVisible = false;
        return true;
      });
  }
}
