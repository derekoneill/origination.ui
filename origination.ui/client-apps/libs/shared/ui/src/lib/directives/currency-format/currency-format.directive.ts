import { Directive, HostListener, Optional, Self } from '@angular/core';
import {
  PgFormatBaseDirective,
  PgFormatDirective,
} from '../pg-format/pg-format.directive';
import { ElementRef } from '@angular/core';

@PgFormatDirective('[currencyFormat]', CurrencyFormatDirective, [])
export class CurrencyFormatDirective extends PgFormatBaseDirective {
  constructor(private e: ElementRef) {
    super(e);
  }

  format(value: string): string {
    // Remove illegal characters before processing
    let filteredValue = value.replace(/[^0-9\.]+/g, '');

    while (
      filteredValue.length >= 2 &&
      filteredValue.startsWith('0') &&
      filteredValue[1] !== '.'
    ) {
      filteredValue = filteredValue.substring(1, filteredValue.length);
    }

    const placeHolderCheck = /(\d+)(\d{3})/; // Format for US locale thousands place
    const decimalParts = filteredValue.split('.');

    // Get the whole number from the decimal, probably not the best way but it works
    let wholeNumber = decimalParts[0];
    let decimal = decimalParts.length > 1 ? '.' + decimalParts[1] : '';

    // If we match three sets replace all special character and add comma
    while (placeHolderCheck.test(wholeNumber)) {
      wholeNumber = wholeNumber
        .replace(/\D/g, '')
        .replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }

    if (decimal.length > 3) {
      decimal = decimal.substring(0, 3);
    }

    return wholeNumber + decimal;
  }
}
