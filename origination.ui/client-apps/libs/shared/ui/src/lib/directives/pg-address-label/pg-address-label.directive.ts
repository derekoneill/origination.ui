import { Directive, ElementRef, Input } from '@angular/core';
import { NativeService } from '@ua/shared/data-access';

const commonOptions = {
  duration: 150,
  easing: 'easeOutQuart',
};

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[pgAddressLabel]',
  // tslint:disable-next-line:no-host-metadata-property
  host: {
    class: 'pg-address-label',
  },
})
export class PgAddressLabelDirective {
  @Input() pgAddressLabel: string;
  isFloating = false;
  get addressFieldName() {
    return this.pgAddressLabel;
  }

  constructor(public el: ElementRef, private native: NativeService) {}

  float() {
    this.isFloating = true;
    return this.native.animate(
      this.el.nativeElement,
      {
        fontSize: '13px',
        top: '8px',
      },
      {
        duration: 150,
        easing: 'easeOutQuart',
        queue: false,
      }
    );
  }

  sink() {
    this.isFloating = false;
    return this.native.animate(
      this.el.nativeElement,
      {
        fontSize: '16px',
        top: '20px',
      },
      {
        duration: 150,
        easing: 'easeOutQuart',
        queue: false,
      }
    );
  }

  // TODO: lets find a way to share state with the scss or at
  // lease manage the colors in a cental place for the javascript too
  setIdleColor() {
    this.native.animate(
      this.el.nativeElement,
      {
        color: '#637381',
      },
      commonOptions
    );
  }
  setHoverColor() {
    this.native.animate(
      this.el.nativeElement,
      {
        color: '#454f5b',
      },
      commonOptions
    );
  }
  setFocusColor() {
    this.native.animate(
      this.el.nativeElement,
      {
        color: '#3185FC',
      },
      commonOptions
    );
  }
  setErrorColor() {
    this.native.animate(
      this.el.nativeElement,
      {
        color: '#f05f55',
      },
      commonOptions
    );
  }
}
