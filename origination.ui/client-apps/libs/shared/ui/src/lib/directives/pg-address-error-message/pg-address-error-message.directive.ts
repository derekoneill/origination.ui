import { Directive, Input, ElementRef } from '@angular/core';

// TODO: Check out DI options for passing address object
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[pgAddressErrorMessage]',
  // tslint:disable-next-line:no-host-metadata-property
  host: {
    class: 'pg-error-message',
  },
})
export class PgAddressErrorMessageDirective {
  address: any;
  @Input() fieldName: string;
  @Input() validator: Function;

  private _errorActive = false;
  public get errorActive() {
    return this._errorActive;
  }

  constructor(public el: ElementRef) {}

  setAddress(address: any) {
    this.address = address;
  }

  runErrorCheck() {
    const err = this.validator.call(null, this.address, this.fieldName);
    this._errorActive = !!err;
  }
}
