import { Directive, ElementRef } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[pgOverlayOrigin]',
  exportAs: 'pgOverlayOrigin',
})
export class PgOverlayOriginDirective {
  private _nativeElement: HTMLElement;

  constructor(private el: ElementRef) {
    this._nativeElement = el.nativeElement;
  }

  getOriginBoundingBox() {
    return this._nativeElement.getBoundingClientRect();
  }
}
