import { Directive, ElementRef, HostListener } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';

@Directive()
export abstract class PgFormatBaseDirective implements ControlValueAccessor {
  protected inputEl: HTMLInputElement;
  _onChange = (_: any) => {};
  _onTouched = () => {};

  constructor(private el: ElementRef) {
    this.inputEl = el.nativeElement;
  }

  @HostListener('blur', ['$event'])
  blur($event) {
    this._onTouched();
  }

  @HostListener('input', ['$event'])
  input($event) {
    this.updateValue();
  }

  updateValue() {
    const newVal = this.format(this.inputEl.value);
    this.inputEl.value = newVal;
    this._onChange(newVal);
  }

  abstract format(inputVal: string): string;

  registerOnTouched(fn: any): void {
    this._onTouched = fn || (() => {});
  }

  registerOnChange(fn: (value: any) => any): void {
    this._onChange = fn;
  }

  writeValue(value: any) {
    this.inputEl.value = value;
    if (value) {
      setTimeout(() => {
        this.updateValue();
      });
    }
  }
}
