import { TimerTextMaskDirective } from './text-mask.directive';
import { Substitute, SubstituteOf } from '@fluffy-spoon/substitute';
import { ElementRef } from '@angular/core';

describe('TextMaskDirective', () => {
  let elementRef: SubstituteOf<ElementRef>;

  beforeEach(() => {
    elementRef = Substitute.for<ElementRef>();
  });

  it('should create an instance', () => {
    const directive = new TimerTextMaskDirective(elementRef);
    expect(directive).toBeTruthy();
  });
});
