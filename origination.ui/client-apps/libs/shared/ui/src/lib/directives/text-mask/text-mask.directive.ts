import {
  Directive,
  ElementRef,
  Input,
  HostListener,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

@Directive({
  // tslint:disable-next-line: directive-selector
  selector: '[timerTextMask]',
})
export class TimerTextMaskDirective implements OnChanges {
  timer = null;

  constructor(private el: ElementRef) {}
  @Input() isMasked: boolean;
  @Input() timeoutSecs = 3;
  @Output() isMaskedEmitter = new EventEmitter();

  ngOnChanges(_: SimpleChanges) {
    if (this.isMasked) {
      this.unmaskElementText();
    } else {
      this.maskElementText();
    }

    this.startTimer();
  }

  @HostListener('keyup') onKeyUp() {
    this.unmaskElementText();
    this.isMaskedEmitter.emit(true);
    this.startTimer();
  }

  private startTimer() {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.maskElementText();
      this.isMaskedEmitter.emit(false);
    }, this.timeoutSecs * 1000);
  }

  private maskElementText() {
    this.el.nativeElement.type = 'password';
  }

  private unmaskElementText() {
    this.el.nativeElement.type = 'tel';
  }
}
