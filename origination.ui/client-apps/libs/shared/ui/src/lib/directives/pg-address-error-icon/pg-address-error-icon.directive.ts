import { Directive, ElementRef, OnInit, Input } from '@angular/core';
import { NativeService } from '@ua/shared/data-access';
@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[pgAddressErrorIcon]',
  // tslint:disable-next-line:no-host-metadata-property
  host: {
    class: 'pg-address-error-icon pg-icon material-icons',
  },
})
export class PgAddressErrorIconDirective implements OnInit {
  @Input() pgAddressErrorIcon: string;

  get addressFieldName() {
    return this.pgAddressErrorIcon;
  }

  isVisible = false;

  constructor(private el: ElementRef, private native: NativeService) {}

  ngOnInit() {
    this.el.nativeElement.textContent = 'warning';
  }

  show() {
    if (this.isVisible) {
      return;
    }
    this.native.animate(
      this.el.nativeElement,
      {
        opacity: 1,
      },
      {
        duration: 150,
        easing: 'easeOutQuart',
      }
    );
    this.isVisible = true;
  }

  hide() {
    if (!this.isVisible) {
      return;
    }
    this.native.animate(
      this.el.nativeElement,
      {
        opacity: 0,
      },
      {
        duration: 150,
        easing: 'easeOutQuart',
      }
    );
    this.isVisible = false;
  }

  setRightPosition(right: number) {
    this.native.animate(
      this.el.nativeElement,
      {
        right: right,
      },
      {
        duration: 150,
        easing: 'easeOutQuart',
      }
    );
  }
}
