import { PgFormatBaseDirective } from '../pg-format/pg-format.directive';
import { Directive, ElementRef } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[singleConsecutiveSpace]',
})
export class SingleConsecutiveSpaceDirective extends PgFormatBaseDirective {
  constructor(private e: ElementRef) {
    super(e);
  }

  format(value: string): string {
    const result = value.replace(/\s+/gi, ' ');
    return result;
  }
}
