import { Directive, Input, ElementRef, OnInit } from '@angular/core';
import { NativeService } from '@ua/shared/data-access';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[pgAddressSuccessIcon]',
  // tslint:disable-next-line:no-host-metadata-property
  host: {
    class: 'pg-address-success-icon pg-icon material-icons',
  },
})
export class PgAddressSuccessIconDirective implements OnInit {
  @Input() pgAddressSuccessIcon: string;
  isVisible = false;

  get addressFieldName() {
    return this.pgAddressSuccessIcon;
  }

  constructor(private elementRef: ElementRef, private native: NativeService) {}

  ngOnInit() {
    this.elementRef.nativeElement.textContent = 'done';
  }

  show() {
    if (this.isVisible) {
      return;
    }
    this.native.animate(
      this.elementRef.nativeElement,
      {
        opacity: 1,
      },
      {
        duration: 150,
        easing: 'easeOutQuart',
      }
    );
    this.isVisible = true;
  }

  hide() {
    if (!this.isVisible) {
      return;
    }
    this.native.animate(
      this.elementRef.nativeElement,
      {
        opacity: 0,
      },
      {
        duration: 150,
        easing: 'easeOutQuart',
      }
    );
    this.isVisible = false;
  }

  setRightPosition(right: number) {
    this.native.animate(
      this.elementRef.nativeElement,
      {
        right: right,
      },
      {
        duration: 150,
        easing: 'easeOutQuart',
      }
    );
  }
}
