import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {
  LocationQueryResponse,
  LocationDetail,
  ValidateZipStateRequest,
  ZipCodeLookupResponse,
  Location,
} from '../models';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AppConfig } from '../config';

@Injectable()
export class LocationService {
  constructor(private http: HttpClient, private appConfig: AppConfig) {}

  queryLocations(q: string): Observable<Location[]> {
    return this.http
      .get<LocationQueryResponse>(
        `${this.appConfig.settings.eCommerceApiUrl}/v1/locations/${q}`
      )
      .pipe(map((res) => res.results));
  }

  getLocationDetailByLocationId(placeId: string): Observable<LocationDetail> {
    return this.http.get<LocationDetail>(
      `${this.appConfig.settings.eCommerceApiUrl}/v1/locations/details/${placeId}`
    );
  }

  validateStateAndZip(
    validateZipStateReq: ValidateZipStateRequest
  ): Observable<boolean> {
    if (
      !validateZipStateReq ||
      !validateZipStateReq.zipCode ||
      !validateZipStateReq.stateCode ||
      validateZipStateReq.zipCode.length !== 5
    ) {
      return of(false);
    }

    return this.http
      .post<ZipCodeLookupResponse>(
        `${this.appConfig.settings.eCommerceApiUrl}/v1/locations/validate`,
        validateZipStateReq
      )
      .pipe(map((res) => res.isValidStateForZip));
  }
}
