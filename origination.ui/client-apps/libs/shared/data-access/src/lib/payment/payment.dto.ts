export enum PayFrequency {
  Weekly = 0,
  EveryOtherWeek = 1,
  TwicePerMonth = 2,
  Monthly = 3,
}

export interface PaymentInformationResponse {
  sessionId: string;
  nextPayDate: string;
  lastPayDate: string;
  leaseId: number;
  paymentFrequency: PayFrequency;

  street1: string;
  street2: string;
  city: string;
  state: string;
  zip: string;
}

export interface PaymentInformation {
  sessionId: string;
  nextPayDate: Date;
  lastPayDate: Date;
  leaseId: number;
  paymentFrequency: PayFrequency;

  street1: string;
  street2: string;
  city: string;
  state: string;
  zip: string;
}

export interface CreditCard {
  sessionId: string;
  leaseId: number;
  creditCardNumber: string;
  expirationMonth: number;
  expirationYear: number;
  firstNameOnCard: string;
  lastNameOnCard: string;
  street1: string;
  street2: string;
  city: string;
  state: string;
  zip: string;
}
