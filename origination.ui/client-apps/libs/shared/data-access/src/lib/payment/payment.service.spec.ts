import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import {
  AppConfig,
  IConfig,
  PaymentInformation,
  PayFrequency,
  CreditCard,
} from '..';
import { PaymentService } from './payment.service';
import { TestBed } from '@angular/core/testing';
import * as moment from 'moment';

describe('PaymentService', () => {
  let httpTestingController: HttpTestingController;
  let appConfig: AppConfig;
  let subject: PaymentService;
  let testSessionId: string;
  let expectedPaymentInformation: PaymentInformation;
  beforeEach(() => {
    appConfig = <AppConfig>{
      settings: <IConfig>{ eCommerceApiUrl: 'some-ecom-api-url' },
    };
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PaymentService, { provide: AppConfig, useValue: appConfig }],
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    subject = TestBed.inject(PaymentService);

    testSessionId = 'some-testing-session-id';
    expectedPaymentInformation = {
      sessionId: testSessionId,
      leaseId: 123456,
      paymentFrequency: PayFrequency.TwicePerMonth,
      nextPayDate: moment('06/01/2020', 'MM/DD/YYYY').toDate(),
      lastPayDate: moment('06/15/2020', 'MM/DD/YYYY').toDate(),
      street1: '10600 S 4000 W',
      street2: '',
      city: 'South Jordan',
      state: 'UT',
      zip: '84009',
    };
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('getPaymentInformationBySessionId() should return the payment information belonging to the specified session', () => {
    subject
      .getPaymentInformationBySessionId(testSessionId)
      .subscribe((response) =>
        expect(response).toEqual(expectedPaymentInformation)
      );
    const req = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/session/${testSessionId}/payment`
    );
    expect(req.request.method).toEqual('GET');
    req.flush(expectedPaymentInformation);
  });

  it('updatePaymentInformation() should update the payment information successfully.', () => {
    subject
      .updatePaymentInformation(expectedPaymentInformation)
      .subscribe((response) =>
        expect(response).toEqual(expectedPaymentInformation)
      );
    const req = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/session/payment`
    );
    expect(req.request.body).toEqual(expectedPaymentInformation);
    expect(req.request.method).toEqual('PUT');
    req.flush(expectedPaymentInformation);
  });

  it('submitPayment() should submit the specified payment information successfully', () => {
    subject.submitPayment(expectedPaymentInformation).subscribe();
    const req = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/session/payment/submit`
    );
    expect(req.request.body).toEqual(expectedPaymentInformation);
    expect(req.request.method).toEqual('POST');
  });

  it('submitCreditCardInformation() should submit credit card information successfully', () => {
    const creditCard: CreditCard = {
      sessionId: testSessionId,
      leaseId: 123456,
      creditCardNumber: '0000000001',
      expirationMonth: 2,
      expirationYear: 2025,
      firstNameOnCard: 'test1',
      lastNameOnCard: 'ecom-test1',
      street1: '10106 South 4000 West',
      street2: '',
      city: 'South Jordan',
      state: 'UT',
      zip: '84009',
    };
    subject.submitCreditCardInformation(creditCard).subscribe();
    const req = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/session/payment/creditCard/submit`
    );
    expect(req.request.body).toEqual(creditCard);
    expect(req.request.method).toEqual('POST');
  });
});
