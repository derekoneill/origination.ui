import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  PaymentInformation,
  CreditCard,
  PaymentInformationResponse,
} from '../models';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { AppConfig } from '../config';

@Injectable()
export class PaymentService {
  constructor(private http: HttpClient, private appConfig: AppConfig) {}

  getPaymentInformationBySessionId(
    sessionId: string
  ): Observable<PaymentInformation> {
    return this.http
      .get<PaymentInformationResponse>(
        `${this.appConfig.settings.eCommerceApiUrl}/v1/session/${sessionId}/payment`
      )
      .pipe(map(this.mapAPIFieldsToUIFields));
  }

  updatePaymentInformation(
    paymentInformation: PaymentInformation
  ): Observable<PaymentInformation> {
    const apiPaymentInfo = this.mapUIFieldsToApiFields(paymentInformation);
    return this.http
      .put<PaymentInformationResponse>(
        `${this.appConfig.settings.eCommerceApiUrl}/v1/session/payment`,
        apiPaymentInfo
      )
      .pipe(map(this.mapAPIFieldsToUIFields));
  }

  submitPayment(paymentInformation: PaymentInformation): Observable<void> {
    return this.http.post<void>(
      `${this.appConfig.settings.eCommerceApiUrl}/v1/session/payment/submit`,
      paymentInformation
    );
  }

  submitCreditCardInformation(creditCardInfo: CreditCard): Observable<void> {
    return this.http.post<void>(
      `${this.appConfig.settings.eCommerceApiUrl}/v1/session/payment/creditCard/submit`,
      creditCardInfo
    );
  }

  private mapAPIFieldsToUIFields(
    paymentInfo: PaymentInformationResponse
  ): PaymentInformation {
    if (paymentInfo) {
      return {
        ...paymentInfo,
        lastPayDate: paymentInfo.lastPayDate
          ? moment(paymentInfo.lastPayDate).toDate()
          : null,
        nextPayDate: paymentInfo.nextPayDate
          ? moment(paymentInfo.nextPayDate).toDate()
          : null,
      };
    } else {
      return <PaymentInformation>{};
    }
  }

  private mapUIFieldsToApiFields(
    paymentInfo: PaymentInformation
  ): PaymentInformation {
    if (paymentInfo) {
      return {
        ...paymentInfo,
        lastPayDate: paymentInfo.lastPayDate
          ? moment(paymentInfo.lastPayDate).toDate()
          : null,
        nextPayDate: paymentInfo.nextPayDate
          ? moment(paymentInfo.nextPayDate).toDate()
          : null,
      };
    }
    return paymentInfo;
  }
}
