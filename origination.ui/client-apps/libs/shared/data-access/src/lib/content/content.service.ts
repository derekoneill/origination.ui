import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AppConfig } from '../config';

@Injectable({
  providedIn: 'root',
})
export class ContentService {
  constructor(private http: HttpClient, private appConfig: AppConfig) {}

  getContent<T>(
    pageKey: string,
    instanceType?: string,
    language?: string
  ): Observable<T> {
    let uri = `${this.appConfig.settings.contentApiUrl}/api/content?pageKey=${pageKey}`;
    if (instanceType !== undefined) {
      uri = `${uri}&instanceType=${instanceType}`;
    }
    if (language !== undefined) {
      uri = `${uri}&language=${language}`;
    }
    return this.http.get<T>(uri);
  }
}
