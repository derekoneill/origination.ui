import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  Session,
  BySessionId,
  ServiceSessionResponse,
  InitSessionRequest,
} from '../models';
import { map } from 'rxjs/operators';
import { AppConfig } from '../config';

@Injectable()
export class SessionService {
  constructor(private http: HttpClient, private appConfig: AppConfig) {}

  getSession(sessionId: string): Observable<Session> {
    return this.http
      .get<ServiceSessionResponse>(
        `${this.appConfig.settings.eCommerceApiUrl}/v1/session/${sessionId}`
      )
      .pipe(
        map((svcSessionRes: ServiceSessionResponse) => {
          return <Session>{
            originalSessionId: svcSessionRes.sessionId,
            sessionId: svcSessionRes.newSessionId || svcSessionRes.sessionId,
            storeId: svcSessionRes.progressiveStoreId,
            storeName: svcSessionRes.progressiveStoreDisplayName,
            locale: svcSessionRes.locale,
            orderToken: svcSessionRes.orderToken,
          };
        })
      );
  }

  public initSession(
    initialSessionRequest: InitSessionRequest
  ): Observable<BySessionId> {
    const payload = { oldSessionId: initialSessionRequest.sessionId };
    return this.http.post<BySessionId>(
      `${this.appConfig.settings.eCommerceApiUrl}/v1/session/init`,
      payload
    );
  }
}
