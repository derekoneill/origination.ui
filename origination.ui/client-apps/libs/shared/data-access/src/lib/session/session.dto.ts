export interface BySessionId {
  sessionId: string;
}

export interface InitSessionRequest {
  authenticationId: string;
  authenticationCode: string;
  sessionId: string;
  storeId: number;
}

export interface HasNewSessionId {
  newSessionId?: string;
}

export interface ServiceSessionResponse extends HasNewSessionId {
  sessionId: string;
  locale: string;
  orderToken: string;
  progressiveStoreId: number;
  progressiveStoreDisplayName: string;
}

export interface Session {
  // this is merchant cart id.
  // original session id.
  originalSessionId: string;
  // use this to store new session id.
  sessionId: string;
  locale: string;
  orderToken: string;
  storeId: number;
  storeName: string;
}
