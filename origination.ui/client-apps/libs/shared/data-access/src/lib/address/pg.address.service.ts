import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable()
export class PgAddressService {
  private addressFieldToFormControlMap;

  constructor() {}

  setAddressFieldToFormControlMap(map) {
    this.addressFieldToFormControlMap = map;
  }

  getFormControlByAddressFieldName(addressField: string): FormControl {
    return this.addressFieldToFormControlMap[addressField];
  }
}
