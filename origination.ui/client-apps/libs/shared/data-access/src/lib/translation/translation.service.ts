import { of as observableOf, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable, Inject, Optional } from '@angular/core';
import { TranslateLoader } from '@ngx-translate/core';

import { HttpClient } from '@angular/common/http';

@Injectable()
export class TranslationService implements TranslateLoader {
  static DEFAULT_LOCALE = 'en-US';

  private storedTranslations: any;
  private locale: string;

  constructor(
    private http: HttpClient,
    @Optional() @Inject('translations') preLoadedTranslations: any
  ) {
    if (preLoadedTranslations) {
      this.storedTranslations = preLoadedTranslations;
      this.locale = TranslationService.DEFAULT_LOCALE;
    }
  }

  getTranslation(lang: string): Observable<any> {
    if (this.storedTranslations && lang === this.locale) {
      return observableOf(this.storedTranslations);
    } else {
      return this.http.get(`./assets/translations/${lang}.json`).pipe(
        map((res) => {
          const json = res;
          this.locale = lang;
          this.storedTranslations = json;
          return json;
        })
      );
    }
  }
}
