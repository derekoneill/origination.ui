import { TestBed } from '@angular/core/testing';

import { ResumeService } from './resume.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
  TestRequest,
} from '@angular/common/http/testing';
import { AppConfig, IConfig } from '..';
import {
  ResumeTypeEnum,
  ResumableState,
  CheckResumeAvailableResponse,
  ResumeStateResponse,
  LoginStatusReason,
} from './resume.dto';

describe('ResumeService', () => {
  let httpTestingController: HttpTestingController;
  let appConfig: AppConfig;
  let subject: ResumeService;
  let storeId: number;
  let authId: string;
  let sessionId: string;
  let merchantCartId: string;
  let ssn: string;
  let lastFourSSN: string;
  let shortAppEnabled: boolean;

  beforeEach(async () => {
    appConfig = <AppConfig>{
      settings: <IConfig>{ eCommerceApiUrl: 'some-ecom-api-url' },
    };
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ResumeService, { provide: AppConfig, useValue: appConfig }],
    });
    storeId = 1;
    authId = 'fakeAuthId';
    sessionId = 'fakeSessionId';
    merchantCartId = 'fakeMerchantCartId';
    ssn = 'fakeSSN';
    lastFourSSN = '1234';
    shortAppEnabled = true;
    subject = TestBed.inject(ResumeService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('checkForLoginResume() should return a resumable status correctly.', () => {
    const expected: CheckResumeAvailableResponse = {
      isAvailable: true,
    };
    subject
      .checkForLoginResume(authId)
      .subscribe((res) => expect(res).toEqual(expected));
    const req: TestRequest = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/checkoutsession/${btoa(
        authId
      )}/resume`
    );
    expect(req.request.method).toEqual('GET');
    req.flush(expected);
  });

  it('attemptSessionResume() should return a correct resume data.', () => {
    const expected: ResumeStateResponse = {
      sessionId: sessionId,
      leaseStatus: 'resuming',
      loginStatusReason: LoginStatusReason.ResumeOk,
      leaseId: 123456,
      approvalLimit: 2000,
      resumable: true,
      resumeData: {
        route: '/apply/income-info',
        authId: 'tester1.ecom@example.com',
        ssn: '4444',
      },
    };
    subject
      .getResumableStateBySession({
        sessionId,
        originalSessionId: merchantCartId,
        shortAppEnabled,
      })
      .subscribe((res) => expect(res).toEqual(expected));
    const req: TestRequest = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/session/resume?sessionId=${sessionId}&merchantCartId=${merchantCartId}&resumeType=${ResumeTypeEnum.Session}&flags={ecomui-feat-short-app:${shortAppEnabled}}`
    );
    expect(req.request.method).toEqual('GET');
    req.flush(expected);
  });

  it('attemptLoginResume() should return a correct resume data.', () => {
    const expected: ResumeStateResponse = {
      sessionId: sessionId,
      leaseStatus: 'resuming',
      loginStatusReason: LoginStatusReason.ResumeOk,
      leaseId: 123456,
      approvalLimit: 2000,
      resumable: true,
      resumeData: {
        route: '/apply/income-info',
        authId: 'tester1.ecom@example.com',
        ssn: '4444',
      },
    };
    subject
      .getResumableStateByAuthenticationId({
        storeId,
        authId,
        ssn,
        originalSessionId: merchantCartId,
        shortAppEnabled,
      })
      .subscribe((res) => expect(res).toEqual(expected));
    const req: TestRequest = httpTestingController.expectOne(
      `${
        appConfig.settings.eCommerceApiUrl
      }/v1/session/resume?storeId=${storeId}&merchantCartId=${merchantCartId}&authId=${authId.replace(
        '+',
        '%2b'
      )}&lastFourSocial=${ssn}&resumeType=${
        ResumeTypeEnum.Login
      }&flags={ecomui-feat-short-app:${shortAppEnabled}}`
    );
    expect(req.request.method).toEqual('GET');
    req.flush(expected);
  });

  it('should call saveResumableState once', () => {
    const resumableState: ResumableState = {
      sessionId: sessionId,
      resumeData: {
        authId: authId,
        ssn: lastFourSSN,
        route: '/apply/income-info',
      },
    };
    subject.saveResumableState(resumableState).subscribe();
    const call: TestRequest = httpTestingController.expectOne({
      url: `${appConfig.settings.eCommerceApiUrl}/v1/session/resume`,
    });
    expect(call.request.method).toEqual('POST');
  });
});
