// tslint:disable: max-line-length
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  CheckResumeAvailableResponse,
  ResumeStateResponse,
  ResumableState,
  ResumeTypeEnum,
  GetResumableStateBySessionRequest,
  GetResumableStateByAuthenticationIdRequest,
} from './resume.dto';
import { AppConfig } from '../config';

@Injectable()
export class ResumeService {
  constructor(private http: HttpClient, private appConfig: AppConfig) {}

  checkForLoginResume(
    authId: string
  ): Observable<CheckResumeAvailableResponse> {
    const url = `${
      this.appConfig.settings.eCommerceApiUrl
    }/v1/checkoutsession/${btoa(authId)}/resume`;
    return this.http.get<CheckResumeAvailableResponse>(url);
  }

  getResumableStateBySession(
    request: GetResumableStateBySessionRequest
  ): Observable<ResumeStateResponse> {
    const url = `${this.appConfig.settings.eCommerceApiUrl}/v1/session/resume?sessionId=${request.sessionId}&merchantCartId=${request.originalSessionId}&resumeType=${ResumeTypeEnum.Session}&flags={ecomui-feat-short-app:${request.shortAppEnabled}}`;
    return this.sendAttemptResume(url);
  }

  getResumableStateByAuthenticationId(
    request: GetResumableStateByAuthenticationIdRequest
  ): Observable<ResumeStateResponse> {
    const url = `${
      this.appConfig.settings.eCommerceApiUrl
    }/v1/session/resume?storeId=${request.storeId}&merchantCartId=${
      request.originalSessionId
    }&authId=${request.authId.replace('+', '%2b')}&lastFourSocial=${
      request.ssn
    }&resumeType=${ResumeTypeEnum.Login}&flags={ecomui-feat-short-app:${
      request.shortAppEnabled
    }}`;
    return this.sendAttemptResume(url);
  }

  private sendAttemptResume(url: string): Observable<ResumeStateResponse> {
    return this.http.get<ResumeStateResponse>(url);
  }

  saveResumableState(resumableState: ResumableState): Observable<void> {
    // only use the last 4 digit now.
    const ssn = resumableState.resumeData.ssn;
    const resumed = {
      sessionId: resumableState.sessionId,
      resumeData: {
        route: resumableState.resumeData.route,
        authId: resumableState.resumeData.authId,
        lastFourSSN: ssn
          ? ssn.substring(ssn.length - 4, ssn.length)
          : undefined,
      },
    };
    return this.http.post<void>(
      `${this.appConfig.settings.eCommerceApiUrl}/v1/session/resume`,
      resumed
    );
  }
}
