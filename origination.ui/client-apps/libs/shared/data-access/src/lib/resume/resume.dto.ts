export enum ResumeTypeEnum {
  Session = 0,
  Login = 1,
}

export interface CheckResumeAvailableResponse {
  isAvailable: boolean;
}

export enum LoginStatusReason {
  ResumeOk = 1,
  ResumeFailedInvalidLogin = 2,
  ResumeFailedSessionNotFound = 3,
  ResumeFailedApplicationDenied = 4,
}

export interface ResumeData {
  route: string;
  authId: string;
  ssn: string;
}

export interface ResumableState {
  sessionId: string;
  resumeData: ResumeData;
}

export interface GetResumableStateBySessionRequest {
  sessionId: string;
  originalSessionId: string;
  shortAppEnabled: boolean;
}

export interface GetResumableStateByAuthenticationIdRequest {
  storeId: number;
  authId: string;
  ssn: string;
  originalSessionId: string;
  shortAppEnabled: boolean;
}

export interface ResumeStateResponse extends ResumableState {
  leaseId?: number;
  leaseStatus: string;
  approvalLimit: number;
  resumable: boolean;
  loginStatusReason: LoginStatusReason;
}
