import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import {
  AppConfig,
  AuthenticationMethod,
  AuthenticationService,
  IConfig,
  OtpAuthenticateRequest,
  OtpAuthenticationResponse,
  OtpSendRequest,
} from '..';

describe('AuthenticationService', () => {
  let httpTestingController: HttpTestingController;
  let appConfig: AppConfig;
  let subject: AuthenticationService;
  beforeEach(() => {
    appConfig = <AppConfig>{
      settings: <IConfig>{ eCommerceApiUrl: 'some-ecom-api-url' },
    };
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AuthenticationService,
        { provide: AppConfig, useValue: appConfig },
      ],
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    subject = TestBed.inject(AuthenticationService);
  });
  afterEach(() => {
    httpTestingController.verify();
  });

  it('sendOtpCode() should send an authentication code successfully', () => {
    subject
      .sendOtpCode(<OtpSendRequest>{
        sessionId: 'some-test-session-id',
        authenticationMethod: AuthenticationMethod.Email,
        authenticationId: 'tester1@example.com',
      })
      .subscribe();
    httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/otpauthentication`
    );
  });

  it('authenticateOtpCode() should verify the authentication code correctly', () => {
    const expected: OtpAuthenticationResponse = {
      token: 'some-test-token',
      refreshToken: 'some-refresh-test-token',
      expDate: new Date(),
      locked: false,
      attempts: 0,
    };
    subject
      .authenticateOtpCode(<OtpAuthenticateRequest>{
        sessionId: 'some-test-session-id',
        authenticationId: 'test1@example.com',
        code: '000000',
        authenticationMethod: AuthenticationMethod.Email,
      })
      .subscribe((response) => expect(response).toEqual(expected));
    const req = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/otpauthentication`
    );
    req.flush(expected);
  });
});
