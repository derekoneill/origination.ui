import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  OtpAuthenticateRequest,
  OtpSendRequest,
  OtpAuthenticationResponse,
} from '../models';
import { AppConfig } from '../config';

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient, private appConfig: AppConfig) {}

  sendOtpCode(payload: OtpSendRequest): Observable<void> {
    return this.http.post<void>(
      `${this.appConfig.settings.eCommerceApiUrl}/v1/otpauthentication`,
      payload
    );
  }

  authenticateOtpCode(
    payload: OtpAuthenticateRequest
  ): Observable<OtpAuthenticationResponse> {
    return this.http.put<OtpAuthenticationResponse>(
      `${this.appConfig.settings.eCommerceApiUrl}/v1/otpauthentication`,
      payload
    );
  }
}
