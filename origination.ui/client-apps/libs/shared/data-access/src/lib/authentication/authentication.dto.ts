export enum AuthenticationMethod {
  SMS = 0,
  Email = 1,
}

export const getFormattedContactInfo = (
  value: string
): { contactMethod: AuthenticationMethod; contactId: string } => {
  let contactMethod = AuthenticationMethod.SMS;
  let contactId = value || '';
  if (contactId.indexOf('@') > -1) {
    contactMethod = AuthenticationMethod.Email;
  } else {
    contactId = '+1' + contactId.replace(/\D/gi, '');
  }
  return {
    contactMethod,
    contactId,
  };
};

export interface OtpSendRequest {
  authenticationMethod: AuthenticationMethod;
  authenticationId: string;
  sessionId: string;
}

export interface OtpAuthenticateRequest {
  sessionId: string;
  authenticationId: string;
  code: string;
  authenticationMethod: AuthenticationMethod;
}

export interface OtpAuthenticationResponse {
  token: string;
  refreshToken: string;
  expDate: Date;
  locked: boolean;
  attempts: number;
}
