import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import * as jwt_decode from 'jwt-decode';
import * as moment from 'moment';
import {AppConfig} from "../config";

@Injectable()
export class TokenService {
  constructor(
    private http: HttpClient,
    private appConfig: AppConfig,
  ) {}

  public getToken(forceRefresh: boolean) {
    const localToken = localStorage.getItem('api-token');
    if (localToken) {
      // Convert epoch timestamp from JWT
      const expDate = jwt_decode(localToken).exp;
      const expDateUtc = moment(expDate * 1000);
      const currentDate = moment.utc();

      if (currentDate.isAfter(expDateUtc) || forceRefresh) {
        return this.getTokenFromServer();
      }
      return of(localToken);
    } else {
      return this.getTokenFromServer();
    }
  }

  private getTokenFromServer() {
    return this.http
      .post<any>(`${this.appConfig.settings.eCommerceApiUrl}/auth/credentials`, {
        username: '{?p$maLz}2QZ56Za]rqyG',
        password: 'T^g$7~7N4wxEtphn{iCK',
        rememberMe: false,
      })
      .pipe(
        map((response) => {
          localStorage.setItem('api-token', response.bearerToken);
          return response.bearerToken;
        }),
        catchError((err) => {
          throw err;
        })
      );
  }
}
