import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {
  ApplicationService,
  AuthenticationService,
  AppConfig,
  ContentService,
  LeaseService,
  LocationService,
  OrderService,
  PaymentService,
  PaymentEstimatorService,
  ResumeService,
  SessionService,
  FeatureFlagService,
  PgAddressService,
} from './index';

@NgModule({
  imports: [HttpClientModule],
  providers: [
    ApplicationService,
    AuthenticationService,
    AppConfig,
    ContentService,
    LeaseService,
    LocationService,
    OrderService,
    PaymentService,
    PaymentEstimatorService,
    ResumeService,
    SessionService,
    FeatureFlagService,
    PgAddressService,
  ],
})
export class DataAccessModule {}
