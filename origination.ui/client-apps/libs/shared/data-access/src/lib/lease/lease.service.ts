import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Lease } from '../models';

import * as moment from 'moment';
import { map } from 'rxjs/operators';
import { AppConfig } from '../config';

@Injectable()
export class LeaseService {
  constructor(private http: HttpClient, private appConfig: AppConfig) {}

  getLeaseByLeaseIdAndSessionId(
    leaseId: string,
    sessionId: string
  ): Observable<Lease> {
    return this.http
      .get(
        `${this.appConfig.settings.eCommerceApiUrl}/v1/session/${sessionId}/leaseInformation/${leaseId}`
      )
      .pipe(
        map((resp: Lease) => {
          resp.nextPaymentDueDate = moment(resp.nextPaymentDueDate).toDate();
          return resp;
        })
      );
  }
}
