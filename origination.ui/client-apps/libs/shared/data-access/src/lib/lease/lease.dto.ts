export interface EarlyBuyoutPayment {
  paymentNumber: number;
  paymentAmount: number;
  remainingEarlyPayoff: number;
}

export interface Lease {
  leaseId: number;
  recurringPaymentAmount: number;
  paymentFrequency: string;
  termPaymentCount: number;
  nextPaymentDueDate: Date;
  fullTermAmount: number;
  ninetyDayPaymentOption: boolean;
  use3MonthVerbiage: boolean;
  earlyBuyoutSchedule: EarlyBuyoutPayment[];
  salesTaxRate: number;
  initialPaymentSalesTaxAmount: number;
  initialPaymentPreSalesTax: number;
  initialPaymentTotal: number;
  periodicPaymentTotal: number;
  accountStatus: string;
  cashPrice: number;
  termMonths: string;
  costOfRental: number;
  hasUsedItem: boolean;
}
