import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { LeaseService } from './lease.service';
import { TestBed } from '@angular/core/testing';
import { AppConfig, IConfig, Lease } from '..';

describe('LeaseService', () => {
  let httpTestingController: HttpTestingController;
  let appConfig: AppConfig;
  let subject: LeaseService;

  beforeEach(() => {
    appConfig = <AppConfig>{
      settings: <IConfig>{ eCommerceApiUrl: 'some-ecom-api-url' },
    };
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [LeaseService, { provide: AppConfig, useValue: appConfig }],
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    subject = TestBed.inject(LeaseService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });
  it('getLeaseByLeaseIdAndSessionId() should return lease information belonging to the specified leaseId and sessionId', () => {
    const leaseId = 123456;
    const sessionId = 'some-test-session-id';
    const expected: Lease = {
      leaseId: leaseId,
      recurringPaymentAmount: 150,
      paymentFrequency: 'monthly',
      termPaymentCount: 12,
      nextPaymentDueDate: new Date('07/01/2020'),
      fullTermAmount: 1800,
      ninetyDayPaymentOption: false,
      use3MonthVerbiage: false,
      earlyBuyoutSchedule: [],
      salesTaxRate: 7.5,
      initialPaymentSalesTaxAmount: 195,
      initialPaymentPreSalesTax: 1600,
      initialPaymentTotal: 195,
      periodicPaymentTotal: 200,
      accountStatus: 'Open',
      cashPrice: 1000,
      termMonths: '12 months',
      costOfRental: 2200,
      hasUsedItem: false,
    };
    subject
      .getLeaseByLeaseIdAndSessionId(`${leaseId}`, sessionId)
      .subscribe((response) => expect(response).toEqual(response));
    const req = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/session/${sessionId}/leaseInformation/${leaseId}`
    );
    req.flush(expected);
  });
});
