import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { IConfig, Order, OrderItem } from '../models';
import { OrderService } from './order.service';
import { TestBed } from '@angular/core/testing';
import { AppConfig } from '@ua/shared/data-access';

describe('OrderService', () => {
  let httpTestingController: HttpTestingController;
  let appConfig: AppConfig;
  let subject: OrderService;
  beforeEach(() => {
    appConfig = <AppConfig>{
      settings: <IConfig>{ eCommerceApiUrl: 'some-ecom-api-url' },
    };
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [OrderService, { provide: AppConfig, useValue: appConfig }],
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    subject = TestBed.inject(OrderService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });
  it('getOrderBySessionId() should get the order information belonging to the specified session id', () => {
    const expected: Order = {
      orderAmount: 1000,
      orderTaxAmount: 195,
      orderLines: [
        <OrderItem>{
          type: 'some-order-type',
          sku: 'some-test-sku',
          name: 'Iphone 11',
          quantity: 1,
          quantityUnit: '1',
          unitPrice: 999,
          taxRate: 7.5,
          totalAmount: 1195,
          totalDiscountAmount: 100,
          totalTaxAmount: 195,
          productUrl: 'some-testing-product-url',
          imageUrl: 'some-image-item-url',
        },
      ],
      subTotal: 1195,
    };
    const sessionId = 'some-testing-session-id';
    subject
      .getOrderBySessionId(sessionId)
      .subscribe((response) => expect(response).toEqual(expected));
    const req = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/session/${sessionId}`
    );
    req.flush(expected);
  });
});
