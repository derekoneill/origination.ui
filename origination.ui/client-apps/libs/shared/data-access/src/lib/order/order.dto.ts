export interface OrderItem {
  type: string;
  sku: string;
  name: string;
  quantity: number;
  quantityUnit: string;
  unitPrice: number;
  taxRate: number;
  totalAmount: number;
  totalDiscountAmount: number;
  totalTaxAmount: number;
  productUrl: string;
  imageUrl: string;
}

export interface Order {
  orderAmount: number;
  orderTaxAmount: number;
  orderLines: OrderItem[];
  subTotal: number;
}
