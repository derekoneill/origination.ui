import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Order } from '../models';
import { AppConfig } from '../config';

@Injectable()
export class OrderService {
  constructor(private http: HttpClient, private appConfig: AppConfig) {}

  getOrderBySessionId(sessionId: string): Observable<Order> {
    return this.http.get<Order>(
      `${this.appConfig.settings.eCommerceApiUrl}/v1/session/${sessionId}`
    );
  }
}
