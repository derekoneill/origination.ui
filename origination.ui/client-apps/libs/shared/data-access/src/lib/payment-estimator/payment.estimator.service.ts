import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  IConfig,
  PayFrequency,
  PaymentEstimatorRequest,
  PaymentEstimate,
  Estimate,
  EcomStoreResponse,
} from '../models';
import { Constants } from '@ua/shared/utilities';
import { AbstractControl } from '@angular/forms';
import { AppConfig } from '../config';

@Injectable()
export class PaymentEstimatorService {
  unavailableStatesStr = '';
  stateSearch: any[] = Constants.StateSearch;
  unavailableStates: string[];
  payFrequencyEnum = PayFrequency;
  paymentFrequencyOptions: any;
  paymentEstimatorRequest: PaymentEstimatorRequest;

  constructor(private http: HttpClient, private appConfig: AppConfig) {
    // Add the states ECOM can't do business with
    this.unavailableStates = Object.keys(Constants.StatesWeDontDoBusinessIn);
    this.unavailableStates.sort();

    if (this.unavailableStates.length > 0) {
      let temp = '';
      for (let i = 0; i < this.unavailableStates.length; i++) {
        if (
          i === this.unavailableStates.length - 1 &&
          this.unavailableStates.length > 1
        ) {
          temp += ' or ' + this.unavailableStates[i];
        } else {
          temp += this.unavailableStates[i];
        }
        if (
          this.unavailableStates.length > 2 &&
          i < this.unavailableStates.length - 1
        ) {
          temp += ', ';
        }
      }
      this.unavailableStatesStr = temp;
    }

    this.paymentFrequencyOptions = this.buildPaymentFrequencyOptionsArray();
  }

  getStore(
    parentStoreId: number,
    pmtEstimateReq: PaymentEstimatorRequest
  ): Observable<EcomStoreResponse> {
    const parentStoreOption = `parentStoreId=${parentStoreId}`;
    const stateOption = `state=${pmtEstimateReq.stateCode}`;
    return this.http.get<EcomStoreResponse>(
      `${this.appConfig.settings.paymentServiceApiUrl}/api/estimator/store?${parentStoreOption}&${stateOption}`
    );
  }

  getPaymentEstimate(
    storeId: number,
    pmtEstimateReq: PaymentEstimatorRequest
  ): Observable<PaymentEstimate> {
    const storeOption = `storeId=${storeId}`;
    const payFrequencyOption = `paymentFrequency=${pmtEstimateReq.payFrequency}`;
    const invoiceOption = `invoiceAmount=${pmtEstimateReq.itemCost}`;
    return this.http.get<PaymentEstimate>(
      `${this.appConfig.settings.paymentServiceApiUrl}/api/estimator/paymentestimate?${storeOption}&${payFrequencyOption}&${invoiceOption}`
    );
  }

  stateValidator(input: AbstractControl) {
    if (!input || !input.value) {
      return null;
    }

    const stateCode = this.stateNameToCode(input.value);
    return this.indexOfStateSearch(stateCode) >= 0
      ? null
      : { invalidState: true };
  }

  unavailableStateValidator(input: AbstractControl) {
    if (!input || !input.value) {
      return null;
    }
    const stateCode = this.stateNameToCode(input.value);
    return this.unavailableStates.indexOf(stateCode) < 0
      ? null
      : { unavailableState: true };
  }

  indexOfStateSearch(stateCode): number {
    for (let i = 0; i < this.stateSearch.length; i++) {
      if (this.stateSearch[i].value === stateCode) {
        return i;
      }
    }
    return -1;
  }

  stateNameToCode(stateName: string): string {
    for (let i = 0; i < this.stateSearch.length; i++) {
      if (this.stateSearch[i].display === stateName.trim()) {
        return this.stateSearch[i].value;
      }
    }
    return 'Unknown';
  }

  stateCodeToName(stateCode: string): string {
    for (let i = 0; i < this.stateSearch.length; i++) {
      if (this.stateSearch[i].value === stateCode.trim()) {
        return this.stateSearch[i].display;
      }
    }
    return 'Unknown';
  }

  buildUnavailableStatesString() {
    // Dynamically build string to display unavailable states
    if (this.unavailableStates.length > 0) {
      let temp = '';
      for (let i = 0; i < this.unavailableStates.length; i++) {
        if (
          i === this.unavailableStates.length - 1 &&
          this.unavailableStates.length > 1
        ) {
          temp += ' or ' + this.unavailableStates[i];
        } else {
          temp += this.unavailableStates[i];
        }
        if (
          this.unavailableStates.length > 2 &&
          i < this.unavailableStates.length - 1
        ) {
          temp += ', ';
        }
      }
      this.unavailableStatesStr = temp;
    }
  }

  buildPaymentFrequencyOptionsArray() {
    const payOptionsArray = [];
    let translatableItem;
    for (const item in this.payFrequencyEnum) {
      if (isNaN(Number(item))) {
        translatableItem = item;
        if (item === 'EveryOtherWeek') {
          translatableItem = 'BiWeekly';
        }
        if (item === 'TwicePerMonth') {
          translatableItem = 'SemiMonthly';
        }
        const freq = {
          id: this.payFrequencyEnum[item],

          //TODO: need a better way to do this.
          // value: this._translateService.instant('EstPayFrequency.' + translatableItem),
          // display: this._translateService.instant('EstPayFrequency.' + translatableItem),
          enumValue: this.payFrequencyEnum[item],
          searchableTerms: ['week, Week'],
        };
        payOptionsArray.push(freq);
      }
    }
    return payOptionsArray;
  }

  public getWorstCaseScenarioEstimate(
    paymentEstimate: PaymentEstimate
  ): Estimate {
    let currentValue: Number = 0;
    let worstCaseEstimate: Estimate;

    for (let i = 0; i < paymentEstimate.estimates.length; i++) {
      const currentEst = paymentEstimate.estimates[i];
      const totalValue =
        currentEst.earlyBuyOut.paymentSchedule[
          currentEst.earlyBuyOut.paymentSchedule.length - 1
        ].cumulativeAmountPaid;
      if (currentValue < totalValue) {
        currentValue = totalValue;
        worstCaseEstimate = currentEst;
      }
    }

    return worstCaseEstimate;
  }
}
