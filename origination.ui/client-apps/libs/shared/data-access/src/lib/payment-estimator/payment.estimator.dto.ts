export interface PaymentEstimatorRequest {
  storeId: number;
  stateCode: string;
  itemCost: number;
  payFrequency: string;
  requestType: string;
  paymentFrequencyOptions: any[];
  unavailableStatesStr: string;
  stateSearch: any[];
  unavailableStates: string[];
}

export interface PaymentSchedule {
  paymentNumber: number;
  totalPaid: number;
  earlyBuyOutSavings: number;
  amountRemaining: number;
  buyoutAmount: number;
  paymentDate: Date;
  costOfLease: number;
  cumulativeAmountPaid: number;
}

export interface EarlyBuyOut {
  expirationDate: Date;
  ninetyDayPayoffAdjustment: number;
  paymentSchedule: PaymentSchedule[];
  message?: any;
}

export interface Estimate {
  leaseTermId: number;
  invoiceAmount: number;
  paymentFrequency: string;
  deposit: number;
  salesTaxRate: number;
  cashPrice: number;
  cashPriceMarkUp: number;
  costOfRental: number;
  initialCashPrice: number;
  ninetyDayBuyout: number;
  ninetyDayBuyoutMarkup: number;
  ninetyDayBuyoutTotalPaid: number;
  netDeposit: number;
  depositSalesTax: number;
  remainingInvoice: number;
  effectiveLeaseBalance: number;
  totalLeasePrice: number;
  initialBalance: number;
  initialPayment: number;
  periodicPayment: number;
  numberOfPayments: number;
  numberOfRecurringPayments: number;
  termMonths: number;
  initialPaymentSalesTaxAmount: number;
  rldCashPrice: number;
  rldCashPriceMarkup: number;
  totalNetRld: number;
  ninetyDayBuyoutNetRld: number;
  leasePricingCalculationId: number;
  estimatedInitialPaymentWithoutOverride: number;
  earlyBuyOut: EarlyBuyOut;
}

export interface PaymentEstimate {
  self: string;
  errors: any[];
  estimates: Estimate[];
}

export interface EcomStoreResponse {
  storeId: number;
  name: string;
  ecommerceProviderId: number;
  errors: Array<{ errorCode: string; errorMessage: string }>;
}
