export interface FeatureFlags {
  isShortApp: boolean;
  monthlyPayFreqDisabled: boolean;
  skipCreditCard: boolean;
}
