import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import {
  FeatureFlags,
  FeatureFlagService,
  AppConfig,
  IConfig,
} from '@ua/shared/data-access';

describe('FeatureFlagService', () => {
  let httpTestingController: HttpTestingController;
  let config: IConfig;
  let subject: FeatureFlagService;
  beforeEach(() => {
    config = <IConfig>{ eCommerceApiUrl: 'some-ecom-api-url' };
    const appConfig = <AppConfig>{ settings: config };
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        FeatureFlagService,
        { provide: AppConfig, useValue: appConfig },
      ],
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    subject = TestBed.inject(FeatureFlagService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('getFeatureFlags() should return feature flags belonging to the authenticationId with the specific store id', () => {
    const storeId = 55555;
    const email = 'test1@example.com';
    const encodedEmail = 'test1%40example.com';
    const expected: FeatureFlags = {
      isShortApp: true,
      monthlyPayFreqDisabled: false,
      skipCreditCard: true,
    };
    subject
      .getFeatureFlags(storeId, email)
      .subscribe((response) => expect(response).toEqual(expected));
    const shortAppReq = httpTestingController.expectOne(
      `${config.eCommerceApiUrl}/v1/featureFlag?flagKey=ecomui-feat-short-app&storeId=${storeId}&emailOrPhone=${encodedEmail}`
    );
    shortAppReq.event(
      new HttpResponse<boolean>({ body: true })
    );
    expect(shortAppReq.request.method).toEqual('GET');
    const monthPayFreqReq = httpTestingController.expectOne(
      `${config.eCommerceApiUrl}/v1/featureFlag?flagKey=ecomui-feat-disable-monthly-pay-freq&storeId=${storeId}&emailOrPhone=${encodedEmail}`
    );
    monthPayFreqReq.event(
      new HttpResponse<boolean>({ body: false })
    );
    expect(monthPayFreqReq.request.method).toEqual('GET');
    const skipCreditCardReq = httpTestingController.expectOne(
      `${config.eCommerceApiUrl}/v1/featureFlag?flagKey=ecom-skip-credit-card&storeId=${storeId}&emailOrPhone=${encodedEmail}`
    );
    skipCreditCardReq.event(
      new HttpResponse<boolean>({ body: true })
    );
    expect(skipCreditCardReq.request.method).toEqual('GET');
  });

  it('getFeatureFlags() should throw 400 error when the storeId is undefined', () => {
    const storeId = undefined;
    const email = 'test1@example.com';
    const encodedEmail = 'test1%40example.com';
    subject.getFeatureFlags(storeId, email).pipe(
      catchError((error) => {
        expect(error.status).toEqual(400);
        expect(error.statusText).toEqual('Invalid Store Id');
        return error;
      })
    );
    httpTestingController.expectNone(
      `${config.eCommerceApiUrl}/v1/featureFlag?flagKey=ecom-skip-credit-card&storeId=${storeId}&emailOrPhone=${encodedEmail}`
    );
  });

  it('getFeatureFlags() should throw 400 error when the emailOrPhone is undefined', () => {
    const storeId = 55555;
    const email = undefined;
    const encodedEmail = undefined;
    subject.getFeatureFlags(storeId, email).pipe(
      catchError((error) => {
        expect(error.status).toEqual(400);
        expect(error.statusText).toEqual('Invalid EmailOrPhone');
        return error;
      })
    );
    httpTestingController.expectNone(
      `${config.eCommerceApiUrl}/v1/featureFlag?flagKey=ecom-skip-credit-card&storeId=${storeId}&emailOrPhone=${encodedEmail}`
    );
  });
});
