import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, forkJoin, throwError } from 'rxjs';
import { FeatureFlags } from '../models';
import { AppConfig } from '../config';

@Injectable({
  providedIn: 'root',
})
export class FeatureFlagService {
  constructor(private http: HttpClient, private appConfig: AppConfig) {}

  getFeatureFlags(
    storeId: number,
    normalizedEmailOrPhone: string
  ): Observable<FeatureFlags> {
    if (!storeId) {
      return throwError(<HttpErrorResponse>{
        status: 400,
        statusText: 'Invalid Store Id',
      });
    }

    if (!normalizedEmailOrPhone) {
      return throwError(<HttpErrorResponse>{
        status: 400,
        statusText: 'Invalid EmailOrPhone',
      });
    }

    const dictionaryStream = {
      isShortApp: this.getFeatureFlag(
        'ecomui-feat-short-app',
        storeId,
        normalizedEmailOrPhone
      ),
      monthlyPayFreqDisabled: this.getFeatureFlag(
        'ecomui-feat-disable-monthly-pay-freq',
        storeId,
        normalizedEmailOrPhone
      ),
      skipCreditCard: this.getFeatureFlag(
        'ecom-skip-credit-card',
        storeId,
        normalizedEmailOrPhone
      ),
    };

    return forkJoin(dictionaryStream);
  }

  getFeatureFlag(
    flagKey: string,
    storeId: number,
    otpId: string
  ): Observable<boolean> {
    const encodededOtpId = encodeURIComponent(otpId);
    return this.http.get<boolean>(
      `${this.appConfig.settings.eCommerceApiUrl}/v1/featureFlag?flagKey=${flagKey}&storeId=${storeId}&emailOrPhone=${encodededOtpId}`
    );
  }

  private isPhoneNumber(emailOrPhone: string): boolean {
    return emailOrPhone.indexOf('@') < 0;
  }
}
