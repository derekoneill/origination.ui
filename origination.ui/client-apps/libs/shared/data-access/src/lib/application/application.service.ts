import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import {
  Application,
  ApplicationResult,
  InvoiceResult,
  LeaseSessionInfo,
  CancelContractResult,
  ApiApplication,
} from '../models';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { AppConfig } from '../config';

@Injectable()
export class ApplicationService {
  constructor(private http: HttpClient, private appConfig: AppConfig) {}

  getApplicationBySessionId(sessionId: string): Observable<Application> {
    return this.http
      .get<any>(
        `${this.appConfig.settings.eCommerceApiUrl}/v1/session/${sessionId}/application`
      )
      .pipe(map(this.mapAPIFieldsToUIFields));
  }

  createApplicationBySessionId(sessionId: string): Observable<Application> {
    return this.http
      .post<Application>(
        `${this.appConfig.settings.eCommerceApiUrl}/v1/session/application`,
        { sessionId }
      )
      .pipe(map((_) => <Application>{}));
  }

  getApplicationResultBySessionId(
    sessionId: string
  ): Observable<ApplicationResult> {
    return this.http.get<ApplicationResult>(
      `${this.appConfig.settings.eCommerceApiUrl}/v1/session/${sessionId}/approval`
    );
  }

  updateApplication(application: Application): Observable<Application> {
    return this.http.put<Application>(
      `${this.appConfig.settings.eCommerceApiUrl}/v1/session/application`,
      this.mapUIFieldsToAPIFields(application)
    );
  }

  cancelContract(sessionId: string): Observable<CancelContractResult> {
    return this.http.delete<CancelContractResult>(
      `${this.appConfig.settings.eCommerceApiUrl}/v1/session/${sessionId}/application/contract`
    );
  }

  submitApplication(
    sessionId: string,
    shortApp: boolean = true
  ): Observable<ApplicationResult> {
    return this.http.post<ApplicationResult>(
      `${this.appConfig.settings.eCommerceApiUrl}/v1/session/application/submit`,
      {
        sessionId: sessionId,
        flags: { 'ecomui-feat-short-app': shortApp },
      }
    );
  }

  submitInvoice(sessionId: string): Observable<InvoiceResult> {
    return this.http.post<InvoiceResult>(
      `${this.appConfig.settings.eCommerceApiUrl}/v1/orders/${sessionId}/submit`,
      { sessionId }
    );
  }

  submitLeaseSessionInfo(
    leaseSessionInfo: LeaseSessionInfo
  ): Observable<LeaseSessionInfo> {
    return this.http.post<LeaseSessionInfo>(
      `${this.appConfig.settings.eCommerceApiUrl}/v1/session/leaseSession`,
      leaseSessionInfo
    );
  }

  private mapUIFieldsToAPIFields(app: Application): ApiApplication {
    const application: ApiApplication = <ApiApplication>{};
    if (!app) {
      return <ApiApplication>{};
    }

    if (app.dateOfBirth) {
      application.dateOfBirth = moment(app.dateOfBirth, 'MM/DD/YYYY').toDate();
    }

    application.sessionId = app.sessionId;
    application.givenName = app.firstName;
    application.familyName = app.lastName;
    application.email = app.emailAddress;
    application.phoneNumber = app.phoneNumber;
    application.ssn = app.ssn;
    application.driverLicenseNumber = app.driverLicense;
    application.driverLicenseState = app.driverLicenseState;
    application.bankRoutingNumber = app.bankRoutingNumber;
    application.bankAccountNumber = app.bankAccountNumber;
    application.monthlyGrossIncome = app.monthlyGrossIncome;
    application.marketingOptIn = app.marketingOptIn;
    application.defaultContactMechanism = app.defaultContactMechanism;
    application.address = {
      streetAddress: app.street1,
      streetAddress2: app.street2,
      postalCode: app.zip,
      city: app.city,
      region: app.state,
      country: 'US',
    };

    return application;
  }

  private mapAPIFieldsToUIFields(app: ApiApplication): Application {
    const application: Application = <Application>{};
    if (!app) {
      return application;
    }

    application.sessionId = app.sessionId;
    application.firstName = app.givenName;
    application.lastName = app.familyName;
    application.emailAddress = app.email;
    application.phoneNumber = app.phoneNumber;
    application.driverLicense = app.driverLicenseNumber;
    application.driverLicenseState = app.driverLicenseState;
    application.bankAccountNumber = app.bankAccountNumber;
    application.bankRoutingNumber = app.bankRoutingNumber;
    application.ssn = app.ssn;
    application.defaultContactMechanism = app.defaultContactMechanism;
    application.marketingOptIn = app.marketingOptIn;
    application.monthlyGrossIncome = app.monthlyGrossIncome;

    if (app.dateOfBirth) {
      application.dateOfBirth = moment(app.dateOfBirth).format('MM/DD/YYYY');
    }

    if (app.address) {
      application.street1 = app.address.streetAddress;
      application.street2 = app.address.streetAddress2;
      application.city = app.address.city;
      application.state = app.address.region;
      application.zip = app.address.postalCode;
      delete app.address;
    }

    return application;
  }
}
