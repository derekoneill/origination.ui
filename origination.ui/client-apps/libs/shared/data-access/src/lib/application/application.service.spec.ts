import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { ApplicationService, AppConfig } from '../index';
import {
  IConfig,
  Application,
  AuthenticationMethod,
  ApplicationResult,
  ApplicationApprovalStatus,
  InvoiceResult,
  LeaseSessionInfo,
  ApiApplication,
} from '../models';

import { TestBed } from '@angular/core/testing';

describe('ApplicationService', () => {
  let httpTestingController: HttpTestingController;
  let appConfig: AppConfig;
  let applicationService: ApplicationService;
  beforeEach(() => {
    appConfig = <AppConfig>{
      settings: <IConfig>{ eCommerceApiUrl: 'some-ecom-api-url' },
    };
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ApplicationService,
        { provide: AppConfig, useValue: appConfig },
      ],
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    applicationService = TestBed.inject(ApplicationService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('getApplicationBySessionId() should return application with the specified session id', () => {
    const testSessionId = 'some-test-session-id';
    const application: Application = {
      sessionId: testSessionId,
      firstName: 'First',
      lastName: 'Last',
      ssn: '555-55-5555',
      dateOfBirth: '01/01/1999',
      street1: '10144 S 6000 W',
      street2: '',
      city: 'South Jordan',
      state: 'UT',
      zip: '84009',
      emailAddress: 'first.last@example.com',
      phoneNumber: '1801-555-5555',
      bankRoutingNumber: '000000001',
      bankAccountNumber: '000000001',
      driverLicense: '1234567890',
      driverLicenseState: 'UT',
      monthlyGrossIncome: 4200,
      marketingOptIn: false,
      defaultContactMechanism: AuthenticationMethod.SMS,
    };
    const apiApplication = <ApiApplication>{};
    apiApplication.sessionId = application.sessionId;
    apiApplication.givenName = application.firstName;
    apiApplication.familyName = application.lastName;
    apiApplication.email = application.emailAddress;
    apiApplication.phoneNumber = application.phoneNumber;
    apiApplication.ssn = application.ssn;
    apiApplication.dateOfBirth = new Date(application.dateOfBirth);
    apiApplication.driverLicenseNumber = application.driverLicense;
    apiApplication.driverLicenseState = application.driverLicenseState;
    apiApplication.bankRoutingNumber = application.bankRoutingNumber;
    apiApplication.bankAccountNumber = application.bankAccountNumber;
    apiApplication.monthlyGrossIncome = application.monthlyGrossIncome;
    apiApplication.marketingOptIn = application.marketingOptIn;
    apiApplication.defaultContactMechanism =
      application.defaultContactMechanism;
    apiApplication.address = {
      streetAddress: application.street1,
      streetAddress2: application.street2,
      postalCode: application.zip,
      city: application.city,
      region: application.state,
      country: 'US',
    };
    applicationService
      .getApplicationBySessionId('some-test-session-id')
      .subscribe((res) => expect(res).toEqual(application));
    const req = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/session/${testSessionId}/application`
    );
    req.flush(apiApplication);
  });

  it('createApplicationBySessionId() should create an empty application', () => {
    const testSessionId = 'some-test-session-id';
    applicationService
      .createApplicationBySessionId(testSessionId)
      .subscribe((res) => expect(res).toEqual(<Application>{}));
    const req = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/session/application`
    );
    req.flush(<Application>{});
  });

  it('getApplicationResultBySessionId() should return an correct application result', () => {
    const testSessionId = 'some-test-session-id';
    const applicationResult: ApplicationResult = {
      sessionId: testSessionId,
      approvalAmount: 1500,
      approved: true,
      approvalDateUtc: new Date('05/25/2020'),
      applicationApprovalStatus: ApplicationApprovalStatus.Approved,
      leaseId: 123456,
      eSignUrl: 'some-esign-url',
      initialPaymentAmount: 300,
    };
    applicationService
      .getApplicationResultBySessionId(testSessionId)
      .subscribe((res) => expect(res).toEqual(applicationResult));
    const req = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/session/${testSessionId}/approval`
    );
    req.flush(applicationResult);
  });

  it('updateApplication() should update an application successfully.', () => {
    const testSessionId = 'some-test-session-id';
    const application: Application = {
      sessionId: testSessionId,
      firstName: 'First',
      lastName: 'Last',
      ssn: '555-55-5555',
      dateOfBirth: '01/01/1999',
      street1: '10144 S 6000 W',
      street2: '',
      city: 'South Jordan',
      state: 'UT',
      zip: '84009',
      emailAddress: 'first.last@example.com',
      phoneNumber: '1801-555-5555',
      bankRoutingNumber: '000000001',
      bankAccountNumber: '000000001',
      driverLicense: '1234567890',
      driverLicenseState: 'UT',
      monthlyGrossIncome: 4200,
      marketingOptIn: false,
      defaultContactMechanism: AuthenticationMethod.SMS,
    };
    applicationService
      .updateApplication(application)
      .subscribe((res) => expect(res).toEqual(application));
    const req = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/session/application`
    );
    req.flush(application);
  });

  it('cancelContract() should cancel the application successfully.', () => {
    const testSessionId = 'some-test-session-id';
    applicationService
      .cancelContract(testSessionId)
      .subscribe((res) => expect(res).toEqual({ contractCanceled: true }));
    const req = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/session/${testSessionId}/application/contract`
    );
    req.flush({ contractCanceled: true });
    httpTestingController.verify();
  });

  it('submitApplication() should submit application and return the application result successfully', () => {
    const testSessionId = 'some-test-session-id';
    const applicationResult: ApplicationResult = {
      sessionId: testSessionId,
      approvalAmount: 1500,
      approved: true,
      approvalDateUtc: new Date('05/25/2020'),
      applicationApprovalStatus: ApplicationApprovalStatus.Approved,
      leaseId: 123456,
      eSignUrl: 'some-esign-url',
      initialPaymentAmount: 300,
    };
    applicationService
      .submitApplication(testSessionId)
      .subscribe((res) => expect(res).toEqual(applicationResult));
    const req = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/session/application/submit`
    );
    req.flush(applicationResult);
  });

  it('submitInvoice() should submit application and return the application result successfully', () => {
    const testSessionId = 'some-test-session-id';
    const invoiceResult: InvoiceResult = {
      esignUrl: 'some-esign-url',
      initialPaymentAmount: 200,
    };
    applicationService
      .submitInvoice(testSessionId)
      .subscribe((res) => expect(res).toEqual(invoiceResult));
    const req = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/orders/${testSessionId}/submit`
    );
    req.flush(invoiceResult);
  });

  it('submitLeaseSessionInfo() should submit lease session info successfully', () => {
    const testSessionId = 'some-test-session-id';
    const leaseSessionInfo: LeaseSessionInfo = {
      sessionId: testSessionId,
      merchantCartId: 'some-merchant-id',
      completeUrl: 'some-complete-return-url',
      leaseId: 2400,
      insertedAtUtc: new Date('05/25/2020'),
    };
    applicationService
      .submitLeaseSessionInfo(leaseSessionInfo)
      .subscribe((res) => expect(res).toEqual(leaseSessionInfo));
    const req = httpTestingController.expectOne(
      `${appConfig.settings.eCommerceApiUrl}/v1/session/leaseSession`
    );
    req.flush(leaseSessionInfo);
  });
});
