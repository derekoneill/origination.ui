import { AuthenticationMethod } from '../authentication/authentication.dto';

export interface Address {
  streetAddress: string;
  streetAddress2?: string;
  postalCode: string;
  city: string;
  region: string;
  country: string;
}

export interface Application {
  sessionId: string;
  firstName: string;
  lastName: string;
  ssn: string;
  phoneNumber: string;
  emailAddress: string;
  dateOfBirth: string;
  bankAccountNumber: string;
  bankRoutingNumber: string;
  monthlyGrossIncome: number;
  street1: string;
  street2: string;
  city: string;
  state: string;
  zip: string;
  driverLicense: string;
  driverLicenseState: string;
  marketingOptIn: boolean;
  defaultContactMechanism: AuthenticationMethod;
}

export interface ApiApplication {
  sessionId: string;
  givenName: string;
  familyName: string;
  email: string;
  phoneNumber: string;
  ssn: string;
  driverLicenseNumber: string;
  driverLicenseState: string;
  address: Address;
  bankAccountNumber: string;
  bankRoutingNumber: string;
  monthlyGrossIncome: number;
  dateOfBirth: Date;
  marketingOptIn: boolean;
  defaultContactMechanism: AuthenticationMethod;
}

export enum ApplicationApprovalStatus {
  Approved = 'Approved',
  PreApproved = 'PreApproved',
  Pending = 'Pending',
  InProcess = 'InProcess',
  Denied = 'Denied',
}

export interface ApplicationResult {
  sessionId: string;
  approvalAmount: number;
  approved: boolean;
  approvalDateUtc: Date;
  applicationApprovalStatus: ApplicationApprovalStatus;
  leaseId: number;
  eSignUrl: string;
  initialPaymentAmount: number;
}

export interface CancelContractResult {
  contractCanceled: boolean;
}

export interface InvoiceResult {
  esignUrl: string;
  initialPaymentAmount: number;
}

export interface LeaseSessionInfo {
  sessionId: string;
  merchantCartId: string;
  completeUrl: string;
  leaseId?: number;
  insertedAtUtc: Date;
}
