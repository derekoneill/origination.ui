import { Injectable } from '@angular/core';
import scrollToElement from 'scroll-to-element';
import Velocity from 'velocity-animate';


@Injectable()
export class NativeService {
  get window(): Window {
    return window;
  }

  set window(win) {
    this.window = win;
  }

  get document(): Document {
    return document;
  }

  get location(): Location {
    return location;
  }

  get localStorage(): Storage {
    return this.window.localStorage;
  }

  get sessionStorage(): Storage {
    return this.window.sessionStorage;
  }

  get parent(): Window {
    return parent;
  }


  scrollTo(nativeElement: any, options: any = null) {
    const opts = options || {
      offset: -100,
      ease: 'out-quad',
      duration: 300
    };
    scrollToElement(nativeElement, opts);
  }

  encodeURI(val: string) {
    return encodeURI(val);
  }

  encodeURIComponent(val: string) {
    return encodeURIComponent(val);
  }

  animate(nativeElement, styleTargets, options): Promise<any> {
    return Velocity(nativeElement, styleTargets, options);
  }

  // NOTE: originally used the animate function because velocity js
  // was simpler to use. Added this function to specifically animate
  // using the webAnimations api so we can slowly transition from velocity
  // to web animations and test each one along the way.
  webAnimationApiAnimate(nativeElement, styleTargets, options) {
    return new Promise((res, rej) => {
      const animation = nativeElement.animate(styleTargets, options);
      animation.onfinish = (evt) => {
        res(animation);
      };
      animation.play();
    });
  }

  setWebAnimationApiStyles(nativeElement, styleTargets) {
    return new Promise((res, rej) => {
      const animation = nativeElement.animate([styleTargets, styleTargets], { duration: 0, delay: 0, fill: 'forwards' });
      animation.onfinish = (evt) => {
        res(animation);
      };
      animation.play();
    });
  }

  checkout(checkoutUrl: string) {
    this.window.top.location.href = checkoutUrl;
  }

  redirectToPage(url: string) {
    this.document.location.href = url;
  }
}
