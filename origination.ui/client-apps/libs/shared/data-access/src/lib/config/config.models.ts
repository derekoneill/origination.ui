export interface IConfig {
  originApiUrl: string;
  contentApiUrl: string;

  // existing apply flow.
  eCommerceApiUrl: string;
  paymentServiceApiUrl: string;
  appInsightsKey: string;
  sessionTimeOutSeconds: number;
  sessionTimeOutWarningSeconds: number;
  storesBlockingUsedItems: any;
}
