import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { IConfig } from './config.models';

@Injectable()
export class AppConfig {
  public settings: IConfig = <IConfig>{};

  constructor(private http: HttpClient) {}

  load() {
    const jsonFile = `assets/config.json`;
    return this.http
      .get<IConfig>(jsonFile)
      .pipe(
        tap((settings) => {
          this.settings = settings;
        })
      )
      .toPromise();
  }
}
