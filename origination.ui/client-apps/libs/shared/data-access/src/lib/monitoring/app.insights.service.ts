import { Injectable } from '@angular/core';
import { AppInsights } from 'applicationinsights-js';
import { AppConfig } from '../config';

@Injectable()
export class AppInsightsService {

  constructor(private appConfig: AppConfig) {
    if (!AppInsights.config) {
      AppInsights.downloadAndSetup({
        instrumentationKey: this.appConfig.settings.appInsightsKey,
      });
    }
  }

  trackPageView(name?: string, url?: string, properties?: any,
    measurements?: any, duration?: number) {
    AppInsights.trackPageView(name, url, properties, measurements, duration);
  }

  trackEvent(name: string, properties?: any, measurements?: any) {
    AppInsights.trackEvent(name, properties, measurements);
  }

  startTrackEvent(name: string) {
    AppInsights.startTrackEvent(name);
  }

  stopTrackEvent(name: string, properties?: any, measurements?: any) {
    AppInsights.stopTrackEvent(name, properties, measurements);
  }

  trackException(error: Error, handledAt?: string,
    properties?: { [name: string]: string },
    measurements?: { [name: string]: number },
    severityLevel?: AI.SeverityLevel) {
    AppInsights.trackException(error, handledAt, properties, measurements, severityLevel);
    AppInsights.flush();
  }

}
