using System.Net.Http;
using Microsoft.AspNetCore.Mvc;

namespace origination.ui.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HealthController : ControllerBase
    {
        [HttpGet]
        public IActionResult Index()
        {
            return Ok("Server is up and healthy");
        }
    }
}